<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__.'/src')
;

$config = new \PhpCsFixer\Config();

return $config->setRules([
    '@Symfony' => true,
    '@Symfony:risky' => true,
    'ordered_imports' => true,
    'array_syntax' => ['syntax' => 'short'],
    'dir_constant' => true,
    'linebreak_after_opening_tag' => true,
    'modernize_types_casting' => true,
    'multiline_whitespace_before_semicolons' => true,
    'no_unreachable_default_argument_value' => true,
    'no_useless_else' => true,
    'no_useless_return' => true,
    'ordered_class_elements' => true,
    'phpdoc_add_missing_param_annotation' => ['only_untyped' => false],
    'phpdoc_order' => true,
    'declare_strict_types' => true,
    'psr_autoloading' => true,
    'no_php4_constructor' => true,
    'echo_tag_syntax' => true,
    'semicolon_after_instruction' => true,
    'general_phpdoc_annotation_remove' => ['annotations' => ["package"]],
    'list_syntax' => ["syntax" => "short"],
    'phpdoc_types_order' => ['null_adjustment'=> 'always_last'],
    'single_line_comment_style' => true,
])->setFinder($finder);
