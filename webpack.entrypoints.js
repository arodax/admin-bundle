/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

module.exports = {
  '/dashboard/index': 'assets/entries/dashboard/index',
  '/security/login': 'assets/entries/security/login',
  '/security/reset_password': 'assets/entries/security/reset_password',
  '/security/process_login_link': 'assets/entries/security/process_login_link',
  '/article/index': 'assets/entries/article/index',
  '/article/edit': 'assets/entries/article/edit',
  '/content/overview': 'assets/entries/content/overview',
  '/section/index': 'assets/entries/section/index',
  '/section/edit': 'assets/entries/section/edit',
  '/gallery/index': 'assets/entries/gallery/index',
  '/gallery/edit': 'assets/entries/gallery/edit',
  '/event/index': 'assets/entries/event/index',
  '/event/edit': 'assets/entries/event/edit',
  '/user/index': 'assets/entries/user/index',
  '/user/edit': 'assets/entries/user/edit',
  '/user_group/index': 'assets/entries/user_group/index',
  '/user_group/edit': 'assets/entries/user_group/edit',
  '/menu_item/index': 'assets/entries/menu_item/index',
  '/menu_item/edit': 'assets/entries/menu_item/edit',
  '/product_category/index': 'assets/entries/product_category/index',
  '/product_category/edit': 'assets/entries/product_category/edit',
  '/product/index': 'assets/entries/product/index',
  '/product/edit': 'assets/entries/product/edit',
  '/translatable/index': 'assets/entries/translatable/index',
  '/translatable/edit': 'assets/entries/translatable/edit',
  '/config/index': 'assets/entries/config/index',
  '/invoice/index': 'assets/entries/invoice/index',
  '/invoice/edit': 'assets/entries/invoice/edit',
  '/shopping_cart/index': 'assets/entries/shopping_cart/index',
  '/shopping_cart/edit': 'assets/entries/shopping_cart/edit',
  '/inquiry/index': 'assets/entries/inquiry/index',
  '/inquiry/edit': 'assets/entries/inquiry/edit',
  '/newsletter/index': 'assets/entries/newsletter/index',
  '/newsletter_recipient/index': 'assets/entries/newsletter_recipient/index',
  '/newsletter_recipient/edit': 'assets/entries/newsletter_recipient/edit',
  '/order/index': 'assets/entries/order/index',
  '/order/edit': 'assets/entries/order/edit',
  '/shipping_method/index': 'assets/entries/shipping_method/index',
  '/shipping_method/edit': 'assets/entries/shipping_method/edit',
  '/payment_method/index': 'assets/entries/payment_method/index',
  '/payment_method/edit': 'assets/entries/payment_method/edit',
  '/grant_project/index': 'assets/entries/grant_project/index',
  '/grant_project/edit': 'assets/entries/grant_project/edit',
  '/grant_call/index': 'assets/entries/grant_call/index',
  '/grant_call/edit': 'assets/entries/grant_call/edit',
  '/grant_project_group/index': 'assets/entries/grant_project_group/index',
  '/grant_project_group/edit': 'assets/entries/grant_project_group/edit'
}
