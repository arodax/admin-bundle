/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

module.exports = {
  env: {
    browser: true
  },
  extends: [
    'standard',
    'plugin:vue/recommended',
    '@vue/typescript/recommended'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaVersion: 2021
  },
  plugins: [
    'vue'
  ],
  rules: {
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    indent: ['error', 2],
    'vue/html-indent': ['error', 2],
    'vue/html-quotes': ['error', 'double'],
    '@typescript-eslint/no-explicit-any': 'off',
    'vue/valid-v-slot': ['error', {
      allowModifiers: true,
    }],
  }
}
