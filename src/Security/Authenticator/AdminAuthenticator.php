<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Authenticator;

use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminAuthenticator extends AbstractAuthenticator implements AuthenticationEntryPointInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    /*
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): bool
    {
        return $request->isMethod(Request::METHOD_POST)
            && ('arodax_admin_security_login' === $request->attributes->get('_route'))
            && $request->request->has('email')
            && $request->request->has('password')
        ;
    }

    public function authenticate(Request $request): Passport
    {
        $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->request->get('email'));

        if (!$user instanceof User) {
            throw new BadCredentialsException();
        }

        $badges = [];

        $badges[] = new CsrfTokenBadge('_token', $request->get('_token'));

        if ($request->request->get('_remember_me')) {
            $badges[] = new RememberMeBadge();
        }

        return new Passport(new UserBadge($user->getEmail()), new PasswordCredentials($request->request->get('password')), $badges);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->logger->info('Successful login.');

        // Get the current hostname
        $hostname = $request->getHost();

        // Create the cookie name
        $cookieName = $hostname . '_arodax_admin_preferred_locale';

        // Check if a preferred locale cookie is set
        if ($request->cookies->has($cookieName)) {
            // Get the preferred locale from the cookie
            $preferredLocale = $request->cookies->get($cookieName);

            // Return a response with the preferred locale
            return new JsonResponse([
                'url' => $this->urlGenerator->generate('arodax_admin_default_index', ['_locale' => $preferredLocale]),
            ], Response::HTTP_OK);
        }

        return new JsonResponse([
            'url' => $this->urlGenerator->generate('arodax_admin_default_index'),
        ], Response::HTTP_OK);
    }


    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $this->logger->notice('Authentication failure during the login.');

        return new JsonResponse([
            'title' => $this->translator->trans($exception->getMessageKey(), $exception->getMessageData()),
            'status' => Response::HTTP_UNAUTHORIZED,
        ], Response::HTTP_UNAUTHORIZED, ['Content-Type' => 'application/problem+json']);
    }

    /*
     * Called whenever secured page is requested and entry_point parameter is defined in security.yaml.
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        if ('html' === $request->getRequestFormat()) {
            return new RedirectResponse($this->urlGenerator->generate('arodax_admin_security_login').'?_target_path='.$request->getRequestUri(), Response::HTTP_FOUND);
        }

        $title = $authException !== null ? $this->translator->trans($authException->getMessageKey(), $authException->getMessageData()) : $this->translator->trans('not_authorized', [], 'arodax_admin');

        return new JsonResponse([
                'title' => $title,
                'status' => Response::HTTP_UNAUTHORIZED,
            ], Response::HTTP_UNAUTHORIZED, ['Content-Type' => 'application/problem+json']);
    }
}
