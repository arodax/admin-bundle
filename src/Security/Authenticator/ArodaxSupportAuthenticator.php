<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Authenticator;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Factory\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Contracts\Translation\TranslatorInterface;


class ArodaxSupportAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly TranslatorInterface $translator,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly UserFactory $userFactory
    ) {
    }


    public function supports(Request $request): bool
    {
        if (
            !$request->isMethod(Request::METHOD_POST)
            || 'arodax_admin_security_login' !== $request->attributes->get('_route')
            || !$request->request->has('email')
            || !$request->request->has('password')
        ) {
            return false;
        }

        $email = $request->request->get('email');

        // Support only email that ends with @arodax.com
        if (!str_ends_with($email, '@arodax.com')) {
            return false;
        }

        // Attempt SMTP authentication
        try {
            $transport = new EsmtpTransport('mail.arodax.com', 587);
            $transport->setUsername($email);
            $transport->setPassword($request->request->get('password'));
            $transport->start();
            $transport->stop();
        } catch (\Exception $e) {
            // If SMTP authentication fails, do not support the request
            // so the next authenticator is used
            $this->logger->error($e->getMessage());
            return false;
        }

        return true;
    }

    public function authenticate(Request $request): Passport
    {
        // Load or create user
        $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->request->get('email'));

        if (!$user instanceof User) {
            // Create a new user
            $user = $this->userFactory->createAdmin($request->request->get('email'), $request->request->get('password'));

            // Persist the new user
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        $badges = [new CsrfTokenBadge('_token', $request->get('_token'))];

        if ($request->request->get('_remember_me')) {
            $badges[] = new RememberMeBadge();
        }

        return new SelfValidatingPassport(new UserBadge($user->getEmail()), $badges);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $firewallName
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->logger->info('Successful login.');

        // Get the current hostname
        $hostname = $request->getHost();

        // Create the cookie name
        $cookieName = $hostname . '_arodax_admin_preferred_locale';

        // Check if a preferred locale cookie is set
        if ($request->cookies->has($cookieName)) {
            // Get the preferred locale from the cookie
            $preferredLocale = $request->cookies->get($cookieName);

            // Return a response with the preferred locale
            return new JsonResponse([
                'url' => $this->urlGenerator->generate('arodax_admin_default_index', ['_locale' => $preferredLocale]),
            ], Response::HTTP_OK);
        }

        return new JsonResponse([
            'url' => $this->urlGenerator->generate('arodax_admin_default_index'),
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $this->logger->notice('Authentication failure during the login.');

        return new JsonResponse([
            'title' => $this->translator->trans($exception->getMessageKey(), $exception->getMessageData()),
            'status' => Response::HTTP_UNAUTHORIZED,
        ], Response::HTTP_UNAUTHORIZED, ['Content-Type' => 'application/problem+json']);
    }

}
