<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Notifier\CustomLoginLinkNotification;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;
use function Symfony\Component\String\u;
use Symfony\Contracts\Translation\TranslatorInterface;

final class LoginLinkProvider
{
    private NotifierInterface $notifier;
    private LoginLinkHandlerInterface $loginLinkHandler;
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;

    public function __construct(NotifierInterface $notifier, LoginLinkHandlerInterface $loginLinkHandler, EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->notifier = $notifier;
        $this->loginLinkHandler = $loginLinkHandler;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * Sends the login link to the user identified by e-mail addreess.
     *
     * @param string $email the email of user, to which login link should be sent
     */
    public function sendLoginLink(string $email): void
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email, 'enabled' => true]);

        if (!$user) {
            throw new BadRequestHttpException('Unable the send the login link.');
        }

        $loginLinkDetails = $this->provideLoginLink($user);

        $notification = new CustomLoginLinkNotification(
            $loginLinkDetails,
            u($this->translator->trans('login link', [], 'arodax_admin'))->title()->toString()
        );

        $recipient = new Recipient($user->getEmail());
        $this->notifier->send($notification, $recipient);
    }

    /**
     * Provides login link details for the given user.
     *
     * @param User $user the user to which login link should be generated
     */
    public function provideLoginLink(User $user): LoginLinkDetails
    {
        $user->setLastLinkRequestedAt(new \DateTimeImmutable());

        $this->entityManager->flush();

        $this->entityManager->refresh($user);

        return $this->loginLinkHandler->createLoginLink($user);
    }
}
