<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Newsletter\EmailMessage;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EmailMessageVoter extends Voter
{
    final public const string CREATE_EMAIL_MESSAGE = 'CREATE_EMAIL_MESSAGE';
    final public const string READ_EMAIL_MESSAGE = 'READ_EMAIL_MESSAGE';
    final public const string UPDATE_EMAIL_MESSAGE = 'UPDATE_EMAIL_MESSAGE';
    final public const string DELETE_EMAIL_MESSAGE = 'DELETE_EMAIL_MESSAGE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an EMAIL_MESSAGE voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_EMAIL_MESSAGE,
            self::READ_EMAIL_MESSAGE,
            self::UPDATE_EMAIL_MESSAGE,
            self::DELETE_EMAIL_MESSAGE,
        ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::CREATE_EMAIL_MESSAGE => $this->canCreate(),
            self::READ_EMAIL_MESSAGE => $this->canRead($subject, $user),
            self::UPDATE_EMAIL_MESSAGE => $this->canUpdate($subject, $user),
            self::DELETE_EMAIL_MESSAGE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if EMAIL_MESSAGE can be created.
     */
    protected function canCreate(): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if EMAIL_MESSAGE can be read.
     */
    protected function canRead(EmailMessage $emailMessage, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if EMAIL_MESSAGE can be updated.
     */
    protected function canUpdate(EmailMessage $emailMessage, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if EMAIL_MESSAGE can be deleted.
     */
    protected function canDelete(EmailMessage $emailMessage, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }
}
