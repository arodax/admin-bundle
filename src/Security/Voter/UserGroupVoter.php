<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\ValueObject\UserGroupUser;
use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserGroupVoter extends Voter
{
    final public const string CREATE_USER_GROUP = 'CREATE_USER_GROUP';
    final public const string READ_USER_GROUP = 'READ_USER_GROUP';
    final public const string UPDATE_USER_GROUP = 'UPDATE_USER_GROUP';
    final public const string DELETE_USER_GROUP = 'DELETE_USER_GROUP';
    final public const string DELETE_USER_FROM_USER_GROUP = 'DELETE_USER_FROM_USER_GROUP';
    final public const string ADD_USER_TO_USER_GROUP = 'DELETE_USER_FROM_USER_GROUP';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_USER_GROUP,
            self::READ_USER_GROUP,
            self::UPDATE_USER_GROUP,
            self::DELETE_USER_GROUP,
            self::DELETE_USER_FROM_USER_GROUP,
            self::ADD_USER_TO_USER_GROUP,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param UserGroup|UserGroupUser $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_USER_GROUP => $this->canCreate($subject, $user),
            self::READ_USER_GROUP => $this->canRead($subject, $user),
            self::UPDATE_USER_GROUP => $this->canUpdate($subject, $user),
            self::DELETE_USER_GROUP => $this->canDelete($subject, $user),
            self::DELETE_USER_FROM_USER_GROUP => $this->canDeleteUserFromUserGroup($subject, $user),
            self::ADD_USER_TO_USER_GROUP => $this->canAddUserToUserGroup($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(UserGroup $userGroup, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(UserGroup $userGroup, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(UserGroup $userGroup, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(UserGroup $userGroup, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }

    protected function canDeleteUserFromUserGroup(UserGroupUser $userGroupUser, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }

    protected function canAddUserToUserGroup(UserGroupUser $userGroupUser, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_GROUP_ADMIN');
    }
}
