<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\Product;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProductVoter extends Voter
{
    final public const string CREATE_PRODUCT = 'CREATE_PRODUCT';
    final public const string READ_PRODUCT = 'READ_PRODUCT';
    final public const string UPDATE_PRODUCT = 'UPDATE_PRODUCT';
    final public const string DELETE_PRODUCT = 'DELETE_PRODUCT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a Product voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Product) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_PRODUCT,
            self::READ_PRODUCT,
            self::UPDATE_PRODUCT,
            self::DELETE_PRODUCT,
        ]);
    }

    /**
     * Voter on product attributes.
     *
     * @param Product $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_PRODUCT => $this->canCreate($subject),
            self::READ_PRODUCT => $this->canRead($subject),
            self::UPDATE_PRODUCT => $this->canUpdate($subject),
            self::DELETE_PRODUCT => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if product can be created.
     */
    protected function canCreate(Product $product): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be read.
     */
    protected function canRead(Product $product): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be updated.
     */
    protected function canUpdate(Product $product): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be deleted.
     */
    protected function canDelete(Product $product): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }
}
