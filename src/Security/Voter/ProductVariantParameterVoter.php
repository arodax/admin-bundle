<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProductVariantParameterVoter extends Voter
{
    final public const string CREATE_PRODUCT_VARIANT_PARAMETER = 'CREATE_PRODUCT_VARIANT_PARAMETER';
    final public const string READ_PRODUCT_VARIANT_PARAMETER = 'READ_PRODUCT_VARIANT_PARAMETER';
    final public const string UPDATE_PRODUCT_VARIANT_PARAMETER = 'UPDATE_PRODUCT_VARIANT_PARAMETER';
    final public const string DELETE_PRODUCT_VARIANT_PARAMETER = 'DELETE_PRODUCT_VARIANT_PARAMETER';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an ProductVariant voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof ProductVariant) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_PRODUCT_VARIANT_PARAMETER,
            self::READ_PRODUCT_VARIANT_PARAMETER,
            self::UPDATE_PRODUCT_VARIANT_PARAMETER,
            self::DELETE_PRODUCT_VARIANT_PARAMETER,
        ]);
    }

    /**
     * Voter on product attributes.
     *
     * @param ProductVariant $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_PRODUCT_VARIANT_PARAMETER => $this->canCreate($subject),
            self::READ_PRODUCT_VARIANT_PARAMETER => $this->canRead($subject),
            self::UPDATE_PRODUCT_VARIANT_PARAMETER => $this->canUpdate($subject),
            self::DELETE_PRODUCT_VARIANT_PARAMETER => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if product can be created.
     */
    protected function canCreate(ProductVariant $productVariant): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be read.
     */
    protected function canRead(ProductVariant $productVariant): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be updated.
     */
    protected function canUpdate(ProductVariant $productVariant): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }

    /*
     * Check if product can be deleted.
     */
    protected function canDelete(ProductVariant $productVariant): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PRODUCT_ADMIN');
    }
}
