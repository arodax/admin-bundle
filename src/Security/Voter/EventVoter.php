<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EventVoter extends Voter
{
    final public const string CREATE_EVENT = 'CREATE_EVENT';
    final public const string READ_EVENT = 'READ_EVENT';
    final public const string UPDATE_EVENT = 'UPDATE_EVENT';
    final public const string DELETE_EVENT = 'DELETE_EVENT';


    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an Event voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Event) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_EVENT,
            self::READ_EVENT,
            self::UPDATE_EVENT,
            self::DELETE_EVENT,
        ]);
    }

    /**
     * Voter on event attributes.
     *
     * @param Event  $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_EVENT => $this->canCreate($subject, $user),
            self::READ_EVENT => $this->canRead($subject, $user),
            self::UPDATE_EVENT => $this->canUpdate($subject, $user),
            self::DELETE_EVENT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if event can be created.
     */
    protected function canCreate(Event $event, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN');
    }

    /*
     * Check if event can be read.
     */
    protected function canRead(Event $event, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN') || ($user instanceof User && \in_array($user, $event->getAuthors(), true));
    }

    /*
     * Check if event can be updated.
     */
    protected function canUpdate(Event $event, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN') || ($user instanceof User && \in_array($user, $event->getAuthors(), true));
    }

    /*
     * Check if event can be deleted.
     */
    protected function canDelete(Event $event, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN') || ($user instanceof User && \in_array($user, $event->getAuthors(), true));
    }
}
