<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Gallery\Gallery;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class GalleryVoter extends Voter
{
    final public const string CREATE_GALLERY = 'CREATE_GALLERY';
    final public const string READ_GALLERY = 'READ_GALLERY';
    final public const string UPDATE_GALLERY = 'UPDATE_GALLERY';
    final public const string DELETE_GALLERY = 'DELETE_GALLERY';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a Gallery voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Gallery) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_GALLERY,
            self::READ_GALLERY,
            self::UPDATE_GALLERY,
            self::DELETE_GALLERY,
        ]);
    }

    /*
     * Voter on user attributes.
     *
     * @param Gallery $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_GALLERY => $this->canCreate($subject, $user),
            self::READ_GALLERY => $this->canRead($subject, $user),
            self::UPDATE_GALLERY => $this->canUpdate($subject, $user),
            self::DELETE_GALLERY => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Gallery $gallery, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_MULTIMEDIA_GALLERY_ADMIN');
    }

    /*
     * Check if gallery can be read.
     */
    protected function canRead(Gallery $gallery, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_MULTIMEDIA_GALLERY_ADMIN');
    }

    /*
     * Check if gallery can be updated.
     */
    protected function canUpdate(Gallery $gallery, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_MULTIMEDIA_GALLERY_ADMIN');
    }

    /*
     * Check if gallery can be deleted.
     */
    protected function canDelete(Gallery $gallery, mixed $currentUser): bool
    {
        if (!$gallery->isDeletable()) {
            return false;
        }

        return $this->security->isGranted('ROLE_MULTIMEDIA_GALLERY_ADMIN');
    }
}
