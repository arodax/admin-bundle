<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Settings;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SettingsVoter extends Voter
{
    final public const string CREATE_SETTINGS = 'CREATE_SETTINGS';
    final public const string READ_SETTINGS = 'READ_SETTINGS';
    final public const string UPDATE_SETTINGS = 'UPDATE_SETTINGS';
    final public const string DELETE_SETTINGS = 'DELETE_SETTINGS';


    public function __construct(
        private readonly Security $security
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Settings) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_SETTINGS,
            self::READ_SETTINGS,
            self::UPDATE_SETTINGS,
            self::DELETE_SETTINGS,
        ]);
    }

    /**
     * @param Settings $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {


        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_SETTINGS => $this->canCreate($subject, $user),
            self::READ_SETTINGS => $this->canRead($subject, $user),
            self::UPDATE_SETTINGS => $this->canUpdate($subject, $user),
            self::DELETE_SETTINGS => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    protected function canCreate(Settings $settings, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_CONFIGURATION_ADMIN');
    }

    protected function canRead(Settings $settings, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_CONFIGURATION_ADMIN');
    }

    protected function canUpdate(Settings $settings, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_CONFIGURATION_ADMIN');
    }

    protected function canDelete(Settings $settings, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_CONFIGURATION_ADMIN');
    }
}
