<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Finance\CreditType;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CreditTypeVoter extends Voter
{
    final public const string CREATE_CREDIT_TYPE = 'CRATE_CREDIT_TYPE';
    final public const string READ_CREDIT_TYPE = 'READ_CREDIT_TYPE';
    final public const string UPDATE_CREDIT_TYPE = 'UPDATE_CREDIT_TYPE';
    final public const string DELETE_CREDIT_TYPE = 'DELETE_CREDIT_TYPE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof CreditType) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_CREDIT_TYPE,
            self::READ_CREDIT_TYPE,
            self::UPDATE_CREDIT_TYPE,
            self::DELETE_CREDIT_TYPE,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param CreditType $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_CREDIT_TYPE => $this->canCreate($subject, $user),
            self::READ_CREDIT_TYPE => $this->canRead($subject, $user),
            self::UPDATE_CREDIT_TYPE => $this->canUpdate($subject, $user),
            self::DELETE_CREDIT_TYPE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(CreditType $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(CreditType $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(CreditType $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(CreditType $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
