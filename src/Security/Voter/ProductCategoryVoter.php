<?php

/**
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProductCategoryVoter extends Voter
{
    final public const string CREATE_PRODUCT_CATEGORY = 'CREATE_PRODUCT_CATEGORY';
    final public const string READ_PRODUCT_CATEGORY = 'READ_PRODUCT_CATEGORY';
    final public const string UPDATE_PRODUCT_CATEGORY = 'UPDATE_PRODUCT_CATEGORY';
    final public const string DELETE_PRODUCT_CATEGORY = 'DELETE_PRODUCT_CATEGORY';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /**
     * Check if this voter is a ProductCategory voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof ProductCategory) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_PRODUCT_CATEGORY,
            self::READ_PRODUCT_CATEGORY,
            self::UPDATE_PRODUCT_CATEGORY,
            self::DELETE_PRODUCT_CATEGORY,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param ProductCategory $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_PRODUCT_CATEGORY => $this->canCreate($subject, $user),
            self::READ_PRODUCT_CATEGORY => $this->canRead($subject, $user),
            self::UPDATE_PRODUCT_CATEGORY => $this->canUpdate($subject, $user),
            self::DELETE_PRODUCT_CATEGORY => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(ProductCategory $productCategory, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_CATEGORY_ADMIN');
    }

    /*
     * Check if productCategory can be read.
     */
    protected function canRead(ProductCategory $productCategory, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_CATEGORY_ADMIN');
    }

    /*
     * Check if productCategory can be updated.
     */
    protected function canUpdate(ProductCategory $productCategory, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_CATEGORY_ADMIN');
    }

    /*
     * Check if productCategory can be deleted.
     */
    protected function canDelete(ProductCategory $productCategory, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_CATEGORY_ADMIN');
    }
}
