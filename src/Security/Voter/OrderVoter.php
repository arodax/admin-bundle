<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OrderVoter extends Voter
{
    final public const string CREATE_ORDER = 'CREATE_ORDER';
    final public const string READ_ORDER = 'READ_ORDER';
    final public const string UPDATE_ORDER = 'UPDATE_ORDER';
    final public const string DELETE_ORDER = 'DELETE_ORDER';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an Order voter.
     */
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof Order) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_ORDER,
            self::READ_ORDER,
            self::UPDATE_ORDER,
            self::DELETE_ORDER,
        ]);
    }

    /**
     * Voter on order attributes.
     *
     * @param Order  $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_ORDER => $this->canCreate($subject),
            self::READ_ORDER => $this->canRead($subject),
            self::UPDATE_ORDER => $this->canUpdate($subject),
            self::DELETE_ORDER => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if order can be created.
     */
    protected function canCreate(Order $order): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_ORDER_ADMIN');
    }

    /*
     * Check if order can be read.
     */
    protected function canRead(Order $order): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_ORDER_ADMIN');
    }

    /*
     * Check if order can be updated.
     */
    protected function canUpdate(Order $order): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_ORDER_ADMIN');
    }

    /*
     * Check if order can be deleted.
     */
    protected function canDelete(Order $order): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_ORDER_ADMIN');
    }
}
