<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ShoppingCartItemVoter extends Voter
{
    final public const string CREATE_SHOPPING_CART_ITEM = 'CREATE_SHOPPING_CART_ITEM';
    final public const string READ_SHOPPING_CART_ITEM = 'READ_SHOPPING_CART_ITEM';
    final public const string UPDATE_SHOPPING_CART_ITEM = 'UPDATE_SHOPPING_CART_ITEM';
    final public const string DELETE_SHOPPING_CART_ITEM = 'DELETE_SHOPPING_CART_ITEM';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /**
     * Check if this voter is an ShoppingCart voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof ShoppingCart) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_SHOPPING_CART_ITEM,
            self::READ_SHOPPING_CART_ITEM,
            self::UPDATE_SHOPPING_CART_ITEM,
            self::DELETE_SHOPPING_CART_ITEM,
        ]);
    }

    /**
     * Voter on shoppingCart attributes.
     *
     * @param ShoppingCart $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_SHOPPING_CART_ITEM => $this->canCreate($subject),
            self::READ_SHOPPING_CART_ITEM => $this->canRead($subject),
            self::UPDATE_SHOPPING_CART_ITEM => $this->canUpdate($subject),
            self::DELETE_SHOPPING_CART_ITEM => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if shoppingCart can be created.
     */
    protected function canCreate(ShoppingCart $shoppingCart): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHOPPING_CART_ADMIN');
    }

    /*
     * Check if shoppingCart can be read.
     */
    protected function canRead(ShoppingCart $shoppingCart): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHOPPING_CART_ADMIN');
    }

    /*
     * Check if shoppingCart can be updated.
     */
    protected function canUpdate(ShoppingCart $shoppingCart): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHOPPING_CART_ADMIN');
    }

    /*
     * Check if shoppingCart can be deleted.
     */
    protected function canDelete(ShoppingCart $shoppingCart): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHOPPING_CART_ADMIN');
    }
}
