<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Newsletter\NewsletterRecipient;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class NewsletterRecipientVoter extends Voter
{
    final public const string CREATE_NEWSLETTER_RECIPIENT = 'CREATE_NEWSLETTER_RECIPIENT';
    final public const string READ_NEWSLETTER_RECIPIENT = 'READ_NEWSLETTER_RECIPIENT';
    final public const string UPDATE_NEWSLETTER_RECIPIENT = 'UPDATE_NEWSLETTER_RECIPIENT';
    final public const string DELETE_NEWSLETTER_RECIPIENT = 'DELETE_NEWSLETTER_RECIPIENT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an NewsletterRecipient voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_NEWSLETTER_RECIPIENT,
            self::READ_NEWSLETTER_RECIPIENT,
            self::UPDATE_NEWSLETTER_RECIPIENT,
            self::DELETE_NEWSLETTER_RECIPIENT,
        ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::CREATE_NEWSLETTER_RECIPIENT => $this->canCreate(),
            self::READ_NEWSLETTER_RECIPIENT => $this->canRead($subject, $user),
            self::UPDATE_NEWSLETTER_RECIPIENT => $this->canUpdate($subject, $user),
            self::DELETE_NEWSLETTER_RECIPIENT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if newsletterRecipient can be created.
     */
    protected function canCreate(): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if newsletterRecipient can be read.
     */
    protected function canRead(NewsletterRecipient $newsletterRecipient, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if newsletterRecipient can be updated.
     */
    protected function canUpdate(NewsletterRecipient $newsletterRecipient, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }

    /*
     * Check if newsletterRecipient can be deleted.
     */
    protected function canDelete(NewsletterRecipient $newsletterRecipient, mixed $user): bool
    {

        return $this->security->isGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN');
    }
}
