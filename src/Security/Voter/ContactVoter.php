<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Contact\Contact;
use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContactVoter extends Voter
{
    final public const string CREATE_CONTACT = 'CREATE_CONTACT';
    final public const string READ_CONTACT = 'READ_CONTACT';
    final public const string UPDATE_CONTACT = 'UPDATE_CONTACT';
    final public const string DELETE_CONTACT = 'DELETE_CONTACT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Contact) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_CONTACT,
            self::READ_CONTACT,
            self::UPDATE_CONTACT,
            self::DELETE_CONTACT,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param Contact $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_CONTACT => $this->canCreate($subject, $user),
            self::READ_CONTACT => $this->canRead($subject, $user),
            self::UPDATE_CONTACT => $this->canUpdate($subject, $user),
            self::DELETE_CONTACT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Contact $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(Contact $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(Contact $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(Contact $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }
}
