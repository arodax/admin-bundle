<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Localization\Translatable;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TranslatableVoter extends Voter
{
    final public const string CREATE_TRANSLATABLE = 'CREATE_TRANSLATABLE';
    final public const string READ_TRANSLATABLE = 'READ_TRANSLATABLE';
    final public const string UPDATE_TRANSLATABLE = 'UPDATE_TRANSLATABLE';
    final public const string DELETE_TRANSLATABLE = 'DELETE_TRANSLATABLE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an I18n voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Translatable) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_TRANSLATABLE,
            self::READ_TRANSLATABLE,
            self::UPDATE_TRANSLATABLE,
            self::DELETE_TRANSLATABLE,
        ]);
    }

    /**
     * Voter on I18n attributes.
     *
     * @param Translatable $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_TRANSLATABLE => $this->canCreate($subject, $user),
            self::READ_TRANSLATABLE => $this->canRead($subject, $user),
            self::UPDATE_TRANSLATABLE => $this->canUpdate($subject, $user),
            self::DELETE_TRANSLATABLE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if I18n can be created.
     */
    protected function canCreate(Translatable $I18n, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_TRANSLATION_ADMIN');
    }

    /*
     * Check if I18n can be read.
     */
    protected function canRead(Translatable $I18n, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_TRANSLATION_ADMIN');
    }

    /*
     * Check if I18n can be updated.
     */
    protected function canUpdate(Translatable $I18n, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_TRANSLATION_ADMIN');
    }

    /*
     * Check if I18n can be deleted.
     */
    protected function canDelete(Translatable $I18n, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_TRANSLATION_ADMIN');
    }
}
