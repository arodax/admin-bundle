<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Finance\Credit;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CreditVoter extends Voter
{
    final public const string CREATE_CREDIT = 'CRATE_CREDIT';
    final public const string READ_CREDIT = 'READ_CREDIT';
    final public const string UPDATE_CREDIT = 'UPDATE_CREDIT';
    final public const string DELETE_CREDIT = 'DELETE_CREDIT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Credit) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_CREDIT,
            self::READ_CREDIT,
            self::UPDATE_CREDIT,
            self::DELETE_CREDIT,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param Credit $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_CREDIT => $this->canCreate($subject, $user),
            self::READ_CREDIT => $this->canRead($subject, $user),
            self::UPDATE_CREDIT => $this->canUpdate($subject, $user),
            self::DELETE_CREDIT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Credit $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(Credit $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(Credit $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(Credit $credit, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
