<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ArodaxSupportVoter extends Voter
{

    private const string SWITCH_USER_ROLE = 'ROLE_ALLOWED_TO_SWITCH';

    protected function supports(string $attribute, $subject): bool
    {
        // If the attribute isn't one we support, return false
        return in_array($attribute, [self::SWITCH_USER_ROLE, 'ROLE_ADMIN']);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // If the user object is anonymous or the user's ID isn't what you expect, deny access
        if (!$user instanceof UserInterface || $user->getEmail() !== 'podpora@arodax.com') {
            return false;
        }

        // If the attribute is the switch user role or admin role, grant access
        return in_array($attribute, [self::SWITCH_USER_ROLE, 'ROLE_ADMIN']);
    }
}
