<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Finance\BankAccount;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BankAccountVoter extends Voter
{
    final public const string CREATE_BANK_ACCOUNT = 'CRATE_BANK_ACCOUNT';
    final public const string READ_BANK_ACCOUNT = 'READ_BANK_ACCOUNT';
    final public const string UPDATE_BANK_ACCOUNT = 'UPDATE_BANK_ACCOUNT';
    final public const string DELETE_BANK_ACCOUNT = 'DELETE_BANK_ACCOUNT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof BankAccount) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_BANK_ACCOUNT,
            self::READ_BANK_ACCOUNT,
            self::UPDATE_BANK_ACCOUNT,
            self::DELETE_BANK_ACCOUNT,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param BankAccount $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_BANK_ACCOUNT => $this->canCreate($subject, $user),
            self::READ_BANK_ACCOUNT => $this->canRead($subject, $user),
            self::UPDATE_BANK_ACCOUNT => $this->canUpdate($subject, $user),
            self::DELETE_BANK_ACCOUNT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(BankAccount $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(BankAccount $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(BankAccount $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(BankAccount $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_ADMIN');
    }
}
