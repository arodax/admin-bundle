<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Grantys\GrantCall;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class GrantCallVoter extends Voter
{
    final public const string CREATE_GRANT_CALL = 'CREATE_GRANT_CALL';
    final public const string READ_GRANT_CALL = 'READ_GRANT_CALL';
    final public const string UPDATE_GRANT_CALL = 'UPDATE_GRANT_CALL';
    final public const string DELETE_GRANT_CALL = 'DELETE_GRANT_CALL';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an GrantCall voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_GRANT_CALL,
            self::READ_GRANT_CALL,
            self::UPDATE_GRANT_CALL,
            self::DELETE_GRANT_CALL,
        ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_GRANT_CALL => $this->canCreate(),
            self::READ_GRANT_CALL => $this->canRead($subject),
            self::UPDATE_GRANT_CALL => $this->canUpdate($subject),
            self::DELETE_GRANT_CALL => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if grant call can be created.
     */
    protected function canCreate(): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant call can be read.
     */
    protected function canRead(GrantCall $grantCall): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant call can be updated.
     */
    protected function canUpdate(GrantCall $grantCall): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant call can be deleted.
     */
    protected function canDelete(GrantCall $grantCall): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }
}
