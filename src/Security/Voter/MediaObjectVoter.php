<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Media\MediaObject;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MediaObjectVoter extends Voter
{
    final public const string CREATE_MEDIA_OBJECT = 'CREATE_MEDIA_OBJECT';
    final public const string READ_MEDIA_OBJECT = 'READ_MEDIA_OBJECT';
    final public const string UPDATE_MEDIA_OBJECT = 'UPDATE_MEDIA_OBJECT';
    final public const string DELETE_MEDIA_OBJECT = 'DELETE_MEDIA_OBJECT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an MediaObject voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof MediaObject) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_MEDIA_OBJECT,
            self::READ_MEDIA_OBJECT,
            self::UPDATE_MEDIA_OBJECT,
            self::DELETE_MEDIA_OBJECT,
        ]);
    }

    /**
     * Voter on MediaObject attributes.
     *
     * @param MediaObject $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_MEDIA_OBJECT => $this->canCreate($subject, $user),
            self::READ_MEDIA_OBJECT => $this->canRead($subject, $user),
            self::UPDATE_MEDIA_OBJECT => $this->canUpdate($subject, $user),
            self::DELETE_MEDIA_OBJECT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if MediaObject can be created.
     */
    protected function canCreate(MediaObject $MediaObject, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if MediaObject can be read.
     */
    protected function canRead(MediaObject $MediaObject, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if MediaObject can be updated.
     */
    protected function canUpdate(MediaObject $MediaObject, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if MediaObject can be deleted.
     */
    protected function canDelete(MediaObject $MediaObject, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }
}
