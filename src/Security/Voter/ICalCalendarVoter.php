<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ICalCalendarVoter extends Voter
{
    final public const string SUBSCRIBE_EVENTS = 'subscribe_events';

    public function __construct(
        #[Autowire(env: 'resolve:APP_SECRET')]
        private readonly string $secret
    ) {
    }

    /**
     * {@inheritdoc}
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === self::SUBSCRIBE_EVENTS && is_string($subject);
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::SUBSCRIBE_EVENTS => $this->canSubscribeEvents($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if the current token can be used to subscribe to the public events calendar.
     */
    protected function canSubscribeEvents(string $token): bool
    {
        return $token === hash('sha256', $this->secret);
    }
}
