<?php

/**
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MenuItemVoter extends Voter
{
    final public const string CREATE_MENU_ITEM = 'CREATE_MENU_ITEM';
    final public const string READ_MENU_ITEM = 'READ_MENU_ITEM';
    final public const string UPDATE_MENU_ITEM = 'UPDATE_MENU_ITEM';
    final public const string DELETE_MENU_ITEM = 'DELETE_MENU_ITEM';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an MenuItem voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof MenuItem) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_MENU_ITEM,
            self::READ_MENU_ITEM,
            self::UPDATE_MENU_ITEM,
            self::DELETE_MENU_ITEM,
        ]);
    }

    /*
     * Voter on menuItem attributes.
     *
     * @param MenuItem $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_MENU_ITEM => $this->canCreate($subject, $user),
            self::READ_MENU_ITEM => $this->canRead($subject, $user),
            self::UPDATE_MENU_ITEM => $this->canUpdate($subject, $user),
            self::DELETE_MENU_ITEM => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if menuItem can be created.
     */
    protected function canCreate(MenuItem $menuItem, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_MENU_ADMIN');
    }

    /*
     * Check if menuItem can be read.
     */
    protected function canRead(MenuItem $menuItem, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_MENU_ADMIN');
    }

    /*
     * Check if menuItem can be updated.
     */
    protected function canUpdate(MenuItem $menuItem, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_SETTING_MENU_ADMIN');
    }

    /*
     * Check if menuItem can be deleted.
     */
    protected function canDelete(MenuItem $menuItem, mixed $user): bool
    {
        if (!$menuItem->isDeletable()) {
            return false;
        }

        return $this->security->isGranted('ROLE_SETTING_MENU_ADMIN');
    }
}
