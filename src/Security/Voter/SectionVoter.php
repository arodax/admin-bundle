<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Content\Section;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SectionVoter extends Voter
{
    final public const string CREATE_SECTION = 'CREATE_SECTION';
    final public const string READ_SECTION = 'READ_SECTION';
    final public const string UPDATE_SECTION = 'UPDATE_SECTION';
    final public const string DELETE_SECTION = 'DELETE_SECTION';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a Section voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Section) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_SECTION,
            self::READ_SECTION,
            self::UPDATE_SECTION,
            self::DELETE_SECTION,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param Section $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_SECTION => $this->canCreate($subject, $user),
            self::READ_SECTION => $this->canRead($subject, $user),
            self::UPDATE_SECTION => $this->canUpdate($subject, $user),
            self::DELETE_SECTION => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Section $section, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_SECTION_ADMIN');
    }

    /*
     * Check if section can be read.
     */
    protected function canRead(Section $section, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_SECTION_ADMIN');
    }

    /*
     * Check if section can be updated.
     */
    protected function canUpdate(Section $section, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_SECTION_ADMIN');
    }

    /*
     * Check if section can be deleted.
     */
    protected function canDelete(Section $section, mixed $currentUser): bool
    {
        if (!$section->isDeletable()) {
            return false;
        }

        return $this->security->isGranted('ROLE_CONTENT_SECTION_ADMIN');
    }
}
