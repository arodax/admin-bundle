<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\ShippingMethod;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ShippingMethodVoter extends Voter
{
    final public const string CREATE_SHIPPING_METHOD = 'CREATE_SHIPPING_METHOD';
    final public const string READ_SHIPPING_METHOD = 'READ_SHIPPING_METHOD';
    final public const string UPDATE_SHIPPING_METHOD = 'UPDATE_SHIPPING_METHOD';
    final public const string DELETE_SHIPPING_METHOD = 'DELETE_SHIPPING_METHOD';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /**
     * Check if this voter is an ShippingMethod voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof ShippingMethod) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_SHIPPING_METHOD,
            self::READ_SHIPPING_METHOD,
            self::UPDATE_SHIPPING_METHOD,
            self::DELETE_SHIPPING_METHOD,
        ]);
    }

    /**
     * Voter on ShippingMethod attributes.
     *
     * @param ShippingMethod $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_SHIPPING_METHOD => $this->canCreate($subject),
            self::READ_SHIPPING_METHOD => $this->canRead($subject),
            self::UPDATE_SHIPPING_METHOD => $this->canUpdate($subject),
            self::DELETE_SHIPPING_METHOD => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if ShippingMethod can be created.
     */
    protected function canCreate(ShippingMethod $ShippingMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHIPPING_METHOD_ADMIN');
    }

    /*
     * Check if ShippingMethod can be read.
     */
    protected function canRead(ShippingMethod $ShippingMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHIPPING_METHOD_ADMIN');
    }

    /*
     * Check if ShippingMethod can be updated.
     */
    protected function canUpdate(ShippingMethod $ShippingMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHIPPING_METHOD_ADMIN');
    }

    /*
     * Check if ShippingMethod can be deleted.
     */
    protected function canDelete(ShippingMethod $ShippingMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_SHIPPING_METHOD_ADMIN');
    }
}
