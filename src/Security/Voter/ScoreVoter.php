<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Score\Score;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ScoreVoter extends Voter
{
    final public const string CREATE_SCORE = 'CRATE_SCORE';
    final public const string READ_SCORE = 'READ_SCORE';
    final public const string UPDATE_SCORE = 'UPDATE_SCORE';
    final public const string DELETE_SCORE = 'DELETE_SCORE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Score) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_SCORE,
            self::READ_SCORE,
            self::UPDATE_SCORE,
            self::DELETE_SCORE,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param Score $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_SCORE => $this->canCreate($subject, $user),
            self::READ_SCORE => $this->canRead($subject, $user),
            self::UPDATE_SCORE => $this->canUpdate($subject, $user),
            self::DELETE_SCORE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Score $score, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(Score $score, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(Score $score, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(Score $score, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}
