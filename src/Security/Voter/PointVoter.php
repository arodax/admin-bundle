<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Geography\Point;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PointVoter extends Voter
{
    final public const string CREATE_POINT = 'CREATE_POINT';
    final public const string READ_POINT = 'READ_POINT';
    final public const string UPDATE_POINT = 'UPDATE_POINT';
    final public const string DELETE_POINT = 'DELETE_POINT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /**
     * Check if this voter is a Point voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Point) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_POINT,
            self::READ_POINT,
            self::UPDATE_POINT,
            self::DELETE_POINT,
        ]);
    }

    /**
     * Voter on point attributes.
     *
     * @param Point $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_POINT => $this->canCreate($subject, $user),
            self::READ_POINT => $this->canRead($subject, $user),
            self::UPDATE_POINT => $this->canUpdate($subject, $user),
            self::DELETE_POINT => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if point can be created.
     */
    protected function canCreate(Point $point, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if point can be read.
     */
    protected function canRead(Point $point, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if point can be updated.
     */
    protected function canUpdate(Point $point, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }

    /*
     * Check if point can be deleted.
     */
    protected function canDelete(Point $point, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_ADMINISTRATION_ACCESS');
    }
}
