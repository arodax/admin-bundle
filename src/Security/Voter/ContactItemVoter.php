<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Contact\ContactItem;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContactItemVoter extends Voter
{
    final public const string CREATE_CONTACT_ITEM = 'CRATE_CONTACT_ITEM';
    final public const string READ_CONTACT_ITEM = 'READ_CONTACT_ITEM';
    final public const string UPDATE_CONTACT_ITEM = 'UPDATE_CONTACT_ITEM';
    final public const string DELETE_CONTACT_ITEM = 'DELETE_CONTACT_ITEM';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof ContactItem) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_CONTACT_ITEM,
            self::READ_CONTACT_ITEM,
            self::UPDATE_CONTACT_ITEM,
            self::DELETE_CONTACT_ITEM,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param ContactItem $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_CONTACT_ITEM => $this->canCreate($subject, $user),
            self::READ_CONTACT_ITEM => $this->canRead($subject, $user),
            self::UPDATE_CONTACT_ITEM => $this->canUpdate($subject, $user),
            self::DELETE_CONTACT_ITEM => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(ContactItem $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(ContactItem $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(ContactItem $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(ContactItem $userContact, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }
}
