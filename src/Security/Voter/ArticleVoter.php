<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ArticleVoter extends Voter
{
    final public const string CREATE_ARTICLE = 'CREATE_ARTICLE';
    final public const string READ_ARTICLE = 'READ_ARTICLE';
    final public const string UPDATE_ARTICLE = 'UPDATE_ARTICLE';
    final public const string DELETE_ARTICLE = 'DELETE_ARTICLE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an Article voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_ARTICLE,
            self::READ_ARTICLE,
            self::UPDATE_ARTICLE,
            self::DELETE_ARTICLE,
        ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::CREATE_ARTICLE => $this->canCreate(),
            self::READ_ARTICLE => $this->canRead($subject, $user),
            self::UPDATE_ARTICLE => $this->canUpdate($subject, $user),
            self::DELETE_ARTICLE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if article can be created.
     */
    protected function canCreate(): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_ARTICLE_ADMIN');
    }

    /*
     * Check if article can be read.
     */
    protected function canRead(Article $article, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_ARTICLE_ADMIN') || ($user instanceof User && \in_array($user, $article->getAuthors(), true));
    }

    /*
     * Check if article can be updated.
     */
    protected function canUpdate(Article $article, mixed $user): bool
    {
        return $this->security->isGranted('ROLE_CONTENT_ARTICLE_ADMIN') || ($user instanceof User && \in_array($user, $article->getAuthors(), true));
    }

    /*
     * Check if article can be deleted.
     */
    protected function canDelete(Article $article, mixed $user): bool
    {
        if (!$article->isDeletable()) {
            return false;
        }

        return $this->security->isGranted('ROLE_CONTENT_ARTICLE_ADMIN') || ($user instanceof User && \in_array($user, $article->getAuthors(), true));
    }
}
