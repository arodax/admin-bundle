<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Arodax\AdminBundle\Entity\Grantys\GrantProject;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class GrantProjectVoter extends Voter
{
    final public const string CREATE_GRANT_PROJECT = 'CREATE_GRANT_PROJECT';
    final public const string READ_GRANT_PROJECT = 'READ_GRANT_PROJECT';
    final public const string UPDATE_GRANT_PROJECT = 'UPDATE_GRANT_PROJECT';
    final public const string DELETE_GRANT_PROJECT = 'DELETE_GRANT_PROJECT';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an GrantProject voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        return \in_array($attribute, [
            self::CREATE_GRANT_PROJECT,
            self::READ_GRANT_PROJECT,
            self::UPDATE_GRANT_PROJECT,
            self::DELETE_GRANT_PROJECT,
        ]);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_GRANT_PROJECT => $this->canCreate(),
            self::READ_GRANT_PROJECT => $this->canRead($subject),
            self::UPDATE_GRANT_PROJECT => $this->canUpdate($subject),
            self::DELETE_GRANT_PROJECT => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if grant project can be created.
     */
    protected function canCreate(): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant project can be read.
     */
    protected function canRead(GrantProject $grantProject): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant project can be updated.
     */
    protected function canUpdate(GrantProject $grantProject): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }

    /*
     * Check if grant project can be deleted.
     */
    protected function canDelete(GrantProject $grantProject): bool
    {
        return $this->security->isGranted('ROLE_GRANTYS_ADMIN');
    }
}
