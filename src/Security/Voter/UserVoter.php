<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    final public const string CREATE_USER = 'CREATE_USER';
    final public const string READ_USER = 'READ_USER';
    final public const string UPDATE_USER = 'UPDATE_USER';
    final public const string DELETE_USER = 'DELETE_USER';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is a User voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof User) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_USER,
            self::READ_USER,
            self::UPDATE_USER,
            self::DELETE_USER,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param User $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_USER => $this->canCreate($subject, $user),
            self::READ_USER => $this->canRead($subject, $user),
            self::UPDATE_USER => $this->canUpdate($subject, $user),
            self::DELETE_USER => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(User $user, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(User $user, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN') || ($currentUser instanceof User && $user->getId() === $currentUser->getId());
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(User $user, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN') || ($currentUser instanceof User && $user->getId() === $currentUser->getId());
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(User $user, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_PEOPLE_USER_ADMIN');
    }
}
