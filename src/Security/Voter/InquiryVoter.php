<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\ServiceDesk\Inquiry;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InquiryVoter extends Voter
{
    final public const string CREATE_INQUIRY = 'CREATE_INQUIRY';
    final public const string READ_INQUIRY = 'READ_INQUIRY';
    final public const string UPDATE_INQUIRY = 'UPDATE_INQUIRY';
    final public const string DELETE_INQUIRY = 'DELETE_INQUIRY';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an Inquiry voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Inquiry) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_INQUIRY,
            self::READ_INQUIRY,
            self::UPDATE_INQUIRY,
            self::DELETE_INQUIRY,
        ]);
    }

    /**
     * Voter on the Inquiry attributes.
     *
     * @param Inquiry $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_INQUIRY => $this->canCreate($subject, $user),
            self::READ_INQUIRY => $this->canRead($subject, $user),
            self::UPDATE_INQUIRY => $this->canUpdate($subject, $user),
            self::DELETE_INQUIRY => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if user can be created.
     */
    protected function canCreate(Inquiry $ticket, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_INQUIRY_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(Inquiry $ticket, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_INQUIRY_ADMIN');
    }

    /*
     * Check if user can be updated.
     */
    protected function canUpdate(Inquiry $ticket, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_INQUIRY_ADMIN');
    }

    /*
     * Check if user can be deleted.
     */
    protected function canDelete(Inquiry $ticket, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_COMMUNICATION_INQUIRY_ADMIN');
    }
}
