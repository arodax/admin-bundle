<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Model\Finance\InvoiceInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InvoiceVoter extends Voter
{
    final public const string CREATE_INVOICE = 'CREATE_INVOICE';
    final public const string READ_INVOICE = 'READ_INVOICE';
    final public const string UPDATE_INVOICE = 'UPDATE_INVOICE';
    final public const string DELETE_INVOICE = 'DELETE_INVOICE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an InvoiceInterface voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof InvoiceInterface) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_INVOICE,
            self::READ_INVOICE,
            self::UPDATE_INVOICE,
            self::DELETE_INVOICE,
        ]);
    }

    /*
     * Voter on InvoiceInterface attributes.
     *
     * @param InvoiceInterface $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_INVOICE => $this->canCreate($subject),
            self::READ_INVOICE => $this->canRead($subject),
            self::UPDATE_INVOICE => $this->canUpdate($subject),
            self::DELETE_INVOICE => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if InvoiceInterface can be created.
     */
    protected function canCreate(InvoiceInterface $InvoiceInterface): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_INVOICE_ADMIN');
    }

    /*
     * Check if InvoiceInterface can be read.
     */
    protected function canRead(InvoiceInterface $InvoiceInterface): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_INVOICE_ADMIN');
    }

    /*
     * Check if InvoiceInterface can be updated.
     */
    protected function canUpdate(InvoiceInterface $InvoiceInterface): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_INVOICE_ADMIN');
    }

    /*
     * Check if InvoiceInterface can be deleted.
     */
    protected function canDelete(InvoiceInterface $InvoiceInterface): bool
    {
        return $this->security->isGranted('ROLE_FINANCE_INVOICE_ADMIN');
    }
}
