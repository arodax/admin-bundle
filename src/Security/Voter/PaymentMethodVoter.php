<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PaymentMethodVoter extends Voter
{
    final public const string CREATE_PAYMENT_METHOD = 'CREATE_PAYMENT_METHOD';
    final public const string READ_PAYMENT_METHOD = 'READ_PAYMENT_METHOD';
    final public const string UPDATE_PAYMENT_METHOD = 'UPDATE_PAYMENT_METHOD';
    final public const string DELETE_PAYMENT_METHOD = 'DELETE_PAYMENT_METHOD';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an PaymentMethod voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof PaymentMethod) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_PAYMENT_METHOD,
            self::READ_PAYMENT_METHOD,
            self::UPDATE_PAYMENT_METHOD,
            self::DELETE_PAYMENT_METHOD,
        ]);
    }

    /**
     * Voter on PaymentMethod attributes.
     *
     * @param PaymentMethod $subject
     *
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::CREATE_PAYMENT_METHOD => $this->canCreate($subject),
            self::READ_PAYMENT_METHOD => $this->canRead($subject),
            self::UPDATE_PAYMENT_METHOD => $this->canUpdate($subject),
            self::DELETE_PAYMENT_METHOD => $this->canDelete($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if PaymentMethod can be created.
     */
    protected function canCreate(PaymentMethod $PaymentMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PAYMENT_METHOD_ADMIN');
    }

    /*
     * Check if PaymentMethod can be read.
     */
    protected function canRead(PaymentMethod $PaymentMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PAYMENT_METHOD_ADMIN');
    }

    /*
     * Check if PaymentMethod can be updated.
     */
    protected function canUpdate(PaymentMethod $PaymentMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PAYMENT_METHOD_ADMIN');
    }

    /*
     * Check if PaymentMethod can be deleted.
     */
    protected function canDelete(PaymentMethod $PaymentMethod): bool
    {
        return $this->security->isGranted('ROLE_ESHOP_PAYMENT_METHOD_ADMIN');
    }
}
