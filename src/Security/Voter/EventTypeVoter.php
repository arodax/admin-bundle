<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Event\EventType;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class EventTypeVoter extends Voter
{
    final public const string CREATE_EVENT_TYPE = 'CREATE_EVENT_TYPE';
    final public const string READ_EVENT_TYPE = 'READ_EVENT_TYPE';
    final public const string UPDATE_EVENT_TYPE = 'UPDATE_EVENT_TYPE';
    final public const string DELETE_EVENT_TYPE = 'DELETE_EVENT_TYPE';

    public function __construct(
        private readonly Security $security
    ) {
    }

    /*
     * Check if this voter is an EventType voter.
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof EventType) {
            return false;
        }

        return \in_array($attribute, [
            self::CREATE_EVENT_TYPE,
            self::READ_EVENT_TYPE,
            self::UPDATE_EVENT_TYPE,
            self::DELETE_EVENT_TYPE,
        ]);
    }

    /**
     * Voter on user attributes.
     *
     * @param EventType $subject
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        return match ($attribute) {
            self::CREATE_EVENT_TYPE => $this->canCreate($subject, $user),
            self::READ_EVENT_TYPE => $this->canRead($subject, $user),
            self::UPDATE_EVENT_TYPE => $this->canUpdate($subject, $user),
            self::DELETE_EVENT_TYPE => $this->canDelete($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /*
     * Check if event type can be created.
     */
    protected function canCreate(EventType $EventType, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN');
    }

    /*
     * Check if user can be read.
     */
    protected function canRead(EventType $EventType, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN');
    }

    /*
     * Check if event type can be updated.
     */
    protected function canUpdate(EventType $EventType, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN');
    }

    /*
     * Check if event type can be deleted.
     */
    protected function canDelete(EventType $EventType, mixed $currentUser): bool
    {
        return $this->security->isGranted('ROLE_EVENT_CALENDAR_ADMIN');
    }
}
