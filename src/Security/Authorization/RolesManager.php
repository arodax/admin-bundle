<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Authorization;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Provides methods for working with user roles.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class RolesManager
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    /**
     * Return hierarchy list of the available system user roles.
     *
     * @return array[]
     */
    public function getRoles(): array
    {
        $roles = [
            [
                'name' => 'ROLE_SUPER_ADMIN',
                'label' => 'super administrator',
                'children' => [
                    [
                        'name' => 'ROLE_ADMIN',
                        'label' => 'administrator',
                        'children' => [
                            [
                                'name' => 'ROLE_CONTENT_ADMIN',
                                'label' => 'content administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_CONTENT_ARTICLE_ADMIN',
                                        'label' => 'articles administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_CONTENT_SECTION_ADMIN',
                                        'label' => 'sections administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_ESHOP_ADMIN',
                                'label' => 'eshop administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_ESHOP_PRODUCT_ADMIN',
                                        'label' => 'eshop products administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_ESHOP_CATEGORY_ADMIN',
                                        'label' => 'eshop categories administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_ESHOP_SHOPPING_CART_ADMIN',
                                        'label' => 'eshop shopping carts administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_ESHOP_ORDER_ADMIN',
                                        'label' => 'eshop orders administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_ESHOP_SHIPPING_METHOD_ADMIN',
                                        'label' => 'eshop shipping methods administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_ESHOP_PAYMENT_METHOD_ADMIN',
                                        'label' => 'eshop payment methods administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_FINANCE_ADMIN',
                                'label' => 'finance administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_FINANCE_INVOICE_ADMIN',
                                        'label' => 'invoices administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_MULTIMEDIA_ADMIN',
                                'label' => 'multimedia administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_MULTIMEDIA_GALLERY_ADMIN',
                                        'label' => 'gallery administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_EVENT_ADMIN',
                                'label' => 'events administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_EVENT_CALENDAR_ADMIN',
                                        'label' => 'calendar administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_PEOPLE_ADMIN',
                                'label' => 'people administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_PEOPLE_USER_ADMIN',
                                        'label' => 'user administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_PEOPLE_USER_GROUP_ADMIN',
                                        'label' => 'user groups administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_COMMUNICATION_ADMIN',
                                'label' => 'communication administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_COMMUNICATION_INQUIRY_ADMIN',
                                        'label' => 'inquiries administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_COMMUNICATION_MAILER_ADMIN',
                                        'label' => 'newsletter administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_SETTING_ADMIN',
                                'label' => 'settings administrator',
                                'children' => [
                                    [
                                        'name' => 'ROLE_SETTING_MENU_ADMIN',
                                        'label' => 'menu administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_SETTING_TRANSLATION_ADMIN',
                                        'label' => 'translations administrator',
                                    ],
                                    [
                                        'name' => 'ROLE_SETTING_CONFIGURATION_ADMIN',
                                        'label' => 'configuration administrator',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'ROLE_GRANTYS_ADMIN ',
                                'label' => 'Grantys administrator',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'ROLE_ADMIN_RECIPIENT',
                'label' => 'all notifications recipient',
                'children' => [
                    [
                        'name' => 'ROLE_ADMIN_ESHOP_RECIPIENT',
                        'label' => 'eshop notifications recipient',
                    ],
                ],
            ],
        ];

        array_walk_recursive($roles, function (&$value, $key): void {
            if ('label' === $key) {
                $value = $this->translator->trans($value, [], 'arodax_admin');
            }
        });

        return $roles;
    }
}
