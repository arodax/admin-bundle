<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security;

use Arodax\AdminBundle\Entity\User\ResetPasswordToken;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class UserPasswordManager
{
    private const int TOKEN_VALIDITY_SECONDS = 3600;
    private const int TOKEN_HASH_BYTES_LENGTH = 32;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator, RouterInterface $router, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->router = $router;
        $this->mailer = $mailer;
    }

    /**
     * Request a new password for the user and send the instructions.
     *
     * @param User   $user      the user who's password reset is reqeusted
     * @param string $recipient The address where the password e-mail will be send. If null, user's default will be used
     *
     * @throws \Exception
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function requestLostPassword(User $user, ?string $recipient = null)
    {
        $recipient ??= $user->getEmail();

        if (null === $recipient) {
            throw new \RuntimeException('The lost password cannot be requested, because email address of the user is empty.');
        }

        $this->mailer->send($this->createLostPasswordEmail($this->createResetPasswordToken($user), $recipient));
    }

    /**
     * Set a new password.
     *
     * @param ResetPasswordToken $token    Token for the reseting a lost password
     * @param string             $password A new password
     *
     * @throws \Exception                                                      when a new password could not be set
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function resetLostPassword(ResetPasswordToken $token, string $password)
    {
        $user = $token->getUser();

        if ($token->getValidTill() <= new \DateTimeImmutable()) {
            throw new \RuntimeException('Token already expired');
        }

        $user->setPlainPassword($password);

        $this->entityManager->persist($user);
        $this->entityManager->remove($token);
        $this->entityManager->flush();

        $this->mailer->send($this->createPasswordModifiedEmail($token->getUser()));
    }

    /**
     * Generate ResetPassword token for the given user.
     *
     * @param User                    $user      User for which hash should be generated
     * @param \DateTimeImmutable|null $validTill Token expiration time
     *
     * @throws \Exception When token could not be generated
     */
    private function createResetPasswordToken(User $user, ?\DateTimeImmutable $validTill = null): ResetPasswordToken
    {
        $validTill ??= (new \DateTimeImmutable())->modify(sprintf('+ %s seconds', self::TOKEN_VALIDITY_SECONDS));

        $hash = bin2hex(random_bytes(self::TOKEN_HASH_BYTES_LENGTH));

        $token = new ResetPasswordToken();
        $token->setValidTill($validTill);
        $token->setUser($user);
        $token->setHash($hash);

        $this->entityManager->persist($token);
        $this->entityManager->flush();

        return $token;
    }

    /**
     * Create email with instructions how to reset the lost password.
     *
     * @param ResetPasswordToken $token     token for the reseting password
     * @param string             $recipient recipient email of the email
     */
    private function createLostPasswordEmail(ResetPasswordToken $token, string $recipient): Email
    {
        // add preferred locale to user because cli has no _locale param to route
        $buttonHref = $this->router->generate('arodax_admin_security_reset_password', ['hash' => $token->getHash()], RouterInterface::ABSOLUTE_URL);
        $buttonText = $this->translator->trans('continue', [], 'arodax_admin');
        $body = $this->translator->trans('reset_pw_text', ['email' => $token->getUser()->getEmail()], 'arodax_admin', $token->getUser()->getPreferredLocale());
        $subject = $this->translator->trans('lost_pw', [], 'arodax_admin', $token->getUser()->getPreferredLocale());

        return (new TemplatedEmail())
            ->to($recipient)
            ->subject($subject)
            ->htmlTemplate('@ArodaxAdmin/emails/default.html.twig')
            ->context([
                'body' => $body,
                'button_href' => $buttonHref,
                'button_text' => $buttonText,
                'subject' => $subject,
            ]);
    }

    private function createPasswordModifiedEmail(User $user): Email
    {
        $body = $this->translator->trans('password_modified_text', ['%email%' => $user->getEmail()], 'arodax_admin');
        $subject = $this->translator->trans('password_modified', [], 'arodax_admin');

        return (new TemplatedEmail())
            ->to($user->getEmail())
            ->subject($subject)
            ->htmlTemplate('@ArodaxAdmin/emails/default.html.twig')
            ->context([
                'body' => $body,
                'subject' => $subject,
            ]);
    }
}
