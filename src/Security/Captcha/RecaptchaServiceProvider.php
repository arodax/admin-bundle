<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security\Captcha;

use ReCaptcha\ReCaptcha;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

readonly class RecaptchaServiceProvider
{
    public function __construct(
        #[Autowire(env: 'default::resolve:GOOGLE_RECAPTCHA_SECRET')]
        private ?string $secretKey
    ) {
    }

    /**
     * Validates reCAPTCHA for the given request.
     */
    public function validateRequest(Request $request): bool
    {
        if (!$request->request->get('g-recaptcha-response')) {
            throw new BadRequestHttpException('reCAPTCHA requires "g-recaptcha-response" among the submitted data, but it was not found in the request body.');
        }

        $recaptcha = new ReCaptcha($this->secretKey);
        $result = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());

        return $result->isSuccess();
    }
}
