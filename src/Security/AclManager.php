<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Security;

use ApiPlatform\Api\IriConverterInterface;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\SecurityBundle\Security;
use function Symfony\Component\String\u;

readonly class AclManager
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private IriConverterInterface $iriConverter,
        private Security $security
    ) {
    }

    public function handleRequest(Request $request): ?object
    {
        if ($request->getMethod() === Request::METHOD_DELETE) {
            $accessor = $request->query->get('accessor');
            $accessed = $request->query->get('accessed');
        } else {
            $accessor = $request->request->get('accessor');
            $accessed = $request->request->get('accessed');
        }

        if (null === $accessed || null === $accessor) {
            throw new BadRequestException('Mandatory parameters missing in the request.');
        }

        if ($request->getMethod() === Request::METHOD_DELETE) {
            $this->deleteAcl($accessor, $accessed);
            return null;
        }

        return $this->setAcl($accessor, $accessed);
    }

    private function deleteAcl(string $accessorIri, string $accessedIri): void
    {
        $accessed = $this->iriConverter->getResourceFromIri($accessedIri);
        $accessor = $this->iriConverter->getResourceFromIri($accessorIri);

        $qb = $this->entityManager->createQueryBuilder();

        if (false === $this->security->isGranted(self::getCrudVoterAttribute($accessed), $accessed)) {
            throw new BadRequestException('Insufficient permissions for setting ACL.');
        }

        $type = match(get_class($accessor)) {
            User::class => 'AclUser',
            UserGroup::class => 'AclUserGroup',
            default => null
        };

        if (null === $type) {
            throw new \LogicException('Unsupported accessor type.');
        }

        $aclClass = get_class($accessed).$type;

        if (false === class_exists($aclClass)) {
            throw new \LogicException('Unsupported accessed type.');
        }


        $acl = $qb->select('acl')
            ->from($aclClass, 'acl')
            ->where($qb->expr()->eq('acl.accessor', ':accessor'))
            ->andWhere($qb->expr()->eq('acl.accessed', ':accessed'))
            ->setParameter('accessor', $accessor)
            ->setParameter('accessed', $accessed)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (null === $acl) {
            throw new BadRequestException('These ACL could not be found.');
        }

        $this->entityManager->remove($acl);
        $this->entityManager->flush();
    }

    private function setAcl(string $accessorIri, string $accessedIri): object
    {
        $accessed = $this->iriConverter->getResourceFromIri($accessedIri);
        $accessor = $this->iriConverter->getResourceFromIri($accessorIri);

        $qb = $this->entityManager->createQueryBuilder();

        if (false === $this->security->isGranted(self::getCrudVoterAttribute($accessed), $accessed)) {
            throw new BadRequestException('Insufficient permissions for setting ACL.');
        }

        $type = match(get_class($accessor)) {
            User::class => 'AclUser',
            UserGroup::class => 'AclUserGroup',
            default => null
        };

        if (null === $type) {
            throw new \LogicException('Unsupported accessor type.');
        }

        $aclClass = get_class($accessed).$type;
        if (false === class_exists($aclClass)) {
            throw new \LogicException('Unsupported accessed type.');
        }

        $acl = $qb->select('acl')
            ->from($aclClass, 'acl')
            ->where($qb->expr()->eq('acl.accessor', ':accessor'))
            ->andWhere($qb->expr()->eq('acl.accessed', ':accessed'))
            ->setParameter('accessor', $accessor)
            ->setParameter('accessed', $accessed)
            ->getQuery()
            ->getOneOrNullResult();
        ;

        if (null === $acl) {
            $acl = new $aclClass;
            $acl->setAccessor($accessor);
            $acl->setAccessed($accessed);
            $this->entityManager->persist($acl);
            $this->entityManager->flush();
        }

        return $acl;
    }

    public static function getCrudVoterAttribute(object $accessed): string
    {
        $classNameParts = is_string($accessed::class) ? explode('\\', $accessed::class) : null;

        if (!is_array($classNameParts)) {
            throw new \RuntimeException('Unable to extract class name from '.$accessed);
        }

        $finalClassName = end($classNameParts);

        if (!is_string($finalClassName) || empty($finalClassName)) {
            throw new \RuntimeException('Invalid class name extracted from '.$accessed);
        }

        return u($finalClassName)->prepend('UPDATE_')->snake()->upper()->toString();
    }
}
