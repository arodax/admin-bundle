<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use ApiPlatform\Validator\ValidatorInterface;
use Arodax\AdminBundle\Entity\Contact\Contact;
use Arodax\AdminBundle\Entity\Contact\ContactItem;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\ShippingMethod;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCartItem;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Exception\OrderException;
use Arodax\AdminBundle\Message\Eshop\OrderCreatedMessage;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bundle\SecurityBundle\Security;
use function Symfony\Component\String\u;

final readonly class CheckoutManager
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ValidatorInterface $validator,
        private ShippingManager $shippingManager,
        private MessageBusInterface $bus,
        private Security $security
    ) {
    }

    public function getPendingOrder(int $id)
    {
        return $this->entityManager->getRepository(Order::class)->find($id);
    }

    /**
     * Create a new order from the given request.
     *
     * @param Request      $request      data which will be used for the order
     * @param ShoppingCart $shoppingCart a shopping car which will be associated with this order
     *
     * @throws OrderException
     */
    public function createOrderFromRequest(Request $request, ShoppingCart $shoppingCart): Order
    {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->entityManager->lock($shoppingCart, LockMode::PESSIMISTIC_READ);
            $this->entityManager->refresh($shoppingCart);

            if ($shoppingCart->getPriceWithTax() !== $request->request->getInt('priceWithTax') || $shoppingCart->getPriceWithoutTax() !== $request->request->getInt('priceWithoutTax')) {
                throw new OrderException('The shopping cart has been modified, please try again.');
            }

            $order = $this->entityManager->getRepository(Order::class)->findOneByShoppingCart($shoppingCart);

            if (null !== $order) {
                throw new OrderException(sprintf('There is already active order "%s" for your shopping cart.', $order->getOrderNumber()));
            }

            $shippingMethod = $this->entityManager->getRepository(ShippingMethod::class)->find($request->request->get('shippingMethod'));

            if (null === $shippingMethod) {
                throw new OrderException('The requested shipping method does not exist.');
            }

            $shippingCost = $this->shippingManager->getShippingPrice($shoppingCart, $shippingMethod);

            // disallow further modification of the shopping cart
            $shoppingCart->setStatus(ShoppingCart::STATUS_CLOSED);

            // lock prices for all items
            /** @var ShoppingCartItem $item */
            foreach ($shoppingCart->getItems() as $item) {
                $item->setPriceWithoutTax($item->getProductVariantPrice()->getPriceWithTax());
                $item->setPriceWithTax($item->getProductVariantPrice()->getPriceWithTax());
                $item->setTaxPrice($item->getProductVariantPrice()->getTaxPrice());
                $item->setTaxRate($item->getProductVariantPrice()->getTaxRate());
                $item->setCurrency($item->getProductVariantPrice()->getCurrency());
            }

            // create user if not exist
            if (null === $user = $this->security->getUser()) {
                $user = $this->entityManager->getRepository(User::class)->findOneByEmail($request->request->get('billingEmail'));
                // check for user, if already exist in the system

                if (!$user) {
                    $user = new User();
                    $user->setEmail($request->request->get('billingEmail'));
                    $user->setEnabled(true);
                    $user->setCreatedAt(new \DateTimeImmutable());
                    $user->setDisplayName(implode(' ', [$request->request->get('billingFirstName'), $request->request->get('billingLastName')]));
                    $user->setRoles(['ROLE_USER']);
                    $this->entityManager->persist($user);
                }
            }

            if ($user) {
                $shoppingCart->setUser($user);
            }

            $order = new Order();
            $order->setShoppingCart($shoppingCart);
            $order->setPriceWithoutTax($shoppingCart->getPriceWithoutTax());
            $order->setPriceWithTax($shoppingCart->getPriceWithTax());
            $order->setShippingPriceWithoutTax($shippingCost['priceWithoutTax']);
            $order->setShippingPriceWithTax($shippingCost['priceWithTax']);
            $order->setStatus(Order::STATUS_NEW);
            $order->setCurrency($shoppingCart->getCurrency());

            $propertyAccessor = PropertyAccess::createPropertyAccessor();

            $fields = ['honorific', 'firstName', 'lastName', 'email', 'phoneNumber', 'companyName', 'companyId', 'postCode', 'street', 'city', 'addressDescription', 'country'];

            foreach (['billing', 'shipping'] as $prefix) {
                if ($user instanceof User && null === $user->getId()) {
                    $contact = new Contact();
                    $contact->setType($prefix);
                    $contact->setUser($user);

                    foreach ($fields as $field) {
                        $fieldName = u($field)->camel()->title()->prepend($prefix)->toString();
                        $contactItem = new ContactItem();
                        $contactItem->setContact($contact);
                        $contactItem->setType($fieldName);
                        $contactItem->setValue($request->request->get($fieldName));
                        $this->entityManager->persist($contactItem);
                    }

                    $this->entityManager->persist($contact);
                }

                foreach ($fields as $field) {
                    $fieldName = u($field)->camel()->title()->prepend($prefix)->toString();
                    if ($propertyAccessor->isWritable($order, $fieldName)) {
                        $propertyAccessor->setValue($order, $fieldName, $request->request->get($fieldName));
                    }
                }
            }

            $order->setShippingMethod($shippingMethod);
            $this->validator->validate($order);

            $this->entityManager->persist($order);
            $this->entityManager->commit();
            $this->entityManager->flush();

            $this->bus->dispatch(new OrderCreatedMessage($order->getId()));
            $this->entityManager->refresh($order);
            $this->entityManager->flush();

            return $order;
        } catch (\Throwable $exception) {
            if ($this->entityManager->getConnection()->isTransactionActive()) {
                $this->entityManager->rollback();
            }

            throw $exception;
        }
    }
}
