<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Arodax\AdminBundle\Entity\Eshop\ProductVariantPrice;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCartItem;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Exception\ProductVariantNotAvailableException;
use Arodax\AdminBundle\Intl\CurrencyResolver;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\TransactionIsolationLevel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\SecurityBundle\Security;

final readonly class ShoppingCartManager
{
    private const string SESSION_NAME = 'arodax_admin_shopping_cart';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private RequestStack $requestStack,
        private Security $security,
        private CurrencyResolver $currencyResolver
    ) {
    }

    /**
     * Add item into shopping cart.
     *
     * @param int $id The Product Variant id
     * @param int $quantity The quantity
     * @param string|null $note The note for the shopping cart item.
     * @param ShoppingCart|null $shoppingCart Optionally pass the specific shopping cart.
     *
     * @return ShoppingCart
     * @throws Exception
     * @throws ProductVariantNotAvailableException
     * @throws \Throwable
     */
    public function addItemById(int $id, int $quantity, ?string $note = null, ?ShoppingCart $shoppingCart = null): void
    {

        $this->entityManager->getConnection()->beginTransaction();
        $this->entityManager->getConnection()->setTransactionIsolation(TransactionIsolationLevel::SERIALIZABLE);

        try {

            if (null === $shoppingCart) {
                $shoppingCart = $this->getShoppingCart();
            }

            // if shopping cart has already been closed get a new one
            if (ShoppingCart::STATUS_OPEN !== $shoppingCart->getStatus()) {
                $shoppingCart = $this->createShoppingCart();
            }

            // check for the product variant availability
            $productVariant = $this->entityManager->getRepository(ProductVariant::class)->findAvailable($id);
            if (!$productVariant instanceof ProductVariant) {
                throw new ProductVariantNotAvailableException();
            }

            // try to find instance of the current item from the shopping cart
            $shoppingCartItem = $this->entityManager->getRepository(ShoppingCartItem::class)->findOneByProductInCart($productVariant, $shoppingCart);

            // decide whether item should be created, updated or removed if quantity is below one
            if ($quantity < 1 && $shoppingCartItem instanceof ShoppingCartItem) {
                $this->entityManager->remove($shoppingCartItem);
            } elseif ($quantity > 0 && $shoppingCartItem instanceof ShoppingCartItem) {
                $shoppingCartItem->setQuantity($quantity);
            } elseif ($quantity > 0 && null === $shoppingCartItem) {
                $shoppingCartItem = new ShoppingCartItem();
                $shoppingCartItem->setShoppingCart($shoppingCart);
                $shoppingCartItem->setProductVariant($productVariant);
                $shoppingCartItem->setProductVariantPrice($this->decidePrice($productVariant));
                $shoppingCartItem->setQuantity($quantity);
                $this->entityManager->persist($shoppingCartItem);
            }

            // Update not only when it's not null, so we won't override the current note.
            if (null !== $note) {
                $shoppingCartItem->setNote($note);
            }

            $this->entityManager->flush();
            $this->entityManager->refresh($shoppingCart);

            // recalculate shopping cart after every item update
            $itemsPriceWithoutTax = null;
            $itemsPriceWithTax = null;

            /** @var ShoppingCartItem $item */
            foreach ($shoppingCart->getItems() as $item) {
                $itemPrice = $item->getProductVariantPrice();

                if ($itemPrice->getPriceWithoutTax()) {
                    $itemsPriceWithoutTax += ($itemPrice->getPriceWithoutTax() * $item->getQuantity());
                }

                if ($itemPrice->getPriceWithTax()) {
                    $itemsPriceWithTax += ($itemPrice->getPriceWithTax() * $item->getQuantity());
                }
            }

            $shoppingCart->setPriceWithoutTax($itemsPriceWithoutTax);
            $shoppingCart->setPriceWithTax($itemsPriceWithTax);

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Throwable $exception) {
            $this->entityManager->getConnection()->rollBack();
            throw $exception;
        }
    }

    /**
     * Get the shopping cart or create a new one from the current session id or the requested one.
     *
     * @param int|null $shoppingCartId optional shopping cart id
     */
    public function getShoppingCart(?int $shoppingCartId = null): ShoppingCart
    {
        $session = $this->requestStack->getSession();

        if (null === $shoppingCartId) {
            $shoppingCartId = $session->get(self::SESSION_NAME);
        }

        $sessionId = hash('crc32b', $session->getId());

        $shoppingCart = $shoppingCartId ? $shoppingCart = $this->entityManager->getRepository(ShoppingCart::class)->find($shoppingCartId) : null;

        if (null === $shoppingCart || $shoppingCart->getSessionId() !== $sessionId) {
            $shoppingCart = $this->createShoppingCart();
        }

        $this->entityManager->refresh($shoppingCart);

        return $shoppingCart;
    }

    /**
     * Return the shopping cart id by the order number.
     *
     * @param int $orderNumber The order number
     *
     * @return int|null The shopping cart id or the null if no order is found
     */
    public function getShoppingCartIdByOrderNumber(int $orderNumber): ?int
    {
        $order = $this->entityManager->getRepository(Order::class)->findOneBy(['orderNumber' => $orderNumber]);

        return $order instanceof Order ? $order->getShoppingCart()->getId() : null;
    }

    /**
     * Create a totally new shopping cart.
     */
    public function createShoppingCart(): ShoppingCart
    {
        $session = $this->requestStack->getSession();

        $shoppingCart = new ShoppingCart();

        $user = $this->security->getUser();

        if ($user instanceof User) {
            $shoppingCart->setUser($user);
        }

        $this->entityManager->persist($shoppingCart);
        $this->entityManager->flush();

        $session->set(self::SESSION_NAME, $shoppingCart->getId());

        return $shoppingCart;
    }

    /**
     * Decide which product variant price to use.
     *
     * @experimental This function is experimental only.
     *
     * @throws ProductVariantNotAvailableException
     */
    private function decidePrice(ProductVariant $productVariant): ProductVariantPrice
    {
        $price = empty($prices = $productVariant->getPrices()) ? null : $prices[0];

        if (null === $price || $price->getCurrency() !== $this->currencyResolver->resolve()) {
            throw new ProductVariantNotAvailableException(sprintf('This product is not available in your currency. The product is available in "%s" but have requested price in "%s."', $price->getCurrency(), $this->currencyResolver->resolve()));
        }

        return $price;
    }
}
