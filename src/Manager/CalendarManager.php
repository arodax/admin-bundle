<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Entity\Event\EventType;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Eluceo\iCal\Domain\Entity\Calendar as ICalCalendar;
use Eluceo\iCal\Domain\Entity\Event as ICalEvent;
use Eluceo\iCal\Domain\ValueObject\Date as ICalDate;
use Eluceo\iCal\Domain\ValueObject\SingleDay as ICalSingleDay;
use Eluceo\iCal\Domain\ValueObject\Timestamp as ICalTimestamp;
use Eluceo\iCal\Domain\ValueObject\UniqueIdentifier as ICalUniqueIdentifier;
use Eluceo\iCal\Domain\ValueObject\TimeSpan as ICalTimeSpan;
use Eluceo\iCal\Domain\ValueObject\DateTime as ICalDateTime;
use Eluceo\iCal\Domain\ValueObject\Uri as ICalUri;
use Eluceo\iCal\Domain\ValueObject\Organizer as ICalOrganizer;
use Eluceo\iCal\Domain\ValueObject\EmailAddress as ICalEmailAddress;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

readonly class CalendarManager
{
    public function __construct(
        private TranslatorInterface $translator,
        private EntityManagerInterface $entityManager,
        private RouterInterface $router,
        private RequestStack $requestStack,
        #[Autowire(env: 'resolve:APP_SECRET')]
        private string $secret
    ) {
    }

    /**
     * Get all events for the given date range.
     *
     * @param \DateTimeImmutable|null $from
     * @param \DateTimeImmutable|null $to
     * @param EventType[]            $types
     * @return array
     */
    public function getEvents(
        \DateTimeImmutable $from = null,
        \DateTimeImmutable $to = null,
        array $types = [],
    ): array {

        if (null === $from) {
            $firstDay = (new \DateTimeImmutable())->modify('first day of this month');

            if (false === $firstDay) {
                throw new \RuntimeException('Could not get the first day of this month.');
            }

            $from = $firstDay;
        }

        if (null === $to) {
            $lastDay = (new \DateTimeImmutable())->modify('last day of this month');
            if (false === $lastDay) {
                throw new \RuntimeException('Could not get the last day of this month.');
            }

            $to = $lastDay;
        }
        $qb = $this->entityManager->createQueryBuilder();

        $qb = $qb->select('event')
            ->from(Event::class, 'event')
            ->where($qb->expr()->gte('event.startAt', ':from'))
            ->andWhere($qb->expr()->lte('event.startAt', ':to'))
            ->leftJoin('event.eventTypes', 'eventTypes')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
        ;

        if ($types !== []) {
            $qb = $qb->andWhere('eventTypes IN (:types)')
                ->setParameter('types', $types)
            ;
        }

        return $qb->getQuery()
            ->getResult()
        ;
    }

    /**
     * Get events as the RFC 5545 calendar
     *
     * @return ICalCalendar
     */
    public function getICalCalendar(?string $locale = null): ICalCalendar
    {
        $qb = $this->entityManager->createQueryBuilder();

        /** @var Event[] $events */
        $events = $qb->select('event')
            ->from(Event::class, 'event')
            ->where($qb->expr()->isNotNull('event.startAt'))
            ->andWhere($qb->expr()->eq('event.status', ':status'))
            ->setParameter('status', Event::STATUS_PUBLISHED)
            ->getQuery()
            ->getResult()
        ;

        $host = $this->router->getContext()->getHost();

        $iCalEvents = array_map(function ($event) use ($host, $locale) {
            // combined description with lead and content
            $description = implode(PHP_EOL.PHP_EOL, [
                $event->translate($locale)->getLead() ?  self::richToPlainText($event->translate()->getLead()): '',
                $event->translate($locale)->getContent() ? self::richToPlainText($event->translate()->getContent()) : '',
            ]);

            // event ID must be unique
            $uniqueId = new ICalUniqueIdentifier(sprintf('%s/api/events/%s', $host, $event->getId()));

            // URI to the event editation
            $uri = new ICalUri(
                $this->router->generate('arodax_admin_events_edit',
                    ['id' => $event->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            );

            $summary = $event->translate($locale)->getHeadline() ?? $this->translator->trans('unknown_event', [], 'arodax_admin');

            $iCalEvent = (new ICalEvent($uniqueId))
                ->setSummary($summary)
                ->setDescription($description)
                ->setUrl($uri)
                ->touch(new ICalTimestamp($event->getUpdatedAt()))
            ;

            // event can be either whole day or time span
            if ($event->isAllDay()) {
                $iCalEvent->setOccurrence(new ICalSingleDay(new ICalDate($event->getStartAt())));
            } else {
                $iCalEvent->setOccurrence(new ICalTimeSpan(
                    new IcalDateTime($event->getStartAt(), true),
                    new IcalDateTime($event->getEndAt(), true))
                );
            }

            // if event has author set him as the organizer (on multiple pick first)
            $authors = $event->getAuthors();
            if (count($authors) > 0) {
                /** @var User $author */
                $author = $authors[0];
                $iCalEvent->setOrganizer(
                    new ICalOrganizer(new ICalEmailAddress($author->getEmail()), $author->getDisplayName())
                );
            }

            return $iCalEvent;

        }, $events);

        return new ICalCalendar($iCalEvents);
    }

    public function getICalCalendarUrl(): string
    {
        $locale = $this->requestStack->getCurrentRequest()?->getLocale();
        $token = hash('sha256', $this->secret);

        $params = [
            'token' => $token,
        ];

        if ($locale) {
            $params['hl'] = $locale;
        }

        $url = $this->router->generate('api_events_ics', $params, UrlGeneratorInterface::ABSOLUTE_URL);
        return preg_replace('/^\w+\:\/\//', 'webcal://', $url);

    }

    /**
     * Convert rich text (HTML) to the plain text alternative.
     *
     * @param string $string
     * @return string
     */
    private static function richToPlainText (string $string): string
    {
        return strip_tags(preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string), '<br>');
    }
}
