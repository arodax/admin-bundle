<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\ServiceDesk\Inquiry;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Security\Captcha\RecaptchaServiceProvider;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use ApiPlatform\Symfony\Validator\Exception\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InquiryManager
{
    private const array DEFAULT_INQUIRY_FORM_FIELDS = ['email', 'personalName', 'companyName', 'phone', 'subject', 'body'];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ValidatorInterface $validator,
        private readonly MailerInterface $mailer,
        private readonly FormFactoryInterface $formFactory,
        private readonly TranslatorInterface $translator,
        private readonly RecaptchaServiceProvider $recaptcha,
    ) {
    }

    /**
     * Creates a base Inquiry form which can be extended and passed as the second argument to
     * InquiryManager::createFromRequest method.
     */
    public function createForm(): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, null,  [
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);

        foreach (self::DEFAULT_INQUIRY_FORM_FIELDS as $FORM_FIELD) {
            $builder->add($FORM_FIELD, TextType::class);
        }

        return $builder->getForm();
    }

    /**
     * Create inquiry from HTTP request.
     *
     * @param FormInterface|null $form Optional form, which extends the base Inquiry form, see InquiryManager::createForm
     * @throws TransportExceptionInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function createFromRequest(Request $request, ?FormInterface $form = null, array $options = []): void
    {
        if (isset($options['recaptcha']) && true === $options['recaptcha'] && !$this->recaptcha->validateRequest($request)) {
            throw new BadRequestHttpException('Invalid reCAPTCHA challenge');
        }

        if (null === $form) {
            $form = $this->createForm();
        }

        $form->submit($request->request->all());

        // handle all "extra data" incoming from custom form
        $formData = $form->getData();
        $diff = array_diff(array_keys($formData), self::DEFAULT_INQUIRY_FORM_FIELDS);
        $data = [];

        if ($diff !== []) {
            $data = (array_map(static fn($key): array => [
                'label' => $key,
                'value' => $form->get($key)->getData(),
            ], $diff));
        }

        $inquiry = new Inquiry();
        $inquiry->setEmail($form->get('email')->getData());
        $inquiry->setPersonalName($form->get('personalName')->getData());
        $inquiry->setCompanyName($form->get('companyName')->getData());
        $inquiry->setPhone($form->get('phone')->getData());
        $inquiry->setSubject($form->get('subject')->getData());
        $inquiry->setBody($form->get('body')->getData());
        $inquiry->setData(array_values($data));

        $violations = [];

        foreach ($this->validateEntity($inquiry)->getIterator() as $value) {
            $violations[] = $value;
        }

        foreach ($this->validateForm($form) as $value) {
            $violations[] = $value;
        }

        if ([] !== $violations) {
            throw new ValidationException(new ConstraintViolationList($violations));
        }

        $this->entityManager->persist($inquiry);
        $this->entityManager->flush();

        $this->notifyAdmin($inquiry, $options);
    }

    /**
     * Validates the form.
     */
    private function validateForm(FormInterface $form): array
    {
        $violations = [];

        $errors = $form->getErrors(true, true);

        foreach ($errors as $error) {
            /** @var ConstraintViolation $cause */
            $cause = $error->getCause();

            $violations[] = new ConstraintViolation(
                $cause->getMessage(),
                $cause->getMessageTemplate(),
                $cause->getParameters(),
                $cause->getRoot(),
                $cause->getPropertyPath(),
                $cause->getInvalidValue(),
                $cause->getPlural(),
                $cause->getCode(),
                $cause->getConstraint(),
                $cause);

        }

        return $violations;
    }

    /**
     * Validates the inquiry entity.
     *
     * @param $data
     */
    private function validateEntity(mixed $data): ConstraintViolationListInterface
    {
        return $this->validator->validate($data);
    }

    /**
     * Notify admins about the new inquiry.
     *
     * @param Inquiry $inquiry The inquiry which should be sent to admins
     *
     * @throws TransportExceptionInterface
     */
    private function notifyAdmin(Inquiry $inquiry, array $options = []): void
    {
        $email = new TemplatedEmail();

        if (!isset($options['email_subject'])) {
            $options['email_subject'] = 'A new inquiry from {personalName}';
        }

        if (isset($options['custom_recipient']) && filter_var($options['custom_recipient'], FILTER_VALIDATE_EMAIL)) {
            $email->addTo($options['custom_recipient']);
        } else {
            $admins = $this->entityManager->getRepository(User::class)->findAdminRecipients();

            foreach ($admins as $admin) {
                if ($admin instanceof User) {
                    if (null === $admin->getEmail()) {
                        continue;
                    }

                    $email->addTo($admin->getEmail());
                }
            }

        }

        $email->subject($this->translator->trans($options['email_subject'], ['personalName' => $inquiry->getPersonalName()], 'arodax_admin'));
        $email->htmlTemplate('@ArodaxAdmin/emails/inquiry.html.twig');
        $email->context(['inquiry' => $inquiry]);
        $this->mailer->send($email);
    }
}
