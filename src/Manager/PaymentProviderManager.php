<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Exception\PaymentProviderException;
use Arodax\AdminBundle\PaymentProvider\BankTransferPaymentProvider;
use Arodax\AdminBundle\PaymentProvider\StripePaymentProvider;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use function Symfony\Component\String\u;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

final class PaymentProviderManager implements ServiceSubscriberInterface
{
    private EntityManagerInterface $entityManager;
    private ContainerInterface $locator;

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $locator)
    {
        $this->entityManager = $entityManager;
        $this->locator = $locator;
    }

    public static function getSubscribedServices(): array
    {
        return [
          BankTransferPaymentProvider::class,
          StripePaymentProvider::class,
        ];
    }

    /**
     * Return all available payment methods.
     *
     * @return PaymentMethod[]
     */
    public function getAvailablePaymentMethods(): array
    {
        $paymentMethods = $this->entityManager->getRepository(PaymentMethod::class)->findAll();

        $payments = [];

        foreach ($paymentMethods as $paymentMethod) {
            array_push($payments, [
                '@id' => '/api/payment-methods/'.$paymentMethod->getId(),
                'id' => $paymentMethod->getId(),
                'name' => $paymentMethod->getName(),
                'description' => $paymentMethod->getDescription(),
                'provider' => $paymentMethod->getProvider(),
            ]);
        }

        return $payments;
    }

    /**
     * Create the payment intent and return OrderPayment for the given order, by selected payment provider.
     *
     * @throws PaymentProviderException            when payment intent could not be created
     * @throws \Stripe\Exception\ApiErrorException when stripe API throws error
     *
     * @return OrderPayment the order payment, representing the payment intent
     */
    public function createPaymentIntent(Order $order, int $paymentMethodId): OrderPayment
    {
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)->find($paymentMethodId);

        if (!$paymentMethod) {
            throw new PaymentProviderException('This payment method is not supported.');
        }

        $class = u($paymentMethod->getProvider())->title()->append('PaymentProvider')->toString();
        $fqcn = 'Arodax\\AdminBundle\\PaymentProvider\\'.$class;

        if (!$this->locator->has($fqcn)) {
            throw new PaymentProviderException('The payment provider is not available for this type of payment.');
        }

        $handler = $this->locator->get($fqcn);
        $handler->createPaymentIntent($paymentMethod, $order);

        $this->entityManager->refresh($order);

        return $order->getPayment();
    }
}
