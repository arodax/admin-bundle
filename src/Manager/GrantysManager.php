<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\Grantys\GrantApplicantType;
use Arodax\AdminBundle\Message\Grantys\GeocodeGrantProject;
use Arodax\AdminBundle\Message\Translator\TranslateEntity;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Entity\Grantys\GrantProject;
use Symfony\Component\Messenger\MessageBusInterface;
use function Symfony\Component\String\u;

final readonly class GrantysManager
{
    public function __construct(
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private MessageBusInterface $messageBus,
    ) {
    }

    /**
     * @throws Exception
     */
    public function parseMediaObject(MediaObject $mediaObject): void
    {
        $this->parseFilePath($mediaObject->getAbsoluteFilePath(), 5);
    }

    /**
     * @throws Exception
     */
    public function parseFilePath(string $path, int $initialRow): void
    {
        $fs = new Filesystem();

        if (false === $fs->exists($path)) {
            throw new FileNotFoundException($path);
        }

        $spreadsheet = IOFactory::load($path);
        $sheetCount = $spreadsheet->getSheetCount();

        for ($z = 0; $z < $sheetCount; $z++) {
            $spreadsheet->setActiveSheetIndex($z);

            $this->logger->debug('Opening Grantys sheet number: {number}, name: {name}', [
                'number' => $z
            ]);

            $sheet = $spreadsheet->getActiveSheet();

            /** @var  $row */
            foreach ($sheet->getRowIterator($initialRow) as $row) {
                $y = $row->getRowIndex();

                // this function parse Excel cell by coordinates to requested format
                $cellValue = static function (string $letter, $type = 'string') use ($sheet, $y): int|string|\DateTimeImmutable|null {
                    $cellValue = $sheet->getCell($letter.$y)->getValue();
                    if (empty($cellValue)) {
                        return null;
                    }


                    return match ($type) {
                        'int' => (int)$cellValue,
                        'money' => (int)$cellValue * 100,
                        'date' => \DateTimeImmutable::createFromFormat('j. n. Y', $cellValue)->setTime(0,0,0),
                        default => (string)$cellValue
                    };
                };

                //@fixme: this function will convert grantys applicant type to associated entity add mapping to admin!
                $associatedType = function (?string $value) {
                    $value = u($value)->lower()->toString();

                    $id = match ($value) {
                        default => 5,
                        'nadace' => 1,
                        'nadační fond' => 1,
                        'obecně prospěšná společnos' => 1,
                        'příspěvková organizace' => 1,
                        'organizační složka zahraničního nadačního fondu' => 1,
                        'organizační složka zahraniční nadace' => 1,
                        'obec nebo městská část hlavního města prahy' => 2,
                        'svazek obcí' => 2,
                        'spolek' => 4,
                        'pobočný spolek' => 4,
                        'sdružení (svaz, spolek, společnost, klub aj.)' => 4,
                        'zahraniční pobočný spolek' => 4,
                        'ústav' => 4,
                        'zahraniční spolek' => 4,
                    };

                    return $this->entityManager->getRepository(GrantApplicantType::class)->find($id);
                };

                // skip this row if it's already in the system
                $grantProject = $this->entityManager->getRepository(GrantProject::class)->findOneBy(['number' => $cellValue('A')]);
                if (null !== $grantProject) {
                    continue;
                }

                $grantProject = new GrantProject();
                $grantProject->setStatus('published');
                $grantProject->setNumber($cellValue('A'));
                $grantProject->translate('cs')->setName($cellValue('B'));
                $grantProject->setProjectStatus($cellValue('C'));
                $grantProject->setVersion($cellValue('D'));
                $grantProject->setNote($cellValue('E'));
                $grantProject->setEmail($cellValue('F'));
                $grantProject->setStartedAt($cellValue('G', 'date'));
                $grantProject->setEndedAt($cellValue('H', 'date'));
                $grantProject->setSignedAt($cellValue('I', 'date'));
                $grantProject->setClosedAt($cellValue('J', 'date'));

                // grant applicant information
                $grantProject->setApplicant($cellValue('K'));
                $grantProject->setApplicantType($associatedType($cellValue('M')));
                $grantProject->setDataBox($cellValue('L'));
                $grantProject->setLegalForm($cellValue('M'));
                $grantProject->setContractNumber($cellValue('N'));
                $grantProject->setStatutory($cellValue('O'));
                $grantProject->setBusinessNumber($cellValue('P'));
                $grantProject->setAddress($cellValue('Q'));
                $grantProject->setCounty($cellValue('R'));
                $grantProject->setSubjectNote($cellValue('S'));
                $grantProject->setContactPersonName($cellValue('T'));
                $grantProject->setContactPersonEmail($cellValue('U'));

                // grant project location
                $grantProject->setContactPersonPhone($cellValue('V'));
                $grantProject->setPlaceOfRealization($cellValue('W'));
                $grantProject->setCountyOfRealization($cellValue('X'));

                // project categories and descriptions
                $grantProject->translate('cs')->setScopeOfApplication($cellValue('Y'));
                $grantProject->translate('cs')->setAnnotation($cellValue('Z'));
                $grantProject->translate('cs')->setFocus($cellValue('AA'));
                $grantProject->setCallNumber($cellValue('AB'));
                $grantProject->translate('cs')->setTechnicalArea($cellValue('AC'));

                // amounts and payment info
                $grantProject->setAccountNumber($cellValue('AD'));
                $grantProject->setIban($cellValue('AE'));
                $grantProject->setSwift($cellValue('AF'));
                $grantProject->setVariableNumber($cellValue('AG'));
                $grantProject->setAmountRequested($cellValue('AH', 'money'));
                $grantProject->setTotalAmount($cellValue('AI', 'money'));
                $grantProject->setApprovedAmount($cellValue('AJ', 'money'));
                $grantProject->setCurrency($cellValue('AK'));

                // installment 1
                $grantProject->setInstallment1At($cellValue('AL', 'date'));
                $grantProject->setInstallment1Amount($cellValue('AM', 'money'));
                $grantProject->setInstallment1Fund($cellValue('AN'));
                $grantProject->setInstallment1FundCode($cellValue('AO'));
                $grantProject->setPayment1At($cellValue('AP', 'date'));
                $grantProject->setPayment1Amount($cellValue('AQ', 'money'));

                // installment 2
                $grantProject->setInstallment2At($cellValue('AR', 'date'));
                $grantProject->setInstallment2Amount($cellValue('AS', 'money'));
                $grantProject->setInstallment2Fund($cellValue('AT'));
                $grantProject->setInstallment2FundCode($cellValue('AU'));
                $grantProject->setPayment2At($cellValue('AV', 'date'));
                $grantProject->setPayment2Amount($cellValue('AW', 'money'));

                // installment 3
                $grantProject->setInstallment3At($cellValue('AX', 'date'));
                $grantProject->setInstallment3Amount($cellValue('AY', 'money'));
                $grantProject->setInstallment3Fund($cellValue('AZ'));
                $grantProject->setInstallment3FundCode($cellValue('BA'));
                $grantProject->setPayment3At($cellValue('BB', 'date'));
                $grantProject->setPayment3Amount($cellValue('BC', 'money'));

                // installment 4
                $grantProject->setInstallment4At($cellValue('BD', 'date'));
                $grantProject->setInstallment4Amount($cellValue('BE', 'money'));
                $grantProject->setInstallment4Fund($cellValue('BF'));
                $grantProject->setInstallment4FundCode($cellValue('BG'));
                $grantProject->setPayment4At($cellValue('BG', 'date'));
                $grantProject->setPayment4Amount($cellValue('BI', 'money'));

                // installment 5
                $grantProject->setInstallment5At($cellValue('BJ', 'date'));
                $grantProject->setInstallment5Amount($cellValue('BK', 'money'));
                $grantProject->setInstallment5Fund($cellValue('BL'));
                $grantProject->setInstallment5FundCode($cellValue('BL'));
                $grantProject->setPayment5At($cellValue('BN', 'date'));
                $grantProject->setPayment5Amount($cellValue('BO', 'money'));

                // message 1
                $grantProject->setMessage1Type($cellValue('BP'));
                $grantProject->setMessage1DueAt($cellValue('BQ', 'date'));
                $grantProject->setMessage1SubmittedAt($cellValue('BR', 'date'));
                $grantProject->setMessage1ApprovedAt($cellValue('BS', 'date'));

                // message 2
                $grantProject->setMessage2Type($cellValue('BT'));
                $grantProject->setMessage2DueAt($cellValue('BU', 'date'));
                $grantProject->setMessage2SubmittedAt($cellValue('BV', 'date'));
                $grantProject->setMessage2ApprovedAt($cellValue('BW', 'date'));

                // message 3
                $grantProject->setMessage3Type($cellValue('BX'));
                $grantProject->setMessage3DueAt($cellValue('BY', 'date'));
                $grantProject->setMessage3SubmittedAt($cellValue('BZ', 'date'));
                $grantProject->setMessage3ApprovedAt($cellValue('CA', 'date'));

                // message 4
                $grantProject->setMessage4Type($cellValue('CB'));
                $grantProject->setMessage4DueAt($cellValue('CC', 'date'));
                $grantProject->setMessage4SubmittedAt($cellValue('CD', 'date'));
                $grantProject->setMessage4ApprovedAt($cellValue('CE', 'date'));

                // message 5
                $grantProject->setMessage5Type($cellValue('CF'));
                $grantProject->setMessage5DueAt($cellValue('CG', 'date'));
                $grantProject->setMessage5SubmittedAt($cellValue('CH', 'date'));
                $grantProject->setMessage5ApprovedAt($cellValue('CI', 'date'));

                $grantProject->mergeNewTranslations();
                $this->entityManager->persist($grantProject);
                $this->entityManager->flush();

                $this->messageBus->dispatch(new GeocodeGrantProject($grantProject->getId()));
                $this->messageBus->dispatch(new TranslateEntity(GrantProject::class, $grantProject->getId(), 'cs'));
            }
        }
    }
}
