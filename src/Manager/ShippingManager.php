<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Manager;

use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Entity\Eshop\ShippingMethod;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\Countries;

final readonly class ShippingManager
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ShoppingCartManager $shoppingCartManager,
        private RequestStack $requestStack,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_PACKETA_API_KEY')]
        private ?string $packetaApiKey = null,
    ) {
    }

    public function getAvailableShippingMethods(): array
    {
        $shoppingCart = $this->shoppingCartManager->getShoppingCart();
        $shippingMethods = $this->entityManager->getRepository(ShippingMethod::class)->findAll();

        $postage = [];

        /** @var ShippingMethod $shippingMethod */
        foreach ($shippingMethods as $shippingMethod) {
            if ($shippingMethod->getPostagePriceRules()) {
                foreach ($shippingMethod->getPostagePriceRules() as $postagePriceRule) {
                    $priceWitTax = null;
                    $priceWithoutTax = null;
                    $currency = null;

                    if (isset($postagePriceRule['expression']) && null != $postagePriceRule['expression']) {
                        $expressionLanguage = new ExpressionLanguage();
                        $result = $expressionLanguage->evaluate($postagePriceRule['expression'], [
                            'shoppingCart' => $shoppingCart,
                        ]);

                        if ($result) {
                            $priceWithoutTax = $postagePriceRule['priceWithoutTax'] ?? null;
                            $priceWitTax = $postagePriceRule['priceWithTax'] ?? null;
                            $currency = $postagePriceRule['currency'] ?? null;
                        } else {
                            continue;
                        }
                    } else {
                        $priceWithoutTax = $postagePriceRule['priceWithoutTax'] ?? null;
                        $priceWitTax = $postagePriceRule['priceWithTax'] ?? null;
                        $currency = $postagePriceRule['currency'] ?? null;
                    }

                    $paymentMethods = [];

                    /** @var PaymentMethod $paymentMethod */
                    foreach ($shippingMethod->getPaymentMethods() as $paymentMethod) {

                        $paymentMethodData = [
                            'id' => $paymentMethod->getId(),
                            'headline' => $paymentMethod->translate()?->getName(),
                            'provider' => $paymentMethod->getProvider(),
                            'priceWithoutTax' => $paymentMethod->getPriceWithoutTax(),
                            'priceWithTax' => $paymentMethod->getPriceWithTax(),
                        ];

                        $paymentMethods[] = $paymentMethodData;
                    }

                    $postageData = [
                        '@id' => '/api/shipping-methods/' . $shippingMethod->getId(),
                        'id' => $shippingMethod->getId(),
                        'headline' => $shippingMethod->translate()->getTitle(),
                        'description' => $shippingMethod->translate()->getDescription(),
                        'priceWithoutTax' => $priceWithoutTax,
                        'priceWithTax' => $priceWitTax,
                        'currency' => $currency,
                        'countries' => Countries::getNames($this->requestStack->getCurrentRequest()?->getLocale()),
                        'provider' => $shippingMethod->getProvider(),
                        'paymentMethods' => $paymentMethods,
                        'apiKey' => null,
                    ];

                    if ($shippingMethod->getProvider() === 'packeta') {
                        $postageData['apiKey'] = $this->packetaApiKey;
                    }

                    $postage[] = $postageData;
                }
            }
        }

        return $postage;
    }

    /**
     * Return calculated postage for the given shopping cart and method.
     */
    public function getShippingPrice(ShoppingCart $shoppingCart, ShippingMethod $shippingMethod): array
    {
        foreach ($shippingMethod->getPostagePriceRules() as $postagePriceRule) {

            if (isset($postagePriceRule['expression'])) {
                $expressionLanguage = new ExpressionLanguage();
                $result = $expressionLanguage->evaluate($postagePriceRule['expression'], [
                    'shoppingCart' => $shoppingCart,
                ]);

                if ($result) {
                    return [
                        'priceWithoutTax' => $postagePriceRule['priceWithoutTax'],
                        'priceWithTax' => $postagePriceRule['priceWithTax'],
                    ];
                } else {
                    continue;
                }
            } else {
                return [
                    'priceWithoutTax' => $postagePriceRule['priceWithoutTax'],
                    'priceWithTax' => $postagePriceRule['priceWithTax'],
                ];
            }
        }

        throw new \Exception('This code should not be reached');
    }
}
