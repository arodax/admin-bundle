<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Response;

use Arodax\AdminBundle\EventListener\ViewListener;

/**
 * This class represents a Vue view.
 */
final readonly class Vue
{
    /**
     * Constructs a Vue view which can be rendered by the VueListener.
     *
     * @see ViewListener
     *
     * @param string $rootComponent    the name of the Vue root component for this view
     * @param string $encoreEntryPoint the webpack entrypoint file, to be rendered in the view
     */
    public function __construct(
        private string $rootComponent,
        private string $encoreEntryPoint,
        private string $title,
        private string $baseTwigTemplate = '@ArodaxAdmin/dashboard.html.twig'
    ) {
    }

    /**
     * Get root component tag in the kebab case.
     *
     * Note that this component must be registered in the entrypoint file.
     */
    public function getRootComponentTag(): string
    {
        $tag = \strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $this->rootComponent));

        return \sprintf('<%s></%s>', $tag, $tag);
    }

    /**
     * Get the webpack entrypoint name for the view.
     */
    public function getEntryPoint(): string
    {
        return $this->encoreEntryPoint;
    }

    /**
     * Gets the title for this view.
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * The base twig template into which the Vue view will be rendered.
     *
     * @return string
     */
    public function getBaseTwigTemplate(): string
    {
        return $this->baseTwigTemplate;
    }
}
