<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Response;

final class View
{
    public function __construct(
        private mixed $data = [],
        private array $serializationContext = [],
        private ?string $template = null,
        private array $templateContext = [],
        private int $statusCode = 200
    ) {
    }

    public function setData(mixed $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setSerializationContext(array $context): self
    {
        $this->serializationContext = $context;
        return $this;
    }

    public function getSerializationContext(): array
    {
        return $this->serializationContext;
    }

    public function setStatusCode(int $status): self
    {
        $this->statusCode = $status;
        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;
        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplateContext(array $context): self
    {
        $this->templateContext = $context;
        return $this;
    }

    public function getTemplateContext(): array
    {
        return $this->templateContext;
    }
}
