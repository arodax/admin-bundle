<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\User;

use Arodax\AdminBundle\Doctrine\Filter\UserAwareFilter;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This event listener enable UserAwareFilter depends on the security voter.
 *
 * @see UserAwareFilter for the actual Doctrine filter implementation.
 */
final readonly class UserAwareFilterEventListener implements EventSubscriberInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security
    ) {
    }

    public function onKernelRequest(): void
    {
        $user = $this->security->getUser();

        if ($user instanceof User && !$this->security->isGranted('ROLE_ADMIN')) {
            /** @var UserAwareFilter $filter */
            $filter = $this->entityManager->getFilters()->enable('user_aware_filter');
            $filter->setParameter('id', $user->getId());
        }
    }
    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => ['', 5]];
    }
}
