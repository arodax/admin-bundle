<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\User;

use Arodax\AdminBundle\Entity\User\User;
use Random\RandomException;

/**
 * @see User for the Doctrine entity related to this listener.
 */
final class UserAvatarListener
{
    /**
     * Handles further updates.
     *
     * @param User $user
     * @throws RandomException
     */
    public function prePersist(User $user): void
    {
        $user->setColor(sprintf('#%06X', random_int(0, 0xFFFFFF)));
    }
}
