<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\User;

use Arodax\AdminBundle\Entity\Score\TotalScoreLog;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @see User for the Doctrine entity related to this listener.
 */
final readonly class UserTotalScoreListener
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * Handles initial insert.
     * @throws NonUniqueResultException when more than 1 latest score log has been found
     */
    public function postPersist(User $user): void
    {

        $this->writeTotalScoreToLog($user);
    }

    /**
     * Handles further updates.
     * @throws NonUniqueResultException when more than 1 latest score log has been found
     */
    public function postUpdate(User $user): void
    {
        $this->writeTotalScoreToLog($user);
    }

    /**
     * Create a log entry of total score if it has been changed.
     *
     * @throws NonUniqueResultException when more than 1 latest score log has been found
     */
    private function writeTotalScoreToLog(User $user): void
    {
        if ($user->getTotalScore() === null) {
            return;
        }

        $qb = $this->entityManager->createQueryBuilder();

        /** @var TotalScoreLog|null $latestScoreLog */
        $latestScoreLog = $qb->select('total_score_log')
            ->from(TotalScoreLog::class, 'total_score_log')
            ->join('total_score_log.user', 'user')
            ->where($qb->expr()->eq('user.id', ':userId'))
            ->setParameter('userId', $user->getId())
            ->orderBy('total_score_log.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        // skip if the latest score log has the same total score
        if ($latestScoreLog !== null && $latestScoreLog->getValue() === $user->getTotalScore()) {
            return;
        }

        $totalScoreLog = new TotalScoreLog();
        $totalScoreLog->setUser($user);
        $totalScoreLog->setValue($user->getTotalScore());
        $totalScoreLog->setCreatedAt(new \DateTimeImmutable());
        $this->entityManager->persist($totalScoreLog);
        $this->entityManager->flush();
    }
}
