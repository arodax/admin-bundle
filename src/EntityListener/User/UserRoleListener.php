<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\User;

use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * @see User for the Doctrine entity related to this listener.
 */
final readonly class UserRoleListener
{
    public function __construct(
        private RoleHierarchyInterface $roleHierarchy
    ) {
    }

    /**
     * Handles initial insert.
     */
    public function postLoad(User $user): void
    {
        $user->setExtendedRoles($this->roleHierarchy->getReachableRoleNames($user->getRoles()));
    }
}
