<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\User;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Message\Password\PasswordChangedMessage;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @see User for the Doctrine entity related to this listener.
 */
final class UserPasswordListener
{
    private bool $isHandlingPasswordChange;

    public function __construct(
        private readonly UserPasswordHasherInterface $userPasswordHasher,
        private readonly MessageBusInterface $messageBus,
        private readonly Security $security,
    ) {
        $this->isHandlingPasswordChange = false;
    }

    /**
     * Handles initial insert.
     */
    public function prePersist(User $user): void
    {
        $this->encodePassword($user);
    }

    /**
     * Handles further updates.
     */
    public function preUpdate(User $user, PreUpdateEventArgs $args): void
    {
        // Check if we are already handling a password change to prevent infinite loop
        if ($this->isHandlingPasswordChange) {
            return;
        }

        $this->isHandlingPasswordChange = true;

        $this->encodePassword($user);

        $em = $args->getObjectManager();
        $meta = $em->getClassMetadata($user::class);

        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $user);

        // Reset the flag
        $this->isHandlingPasswordChange = false;
    }

    /**
     * Encode user plaintext password with the password encoder.
     */
    private function encodePassword(User $user): void
    {
        if (null === $user->getPlainPassword()) {
            return;
        }

        if (null === $user->getId()) {
            // This is a new user, we don't have to send any message
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $user->getPlainPassword()));

            return;
        }

        $user->setPassword($this->userPasswordHasher->hashPassword($user, $user->getPlainPassword()));

        $this->messageBus->dispatch(
            new PasswordChangedMessage(
                userId: $user->getId(),
                updatedByUserId: $this->security->getUser()?->getId() ?? null,
                currentPasswordHash: \hash(algo: 'sha256', data: $user->getPlainPassword())
            )
        );
    }
}
