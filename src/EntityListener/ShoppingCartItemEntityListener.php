<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\ShoppingCartItem;

final class ShoppingCartItemEntityListener
{
    public function prePersist(ShoppingCartItem $shoppingCartItem): void
    {
        $this->checkCurrencyCompatibility($shoppingCartItem);
    }

    public function preUpdate(ShoppingCartItem $shoppingCartItem): void
    {
        $this->checkCurrencyCompatibility($shoppingCartItem);
    }

    private function checkCurrencyCompatibility(ShoppingCartItem $shoppingCartItem)
    {
        if ($shoppingCartItem->getShoppingCart()->getCurrency() !== $shoppingCartItem->getProductVariantPrice()->getCurrency()) {
            throw new \LogicException('You cannot mix two different currencies in the shopping cart');
        }
    }
}
