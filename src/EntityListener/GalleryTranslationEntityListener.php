<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Gallery\GalleryTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class GalleryTranslationEntityListener
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function prePersist(GalleryTranslation $galleryTranslation): void
    {
        $this->generateSlug($galleryTranslation);
    }

    public function preUpdate(GalleryTranslation $galleryTranslation): void
    {
        if (null === $galleryTranslation->getSlug()) {
            $this->generateSlug($galleryTranslation);
        }
    }

    private function generateSlug(GalleryTranslation $galleryTranslation): void
    {
        $slug = null;

        if ($galleryTranslation->getTitle()) {
            $slug = $galleryTranslation->getTitle();
        } elseif ($galleryTranslation->getName()) {
            $slug = $galleryTranslation->getName();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $galleryTranslation->setSlug($slug);
    }
}
