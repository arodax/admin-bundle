<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\ProductTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class ProductTranslationEntityListener
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function prePersist(ProductTranslation $productTranslation): void
    {
        $this->generateSlug($productTranslation);
    }

    public function preUpdate(ProductTranslation $productTranslation): void
    {
        if (null === $productTranslation->getSlug()) {
            $this->generateSlug($productTranslation);
        }
    }

    private function generateSlug(ProductTranslation $productTranslation): void
    {
        $slug = null;

        if ($productTranslation->getTitle()) {
            $slug = $productTranslation->getTitle();
        } elseif ($productTranslation->getName()) {
            $slug = $productTranslation->getName();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $productTranslation->setSlug($slug);
    }
}
