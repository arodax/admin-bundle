<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\ProductCategoryTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class ProductCategoryTranslationEntityListener
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function prePersist(ProductCategoryTranslation $productCategoryTranslation): void
    {
        $this->generateSlug($productCategoryTranslation);
    }

    public function preUpdate(ProductCategoryTranslation $productCategoryTranslation): void
    {
        if (null === $productCategoryTranslation->getSlug()) {
            $this->generateSlug($productCategoryTranslation);
        }
    }

    private function generateSlug(ProductCategoryTranslation $productCategoryTranslation): void
    {
        $slug = null;

        if ($productCategoryTranslation->getTitle()) {
            $slug = $productCategoryTranslation->getTitle();
        } elseif ($productCategoryTranslation->getName()) {
            $slug = $productCategoryTranslation->getName();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $productCategoryTranslation->setSlug($slug);
    }
}
