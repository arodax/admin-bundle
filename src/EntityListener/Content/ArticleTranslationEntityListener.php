<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Content;

use Arodax\AdminBundle\Entity\Content\ArticleTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class ArticleTranslationEntityListener
{
    public function __construct(
        private SluggerInterface $slugger
    ) {
    }

    public function prePersist(ArticleTranslation $articleTranslation): void
    {
        $this->generateSlug($articleTranslation);
    }

    public function preUpdate(ArticleTranslation $articleTranslation): void
    {
        if (null === $articleTranslation->getSlug()) {
            $this->generateSlug($articleTranslation);
        }
    }

    private function generateSlug(ArticleTranslation $articleTranslation): void
    {
        $slug = null;

        if ($articleTranslation->getTitle()) {
            $slug = $articleTranslation->getTitle();
        } elseif ($articleTranslation->getHeadline()) {
            $slug = $articleTranslation->getHeadline();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $articleTranslation->setSlug($slug);
    }
}
