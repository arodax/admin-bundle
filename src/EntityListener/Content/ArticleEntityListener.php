<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Content;

use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\User\User;
use Symfony\Bundle\SecurityBundle\Security;

final readonly class ArticleEntityListener
{
    public function __construct(
        private Security $security,
    ) {
    }

    public function prePersist(Article $article): void
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if (null !== $user) {
            $article->addAuthor($user);
        }

        $article->setCreatedAt(new \DateTimeImmutable());
    }

    public function preUpdate(): void
    {

    }

}
