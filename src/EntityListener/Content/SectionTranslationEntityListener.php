<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Content;

use Arodax\AdminBundle\Entity\Content\SectionTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class SectionTranslationEntityListener
{
    public function __construct(
        private SluggerInterface $slugger
    ) {
    }

    public function prePersist(SectionTranslation $sectionTranslation): void
    {
        $this->generateSlug($sectionTranslation);
    }

    public function preUpdate(SectionTranslation $sectionTranslation): void
    {
        if (true === empty($sectionTranslation->getSlug())) {
            $this->generateSlug($sectionTranslation);
        }
    }

    private function generateSlug(SectionTranslation $sectionTranslation): void
    {
        $slug = null;

        if ($sectionTranslation->getName()) {
            $slug = $sectionTranslation->getName();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $sectionTranslation->setSlug($slug);
    }
}
