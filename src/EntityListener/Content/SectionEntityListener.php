<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Content;

use Arodax\AdminBundle\Entity\Content\Section;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class SectionEntityListener
{

    public function __construct(
        private SluggerInterface $slugger,
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * This will persist section in the nested tree on the first flush, into the correct hierarchy position.
     *
     * @param Section $section
     * @param PostPersistEventArgs $eventArgs
     * @return void
     */
    public function postPersist(Section $section, PostPersistEventArgs $eventArgs): void
    {
        $repository = $this->entityManager->getRepository(Section::class);

        if (null !== $section->getParent()) {
            $parentSection = $section->getParent();
            $repository->persistAsLastChildOf($section, $parentSection);
        }

        $repository->recover();
    }


    public function preUpdate(Section $section): void
    {
        $repository = $this->entityManager->getRepository(Section::class);
        $sectionTree = $repository->getPath($section);

        $slugs = [];
        // traverse up
        /* @var Section $section */
        foreach ($sectionTree as $leaf) {
            foreach ($leaf->getTranslations() as $translation) {
                $slug = $this->slugger->slug($translation->getSlug() ?? $translation->getName() ?? 'section-'.$leaf->getId())->lower()->toString();
                $slugs[$translation->getLocale()][] = $slug;
                $leaf->translate($translation->getLocale())->setPath(implode('/', $slugs[$translation->getLocale()]));
            }
        }

        // traverse down
        foreach ($repository->children($section) as $leaf) {
            foreach ($leaf->getTranslations() as $translation) {
                $slug = $this->slugger->slug($translation->getSlug() ?? $translation->getName() ?? 'section-'.$leaf->getId())->lower()->toString();
                $slugs[$translation->getLocale()][] = $slug;
                $leaf->translate($translation->getLocale())->setPath(implode('/', $slugs[$translation->getLocale()]));
            }
        }
    }

    /**
     * After section has been removed recalculate the tree.
     *
     * @param Section $section
     * @return void
     */
    public function postRemove(Section $section)
    {
        $repository = $this->entityManager->getRepository(Section::class);
        $repository->recover();
    }
}
