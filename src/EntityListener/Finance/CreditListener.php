<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Finance;

use Arodax\AdminBundle\Entity\Finance\Credit;
use Arodax\AdminBundle\Entity\Finance\CreditLog;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @see Credit for the Doctrine entity related to this listener.
 */
final readonly class CreditListener
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * Handles initial insert.
     * @throws NonUniqueResultException when more than 1 latest credit log has been found
     */
    public function postPersist(Credit $credit): void
    {

        $this->writeCreditToLog($credit);
    }

    /**
     * Handles further updates.
     * @throws NonUniqueResultException when more than 1 latest credit log has been found
     */
    public function postUpdate(Credit $credit): void
    {
        $this->writeCreditToLog($credit);
    }

    /**
     * Create a log entry of credit if it has been changed.
     *
     * @throws NonUniqueResultException when more than 1 latest credit log has been found
     */
    private function writeCreditToLog(Credit $credit): void
    {
        if ($credit->getValue() === null) {
            return;
        }

        $qb = $this->entityManager->createQueryBuilder();

        /** @var CreditLog|null $latestCreditLog */
        $latestCreditLog = $qb->select('credit_log')
            ->from(CreditLog::class, 'credit_log')
            ->join('credit_log.user', 'user')
            ->where($qb->expr()->eq('user.id', ':user'))
            ->setParameter('user', $credit->getUser())
            ->andWhere($qb->expr()->eq('credit_log.type', ':type'))
            ->setParameter('type', $credit->getCreditType())
            ->orderBy('credit_log.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        // skip if the latest score log has the same total score
        if ($latestCreditLog !== null && $latestCreditLog->getValue() === $credit->getValue()) {
            return;
        }

        $creditLog = new CreditLog();
        $creditLog->setUser($credit->getUser());
        $creditLog->setValue($credit->getValue());
        $creditLog->setType($credit->getCreditType());
        $creditLog->setCreatedAt(new \DateTimeImmutable());
        $this->entityManager->persist($creditLog);
        $this->entityManager->flush();
    }
}
