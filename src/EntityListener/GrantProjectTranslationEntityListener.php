<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class GrantProjectTranslationEntityListener
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function prePersist(GrantProjectTranslation $grantProjectTranslation): void
    {
        $this->generateSlug($grantProjectTranslation);
    }

    public function preUpdate(GrantProjectTranslation $grantProjectTranslation): void
    {
        if (null === $grantProjectTranslation->getSlug()) {
            $this->generateSlug($grantProjectTranslation);
        }
    }

    private function generateSlug(GrantProjectTranslation $grantProjectTranslation): void
    {
        $slug = $grantProjectTranslation->getName();

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $grantProjectTranslation->setSlug($slug);
    }
}
