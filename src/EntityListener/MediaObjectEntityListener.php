<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Intervention\Image\ImageManager;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Mime\MimeTypes;
use Vich\UploaderBundle\Storage\StorageInterface;
use Intervention\Image\Drivers\Gd\Driver as GdDriver;

/**
 * Automatically handles various media handling for newly uploaded media objects.
 */
final readonly class MediaObjectEntityListener
{
    public function __construct(
        private StorageInterface $storage,
        #[Autowire(param: 'arodax_admin.public_dir')]
        private string $publicDir
    ) {
    }

    /**
     * Handles initial entity insert.
     */
    public function postPersist(MediaObject $mediaObject): void
    {
        $mediaObject->setContentUrl($this->resolvePath($mediaObject));
        $mime = $this->guessMime($mediaObject);

        $mediaObject->setMime($mime);
        if (preg_match('/^image\/(jpg|jpeg|gif|png|avif|webp)/', $mime)) {
            $mediaObject->setMetadata($this->getImageMetaData($mediaObject));
        } else {
            $mediaObject->setMetadata($this->getFileMetaData($mediaObject));
        }
    }

    /**
     * Resolve content url whenever media object is loaded.
     */
    public function postLoad(MediaObject $mediaObject): void
    {
        $mediaObject->setContentUrl($this->resolvePath($mediaObject));
        $mediaObject->setAbsoluteFilePath($this->publicDir.'/media/'.$mediaObject->getFilePath());
    }

    /**
     * Guess mime from the path.
     */
    private function guessMime(MediaObject $mediaObject): ?string
    {
        if (!$mediaObject->getContentUrl()) {
            $mediaObject->setContentUrl($this->resolvePath($mediaObject));
        }

        $mimeGuesser = new MimeTypes();

        $path = rtrim($this->publicDir, '/').$mediaObject->getContentUrl();

        return $mimeGuesser->guessMimeType($path);
    }

    /**
     * Resolve stored path of the media object.
     */
    private function resolvePath(MediaObject $mediaObject): ?string
    {
        return $this->storage->resolveUri($mediaObject, 'file');
    }

    /**
     * Get image metadata.
     * @return array{width: mixed, height: mixed, orientation: string, filesize: int|false}
     */
    private function getImageMetaData(MediaObject $mediaObject): array
    {
        if (!$mediaObject->getContentUrl()) {
            $mediaObject->setContentUrl($this->resolvePath($mediaObject));
        }

        $manager = new ImageManager(driver: new GdDriver());
        $image = $manager->read(input: $this->publicDir.'/media/'.$mediaObject->getFilePath());

        return [
            'width' => $image->width(),
            'height' => $image->height(),
            'orientation' => $image->width() > $image->height() ? 'landscape' : 'portrait',
            'filesize' => filesize($this->publicDir.'/media/'.$mediaObject->getFilePath()),
        ];
    }

    /**
     * Get file metadata.
     * @return array{filesize: int|false}
     */
    private function getFileMetaData(MediaObject $mediaObject): array
    {
        if (!$mediaObject->getContentUrl()) {
            $mediaObject->setContentUrl($this->resolvePath($mediaObject));
        }

        return [
            'filesize' => filesize($this->publicDir.'/media/'.$mediaObject->getFilePath()),
        ];
    }
}
