<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Intl\CurrencyResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @see ShoppingCart
 */
final readonly class ShoppingCartEntityListener
{
    public function __construct(
        private CurrencyResolver $currencyResolver,
        private RequestStack $requestStack
    ) {
    }

    /**
     * Hadndles initial persist of the shopping cart.
     */
    public function prePersist(ShoppingCart $shoppingCart): void
    {
        $shoppingCart->setCurrency($this->currencyResolver->resolve());

        $session = $this->requestStack->getSession();

        if ($session->isStarted()) {
            $shoppingCart->setSessionId($this->hashSession($session));
        }
    }

    private function hashSession(SessionInterface $session): string
    {
        return hash('crc32b', $session->getId());
    }
}
