<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Doctrine\ORM\DoctrinePropertySequenceGenerator;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

final readonly class OrderEntityListener
{
    public function __construct(private DoctrinePropertySequenceGenerator $sequenceGenerator)
    {
    }

    /**
     * @throws NonUniqueResultException
     */
    public function prePersist(Order $order): void
    {
        $number = $this->sequenceGenerator->generate(entityClass: $order::class, propertyPath: 'orderNumber', prefix: date('Y'), length: 10, resetOnPrefixChange: true);
        $order->setOrderNumber($number);
    }
}
