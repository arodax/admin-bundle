<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Event\EventTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class EventTranslationEntityListener
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function prePersist(EventTranslation $eventTranslation): void
    {
        $this->generateSlug($eventTranslation);
    }

    public function preUpdate(EventTranslation $eventTranslation): void
    {
        if (null === $eventTranslation->getSlug()) {
            $this->generateSlug($eventTranslation);
        }
    }

    private function generateSlug(EventTranslation $eventTranslation): void
    {
        $slug = null;

        if ($eventTranslation->getHeadline()) {
            $slug = $eventTranslation->getHeadline();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $eventTranslation->setSlug($slug);
    }
}
