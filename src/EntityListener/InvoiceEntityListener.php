<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Doctrine\ORM\DoctrinePropertySequenceGenerator;
use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Arodax\AdminBundle\Message\PrintInvoiceMessage;
use Arodax\AdminBundle\Model\Finance\InvoiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @see InvoiceCzech
 */
final readonly class InvoiceEntityListener
{
    public function __construct(private MessageBusInterface $bus, private DoctrinePropertySequenceGenerator $sequenceGenerator)
    {
    }

    /**
     * @throws NonUniqueResultException
     */
    public function prePersist(InvoiceInterface $invoice): void
    {
        $number = $this->sequenceGenerator->generate($invoice::class, 'invoiceNumber', date('Y'), resetOnPrefixChange: true);

        $invoice->setInvoiceNumber($number);
        $invoice->setVariableNumber($number);
    }

    public function postPersist(InvoiceInterface $invoice): void
    {
        $this->bus->dispatch(new PrintInvoiceMessage($invoice->getId()));
    }

    public function postUpdate(InvoiceInterface $invoice): void
    {
        $this->bus->dispatch(new PrintInvoiceMessage($invoice->getId()));
    }
}
