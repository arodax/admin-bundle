<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectGroupTranslation;
use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class GrantProjectGroupTranslationEntityListener
{
    public function __construct(
        private SluggerInterface $slugger
    ) {
    }

    public function prePersist(GrantProjectGroupTranslation $grantProjectGroupTranslation): void
    {
        $this->generateSlug($grantProjectGroupTranslation);
    }

    public function preUpdate(GrantProjectGroupTranslation $grantProjectGroupTranslation): void
    {
        if (null === $grantProjectGroupTranslation->getSlug()) {
            $this->generateSlug($grantProjectGroupTranslation);
        }
    }

    private function generateSlug(GrantProjectGroupTranslation $grantProjectGroupTranslation): void
    {
        $slug = null;

        if ($grantProjectGroupTranslation->getName()) {
            $slug = $grantProjectGroupTranslation->getName();
        }

        if ($slug) {
            $slug = $this->slugger->slug($slug)->lower()->toString();
        }

        $grantProjectGroupTranslation->setSlug($slug);
    }
}
