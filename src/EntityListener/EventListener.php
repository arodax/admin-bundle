<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Model\User\UserInterface;

/**
 * Listener for Doctrine evets related to Event.
 *
 * @see Event
 */
final readonly class EventListener
{
    public function __construct(private Security $security)
    {
    }

    /**
     * Handles initial entity persistance.
     */
    public function prePersist(Event $event): void
    {
        if ($this->security->getUser() instanceof UserInterface) {
            $event->addAuthor($this->security->getUser());
        }
    }
}
