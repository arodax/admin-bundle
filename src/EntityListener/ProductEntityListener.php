<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\Product;
use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Arodax\AdminBundle\Entity\Eshop\ProductVariantPrice;

final class ProductEntityListener
{
    public function prePersist(Product $product): void
    {
        $this->addProductVariant($product);
    }

    private function addProductVariant(Product $product): void
    {
        if (empty($product->getVariants())) {
            // create product variant first
            $productVariant = new ProductVariant();
            $productVariant->setProduct($product);
            // create product variant price
            $productVariantPrice = new ProductVariantPrice();
            $productVariantPrice->setProductVariant($productVariant);
            $productVariantPrice->setCurrency('CZK');
            $productVariantPrice->setTaxRate(2100);
            $productVariant->addPrice($productVariantPrice);

            $product->addVariant($productVariant);
        }
    }
}
