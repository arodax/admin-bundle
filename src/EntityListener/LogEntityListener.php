<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Log\Log;
use Symfony\Contracts\Translation\TranslatorInterface;

final readonly class LogEntityListener
{
    public function __construct(
        private TranslatorInterface $translator
    ) {
    }

    public function postLoad(Log $log): void
    {
        $message = $this->translator->trans($log->getMessage(), [], 'arodax_admin');

        if (!str_contains($message, '{')) {
            $log->setFormattedMessage($message);
        }

        $replacements = $this->getReplacements($message, $log->getContext());

        if ($replacements) {
            $message = strtr($message, $replacements);
        }

        $log->setFormattedMessage(strtr($message, $replacements));
    }

    private function getReplacements(string $message, array $context): array
    {
        $replacements = [];

        foreach ($context as $key => $val) {
            $placeholder = '{'.$key.'}';

            if (str_contains($message, $placeholder) && (null === $val || is_scalar($val) || (\is_object($val) && method_exists($val, '__toString')))) {
                $replacements[$placeholder] = $val;
            }
        }

        return $replacements;
    }
}
