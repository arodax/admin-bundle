<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EntityListener;

use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\PaymentProvider\StripePaymentProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

final readonly class OrderPaymentEntityListener implements ServiceSubscriberInterface
{
    public function __construct(private ContainerInterface $locator)
    {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function postLoad(OrderPayment $orderPayment): void
    {
        // Fetch extra credentials for stripe payment API required for pending status orders
        if (OrderPayment::STATUS_PENDING === $orderPayment->getStatus() && StripePaymentProvider::PROVIDER_NAME === $orderPayment->getPaymentMethod()?->getProvider()) {
            $handler = $this->locator->get(StripePaymentProvider::class);
            $orderPayment->setcredentials(array_merge($orderPayment->getcredentials() ?? [], $handler->getClientCredentials($orderPayment)));
        }
    }

    public static function getSubscribedServices(): array
    {
        return [
            StripePaymentProvider::class,
        ];
    }
}
