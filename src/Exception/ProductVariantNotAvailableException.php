<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Exception;

use Throwable;

/**
 * This exception is thrown on product variants.
 */
class ProductVariantNotAvailableException extends \Exception
{
    public function __construct($message = 'The requested product is not available', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
