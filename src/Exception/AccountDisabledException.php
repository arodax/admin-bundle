<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * This exception is thrown when user account is disabled.
 */
class AccountDisabledException extends AccountStatusException
{
}
