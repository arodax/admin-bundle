<?php

/*
 * This file is part of the arodax/admin-bundle package.
 *
 * (c) ARODAX  <info@arodax.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Exception;

/**
 * Interface for distinct all tree exceptions from other exceptions.
 */
interface TreeExceptionInterface
{
}
