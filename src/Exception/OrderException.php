<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\Exception;

/**
 * This exception is thrown when order could not be created or paid.
 */
class OrderException extends \Exception
{
}
