<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Intl;

use Arodax\AdminBundle\I18n\LocalesUtilityInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use DeepL\DeepLException;
use DeepL\Translator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Provides translation service via DeepL Translator client.
 */
final class DeeplTranslatorClient
{
    /*
     * In case there is no default DeepL API key provided, we use
     * free auth-key limited to 500.000 characters / month.
     */
    private string $authKey = 'fdd910f6-5282-7d28-0bac-39d04c04cc46:fx';

    /**
     * @param string|null $authKey The DeepL API key.
     */
    public function __construct(
        private readonly EntityManagerInterface  $entityManager,
        private readonly LocalesUtilityInterface $localeUtility,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_DEEPL_API_KEY')]
        ?string $authKey = null,
    ) {
        if (false === empty($authKey)) {
            $this->authKey = $authKey;
        }
    }

    /**
     * Translate the whole entity to all other available locales.
     *
     * @param TranslatableInterface $translatable
     * @param string $locale
     * @return void
     */
    public function translateEntity(TranslatableInterface $translatable, string $locale): void
    {
        $pa = new PropertyAccessor();

        // If TranslatableInterface is read only, we skip translation.
        if ($pa->isReadable($translatable, 'readOnly') && true === $pa->getValue($translatable, 'readOnly')) {
            return;
        }

        $fields = [];
        $targets = [];

        $translations = $translatable->getTranslations()->toArray();

        if (false === isset($translations[$locale])) {
            return;
        }

        $meta = $this->entityManager->getClassMetadata($translations[$locale]::class);
        foreach ($meta->getFieldNames() as $fieldName) {
            if (in_array($fieldName, ['id', 'locale', 'translatable'])) {
                continue;
            }

            $fields[$fieldName] = $pa->getValue($translations[$locale], $fieldName);
        }

        // get all
        $fields = array_unique(array_filter($fields));

        foreach ($this->localeUtility->getContentLocales() as $item) {
            if ($item['code'] !== $locale) {
                $targets[] = $item['code'];
            }
        }

        $targets = array_unique(array_filter($targets));

        foreach ($targets as $target) {
            $translation = $translatable->translate($target, false);

            foreach ($fields as $fieldName => $fieldValue) {
                $result = $this->translateString($locale, $target, $fieldValue);
                $pa->setValue($translation, $fieldName, $result);
            }

            $translatable->mergeNewTranslations();
        }

        $this->entityManager->flush();
    }

    /**
     * Translate the string with DeepL translator.
     */
    public function translateString(?string $source, string $target, string $input): string
    {
        try {

            $source = $this->resolveSourceLocale($source);
            $target = $this->resolveTargetLocale($target);

            $translator = new Translator($this->authKey, [
                'tag_handling' => 'html'
            ]);
            return $translator->translateText($input, $source, $target)->text;
        } catch (DeepLException $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Resolve source locale. This should decide, for example to which
     * english dialect we translate.
     *
     * @param string $locale
     * @return string|null
     */
    private function resolveSourceLocale(string $locale): ?string
    {
        $locale = strtoupper($locale);

        $supportedLocales = [
            'BG', 'CS', 'DA', 'DE', 'EL', 'EN', 'ES', 'ET', 'FI', 'FR', 'HU',
            'ID', 'IT', 'JA', 'KO', 'LT', 'LV', 'NB', 'NL', 'PL', 'PT', 'RO',
            'RU', 'SK', 'SL', 'SV', 'TR', 'UK', 'ZH'
        ];

        return in_array($locale, $supportedLocales) ? $locale : null;
    }

    /**
     * Resolve target locale. This should decide, for example to which
     * english dialect we translate.
     *
     * @param string $locale
     * @return string
     */
    private function resolveTargetLocale(string $locale): string
    {
        $locale = strtoupper($locale);

        $supportedLocales = [
            'BG', 'CS', 'DA', 'DE', 'EL', 'EN-GB', 'EN-US', 'ES', 'ET', 'FI', 'FR', 'HU',
            'ID', 'IT', 'JA', 'KO', 'LT', 'LV', 'NB', 'NL', 'PL', 'PT', 'PT-BR', 'PT-PT', 'RO',
            'RU', 'SK', 'SL', 'SV', 'TR', 'UK', 'ZH'
        ];

        return in_array($locale, $supportedLocales) ? $locale : 'EN-US';
    }
}
