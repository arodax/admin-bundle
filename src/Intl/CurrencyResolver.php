<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Intl;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @experimental
 */
class CurrencyResolver
{
    public function __construct(
        protected ParameterBagInterface $parameterBag
    ) {
    }

    public function resolve(): string
    {
        $defaultCurrency = $this->parameterBag->get('arodax_admin.default_currency');

        if (null === $defaultCurrency) {
            throw new \RuntimeException('The default currency has not been set. Have you set arodax_admin.default_currency parameter in the arodax_admin.yaml?');
        }

        return $defaultCurrency;
    }
}
