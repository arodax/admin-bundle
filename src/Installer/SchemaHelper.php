<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Installer;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManagerInterface;

final class SchemaHelper
{
    private readonly ?AbstractSchemaManager $schemaManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->schemaManager = $entityManager->getConnection()->getSchemaManager();
    }

    /**
     * List all tables managed by Doctrine.
     */
    public function listTableNames(): array
    {
        return $this->schemaManager->listTableNames();
    }
}
