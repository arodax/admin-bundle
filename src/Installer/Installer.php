<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Installer;

use ApiPlatform\Validator\ValidatorInterface;
use Arodax\AdminBundle\Entity\Settings;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Factory\UserFactory;
use Arodax\AdminBundle\PaymentProvider\BankTransferPaymentProvider;
use Arodax\AdminBundle\PaymentProvider\StripePaymentProvider;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Bundle\SecurityBundle\Security;
use function Symfony\Component\String\u;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Installer
{
    public const string AA_TABLE_PREFIX = 'aa_';
    private bool $dev;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
        private readonly Security $security,
        private readonly CacheInterface $cache,
        private readonly SchemaHelper $schemaHelper,
        private readonly ValidatorInterface $validator,
        private readonly UserFactory $userFactory,
        #[Autowire(param: 'kernel.environment')]
        private readonly string $isDev,
        #[Autowire(param: 'arodax_admin.available_locales')]
        private readonly array $availableLocales
    ) {
        $this->dev = 'dev' === $isDev;
    }

    /**
     * Check whether is the current database schema outdated and needs to be updatd.
     */
    public function isSchemaOutdated(): bool
    {
        return !empty($this->updateSql());
    }

    /**
     * Check if this is a fresh install, i.e. no tables named with aa_ prefix exist in the
     * database and there is no user with admin privileges.
     */
    public function isFreshInstall(): bool
    {
        $tables = \count(array_filter($this->schemaHelper->listTableNames(), function ($tableName) {
            return 0 === strpos($tableName, self::AA_TABLE_PREFIX);
        }));

        if (0 === $tables) {
            return true;
        }

        $admins = \count($this->entityManager->getRepository(User::class)->findAdmins());

        return 0 === $admins;
    }

    /**
     * Execute fresh install, creates schema and load initial data.
     *
     * @throws InvalidArgumentException
     */
    public function executeFreshInstall(string $initialUserEmail, string $initialUserPassword): void
    {
        $this->updateSchema();
        $this->installAdmin($initialUserEmail, $initialUserPassword);
        $this->installMenu();
        $this->installPaymentMethods();
        $this->installSettings();
    }

    /**
     * Install initial admin user.
     */
    public function installAdmin(string $email, string $password): void
    {
        if (!$this->dev) {
            throw new AccessDeniedException($this->translator->trans('install.not_possible', [], 'arodax_admin'));
        }

        $user = $this->userFactory->createAdmin($email, $password);

        $this->validator->validate($user);

        $this->entityManager->persist($user);
    }

    /**
     * Install settings (or update).
     *
     * @return void
     */
    public function installSettings(): void
    {
        $this->installSetting('login_logo', null);
        $this->installSetting('header_logo', null);
        $this->installSetting('header_background_light', null);
        $this->installSetting('header_background_light_dark', 'true');
        $this->installSetting('header_background_dark', null);
        $this->installSetting('header_background_dark_dark', 'true');
        $this->installSetting('header_logo_small', '/build/images/logo/header_small.svg');
        $this->installSetting('header_logo_big', '/build/images/logo/header_big.svg');
        $this->installSetting('currency_custom', null);
        $this->installSetting('token_chatgpt', null);
    }

    /**
     * Install initial admin menu.
     *
     * @throws InvalidArgumentException
     */
    public function installMenu(): void
    {
        $this->cache->delete('arodax_admin_menu');

        $tree = [
            [
                'node_name' => 'arodax_admin',
                'title' => 'admin',
                'link' => 'arodax_admin_default_index',
                'icon' => null,
                'parent' => null,
                'security' => null,
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin'])?->isEnabled() ?? true
            ],
            // dashboard
            [
                'node_name' => 'arodax_admin_dashboard',
                'title' => 'dashboard',
                'link' => 'arodax_admin_dashboard_index',
                'icon' => 'fa-fw fa-light fa-house-user',
                'parent' => 'arodax_admin',
                'security' => null,
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_dashboard'])?->isEnabled() ?? true
            ],
            // content
            [
                'node_name' => 'arodax_admin_content',
                'title' => 'content',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-folder-grid',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_CONTENT_ARTICLE_ADMIN") or is_granted("ROLE_CONTENT_SECTION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_content'])?->isEnabled() ?? true
            ],
            // content -> articles
            [
                'node_name' => 'arodax_admin_articles_index',
                'title' => 'articles',
                'link' => 'arodax_admin_articles_index',
                'icon' => null,
                'parent' => 'arodax_admin_content',
                'security' => 'is_granted("ROLE_CONTENT_ARTICLE_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_articles_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_articles_edit',
                'title' => 'article editation',
                'link' => 'arodax_admin_articles_edit',
                'icon' => null,
                'parent' => 'arodax_admin_articles_index',
                'security' => 'is_granted("ROLE_CONTENT_ARTICLE_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_articles_edit'])?->isEnabled() ?? true
            ],
            // content -> sections
            [
                'node_name' => 'arodax_admin_sections_index',
                'title' => 'sections',
                'link' => 'arodax_admin_sections_index',
                'icon' => null,
                'parent' => 'arodax_admin_content',
                'security' => 'is_granted("ROLE_CONTENT_SECTION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_sections_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_sections_edit',
                'title' => 'section editation',
                'link' => 'arodax_admin_sections_edit',
                'icon' => null,
                'parent' => 'arodax_admin_sections_index',
                'security' => 'is_granted("ROLE_CONTENT_SECTION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_sections_edit'])?->isEnabled() ?? true
            ],
            // eshop
            [
                'node_name' => 'arodax_admin_eshop',
                'title' => 'e-shop',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-cart-shopping',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_ESHOP_PRODUCT_ADMIN") or is_granted("ROLE_ESHOP_CATEGORY_ADMIN") or is_granted("ROLE_ESHOP_SHOPPING_CART_ADMIN") or is_granted("ROLE_ESHOP_ORDER_ADMIN") or is_granted("ROLE_ESHOP_SHIPPING_METHOD_ADMIN") or is_granted("ROLE_ESHOP_PAYMENT_METHOD_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_eshop'])?->isEnabled() ?? true
            ],
            // eshop -> products
            [
                'node_name' => 'arodax_admin_products_index',
                'title' => 'products',
                'link' => 'arodax_admin_products_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_PRODUCT_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_products_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_products_edit',
                'title' => 'product editation',
                'link' => 'arodax_admin_products_edit',
                'icon' => null,
                'parent' => 'arodax_admin_products_index',
                'security' => 'is_granted("ROLE_ESHOP_PRODUCT_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_products_edit'])?->isEnabled() ?? true
            ],
            // eshop -> product categories
            [
                'node_name' => 'arodax_admin_product_categories_index',
                'title' => 'product categories',
                'link' => 'arodax_admin_product_categories_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_CATEGORY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_product_categories_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_product_categories_edit',
                'title' => 'product category editation',
                'link' => 'arodax_admin_product_categories_edit',
                'icon' => null,
                'parent' => 'arodax_admin_product_categories_index',
                'security' => 'is_granted("ROLE_ESHOP_CATEGORY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_product_categories_edit'])?->isEnabled() ?? true
            ],
            // eshop -> shopping carts
            [
                'node_name' => 'arodax_admin_shopping_carts_index',
                'title' => 'shopping carts',
                'link' => 'arodax_admin_shopping_carts_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_SHOPPING_CART_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_shopping_carts_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_shopping_carts_edit',
                'title' => 'shopping cart editation',
                'link' => 'arodax_admin_shopping_carts_edit',
                'icon' => null,
                'parent' => 'arodax_admin_shopping_carts_index',
                'security' => 'is_granted("ROLE_ESHOP_SHOPPING_CART_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_shopping_carts_edit'])?->isEnabled() ?? true
            ],
            // eshop -> order
            [
                'node_name' => 'arodax_admin_orders_index',
                'title' => 'orders',
                'link' => 'arodax_admin_orders_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_ORDER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_orders_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_orders_edit',
                'title' => 'order editation',
                'link' => 'arodax_admin_orders_edit',
                'icon' => null,
                'parent' => 'arodax_admin_orders_index',
                'security' => 'is_granted("ROLE_ESHOP_ORDER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_orders_edit'])?->isEnabled() ?? true
            ],
            // eshop -> shipping methods
            [
                'node_name' => 'arodax_admin_shipping_methods_index',
                'title' => 'shipping methods',
                'link' => 'arodax_admin_shipping_methods_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_SHIPPING_METHOD_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_shipping_methods_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_shipping_methods_edit',
                'title' => 'shipping method editation',
                'link' => 'arodax_admin_shipping_methods_edit',
                'icon' => null,
                'parent' => 'arodax_admin_shipping_methods_index',
                'security' => 'is_granted("ROLE_ESHOP_SHIPPING_METHOD_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_shipping_methods_edit'])?->isEnabled() ?? true
            ],
            // eshop -> payment methods
            [
                'node_name' => 'arodax_admin_payment_methods_index',
                'title' => 'payment methods',
                'link' => 'arodax_admin_payment_methods_index',
                'icon' => null,
                'parent' => 'arodax_admin_eshop',
                'security' => 'is_granted("ROLE_ESHOP_PAYMENT_METHOD_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_payment_methods_edit'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_payment_methods_edit',
                'title' => 'payment method editation',
                'link' => 'arodax_admin_payment_methods_edit',
                'icon' => null,
                'parent' => 'arodax_admin_payment_methods_index',
                'security' => 'is_granted("ROLE_ESHOP_PAYMENT_METHOD_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_payment_methods_edit'])?->isEnabled() ?? true
            ],

            // finance
            [
                'node_name' => 'arodax_admin_finance',
                'title' => 'finance',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-display-chart-up-circle-dollar',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_FINANCE_INVOICE_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_finance'])?->isEnabled() ?? true
            ],
            // finance -> invoice
            [
                'node_name' => 'arodax_admin_invoices_index',
                'title' => 'invoices',
                'link' => 'arodax_admin_invoices_index',
                'icon' => null,
                'parent' => 'arodax_admin_finance',
                'security' => 'is_granted("ROLE_FINANCE_INVOICE_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_invoices_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_invoices_edit',
                'title' => 'invoice editation',
                'link' => 'arodax_admin_invoices_edit',
                'icon' => null,
                'parent' => 'arodax_admin_invoices_index',
                'security' => 'is_granted("ROLE_FINANCE_INVOICE_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_invoices_edit'])?->isEnabled() ?? true
            ],
            // multimedia
            [
                'node_name' => 'arodax_admin_multimedia',
                'title' => 'multimedia',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-photo-film-music',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_MULTIMEDIA_GALLERY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_multimedia'])?->isEnabled() ?? true
            ],
            // multimedia -> gallery
            [
                'node_name' => 'arodax_admin_galleries_index',
                'title' => 'galleries',
                'link' => 'arodax_admin_galleries_index',
                'icon' => null,
                'parent' => 'arodax_admin_multimedia',
                'security' => 'is_granted("ROLE_MULTIMEDIA_GALLERY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_galleries_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_galleries_edit',
                'title' => 'gallery editation',
                'link' => 'arodax_admin_galleries_edit',
                'icon' => null,
                'parent' => 'arodax_admin_galleries_index',
                'security' => 'is_granted("ROLE_MULTIMEDIA_GALLERY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_galleries_edit'])?->isEnabled() ?? true
            ],
            // events
            [
                'node_name' => 'arodax_admin_event',
                'title' => 'events',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-calendar-clock',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_EVENT_CALENDAR_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_event'])?->isEnabled() ?? true
            ],
            // events -> calendar
            [
                'node_name' => 'arodax_admin_events_index',
                'title' => 'calendar',
                'link' => 'arodax_admin_events_index',
                'icon' => null,
                'parent' => 'arodax_admin_event',
                'security' => 'is_granted("ROLE_EVENT_CALENDAR_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_events_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_events_edit',
                'title' => 'event editation',
                'link' => 'arodax_admin_events_edit',
                'icon' => null,
                'parent' => 'arodax_admin_events_index',
                'security' => 'is_granted("ROLE_EVENT_CALENDAR_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_events_edit'])?->isEnabled() ?? true
            ],
            // people
            [
                'node_name' => 'arodax_admin_people',
                'title' => 'people',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-users',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_PEOPLE_USER_ADMIN") or is_granted("ROLE_PEOPLE_USER_GROUP_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_people'])?->isEnabled() ?? true
            ],
            // people -> users
            [
                'node_name' => 'arodax_admin_users_index',
                'title' => 'users',
                'link' => 'arodax_admin_users_index',
                'icon' => null,
                'parent' => 'arodax_admin_people',
                'security' => 'is_granted("ROLE_PEOPLE_USER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_users_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_users_edit',
                'title' => 'user editation',
                'link' => 'arodax_admin_users_edit',
                'icon' => null,
                'parent' => 'arodax_admin_users_index',
                'security' => 'is_granted("ROLE_PEOPLE_USER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_users_edit'])?->isEnabled() ?? true
            ],
            // people -> user group
            [
                'node_name' => 'arodax_admin_user_groups_index',
                'title' => 'user groups',
                'link' => 'arodax_admin_user_groups_index',
                'icon' => null,
                'parent' => 'arodax_admin_people',
                'security' => 'is_granted("ROLE_PEOPLE_USER_GROUP_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_user_groups_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_user_groups_edit',
                'title' => 'user groups editation',
                'link' => 'arodax_admin_user_groups_edit',
                'icon' => null,
                'parent' => 'arodax_admin_user_groups_index',
                'security' => 'is_granted("ROLE_PEOPLE_USER_GROUP_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_user_groups_edit'])?->isEnabled() ?? true
            ],
            // communication
            [
                'node_name' => 'arodax_admin_communication',
                'title' => 'communication',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-messages',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_COMMUNICATION_INQUIRY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_communication'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_inquiries_index',
                'title' => 'inquiries',
                'link' => 'arodax_admin_inquiries_index',
                'icon' => null,
                'parent' => 'arodax_admin_communication',
                'security' => 'is_granted("ROLE_COMMUNICATION_INQUIRY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_inquiries_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_inquiries_edit',
                'title' => 'inquiry editation',
                'link' => 'arodax_admin_inquiries_edit',
                'icon' => null,
                'parent' => 'arodax_admin_inquiries_index',
                'security' => 'is_granted("ROLE_COMMUNICATION_INQUIRY_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_inquiries_edit'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_newsletter_index',
                'title' => 'newsletter',
                'link' => 'arodax_admin_newsletter_index',
                'icon' => null,
                'parent' => 'arodax_admin_communication',
                'security' => 'is_granted("ROLE_COMMUNICATION_NEWSLETTER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_newsletter_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_newsletter_recipient_index',
                'title' => 'newsletter_recipients',
                'link' => 'arodax_admin_newsletter_recipient_index',
                'icon' => null,
                'parent' => 'arodax_admin_communication',
                'security' => 'is_granted("ROLE_COMMUNICATION_NEWSLETTER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_newsletter_recipient_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_newsletter_recipient_edit',
                'title' => 'newsletter_recipients_editation',
                'link' => 'arodax_admin_newsletter_recipient_edit',
                'icon' => null,
                'parent' => 'arodax_admin_newsletter_recipient_index',
                'security' => 'is_granted("ROLE_COMMUNICATION_NEWSLETTER_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_newsletter_recipient_edit'])?->isEnabled() ?? true
            ],
            // settings
            [
                'node_name' => 'arodax_admin_settings',
                'title' => 'settings',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-gears',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_SETTING_MENU_ADMIN") or is_granted("ROLE_SETTING_TRANSLATION_ADMIN") or is_granted("ROLE_SETTING_CONFIGURATION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_settings'])?->isEnabled() ?? true
            ],
            // settings -> menu
            [
                'node_name' => 'arodax_admin_menu_items_index',
                'title' => 'menu',
                'link' => 'arodax_admin_menu_items_index',
                'icon' => null,
                'parent' => 'arodax_admin_settings',
                'security' => 'is_granted("ROLE_SETTING_MENU_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_menu_items_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_menu_items_edit',
                'title' => 'menu editation',
                'link' => 'arodax_admin_menu_items_edit',
                'icon' => null,
                'parent' => 'arodax_admin_menu_items_index',
                'security' => 'is_granted("ROLE_SETTING_MENU_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_menu_items_edit'])?->isEnabled() ?? true
            ],
            // settings -> translations
            [
                'node_name' => 'arodax_admin_i18ns_index',
                'title' => 'translations',
                'link' => 'arodax_admin_i18ns_index',
                'icon' => null,
                'parent' => 'arodax_admin_settings',
                'security' => 'is_granted("ROLE_SETTING_TRANSLATION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_i18ns_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_i18ns_edit',
                'title' => 'translation editation',
                'link' => 'arodax_admin_i18ns_edit',
                'icon' => null,
                'parent' => 'arodax_admin_i18ns_index',
                'security' => 'is_granted("ROLE_SETTING_TRANSLATION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_i18ns_edit'])?->isEnabled() ?? true
            ],
            // settings -> configuration
            [
                'node_name' => 'arodax_admin_settings_index',
                'title' => 'configuration',
                'link' => 'arodax_admin_settings_index',
                'icon' => null,
                'parent' => 'arodax_admin_settings',
                'security' => 'is_granted("ROLE_SETTING_CONFIGURATION_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_settings_index'])?->isEnabled() ?? true
            ],
            // grantys
            [
                'node_name' => 'arodax_admin_grantys',
                'title' => 'grantys',
                'link' => null,
                'icon' => 'fa-fw fa-light fa-memo-circle-check',
                'parent' => 'arodax_admin',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grantys'])?->isEnabled() ?? true
            ],
            // grantys -> grant projects
            [
                'node_name' => 'arodax_admin_grant_projects_index',
                'title' => 'grant projects',
                'link' => 'arodax_admin_grant_projects_index',
                'icon' => null,
                'parent' => 'arodax_admin_grantys',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_projects_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_grant_projects_edit',
                'title' => 'grant project editation',
                'link' => 'arodax_admin_grant_projects_edit',
                'icon' => null,
                'parent' => 'arodax_admin_grant_projects_index',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_projects_edit'])?->isEnabled() ?? true
            ],
            // grantys -> grant_project_groups
            [
                'node_name' => 'arodax_admin_grant_project_groups_index',
                'title' => 'grant_project_groups',
                'link' => 'arodax_admin_grant_project_groups_index',
                'icon' => null,
                'parent' => 'arodax_admin_grantys',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_projects_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_grant_project_groups_edit',
                'title' => 'grant_project_groups_editation',
                'link' => 'arodax_admin_grant_project_groups_edit',
                'icon' => null,
                'parent' => 'arodax_admin_grant_project_groups_index',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_project_groups_edit'])?->isEnabled() ?? true
            ],
            // grantys -> grant projects
            [
                'node_name' => 'arodax_admin_grant_calls_index',
                'title' => 'grant calls',
                'link' => 'arodax_admin_grant_calls_index',
                'icon' => null,
                'parent' => 'arodax_admin_grantys',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_calls_index'])?->isEnabled() ?? true
            ],
            [
                'node_name' => 'arodax_admin_grant_calls_edit',
                'title' => 'grant calls editation',
                'link' => 'arodax_admin_grant_calls_edit',
                'icon' => null,
                'parent' => 'arodax_admin_grant_calls_index',
                'security' => 'is_granted("ROLE_GRANTYS_ADMIN")',
                'enabled' => $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin_grant_calls_edit'])?->isEnabled() ?? true
            ],
        ];

        // remove old menu, so we get the fresh structure
        $menu = $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'arodax_admin']) ?? $this->entityManager->getRepository(MenuItem::class)->findOneBy(['nodeName' => 'admin']);

        if (null !== $menu) {
            $this->entityManager->remove($menu);
            $this->entityManager->flush();
        }

        $treeRepository = $this->entityManager->getRepository(MenuItem::class);

        $items = [];

        /** @var non-empty-array<array> $node */
        foreach ($tree as $node) {
            $items[$node['node_name']] = new MenuItem();
            $items[$node['node_name']]->setEnabled(true);
            $items[$node['node_name']]->setNodeName($node['node_name']);
            $items[$node['node_name']]->setInternal(true);

            // translations for the menu item
            foreach ($this->availableLocales as $locale) {
                $items[$node['node_name']]->translate($locale)->setTitle(u($this->translator->trans($node['title'], [], 'arodax_admin', $locale))->title()->toString());

                if (isset($node['link'])) {
                    $items[$node['node_name']]->translate($locale)->setLinkType('route');
                    $items[$node['node_name']]->translate($locale)->setLink($node['link']);
                }

                if (isset($node['icon'])) {
                    $items[$node['node_name']]->setIcon($node['icon']);
                }

                if (isset($node['security'])) {
                    $items[$node['node_name']]->setSecurity($node['security']);
                }

                if (isset($node['enabled'])) {
                    $items[$node['node_name']]->setEnabled($node['enabled']);
                }
            }

            // merge translations before persist
            $items[$node['node_name']]->mergeNewTranslations();

            // if no parent node exist, persist as last child of the root
            if (null === $node['parent']) {
                $treeRepository->persistAsLastChild($items[$node['node_name']]);
            } else {
                $treeRepository->persistAsLastChildOf($items[$node['node_name']], $items[$node['parent']], );
            }
        }

        $this->entityManager->flush();
    }

    public function installPaymentMethods(): void
    {
        $paymentMethodBankTransfer = new PaymentMethod();
        $paymentMethodBankTransfer->setProvider(BankTransferPaymentProvider::class);
        $paymentMethodBankTransfer->translate('cs')->setName('Bankovní převod');
        $paymentMethodBankTransfer->translate('en')->setName('Bank transfer');
        $this->entityManager->persist($paymentMethodBankTransfer);
        $this->entityManager->flush();

        $paymentMethodCreditCard = new PaymentMethod();
        $paymentMethodCreditCard->setProvider(StripePaymentProvider::class);
        $paymentMethodCreditCard->translate('cs')->setName('Platební karta');
        $paymentMethodCreditCard->translate('en')->setName('Credit card');
        $this->entityManager->persist($paymentMethodCreditCard);
        $this->entityManager->flush();
    }

    /**
     * Updates database schema.
     *
     * This method will keep Doctrine unrelated tables in the database.
     *
     * @throws AccessDeniedException
     */
    public function updateSchema(): void
    {
        if (!$this->security->isGranted('ROLE_ADMIN') && !$this->dev) {
            throw new AccessDeniedException($this->translator->trans('install.not_possible', [], 'arodax_admin'));
        }

        $metadatas = $this->entityManager->getMetadataFactory()->getAllMetadata();

        $schemaTool = new SchemaTool($this->entityManager);
        $schemaTool->updateSchema($metadatas, true);
    }

    /**
     * Find SQL to run for updating database.
     *
     * This method will return sql which should be run to update the database schema,
     * without running those SQL.
     */
    private function updateSql(): array
    {
        $metadatas = $this->entityManager->getMetadataFactory()->getAllMetadata();

        $schemaTool = new SchemaTool($this->entityManager);

        return $schemaTool->getUpdateSchemaSql($metadatas, true);
    }

    /**
     * Install single parameter for the settings.
     *
     * @param string $key
     * @param string|null $value
     * @return void
     */
    private function installSetting(string $key, ?string $value = null): void
    {
        $settings = $this->entityManager->getRepository(Settings::class)->findOneBy(['key' => $key]);

        if (null === $settings) {
            $settings = new Settings();
            $settings->setKey($key);
            $settings->setValue($value);
            $this->entityManager->persist($settings);
            $this->entityManager->flush();
        }
    }
}
