<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\ShippingProvider;

class PacketaShippingProvider implements ShippingProviderInterface
{
    public const string PROVIDER_NAME = 'packeta';
}
