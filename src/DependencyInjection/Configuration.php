<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This class defines configurateion of G5 content management platform configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Describe and define default configuration.
     *
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('arodax_admin');

        $treeBuilder
            ->getRootNode()
                ->children()
                    ->arrayNode('locales')
                    ->end()
                    ->arrayNode('images')
                        ->children()
                            ->arrayNode('filters')
                                ->arrayPrototype()
                                    ->scalarPrototype()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('i18n')
                        ->children()
                            ->arrayNode('admin')
                                ->arrayPrototype()
                                    ->scalarPrototype()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('app')
                                ->arrayPrototype()
                                    ->scalarPrototype()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->scalarNode('default_currency')
            ->end();

        return $treeBuilder;
    }
}
