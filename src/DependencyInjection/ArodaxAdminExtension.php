<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Composer\Composer;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

/**
 * This class handles ARODAX Admin content management platform configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html to learn more.
 */
class ArodaxAdminExtension extends Extension implements PrependExtensionInterface
{
    /**
     *  Loads a configuration into container.
     *
     * @param array            $configs   an array of configuration values
     * @param ContainerBuilder $container a ContainerBuilder instance
     *
     * @throws \Exception when services file could not be loaded
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yaml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('arodax_admin.images.filters', $config['images']['filters'] ?? []);
        $container->setParameter('arodax_admin.translatable.app.locales', $config['translatable']['app']['locales'] ?? []);
        $container->setParameter('arodax_admin.default_currency', $config['default_currency'] ?? null);
    }

    /**
     * Prepend and update various configuration.
     *
     * This method mainly loads all configuration files in bundle config/packages directory and include
     * it into the main application configuration.
     *
     * @see https://github.com/symfony/symfony/issues/24461 to learn more.
     *
     * @throws \ReflectionException
     */
    public function prepend(ContainerBuilder $container): void
    {
        $extensionConfigsRefl = new \ReflectionProperty(ContainerBuilder::class, 'extensionConfigs');
        $extensionConfigsRefl->setAccessible(true);
        $extensionConfigs = $extensionConfigsRefl->getValue($container);

        // If Doctrine DBAL use "short" form of the connections configuration, we turn this into the more verbose one, so mapping_types could be properly injected.
        if (!isset($extensionConfigs['doctrine'][0]['dbal']['connections'])) {
            $connection = $extensionConfigs['doctrine'][0]['dbal'];
            $extensionConfigs['doctrine'][0]['dbal'] = [];
            $extensionConfigs['doctrine'][0]['dbal']['connections']['default'] = $connection;
        }

        // If Doctrine ORM use "short" form of the ORM configuration, we turn this into the more verbose one, so filters could be properly injected.
        if (!isset($extensionConfigs['doctrine'][0]['orm']['entity_managers'])) {
            $extensionConfigs['doctrine'][0]['orm']['entity_managers']['default']['mappings'] = $extensionConfigs['doctrine'][0]['orm']['mappings'] ?? [];
            $extensionConfigs['doctrine'][0]['orm']['entity_managers']['default']['auto_mapping'] = $extensionConfigs['doctrine'][0]['orm']['auto_mapping'] ?? true;
            $extensionConfigs['doctrine'][0]['orm']['entity_managers']['default']['naming_strategy'] = $extensionConfigs['doctrine'][0]['orm']['naming_strategy'] ?? 'doctrine.orm.naming_strategy.underscore';
            unset($extensionConfigs['doctrine'][0]['orm']['mappings']);
            unset($extensionConfigs['doctrine'][0]['orm']['auto_mapping']);
            unset($extensionConfigs['doctrine'][0]['orm']['naming_strategy']);
        }

        $finder = new Finder();
        $filesystem = new Filesystem();
        $finder->files();
        $finder->in(__DIR__.'/../../config/packages')->depth(0);

        if ($filesystem->exists(__DIR__.'/../../config/packages/'.$container->getParameter('kernel.environment'))) {
            $finder->in(__DIR__.'/../../config/packages/'.$container->getParameter('kernel.environment'));
        }

        $finder->name('*.yaml');

        foreach ($finder as $file) {
            $packageName = $file->getBasename('.yaml');
            $packageConfig = Yaml::parseFile($file->getPathname());

            if (!\array_key_exists($packageName, $packageConfig)) {
                throw new InvalidConfigurationException(sprintf('Expected to find root key "%s" in file "%s" but not found.', $packageName, $file->getPathname()));
            }

            if ('arodax_admin' !== $packageName) {
                $extensionConfigs[$packageName][0] = $this->mergeConfigs($extensionConfigs[$packageName][0] ?? [], $packageConfig[$packageName] ?? []);
            } else {
                $extensionConfigs[$packageName][0] = $this->mergeConfigs($packageConfig[$packageName] ?? [], $extensionConfigs[$packageName][0] ?? []);
            }
        }

        $container->setParameter('arodax_admin.bundle_dir', $this->getBundleDir());

        $reflector = new \ReflectionClass(Composer::class);
        $container->setParameter('kernel.vendor_dir', \dirname($reflector->getFileName()).'/../../../../');

        $extensionConfigsRefl->setValue($container, $extensionConfigs);
    }

    /**
     * Return the root directory of the bundle.
     */
    public function getBundleDir(): string
    {
        return \dirname(__DIR__);
    }

    /**
     * Return available locales found in translations resources.
     */
    public function getAvailableLocales(): array
    {
        $finder = new Finder();
        $finder->files()->name('*.yaml');
        $finder->in(__DIR__.'/../../translations')->depth(0);

        $locales = [];

        foreach ($finder as $file) {
            $fileName = $file->getBasename('.yaml');
            if (preg_match('/^\w+\.(\w+)/', $fileName, $matches)) {
                $locales[] = $matches[1];
            }
        }

        return array_unique($locales);
    }

    /**
     * This method will merge bundle config with existing config recursively, handling both scalar and array values.
     *
     * @param array $appConfig def
     */
    private function mergeConfigs(array $appConfig, array $bundleConfig): array
    {
        foreach ($bundleConfig as $key => $val) {
            if (isset($appConfig[$key])) {
                if (!\is_array($appConfig[$key])) {
                    $appConfig[$key] = $val;
                } elseif (array_keys($appConfig[$key]) === range(0, \count($appConfig[$key]) - 1)) {
                    $appConfig[$key] = array_merge($appConfig[$key], $val);
                } else {
                    $appConfig[$key] = $this->mergeConfigs($appConfig[$key], $val ?? []);
                }
            } else {
                $appConfig[$key] = $val;
            }
        }

        return $appConfig;
    }
}
