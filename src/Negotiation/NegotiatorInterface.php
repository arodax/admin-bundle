<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Negotiation;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Koranda <martin.koranda@gmail.com>
 */
interface NegotiatorInterface
{
    public function getRequestFormat(Request $request): string;

    public function getResponseFormat(Request $request): string;
}
