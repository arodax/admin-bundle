<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Negotiation;

use Negotiation\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Negotiation\Negotiator as AcceptNegotiator;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
class Negotiator implements NegotiatorInterface
{
    public const array DEFAULT_REQUEST_FORMATS = [
        'application/json'  =>  'json',
        'application/xml'   =>  'xml',
        'application/x-www-form-urlencoded' =>  'form',
        'multipart/form-data' =>  'form',
    ];
    public const array DEFAULT_RESPONSE_FORMATS = [
        'application/json'  =>  'json',
        'application/xml'   =>  'xml',
        'text/html'         =>  'html',
    ];

    public function __construct(
        private readonly array $requestFormats = self::DEFAULT_REQUEST_FORMATS,
        private readonly array $responseFormats = self::DEFAULT_RESPONSE_FORMATS
    ) {
    }

    public function getRequestFormat(Request $request): string
    {
        $contentType = $request->getContentTypeFormat() ?: $request->headers->get('CONTENT_TYPE');

        if (empty($contentType)) {
            throw new UnsupportedMediaTypeHttpException('Content-Type request header is empty');
        }

        if (false !== $pos = strpos($contentType, ';')) {
            $contentType = trim(substr($contentType, 0, $pos));
        }

        if (\in_array($contentType, $this->requestFormats, true)) {
            return $contentType;
        }

        if (!empty($this->requestFormats[$contentType])) {
            return $this->requestFormats[$contentType];
        }

        throw new UnsupportedMediaTypeHttpException(sprintf('Unsupported Content-Type format. Supported are %s, but %s given',
            join(' | ', array_keys($this->requestFormats)), $contentType));
    }

    /**
     * @throws Exception
     */
    public function getResponseFormat(Request $request): string
    {
        $requestHeader = $request->headers->get('Accept');

        if (empty($requestHeader)) {
            throw new NotAcceptableHttpException('Accept request header empty');
        }

        $negotiator = new AcceptNegotiator();
        $format = $negotiator->getBest($requestHeader, array_keys($this->responseFormats));

        if ($format === null) {
            throw new NotAcceptableHttpException(sprintf('Unsupported Accept format. Supported are %s, but %s given',
                implode(' | ', array_keys($this->responseFormats)), $requestHeader));
        }

        return $this->responseFormats[$format->getValue()];
    }
}
