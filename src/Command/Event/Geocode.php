<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Event;

use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Entity\Event\EventType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'arodax_admin:event:attach-event-type',
    description: 'Attach all events to the given event type',
)]
final class Geocode extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('event-type', InputArgument::REQUIRED, 'The event type');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $eventType = $this->entityManager->getRepository(EventType::class)->find($input->getArgument('event-type'));

        if (null === $eventType) {
            $io->error('There is no such of event type');
            return Command::FAILURE;
        }

        $events = $this->entityManager->getRepository(Event::class)->findAll();

        foreach ($events as $event) {
            $event->addEventType($eventType);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
