<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Translator;

use Composer\Console\Input\InputArgument;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Finder\Finder;
#[AsCommand(
    name: 'arodax_admin:translator:add-translations',
    description: 'Find and list unique translation keys from .vue files.',
)]
final class AddTranslations extends Command
{
    public function __construct(
        private LoggerInterface $logger,
        #[Autowire(param: 'arodax_admin.bundle_dir')]
        private readonly string $bundleDir
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(name: 'master-language', mode: InputArgument::OPTIONAL, description: 'Master language code (e.g. "cs").', default: 'cs');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $masterLanguage = $input->getArgument('master-language');

        $io = new SymfonyStyle($input, $output);
        $finder = new Finder();
        $translationKeys = [];

        $finder->files()->in("{$this->bundleDir}/../assets/components")->name('*.vue');
        $finder->files()->in("{$this->bundleDir}/../assets/views")->name('*.vue');

        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                $this->logger->info('Processing file: ' . $file->getRealPath());

                $content = $file->getContents();
                preg_match_all('/\$t\([\'"](\w+)[\'"]\)/', $content, $matches);
                $translationKeys = array_merge($translationKeys, $matches[1]);
            }

            $translationKeys = array_unique($translationKeys);
            $io->success('Unique translation keys extracted successfully.');
        } else {
            $io->error('No .vue files found. Please check the path.');
            return Command::FAILURE;
        }

        $masterTranslationFile = "{$this->bundleDir}/../translations/arodax_admin+intl-icu.{$masterLanguage}.json";

        if (!file_exists($masterTranslationFile)) {
            $io->error("The master translation file {$masterTranslationFile} does not exist.");
            return Command::FAILURE;
        }

        $masterTranslations = json_decode(file_get_contents($masterTranslationFile), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $io->error('Failed to decode JSON from the master translation file.');
            return Command::FAILURE;
        }

        foreach ($translationKeys as $key) {
            if (!array_key_exists($key, $masterTranslations)) {
                $this->logger->warning("Missing translation key: '{$key}'");
                $translation = $io->ask("How would you translate key '{$key}' into '{$masterLanguage}'?");

                if (!empty($translation)) {
                    // Update the master translations array with the new translation
                    $masterTranslations[$key] = $translation;

                    // Immediately save the updated translations back to the file
                    $this->saveTranslationsToFile($masterTranslations, $masterTranslationFile, $io);
                }
            }
        }

        $io->success("Translations update process completed.");

        return Command::SUCCESS;
    }

    private function saveTranslationsToFile(array $translations, string $filePath, SymfonyStyle $io): void
    {
        $jsonContent = json_encode($translations, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        if ($jsonContent === false) {
            $io->error('Failed to encode translations to JSON.');
            return;
        }

        if (file_put_contents($filePath, $jsonContent) === false) {
            $io->error("Failed to write updated translations to {$filePath}.");
            return;
        }

        $io->note("Updated translations were successfully saved to {$filePath}.");
    }
}
