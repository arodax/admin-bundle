<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Translator;

use Arodax\AdminBundle\Intl\DeeplTranslatorClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(
    name: 'arodax_admin:translator:sync-translations',
    description: 'Sync translations among multiple languages.',
)]
final class SyncTranslations extends Command
{
    private array $languagePairs = [
        ['from' => 'cs', 'to' => 'en'], // Czech to English
        ['from' => 'cs', 'to' => 'sk'], // Czech to Slovak
        ['from' => 'cs', 'to' => 'pl'], // Czech to Polish
        ['from' => 'cs', 'to' => 'hu'], // Czech to Hungarian
        ['from' => 'cs', 'to' => 'de'], // Czech to German
        ['from' => 'cs', 'to' => 'ro'], // Czech to Romanian
    ];

    public function __construct(
        private readonly DeeplTranslatorClient $translatorClient,
        private readonly LoggerInterface $logger,
        #[Autowire(param: 'arodax_admin.bundle_dir')]
        private readonly string $bundleDir
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->languagePairs as $pair) {
            $this->syncTranslations($pair['from'], $pair['to'], $io);
        }

        $io->success('Translations synced successfully.');

        return Command::SUCCESS;
    }

    private function syncTranslations(string $fromLang, string $toLang, SymfonyStyle $io): void
    {
        $fromFile = "{$this->bundleDir}/../translations/arodax_admin+intl-icu.{$fromLang}.json";
        $toFile = "{$this->bundleDir}/../translations/arodax_admin+intl-icu.{$toLang}.json";

        $fromTranslations = json_decode(file_get_contents($fromFile), true);
        if (!file_exists($toFile)) {
            // If the destination file does not exist, create it and initialize with an empty array.
            $toTranslations = [];
            file_put_contents($toFile, json_encode($toTranslations));
        } else {
            $toTranslations = json_decode(file_get_contents($toFile), true) ?: [];
        }

        $missingInToLang = array_diff_key($fromTranslations, $toTranslations);
        $missingCount = count($missingInToLang);
        $this->logger->warning("Found {$missingCount} missing translations in {$toLang}");

        foreach ($missingInToLang as $key => $value) {
            $this->logger->warning("Missing translation for key {$key} in {$toLang}. Remaining: " . (--$missingCount));
            $translatedString = $this->translatorClient->translateString($fromLang, $toLang, $value);

            if (stripos($translatedString, 'bad request') !== false) {
                continue;
            }

            $toTranslations[$key] = $translatedString;

            // Instead of waiting to write the entire array, update and write one by one
            file_put_contents($toFile, json_encode($toTranslations, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        }
    }
}

