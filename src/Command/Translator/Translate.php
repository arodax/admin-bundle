<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Translator;

use Arodax\AdminBundle\Intl\DeeplTranslatorClient;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'arodax_admin:translator:translate',
    description: 'Translate the given input with the translator service.',
)]
final class Translate extends Command
{
    public function __construct(
        private readonly DeeplTranslatorClient $translatorClient
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('source', InputArgument::REQUIRED, 'Source language');
        $this->addArgument('target', InputArgument::REQUIRED, 'Target language');
        $this->addArgument('input', InputArgument::REQUIRED, 'The input string which should be translated.');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $result = $this->translatorClient->translateString(
            $input->getArgument('source'),
            $input->getArgument('target'),
            $input->getArgument('input')
        );

        $io->writeln($result);

        return Command::SUCCESS;
    }
}
