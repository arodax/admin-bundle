<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Mail;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\RouterInterface;
#[AsCommand(
    name: 'arodax_admin:mail:send',
    description: 'Send the email to the given recipient.'
)]
class Send extends Command
{
    public function __construct(protected MailerInterface $mailer, protected RouterInterface $router)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('to', InputArgument::REQUIRED, 'The email addresss of the recipient');
        $this->addArgument('subject', InputArgument::REQUIRED, 'The subject of the email');
        $this->addArgument('body', InputArgument::REQUIRED, 'The body of the email');

        $this->addOption('template', 't', InputOption::VALUE_REQUIRED, 'Path to the Twig template', '@ArodaxAdmin/emails/default.html.twig');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $to = $input->getArgument('to');
        $subject = $input->getArgument('subject');
        $body = $input->getArgument('body');
        $template = $input->getOption('template');

        $email = new TemplatedEmail();
        $email->to($to);
        $email->subject($subject);
        $email->htmlTemplate($template);
        $email->context([
            'subject' => $subject,
            'body' => $body,
        ]);

        $this->mailer->send($email);

        $io->success(sprintf('Successfully sent email with the subject %s to %s', $subject, $to));

        return 1;
    }
}
