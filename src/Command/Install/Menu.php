<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Install;

use Arodax\AdminBundle\Installer\Installer;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'arodax_admin:install:menu',
    description: 'Update menu to the newest version.'
)]
final class Menu extends Command
{
    public function __construct(
        private readonly Installer $updater
    ) {
        parent::__construct();
    }

    /**
     * @throws \Throwable
     * @throws InvalidArgumentException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->updater->installMenu();

        $io->success('The admin menu has been updated.');

        return Command::SUCCESS;
    }
}
