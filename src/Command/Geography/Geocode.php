<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Geography;

use Arodax\AdminBundle\Geography\Geocoder;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'arodax_admin:geography:geocode',
    description: 'Parse Grantys exported spreadsheet',
)]
final class Geocode extends Command
{
    public function __construct(
        private readonly Geocoder $geocoder
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('location', InputArgument::REQUIRED, 'The location which should be geocoded');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $result = $this->geocoder->geocode($input->getArgument('location'));

        $io->writeln(json_encode($result, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));

        return Command::SUCCESS;
    }
}
