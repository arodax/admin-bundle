<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Printer;

use Arodax\AdminBundle\Utils\Media\PdfPrinter;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'arodax_admin:printer:print-template',
    description: 'Print a template into the given destination.'
)]
class PrintTemplate extends Command
{
    public function __construct(private readonly PdfPrinter $pdfPrinter)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('template', InputArgument::REQUIRED, 'The template name');
        $this->addArgument('output', InputArgument::REQUIRED, 'The output path');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $template = $input->getArgument('template');
        $output = $input->getArgument('output');

        $this->pdfPrinter->printToFile($template, $output);

        $io->success(sprintf('Template successfully rendered and saved to %s', $output));

        return Command::SUCCESS;
    }
}
