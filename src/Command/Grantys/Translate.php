<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Command\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProject;
use Arodax\AdminBundle\Message\Translator\TranslateEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'arodax_admin:grantys:translate-projects',
    description: 'Geocode all Grantys projects',
)]
final class Translate extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MessageBusInterface $messageBus
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('id', InputArgument::OPTIONAL, 'The optional Grant project id, if not set all will be translated.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getArgument('id')) {
            $project = $this->entityManager->getRepository(GrantProject::class)->find($input->getArgument('id'));
            if (null !== $project) {
                $this->messageBus->dispatch(new TranslateEntity(GrantProject::class, $project->getId(), 'cs'));
            }
        } else {
            $projects = $this->entityManager->getRepository(GrantProject::class)->findAll();
            foreach ($projects as $project) {
                $this->messageBus->dispatch(new TranslateEntity(GrantProject::class, $project->getId(), 'cs'));
            }
        }


        return Command::SUCCESS;
    }
}
