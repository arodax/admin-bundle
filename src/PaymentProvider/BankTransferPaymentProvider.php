<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\PaymentProvider;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Exception\PaymentProviderException;
use Arodax\AdminBundle\Message\Eshop\OrderPaidMessage;
use Arodax\AdminBundle\ValueObject\BankTransferPayment;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\Exceptions\MaskNotFoundException;


final class BankTransferPaymentProvider extends DefaultPaymentProvider implements PaymentProviderInterface
{
    public const string PROVIDER_NAME = 'bankTransfer';

    /**
     * Create the new OrderPayment for the given Order with the paymentKey coming from order number.
     *
     * @throws PaymentProviderException when trying to crete the payment intent for oder without order number
     */
    public function createPaymentIntent(PaymentMethod $paymentMethod, Order $order): OrderPayment
    {
        if (Order::STATUS_NEW !== $order->getStatus()) {
            throw new \LogicException('Could not generate an payment intent because the payment order is already processing or finished.');
        }

        if (!$order->getOrderNumber()) {
            throw new PaymentProviderException('The payment intent for this order could not be created because, the order has no order number.');
        }

        $orderPayment = new OrderPayment();

        $orderPayment
            ->setOrder($order)
            ->setPaymentKey($order->getOrderNumber())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setPaymentMethod($paymentMethod)
            ->setcredentials([
                'accountNumber' => $this->getAccountNumber($paymentMethod),
                'variableNumber' => $this->getVariableNumber($paymentMethod, $order),
                'specificNumber' => $this->getSpecificNumber($paymentMethod, $order),
                'iban' => $this->getIban($paymentMethod),
                'swift' => $this->getSwiftCode($paymentMethod),
            ])
        ;

        $this->entityManager->persist($orderPayment);
        $this->entityManager->flush();

        return $orderPayment;
    }

    /**
     * Try to fetch e-mail transaction notifications from bank.
     *
     * @param string $user
     * @param string $password
     * @param string $host
     * @param int $port
     * @param string $path
     * @return void
     * @throws MaskNotFoundException
     * @throws \Webklex\PHPIMAP\Exceptions\AuthFailedException
     * @throws \Webklex\PHPIMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\PHPIMAP\Exceptions\FolderFetchingException
     * @throws \Webklex\PHPIMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\PHPIMAP\Exceptions\ImapBadRequestException
     * @throws \Webklex\PHPIMAP\Exceptions\ImapServerErrorException
     * @throws \Webklex\PHPIMAP\Exceptions\ResponseException
     * @throws \Webklex\PHPIMAP\Exceptions\RuntimeException
     */
    public function checkBankTransactionNotificationEmail(
        string $user,
        string $password,
        string $host,
        int $port,
        string $expectedSender,
        string $path = 'INBOX',
    ) : void {
        $this->logger->debug('Checking the bank transaction notification email, now trying to connect with provided credentials.', [
            'user' => $user,
            'host' => $host,
            'port' => $port,
            'path' => $path,
        ]);

        $client = (new ClientManager())->make([
            'username' => $user,
            'password' => $password,
            'host' => $host,
            'port' => $port,
            'protocol' => 'imap',
        ]);

        $client->connect();

        $this->logger->debug('Connected to the email server, now trying to fetch all folders and find the correct one with path {path} ', [
            'path' => $path,
        ]);

        $folders = $client->getFolders();

        foreach ($folders as $folder) {
            if ($folder->path !== $path) {
                continue;
            }


            $this->logger->debug('Found the correct folder at {path}, now trying to fetch all messages.', [
                'path' => $path,
            ]);

            foreach ($folder->messages()->all()->get() as $message) {
                if ($message->from->get()->mail !== $expectedSender) {
                    continue;
                }

                $this->logger->debug('Found the correct message from the expected sender, now trying to parse the message content.', [
                    'message' => $message->subject->get(),
                ]);

                $result = match ($expectedSender) {
                    'infoservis@moneta.cz' => $this->parseMonetaIncomingEmailBody($message->getHTMLBody()),
                    default => null,
                };

                $this->logger->debug('Parsed the message content, payment {amount} from {sender} with variable symbol {vs} has been found.', [
                    'amount' => $result->getAmount(),
                    'sender' => $result->getAccountNumber(),
                    'vs' => $result->getVariableNumber(),
                ]);

                if ($result->getVariableNumber() === null) {
                    $this->logger->error('Could not find the variable number in the incoming payment email.');
                    continue;
                }


                try {
                    $message->delete(false);
                } catch (\Throwable $e) {
                    $this->logger->error('Could not delete the message from the server.', [
                        'error' => $e->getMessage(),
                    ]);
                }


                // now find order by the variable number
                $order = $this->entityManager->getRepository(Order::class)->findOneBy([
                    'orderNumber' => $result->getVariableNumber(),
                ]);

                if (null === $order) {
                    $this->logger->error('Could not find the order with the variable number {vs}.', [
                        'vs' => $result->getVariableNumber(),
                    ]);

                    continue;
                }

                if ($order->getStatus() !== Order::STATUS_PAID) {
                    if ($order->getTotalOrderPriceWithTax() !== $result->getAmount()){
                        $this->logger->error('The order with the variable number {vs} has different price than the incoming payment.', [
                            'expected' => $order->getTotalOrderPriceWithTax(),
                            'actual' => $result->getAmount(),
                            'vs' => $result->getVariableNumber(),
                        ]);

                        continue;
                    }

                    $order->getPayment()->setStatus(OrderPayment::STATUS_PAID);
                    $order->getPayment()->setPaidAt(new \DateTimeImmutable());

                    $order->setStatus(Order::STATUS_PAID);

                    $this->messageBus->dispatch(new OrderPaidMessage($order->getId()));

                    $this->logger->info('Order has been paid by tht bank transfer.');
                }

            }
        }

    }

    /**
     * Try to parse the incoming email from Moneta bank.
     *
     * @param string $rawMessageBody
     * @return BankTransferPayment
     */
    private function parseMonetaIncomingEmailBody(string $rawMessageBody): BankTransferPayment
    {
        // Regex to find the price
        $priceRegex = "/Částka:<\/td>\s*<td[^>]*>([\d\s,]+) Kč/";
        $price = null;
        if (preg_match($priceRegex, $rawMessageBody, $priceMatches)) {
            // Remove spaces and replace comma with dot, then convert to float and multiply by 100 for an integer representation
            $amountString = str_replace(' ', '', $priceMatches[1]);
            $amountString = str_replace(',', '.', $amountString);
            $price = (int)round((float)$amountString * 100);
        }

        // Regex to ensure VS is in a td with "Popis:"
        $vsRegex = "/Popis:<\/td>\s*<td[^>]*>.*?VS:\s*(\d+)/";
        $variableSymbol = null;
        if (preg_match($vsRegex, $rawMessageBody, $vsMatches)) {
            $variableSymbol = $vsMatches[1] ?? null;
        }

        // Regex to find the sender
        $senderRegex = "/Od:<\/td>\s*<td[^>]*>([\d\/]+)<\/td>/";
        $sender = null;
        if (preg_match($senderRegex, $rawMessageBody, $senderMatches)) {
            $sender = $senderMatches[1] ?? null;
        }

        return new BankTransferPayment(
            transactionAt: new \DateTimeImmutable(),
            accountNumber: $sender,
            amount: $price, // Now amount is an integer representing cents
            variableNumber: $variableSymbol,
        );
    }

    /**
     * Get the correct bank account number for the given order and the payment method.
     *
     * @param PaymentMethod $paymentMethod
     * @return string|null
     */
    private function getAccountNumber(PaymentMethod $paymentMethod): ?string
    {
        return $paymentMethod->getCredentialExpressions()['accountNumber'] ?? null;
    }

    /**
     * Get correct variable number for the given order and the payment method.
     */
    private function getVariableNumber(PaymentMethod $paymentMethod, Order $order): ?string
    {
        $expressionLanguage = new ExpressionLanguage();

        if (isset($paymentMethod->getCredentialExpressions()['variableNumber'])) {
            $variableNumber = $expressionLanguage->evaluate($paymentMethod->getCredentialExpressions()['variableNumber'], [
                'order' => $order,
            ]);

            if (!$variableNumber) {
                return $order->getOrderNumber();
            }

            return (string) $variableNumber;
        }

        return $order->getOrderNumber();
    }

    /**
     * Get correct specific number for the given order and the payment method.
     */
    private function getSpecificNumber(PaymentMethod $paymentMethod, Order $order): ?string
    {
        $expressionLanguage = new ExpressionLanguage();

        if (isset($paymentMethod->getCredentialExpressions()['specificNumber'])) {
            $specificNumber = $expressionLanguage->evaluate($paymentMethod->getCredentialExpressions()['specificNumber'], [
                'order' => $order,
            ]);

            if (!$specificNumber) {
                return null;
            }

            return (string) $specificNumber;
        }

        return null;
    }

    private function getIban(PaymentMethod $paymentMethod): ?string
    {
        return $paymentMethod->getCredentialExpressions()['iban'] ?? null;
    }

    private function getSwiftCode(PaymentMethod $paymentMethod): ?string
    {
        return $paymentMethod->getCredentialExpressions()['swift'] ?? null;
    }
}
