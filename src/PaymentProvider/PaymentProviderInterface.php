<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\PaymentProvider;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;

/**
 * This interface must be implemented by all payment provider classes.
 */
interface PaymentProviderInterface
{
    /** @var string The payment is pending, waiting for payment confirmation etc. */
    public const string STATUS_PENDING = 'pending';

    /** @var string The payment has been paid. */
    public const string STATUS_PAID = 'paid';

    /**
     * Create a new payment intent.
     *
     * @param Order $order the Payment order for the current payment intent
     *
     * @return mixed
     */
    public function createPaymentIntent(PaymentMethod $paymentMethod, Order $order): OrderPayment;
}
