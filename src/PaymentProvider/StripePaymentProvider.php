<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\PaymentProvider;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Message\Eshop\OrderPaidMessage;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\SignatureVerificationException;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\StripeClient;
use Stripe\Webhook;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

final readonly class StripePaymentProvider implements PaymentProviderInterface
{
    public const string PROVIDER_NAME = 'stripe';

    /**
     * @param string|null $stripeSecret   The API secret key available from the https://dashboard.stripe.com/test/apikeys
     * @param string|null $endpointSecret The webhook endpoint secret available from the https://dashboard.stripe.com/test/webhooks
     * @param string|null $publishableKey The API publishable key https://stripe.com/docs/keys
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MessageBusInterface $bus,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_STRIPE_RESTRICTED_SECRET')]
        private ?string $stripeSecret = null,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_STRIPE_ENDPOINT_SECRET')]
        private ?string $endpointSecret = null,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_STRIPE_PUBLISHABLE_KEY')]
        private ?string $publishableKey = null
    ) {
    }

    /**
     * Create a payment intent and return the client secret for the given intent.
     *
     * @throws ApiErrorException when the client secret could not be generated
     *
     * @return OrderPayment The OrderPayment with the PaymentKey used as the secret for the https://stripe.com/docs/js/payment_intents/confirm_card_payment API.
     */
    public function createPaymentIntent(PaymentMethod $paymentMethod, Order $order): OrderPayment
    {
        if (Order::STATUS_NEW !== $order->getStatus()) {
            throw new \LogicException('Could not generate a payment link because the payment order is already processing or finished.');
        }

        Stripe::setApiKey($this->stripeSecret);

        $intent = PaymentIntent::create([
            'amount' => $order->getPriceWithTax() + $order->getShippingPriceWithTax(),
            'currency' => $order->getCurrency(),
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);

        $orderPayment = new OrderPayment();
        $orderPayment->setOrder($order);
        $orderPayment->setPaymentKey($intent->id);
        $orderPayment->setStatus(OrderPayment::STATUS_PENDING);
        $orderPayment->setPaymentMethod($paymentMethod);
        $order->setCreatedAt(new \DateTimeImmutable());
        $this->entityManager->persist($orderPayment);
        $this->entityManager->flush();

        return $orderPayment;
    }

    /**
     * Handles stripe webhook.
     *
     * @throws SignatureVerificationException
     * @throws \Exception
     */
    public function handleWebhook(Request $request): void
    {
        $event = Webhook::constructEvent($request->getContent(), $request->headers->get('stripe-signature'), $this->endpointSecret);

        if ('payment_intent.succeeded' === $event->type) {
            $orderPayment = $this->entityManager->getRepository(OrderPayment::class)->findOneBy(['paymentKey' => $event->data->object->id, 'status' => OrderPayment::STATUS_PENDING]);

            if (!$orderPayment) {
                throw new \Exception(sprintf('There is no pending payment order with the given payment id "%s"', $event->data->object->id));
            }

            $orderPayment->setStatus(OrderPayment::STATUS_PAID);
            $orderPayment->getOrder()->setStatus(Order::STATUS_IN_PROGRESS);
            $this->entityManager->flush();

            $this->bus->dispatch(new OrderPaidMessage($orderPayment->getOrder()->getId()));
        }
    }

    /**
     * Return the payment intent information for the given OrderPayment or null, null will
     * be returned when the intent could not be fetched from the Stripe API.
     */
    public function getPaymentIntent(OrderPayment $orderPayment): ?PaymentIntent
    {
        try {
            $stripe = $stripe = new StripeClient($this->stripeSecret);

            return $stripe->paymentIntents->retrieve($orderPayment->getPaymentKey());
        } catch (ApiErrorException $apiErrorException) {
            return null;
        }
    }

    /**
     * Get Stripe credentials for the given OrderPayment.
     */
    public function getClientCredentials(OrderPayment $orderPayment): array
    {
        $intent = $this->getPaymentIntent($orderPayment);

        return [
            'clientSecret' => $intent?->client_secret,
            'publishableKey' => $this->publishableKey,
        ];
    }

    /**
     * Manually verify the payment status from the Stripe API for the given OrderPayment.
     */
    public function verifyConfirmPayment(OrderPayment $orderPayment): void
    {
        $paymentIntent = $this->getPaymentIntent($orderPayment);
        if ('succeeded' === $paymentIntent['status'] && OrderPayment::STATUS_PENDING === $orderPayment->getStatus()) {
            $orderPayment->setStatus(OrderPayment::STATUS_PAID);
            $orderPayment->getOrder()->setStatus(Order::STATUS_IN_PROGRESS);
            $orderPayment->setPaidAt(new \DateTimeImmutable());
            $this->entityManager->flush();

            $this->bus->dispatch(new OrderPaidMessage($orderPayment->getOrder()->getId()));
        }
    }
}
