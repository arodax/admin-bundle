<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\PaymentProvider;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\OrderPayment;
use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Exception\PaymentProviderException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class DefaultPaymentProvider implements PaymentProviderInterface
{
    public const PROVIDER_NAME = 'default';

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected MessageBusInterface $messageBus,
        protected LoggerInterface $logger,
    ) {
    }

    /**
     * Create the new OrderPayment for the given Order with the paymentKey coming from order number.
     *
     * @throws PaymentProviderException when trying to crete the payment intent for oder without order number
     */
    public function createPaymentIntent(PaymentMethod $paymentMethod, Order $order): OrderPayment
    {
        if (Order::STATUS_NEW !== $order->getStatus()) {
            throw new \LogicException('Could not generate a payment link because the payment order is already processing or finished.');
        }

        if (!$order->getOrderNumber()) {
            throw new PaymentProviderException('The payment intent for this order could not be created because, the order has no order number.');
        }

        $orderPayment = new OrderPayment();

        $orderPayment
            ->setOrder($order)
            ->setPaymentKey($order->getOrderNumber())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setPaymentMethod($paymentMethod)
        ;

        $this->entityManager->persist($orderPayment);
        $this->entityManager->flush();

        return $orderPayment;
    }
}
