<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * This class extends Twig with some usefully methods for working with routes.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class RouterRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private RouterInterface $router,
        private RequestStack $requestStack
    ) {
    }

    /**
     * Get alternate path for the current request as array in given locales.
     */
    public function i18nRequestPaths(array $locales = ['cs', 'en', 'sk', 'de', 'pl', 'hu', 'ro']): array
    {
        $paths = [];

        foreach ($locales as $locale) {
            $paths[$locale]['code'] = $locale;
            $paths[$locale]['name'] = Languages::getName($locale, $locale);
            $paths[$locale]['path'] = $this->i18nRequestPath($locale);
        }

        return $paths;
    }

    /**
     * Get alternate path for the current request.
     */
    public function i18nRequestPath(string $locale): ?string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request instanceof Request) {
            return null;
        }

        $route = $request->get('_route');
        $routeParams = $request->attributes->get('_route_params');
        $queryParams = $request->query->all();

        try {
            return $this->router->generate($route, array_merge($routeParams ?? [], $queryParams, ['_locale' => $locale]));
        } catch (\Throwable) {
            return null;
        }
    }
}
