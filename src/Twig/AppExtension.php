<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * This class lazy loads all Twig extensions (functions, filters etc.) from the runtime class.
 */
class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('codeToLanguage', [IntlRuntime::class, 'codeToLanguage']),
            new TwigFilter('serialize', [SerializerRuntime::class, 'serialize']),
            new TwigFilter('money', [MoneyRuntime::class, 'format']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('contentLocales', [IntlRuntime::class, 'contentLocales']),
            new TwigFunction('adminLocales', [IntlRuntime::class, 'adminLocales']),
            new TwigFunction('i18nRequestPath', [RouterRuntime::class, 'i18nRequestPath']),
            new TwigFunction('i18nRequestPaths', [RouterRuntime::class, 'i18nRequestPaths']),
            new TwigFunction('publishableKey', fn(): ?string => (new StripeRuntime())->publishableKey()),
            new TwigFunction('settingsValue', [SettingsRuntime::class, 'getValue']),
        ];
    }
}
