<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Arodax\AdminBundle\Entity\Settings;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\RuntimeExtensionInterface;

readonly class SettingsRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function getValue(string $key, ?string $default = null): ?string
    {
        $value = $this->entityManager->getRepository(Settings::class)->findOneBy(['key' => $key])?->getValue();

        if ($value === null) {
            return $default;
        }

        return $value;
    }
}
