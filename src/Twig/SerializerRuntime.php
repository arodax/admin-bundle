<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Symfony\Component\Serializer\SerializerInterface;
use Twig\Extension\RuntimeExtensionInterface;

readonly class SerializerRuntime implements RuntimeExtensionInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    /**
     * This method will serialize data using the symfony serializer.
     *
     * @param object $data   Data for the serialization
     * @param string $format Format to which data should be serialized
     *
     * @return string Language name
     */
    public function serialize(mixed $data, string $format = 'json', array $context = []): string
    {
        return $this->serializer->serialize($data, $format, $context);
    }
}
