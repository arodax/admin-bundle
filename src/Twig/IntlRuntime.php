<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Arodax\AdminBundle\I18n\LocalesUtilityInterface;
use Symfony\Component\Intl\Languages;
use Twig\Extension\RuntimeExtensionInterface;

readonly class IntlRuntime implements RuntimeExtensionInterface
{
    public function __construct(private LocalesUtilityInterface $localeUtility)
    {
    }

    /**
     * This method will parse language code (en, cs, ru ..) to language name.
     *
     * @param string $code                Code of the language
     * @param string $translationLanguage To what language, the return value should be translated. By default, language will be translated to its original name.
     *
     * @return string Language name
     */
    public function codeToLanguage(string $code, $translationLanguage = null): string
    {
        return Languages::getName($code, $translationLanguage ?? $code);
    }

    /**
     * This method will return available content locales array.
     */
    public function contentLocales(): array
    {
        return $this->localeUtility->getContentLocales();
    }

    /**
     * This method will return available admin locales array.
     */
    public function adminLocales(): array
    {
        return $this->localeUtility->getContentLocales();
    }
}
