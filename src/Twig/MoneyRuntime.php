<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Twig\Extension\RuntimeExtensionInterface;

class MoneyRuntime implements RuntimeExtensionInterface
{
    public function __construct(private ?string $locale = null)
    {
        if (null === $this->locale) {
            $this->locale = \Locale::getDefault();
        }
    }

    private array $currencySymbols = [
        'CZK' => 'Kč',
        'USD' => '$',
        'EUR' => '€',
        'GBP' => '£',
        'JPY' => '¥',
        'AUD' => 'A$',
        'CAD' => 'C$',
        'CHF' => 'Fr.',
        'CNY' => '¥',
        'SEK' => 'kr',
        'NZD' => 'NZ$',
        'ALL' => 'Lek',
        'AFN' => '؋',
        'ARS' => '$',
        'AWG' => 'ƒ',
        'AZN' => '₼',
        'BYN' => 'Br',
        'BZD' => 'BZ$',
        'BOB' => '$b',
        'BAM' => 'KM',
        'BWP' => 'P',
        'BGN' => 'лв',
        'BRL' => 'R$',
        'KHR' => '៛',
        'CRC' => '₡',
        'HRK' => 'kn',
        'CUP' => '₱',
        'DOP' => 'RD$',
        'EGP' => '£',
        'FKP' => '£',
        'GHS' => '¢',
        'GTQ' => 'Q',
        'GYD' => '$',
        'HNL' => 'L',
        'HKD' => '$',
        'HUF' => 'Ft',
        'ISK' => 'kr',
        'INR' => '₹',
        'TWD' => 'NT$',
        'TTD' => 'TT$'
    ];

    /**
     * This method will format money to user-friendly format.
     *
     * @param mixed $moneyAmount atomic money amount
     * @param string|null $currency The money currency
     * @param string|null $locale The optional locale
     *
     * @return string Language name
     */
    public function format(
        mixed $moneyAmount,
        ?string $currency = 'CZK',
        ?string $locale = null,
        ?int $decimalPlaces = 2
    ): string {
        if (null === $locale) {
            $locale = $this->locale;
        }

        if (null === $currency) {
            $currency = 'CZK';
        }

        $moneyAmount = (float)$moneyAmount / 100;

        $formatter = new \NumberFormatter($locale, \NumberFormatter::DECIMAL);
        $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, 0);
        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $decimalPlaces ?? 2);

        $formattedAmount = $formatter->format($moneyAmount);

        // Use the symbol from the mapping array if it exists, otherwise use the currency code.
        $currencySymbol = $this->currencySymbols[$currency] ?? $currency;

        return $formattedAmount . ' ' . $currencySymbol;
    }
}
