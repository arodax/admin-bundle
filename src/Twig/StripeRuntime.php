<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Twig;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Twig\Extension\RuntimeExtensionInterface;

readonly class StripeRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_STRIPE_PUBLISHABLE_KEY')]
        private ?string $publishableKey = null
    ) {
    }

    /**
     * Get the publishable key for the stripe API.
     */
    public function publishableKey(): ?string
    {
        return $this->publishableKey;
    }
}
