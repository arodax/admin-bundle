<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Menu;

use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Arodax\AdminBundle\Repository\NestedTreeRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method MenuItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuItem[]    findAll()
 * @method MenuItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuItemRepository extends NestedTreeRepository
{
    public function childrenQueryBuilder($node = null, $direct = false, $sortByField = null, $direction = 'ASC', $includeNode = false)
    {
        $qb = parent::childrenQueryBuilder($node, $direct, $sortByField, $direction, $includeNode);
        $qb->leftJoin('node.translations', 'translations')->addSelect('translations');
        $qb->leftJoin('node.parent', 'parent')->addSelect('parent');

        return $qb;
    }

    /**
     * Find single menu item by it's node name.
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function findByNodeName(string $nodeName)
    {
        $qb = $this->createQueryBuilder('menu_item');
        $qb->where($qb->expr()->eq('menu_item.nodeName', ':node_name'));
        $qb->setParameter('node_name', $nodeName);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
