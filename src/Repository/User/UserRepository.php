<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\User;

use Arodax\AdminBundle\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByEmail(?string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find all users who are receivers of the admin notifications.
     */
    public function findAdminRecipients(): ?array
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->like('u.roles', ':roles'));
        $qb->setParameter('roles', '%ROLE_ADMIN_RECIPIENT%');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all users who are recievers of the eshop noficiations.
     */
    public function findEshopRecipients(): ?array
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->like('u.roles', ':roles'));
        $qb->setParameter('roles', '%ROLE_ADMIN_ESHOP_RECIPIENT%');

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all users with, with ROLE_ADMIN permission.
     */
    public function findAdmins(): ?array
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->like('u.roles', ':roles'));
        $qb->setParameter('roles', '%ROLE_ADMIN%');

        return $qb->getQuery()->getResult();
    }
}
