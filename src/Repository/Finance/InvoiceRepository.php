<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Finance;

use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InvoiceCzech|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceCzech|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceCzech[]    findAll()
 * @method InvoiceCzech[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvoiceCzech::class);
    }
}
