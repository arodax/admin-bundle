<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductVariant|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductVariant|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductVariant[]    findAll()
 * @method ProductVariant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductVariantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductVariant::class);
    }

    /**
     * Get available published product variant.
     *
     * @throws NonUniqueResultException
     */
    public function findAvailable(int $id): ?ProductVariant
    {
        $qb = $this->createQueryBuilder('product_variant');

        return $qb
            ->join('product_variant.product', 'product')
            ->where($qb->expr()->eq('product_variant.id', ':id'))
            ->setParameter('id', $id)
            ->andWhere($qb->expr()->lte('product.publishedAt', ':now'))
            ->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
