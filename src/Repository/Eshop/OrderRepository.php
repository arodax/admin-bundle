<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Eshop;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * Find the order associated with the given shopping cart.
     *
     * @throws NonUniqueResultException
     */
    public function findOneByShoppingCart(ShoppingCart $shoppingCart): ?Order
    {
        $qb = $this->createQueryBuilder('order');
        $qb->where($qb->expr()->eq('order.shoppingCart', 'shoppingCart'));
        $qb->setParameter('shoppingCart', $shoppingCart);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
