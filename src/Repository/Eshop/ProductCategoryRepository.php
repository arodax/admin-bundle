<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Arodax\AdminBundle\Repository\NestedTreeRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method ProductCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCategory[]    findAll()
 * @method ProductCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCategoryRepository extends NestedTreeRepository
{
    public function childrenQueryBuilder($node = null, $direct = false, $sortByField = null, $direction = 'ASC', $includeNode = false)
    {
        $qb = parent::childrenQueryBuilder($node, $direct, $sortByField, $direction, $includeNode);
        $qb->leftJoin('node.translations', 'translations')->addSelect('translations');
        $qb->leftJoin('node.parent', 'parent')->addSelect('parent');

        return $qb;
    }

    /**
     * Get available published product category.
     *
     * @throws NonUniqueResultException
     */
    public function findAvailableBySlug(string $slug): ?ProductCategory
    {
        $qb = $this->createQueryBuilder('product_category');

        return $qb
            ->join('product_category.translations', 'translations')
            ->where($qb->expr()->eq('translations.slug', ':slug'))
            ->setParameter('slug', $slug)
            ->andWhere($qb->expr()->lte('product_category.publishedAt', ':now'))
            ->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
