<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Eshop;

use Arodax\AdminBundle\Entity\Eshop\Product;
use Arodax\AdminBundle\Model\Eshop\ProductInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * Find all available products.
     *
     * @param int         $id    The category id
     * @param string|null $sort  The property which will be used for sorting
     * @param string|null $order The sort order (ASC, DESC)
     *
     * @return Product[]|null
     */
    public function findByAvailable(?string $sort = 'product.publishedAt', ?string $order = 'ASC')
    {
        $qb = $this->createAvailableProductsQueryBuilder($sort, $order);

        return $qb
            ->getQuery()
            ->getResult();
    }

    /**
     * Find all available products in the given section id.
     *
     * @param int         $id    The category id
     * @param string|null $sort  The property which will be used for sorting
     * @param string|null $order The sort order (ASC, DESC)
     *
     * @return Product[]|null
     */
    public function findByAvailableCategoryId(int $id, ?string $sort = 'product.publishedAt', ?string $order = 'ASC')
    {
        $qb = $this->createAvailableProductsQueryBuilder($sort, $order);

        return $qb
            ->andWhere($qb->expr()->eq('categories.id', ':id'))->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    private function createAvailableProductsQueryBuilder(?string $sort = 'product.publishedAt', ?string $order = 'ASC'): QueryBuilder
    {
        $qb = $this->createQueryBuilder('product');

        return $products = $qb->select('product')
            ->join('product.categories', 'categories')
            ->where($qb->expr()->eq('product.status', ':status'))->setParameter('status', ProductInterface::STATUS_PUBLISHED)
            ->andwhere($qb->expr()->lte('product.publishedAt', ':now'))->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
            ->orderBy($sort, $order);
    }
}
