<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCartItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShoppingCartItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingCartItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingCartItem[]    findAll()
 * @method ShoppingCartItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingCartItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCartItem::class);
    }

    public function findOneByProductInCart(ProductVariant $productVariant, ShoppingCart $shoppingCart)
    {
        $qb = $this->createQueryBuilder('shopping_cart_item');
        $qb->join('shopping_cart_item.shoppingCart', 'shopping_cart');
        $qb->join('shopping_cart_item.productVariant', 'product_variant');
        $qb->where($qb->expr()->eq('shopping_cart.id', ':shopping_cart'))->setParameter('shopping_cart', $shoppingCart->getId());
        $qb->andWhere($qb->expr()->eq('product_variant.id', ':product_variant'))->setParameter('product_variant', $productVariant->getId());

        return $qb->getQuery()->getOneOrNullResult();
    }
}
