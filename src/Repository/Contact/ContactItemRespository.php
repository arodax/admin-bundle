<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Contact;

use Arodax\AdminBundle\Entity\Contact\ContactItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactItem[]    findAll()
 * @method ContactItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactItemRespository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactItem::class);
    }
}
