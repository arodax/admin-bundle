<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Content;

use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Repository\NestedTreeRepository;

/**
 * @method Section|null find($id, $lockMode = null, $lockVersion = null)
 * @method Section|null findOneBy(array $criteria, array $orderBy = null)
 * @method Section[]    findAll()
 * @method Section[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SectionRepository extends NestedTreeRepository
{
    public function childrenQueryBuilder($node = null, $direct = false, $sortByField = null, $direction = 'ASC', $includeNode = false)
    {
        $qb = parent::childrenQueryBuilder($node, $direct, $sortByField, $direction, $includeNode);
        $qb->leftJoin('node.translations', 'translations')->addSelect('translations');
        $qb->leftJoin('node.parent', 'parent')->addSelect('parent');

        return $qb;
    }
}
