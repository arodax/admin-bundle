<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Content;

use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Model\Content\ArticleInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use function Symfony\Component\String\u;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * Find the published article by it's slug. This is usefull method with the combiation of the Param Converter of the
     * typehinted article.

     *
     * @example how to use this method in the controller:
     *
     * Route("/{slug}", name="default_help", methods={"GET"})
     * Entity("article", expr="repository.findOnePublishedBySlug(slug, _locale)")
     *
     * @param string $slug   The article slug
     * @param string $locale The locale for the article
     *
     * @return mixed
     *
     * @throws NonUniqueResultException when more then 1 article is found with the same slug in the requested locale
     * @throws \Exception               When date could not be created
     */
    public function findOnePublishedBySlug(string $slug, string $locale)
    {
        $qb = $this->createQueryBuilder('article');

        return $qb->join('article.translations', 'translations')
            ->where($qb->expr()->lte('article.publishedAt', ':now'))->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
            ->andWhere($qb->expr()->eq('translations.slug', ':slug'))->setParameter('slug', $slug)
            ->andWhere($qb->expr()->eq('translations.locale', ':locale'))->setParameter('locale', $locale)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find the published article by it's slug and section path.
     * This is usefull method with the combiation of the Param Converter of the
     * typehinted article.
     *
     * @example how to use this method in the controller:
     *
     * Route("/{path}/{slug}", name="default_help", methods={"GET"})
     * Entity("article", expr="repository.findOnePublishedByPathSlug(path, slug, _locale)")
     *
     * @param string $path   The section path
     * @param string $slug   The article slug
     * @param string $locale the locale for the article, the same locale must be applied to the section path
     *
     * @return mixed
     *
     * @throws NonUniqueResultException when more then 1 article is found with the same slug in the requested locale
     * @throws \Exception               When date could not be created
     */
    public function findOnePublishedbyPathSlug(string $path, string $slug, string $locale)
    {
        $qb = $this->createQueryBuilder('article');

        return $qb
            ->join('article.translations', 'translations')
            ->join('article.sections', 'sections')
            ->join('sections.translations', 'sections_translations')
            ->where($qb->expr()->lte('article.publishedAt', ':now'))->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
            ->andWhere($qb->expr()->eq('translations.slug', ':slug'))->setParameter('slug', $slug)
            ->andWhere($qb->expr()->eq('translations.locale', ':locale'))->setParameter('locale', $locale)
            ->andWhere($qb->expr()->eq('sections_translations.path', ':path'))->setParameter('path', $path)
            ->andWhere($qb->expr()->lte('sections.publishedAt', ':now'))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find the published article by it's full path (usually from URL).
     *
     * @see ArticleRepository::findOnePublishedbyPathSlug for the further description.
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function findOnePublishedByPath(string $path, string $locale)
    {
        $paths = explode('/', $path);

        // expect at least section + article slug
        if (\count($paths) < 2) {
            return null;
        }

        $slug = array_pop($paths);

        return $this->findOnePublishedbyPathSlug(implode('/', $paths), $slug, $locale);
    }

    /**
     * Find the published articles by section id, to which articles belong.
     *
     * Find the published articles by its section id.
     *
     * @param int|array<int> $id The section id or array of multiple ids.
     * @param string|null $sort The property which will be used for sorting
     * @param string|null $order The sort order (ASC, DESC)
     * @param int|null $limit The limit of the results
     * @param string|null $onlyLocale The locale in which the article should be returned
     * @param array|null $skipIds The ids which should be skipped
     *
     * @return int|mixed|string
     * @throws \Exception
     */
    public function findPublishedBySectionId(
        int|array $id,
        ?string $sort = 'article.publishedAt',
        ?string $order = 'ASC',
        ?int $limit = null,
        ?string $onlyLocale = null,
        ?array $skipIds = null,
    ) {
        $qb = $this->createQueryBuilder('article');

        if (is_int($id)) {
            $id = [$id];
        }

         $qb->select('article')
            ->join('article.sections', 'sections')
            ->where($qb->expr()->eq('article.status', ':status'))->setParameter('status', ArticleInterface::STATUS_PUBLISHED)
            ->andWhere($qb->expr()->in('sections.id', $id))
            ->andwhere($qb->expr()->lte('article.publishedAt', ':now'))->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')))
         ;

        if (null !== $skipIds) {
            $qb->andWhere($qb->expr()->notIn('article.id', $skipIds));
        }

        $qb->orderBy($sort, $order);

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $onlyLocale) {
            $qb->join('article.translations', 'translations')
                ->andWhere('translations.locale', ':locale')
                ->setParameter('locale', $onlyLocale)
            ;
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Find article by fulltext like queyr with addiational parameters.
     *
     * @param string      $query     The string we are searching for
     * @param string|null $locale    Optional locale
     * @param array|null  $sections  Optional sections
     * @param bool|null   $published Search only published articles?
     * @param string|null $sort      The property which will be used for sorting
     * @param string|null $order     The sort order (ASC, DESC)
     *
     * @return int|mixed|string
     *
     * @throws \Exception
     */
    public function findArticleFulltext(string $query, ?string $locale = null, ?array $sections = null, ?bool $published = true, ?string $sort = 'article.publishedAt', ?string $order = 'ASC')
    {
        $qb = $this->createQueryBuilder('article');

        $qb->select('article')
            ->join('article.translations', 'translations')
        ;

        // search in headline and content
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('translations.headline', ':headline'),
            $qb->expr()->like('translations.content', ':content')
        ))
            ->setParameter('headline', u($query)->prepend('%')->append('%')->toString())
            ->setParameter('content', u($query)->prepend('%')->append('%')->toString())
        ;

        // search articles only in specific sections
        if (!empty($sections)) {
            $qb->join('article.sections', 'sections');

            $sectionsIdentifiers = [];

            // allow to pass both numberic id and section objects
            foreach ($sections as $section) {
                $sectionsIdentifiers[] = $section instanceof Section ? $section->getId() : $section;
            }

            $qb->andWhere($qb->expr()->in('sections.id', ':sections'))
                ->setParameter('sections', $sectionsIdentifiers);
        }

        // search article in specific locale
        if (null !== $locale) {
            $qb->andWhere($qb->expr()->eq('translations.locale', ':locale'))->setParameter('locale', $locale);
        }

        // search only published articles
        if (true === $published) {
            $qb->andWhere($qb->expr()->lte('article.publishedAt', ':now'))->setParameter('now', new \DateTimeImmutable('now', new \DateTimeZone('UTC')));
        }

        $qb->orderBy($sort, $order);

        return $qb->getQuery()->getResult();
    }

    /**
     * @deprecated Method getOnePublishedBySlug is deprecated, use findOnePublishedBySlug
     *
     * @throws NonUniqueResultException
     */
    public function getOnePublishedBySlug(string $slug, string $locale): void
    {
        @trigger_error('Method getOnePublishedBySlug is deprecated, use findOnePublishedBySlug', \E_USER_DEPRECATED);
        $this->findOnePublishedBySlug($slug, $locale);
    }

    /**
     * @deprecated Method getPublishedBySectionId is deprecated, use findPublishedBySectionId
     */
    public function getPublishedBySectionId(int $id, ?string $sort = 'article.publishedAt', ?string $order = 'ASC'): void
    {
        @trigger_error('Method getPublishedBySectionId is deprecated, use findPublishedBySectionId', \E_USER_DEPRECATED);
        $this->findPublishedBySectionId($id, $sort, $order);
    }
}
