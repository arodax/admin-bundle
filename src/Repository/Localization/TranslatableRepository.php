<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Repository\Localization;

use Arodax\AdminBundle\Entity\Localization\Translatable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Translatable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Translatable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Translatable[]    findAll()
 * @method Translatable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TranslatableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Translatable::class);
    }

    public function findByLocale(string $locale)
    {
        $qb = $this->createQueryBuilder('translatable');

        return $qb
            ->join('translatable.translations', 't')
            ->where($qb->expr()->eq('t.locale', ':locale'))
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult();
    }
}
