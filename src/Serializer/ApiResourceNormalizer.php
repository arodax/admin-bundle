<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Serializer;

use Arodax\AdminBundle\Model\User\UserInterface;
use Arodax\AdminBundle\Serializer\Decorator\UserDecorator;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This normalizer allows to call decorators for normalized data.
 *
 *  Note that all injected services from Arodax\AdminBundle\Serializer\Decorator all lazy-loaded.
 */
final readonly class ApiResourceNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    public const array FORMAT = ['json', 'jsonld'];

    private DenormalizerInterface|NormalizerInterface $decorated;

    public function __construct(NormalizerInterface $decorated, private UserDecorator $userDecorator)
    {
        if (!$decorated instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException(sprintf('The decorated normalizer must implement the %s.', DenormalizerInterface::class));
        }

        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $data = $this->decorated->normalize($object, $format, $context);
        if (\is_array($data)) {
            $this->decorate($object, $data);
        }

        return $data;
    }

    public function setSerializer(SerializerInterface $serializer): void
    {
        if ($this->decorated instanceof SerializerAwareInterface) {
            $this->decorated->setSerializer($serializer);
        }
    }

    /**
     * Call decorators on the provided data.
     *
     * @param $object mixed Denormalized object
     * @param array $normalizedData Normalized data
     *
     * @return void Decorated data
     */
    private function decorate(mixed $object, array $normalizedData): void
    {
        if ($object instanceof UserInterface) {
            $normalizedData = $this->userDecorator->decorate($object, $normalizedData);
        }
    }

    /**
     *  Returns the types potentially supported by this normalizer.
     *
     *  For each supported formats (if applicable), the supported types should be
     *  returned as keys, and each type should be mapped to a boolean indicating
     *  if the result of supportsNormalization() can be cached or not
     *  (a result cannot be cached when it depends on the context or on the data.)
     *  A null value means that the normalizer does not support the corresponding
     *  type.
     *
     *  Use type "object" to match any classes or interfaces,
     *  and type "*" to match any types.
     *
     * @param string|null $format
     * @return array<class-string|'*'|'object'|string, bool|null>
     *
     * @return true[]
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            'object' => true,
        ];
    }
}
