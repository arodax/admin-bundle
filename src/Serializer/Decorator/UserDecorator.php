<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Serializer\Decorator;

use Arodax\AdminBundle\Model\User\UserInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class UserDecorator
{
    public function __construct(private readonly RoleHierarchyInterface $roleHierarchy)
    {
    }

    public function decorate(UserInterface $user, array $normalizedData): array
    {
        if (isset($normalizedData['roles']) && \is_array($normalizedData['roles'])) {
            $normalizedData['roles'] = $this->roleHierarchy->getReachableRoleNames($normalizedData['roles']);
        }

        return $normalizedData;
    }
}
