<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler\Eshop;

use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Message\Eshop\OrderPaidMessage;
use Arodax\AdminBundle\Notifier\EshopNotifier;
use Doctrine\ORM\EntityManagerInterface;

#[AsMessageHandler]
final readonly class OrderPaidHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private EshopNotifier $eshopNotifier,
        private LoggerInterface $logger,
    ) {
    }

    public function __invoke(OrderPaidMessage $message): void
    {
        $order = $this->entityManager->getRepository(Order::class)->find($message->getOrderId());

        if ($order === null) {
            $this->logger->error('Order not found', ['order_id' => $message->getOrderId()]);
            return;
        }

        $this->eshopNotifier->notifySeller(order: $order, subject: 'The order has been paid', template: '@ArodaxAdmin/emails/order_paid_seller.html.twig');
    }
}
