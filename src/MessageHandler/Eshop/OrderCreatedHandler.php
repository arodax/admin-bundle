<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler\Eshop;

use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Message\Eshop\OrderCreatedMessage;
use Arodax\AdminBundle\Notifier\EshopNotifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

#[AsMessageHandler]
final readonly class OrderCreatedHandler
{
    public function __construct(private EntityManagerInterface $entityManager, private EshopNotifier $eshopNotifier)
    {
    }
    public function __invoke(OrderCreatedMessage $message)
    {
        $order = $this->entityManager->getRepository(Order::class)->find($message->getOrderId());
        if ($order !== null) {
            $this->eshopNotifier->notifySeller($order, 'A new order from eshop', '@ArodaxAdmin/emails/eshop_order_open_admin.html.twig');
            $this->eshopNotifier->notifyBuyer($order, 'A new order from eshop', '@ArodaxAdmin/emails/eshop_order_open_buyer.html.twig');
        }
    }
}
