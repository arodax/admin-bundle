<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler;

use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Message\LostPasswordMessage;
use Arodax\AdminBundle\Repository\User\UserRepository;
use Arodax\AdminBundle\Security\UserPasswordManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

#[AsMessageHandler]
final readonly class LostPasswordHandler
{
    public function __construct(private UserRepository $userRepository, private UserPasswordManager $userPasswordManager, private LoggerInterface $logger)
    {
    }
    public function __invoke(LostPasswordMessage $lostPassword)
    {
        $email = $lostPassword->getEmail();
        $user = $this->userRepository->findOneByEmail($email);

        if (!$user instanceof User) {
            $this->logger->notice('Requested a lost password recovery for invalid email {email}', ['email' => $email]);

            return;
        }

        $this->userPasswordManager->requestLostPassword($user);
    }
}
