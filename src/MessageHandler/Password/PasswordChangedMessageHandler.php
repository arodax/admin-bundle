<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler\Password;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserPasswordLog;
use Arodax\AdminBundle\Message\Password\PasswordChangedMessage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler(handles: PasswordChangedMessage::class)]
final readonly class PasswordChangedMessageHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * Log user password change into the log.
     *
     * @param PasswordChangedMessage $message
     * @return void
     */
    public function __invoke(PasswordChangedMessage $message): void
    {
        $user = $this->entityManager->find(User::class, $message->getUserId());

        if (null !== $message->getUpdatedByUserId()) {
            $passwordChangedBy = $this->entityManager->find(User::class, $message->getUpdatedByUserId());
        } else {
            $passwordChangedBy = null;
        }


        if (null === $user) {
            return;
        }

        $qb = $this->entityManager->createQueryBuilder();

        /** @var UserPasswordLog $lastLog */
        $lastLog = $qb->select(select: 'user_password_log')
            ->from(from: UserPasswordLog::class, alias: 'user_password_log')
            ->where('user_password_log.user = :user')
            ->setParameter('user', $user)
            ->orderBy('user_password_log.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (null !== $lastLog && $lastLog->getCurrentPasswordHash() === $message->getCurrentPasswordHash()) {
            return;
        }

        $userPasswordLog = new UserPasswordLog();
        $userPasswordLog->setUser($user);
        $userPasswordLog->setCurrentPasswordHash($message->getCurrentPasswordHash());

        $userPasswordLog->setUpdatedBy($passwordChangedBy);

        $userPasswordLog->setCreatedAt(new \DateTimeImmutable());

        $this->entityManager->persist($userPasswordLog);
        $this->entityManager->flush();
    }
}
