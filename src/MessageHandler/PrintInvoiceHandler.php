<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler;

use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Arodax\AdminBundle\Message\PrintInvoiceMessage;
use Arodax\AdminBundle\Printer\InvoicePrinter;
use Arodax\AdminBundle\Repository\Finance\InvoiceRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

#[AsMessageHandler]
final readonly class PrintInvoiceHandler
{
    public function __construct(private InvoiceRepository $invoiceRepository, private LoggerInterface $logger, private InvoicePrinter $invoicePrinter)
    {
    }
    public function __invoke(PrintInvoiceMessage $printInvoice)
    {
        $invoice = $this->invoiceRepository->find($printInvoice->getInvoiceId());

        if (!$invoice instanceof InvoiceCzech) {
            $this->logger->notice('Requested to print an invoice id {id} but this invoice does not exist.', ['id' => $printInvoice->getInvoiceId()]);

            return;
        }

        $this->invoicePrinter->print($invoice);

        $this->logger->notice('Sucessfully printed invoice id {id}.', ['id' => $printInvoice->getInvoiceId()]);
    }
}
