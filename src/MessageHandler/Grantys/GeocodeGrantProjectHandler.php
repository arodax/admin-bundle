<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProject;
use Arodax\AdminBundle\Geography\Districts;
use Arodax\AdminBundle\Geography\Geocoder;
use Arodax\AdminBundle\Message\Grantys\GeocodeGrantProject;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use function Symfony\Component\String\u;

#[AsMessageHandler]
final readonly class GeocodeGrantProjectHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Geocoder $geocoder,
        private LoggerInterface $logger,
    ) {
    }
    public function __invoke(GeocodeGrantProject $message): void
    {
        $grantProject = $this->entityManager->find(GrantProject::class, $message->getId());


        if (null === $grantProject) {
            return;
        }

        $address = $grantProject->getPlaceOfRealization();

        if (empty($address)) {
            $address = $grantProject->getAddress();
        }

        if (empty($address)) {
            return;
        }

        try {

            $data = $this->geocoder->geocode($address);

            if (isset($data['results'][0], $data['status']) && $data['status'] === 'OK') {

                foreach ($data['results'][0]['address_components'] as $addressComponent) {
                    // district
                    if ($addressComponent['types'][0] === 'administrative_area_level_2') {

                        $district = $addressComponent['short_name'];
                        $district = u($district)->replace('District', '')->trim()->toString();

                        $this->logger->warning('Found district {district} for {address}', [
                            'district' => $district,
                            'address' => $address
                        ]);

                        $value = Districts::tryFrom($district);
                        $grantProject->setDistrictOfRealization($value?->name);
                    }

                    // city
                    if ($addressComponent['types'][0] === 'locality') {

                        $city = $addressComponent['short_name'];

                        $this->logger->warning('Found city {city} for {address}', [
                            'city' => $city,
                            'address' => $address
                        ]);

                        $grantProject->setCityOfRealization($city);
                    }
                }
            }

            $this->entityManager->flush();

        } catch (\Throwable $throwable) {
            $this->logger->error('Could not geocode Grantys project because of : '.$throwable->getMessage(), ['exception' => $throwable]);
            return;
        }
    }
}
