<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\MessageHandler\Translate;

use Arodax\AdminBundle\Intl\DeeplTranslatorClient;
use Arodax\AdminBundle\Message\Translator\TranslateEntity;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class TranslateEntityHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private DeeplTranslatorClient $translatorClient,
        private LoggerInterface $logger,
    ) {
    }
    public function __invoke(TranslateEntity $message): void
    {
        $entity = $this->entityManager->find($message->getClass(), $message->getId());

        if (null === $entity) {
            $this->logger->error('The requested entity {class} with the {id} for translation has not been found.', [
                'class' => $message->getClass(),
                'id' => $message->getId()
            ]);

            return;
        }

        if (!$entity instanceof TranslatableInterface) {
            $this->logger->error('The requested entity {class} is not a translatable entity.', [
                'class' => $message->getClass(),
            ]);

            return;
        }

        $this->translatorClient->translateEntity($entity, $message->getSource());
    }
}
