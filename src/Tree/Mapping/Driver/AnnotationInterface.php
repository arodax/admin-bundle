<?php

/*
 * This file is part of the arodax/admin-bundle package.
 *
 * (c) ARODAX  <info@arodax.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tree\Mapping\Driver;

use Arodax\AdminBundle\Tree\Mapping\Annotation as Tree;

/**
 * Annotation driver interface, provides method
 * to set custom annotation reader.
 *
 * @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
interface AnnotationInterface extends DriverInterface
{
    /*
     * Annotation or attribute to define the tree type
     */
    public const string TREE = Tree\Tree::class;

    /*
     * Annotation or attribute to mark field as one which will store left value
     */
    public const string LEFT = Tree\Left::class;

    /*
     * Annotation or attribute to mark field as one which will store right value
     */
    public const string RIGHT = Tree\Right::class;

    /*
     * Annotation or attribute to mark relative parent field
     */
    public const string PARENT_NODE = Tree\ParentNode::class;

    /*
     * Annotation or attribute to mark node level
     */
    public const string LEVEL = Tree\Level::class;

    /*
     * Annotation or attribute to mark field as tree root
     */
    public const string ROOT = Tree\Root::class;


    /*
     * Annotation or attribute to specify closure tree class
     */
    public const string CLOSURE = Tree\Closure::class;


    /*
     * Annotation or attribute to specify path class
     */
    public const string PATH = Tree\Path::class;


    /*
     * Annotation or attribute to specify path source class
     */
    public const string PATH_SOURCE = Tree\PathSource::class;

    /*
     * Annotation or attribute to specify path hash class
     */
    public const string PATH_HASH = Tree\PathHash::class;

    /*
     * Annotation or attribute to mark the field to be used to hold the lock time
     */
    public const string LOCK_TIME = Tree\LockTime::class;

    /**
     * Set annotation reader class
     * since older doctrine versions do not provide an interface
     * it must provide these methods:
     *     getClassAnnotations([reflectionClass])
     *     getClassAnnotation([reflectionClass], [name])
     *     getPropertyAnnotations([reflectionProperty])
     *     getPropertyAnnotation([reflectionProperty], [name])
     *
     * @param object $reader - annotation reader class
     */
    public function setAnnotationReader(object $reader);
}
