<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\ArgumentResolver;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\ValueObject\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;


final readonly class UserGroupUserResolver implements ValueResolverInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return UserGroupUser::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $userGroup = $request->attributes->get('userGroup');
        $user = $request->attributes->get('user');

        if (null === $userGroup || null === $user) {
            return;
        }

        $userGroup = $this->entityManager->find(UserGroup::class, $userGroup);

        if (null === $userGroup) {
            return;
        }

        $user = $this->entityManager->find(User::class, $user);

        if (null === $user) {
            return;
        }

        yield new UserGroupUser($userGroup, $user);
    }
}
