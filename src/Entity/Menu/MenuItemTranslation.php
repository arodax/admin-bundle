<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Menu;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'aa_menu_item_translation')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class MenuItemTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['menu_item:read'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $title = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $link = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?array $routeParams = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $linkType = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    public function getHeadline(): ?string
    {
        return $this->title;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getRouteParams(): ?array
    {
        return $this->routeParams;
    }

    public function setRouteParams(?array $routeParams): self
    {
        $this->routeParams = $routeParams;

        return $this;
    }

    public function getLinkType(): ?string
    {
        return $this->linkType;
    }

    public function setLinkType(string $linkType = null): self
    {
        $this->linkType = $linkType;

        return $this;
    }
}
