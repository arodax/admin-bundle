<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Menu;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\Menu\MenuLinkInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Menu\MenuItemRepository;
use Arodax\AdminBundle\Security\Voter\MenuItemVoter;
use Arodax\AdminBundle\Tree\Mapping\Annotation as Tree;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\Menu\NodeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping as ORM;

/**
 * @method MenuItemTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.MenuItemVoter::READ_MENU_ITEM.'", object)'),
        new Put(security: 'is_granted("'.MenuItemVoter::UPDATE_MENU_ITEM.'", object)'),
        new Delete(security: 'is_granted("'.MenuItemVoter::DELETE_MENU_ITEM.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.MenuItemVoter::CREATE_MENU_ITEM.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['menu_item:read']],
    denormalizationContext: ['groups' => ['menu_item:write']]
)]
#[Tree\Tree(type: 'nested')]
#[ORM\Entity(repositoryClass:MenuItemRepository::class)]
#[ORM\Table(name: 'aa_menu_item')]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['title' => 'partial'])]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['internal'])]
class MenuItem implements MenuLinkInterface, NodeInterface, TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    protected ?int $id = null;

    /*
     * Optional node name for the item.
     */
    #[ORM\Column(type: 'string', length: 191, unique: true, nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $nodeName = null;

    /*
     * Nested tree left value.
     */
    #[Tree\Left]
    #[ORM\Column(name: 'lft', type: 'integer')]
    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    protected $lft;

    /*
     * Nested tree level.
     */
    #[Tree\Level]
    #[ORM\Column(name: 'lvl', type: 'integer')]
    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    protected $lvl;

    /*
     * Nested tree right value.
     */
    #[Tree\Right]
    #[ORM\Column(name: 'rgt', type: 'integer')]
    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    protected $rgt;

    /*
     * Nested tree root.
     */
    #[Tree\Root]
    #[ORM\ManyToOne(targetEntity: 'MenuItem')]
    #[ORM\JoinColumn(name: 'tree_root', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    protected $root;

    /*
     * Nested tree parent node.
     */
    #[Tree\ParentNode]
    #[MaxDepth(1)]
    #[ORM\ManyToOne(targetEntity: 'MenuItem', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['menu_item:write', 'menu_item:tree'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    protected $parent;

    /**
     * Nested tree children nodes.
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: 'MenuItem')]
    #[ORM\OrderBy(value: ['lft' => 'ASC'])]
    #[Groups(groups: ['menu_item:tree'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    protected $children;

    /*
     * Optional menu icon.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $icon = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['menu_item:write'])]
    protected $newTranslations;

    /*
     * Is menu enabled?
     */
    #[ORM\Column(name: 'enabled', type: 'boolean')]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected bool $enabled = false;

    /*
     * Security expression rules.
     */
    #[ORM\Column(name: 'security', type: 'text', nullable: true)]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    protected ?string $security = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    private bool $deletable = true;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['menu_item:read', 'menu_item:write'])]
    private bool $internal = false;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    /*
     * Get id of the menu item.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /*
     * Get optionally node name of the menu item.
     */
    public function getNodeName(): ?string
    {
        return $this->nodeName;
    }

    #[Groups(groups: ['menu_item:read', 'menu_item:tree'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getHeadline();
    }

    /*
     * Set optional node name of the menu item.
     */
    public function setNodeName(?string $nodeName): self
    {
        $this->nodeName = $nodeName;

        return $this;
    }

    /*
     * Get icon for the menu item.
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /*
     * Set icon for the menu item.
     */
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /*
     * Get name (translated title) of the node.
     */
    public function getName(): string
    {
        return $this->translate()->getTitle() ?? '';
    }

    #[Groups(groups: ['menu_item:tree'])]
    public function getTitle(): ?string
    {
        return $this->translate()->getTitle();
    }

    /**
     * Get default options for the node.
     *
     * @todo: Check for the non-router links.
     */
    public function getOptions(): array
    {
        return [
            'route' => $this->translate()->getLink(),
            'icon' => $this->getIcon(),
        ];
    }

    /*
     * Get children nodes of this node.
     */
    public function getChildren(): \Traversable
    {
        return $this->children;
    }

    /**
     * Get the parent node of the section.
     *
     * If parent is null, this section is on the root level.
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    #[Groups(groups: ['menu_item:tree'])]
    public function getParentId(): ?int
    {
        if ($this->parent) {
            return $this->parent->getId();
        }

        return null;
    }

    /*
     * Set the parent section of this section.
     *
     * Setting this value will not affect lft, rgt and lvl properties of the three.
     * These values needs to be correctly calculated by the data persister.
     */
    public function setParent(self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSecurity(): ?string
    {
        return $this->security;
    }

    public function setSecurity(?string $security): self
    {
        $this->security = $security;

        return $this;
    }

    public function getLft()
    {
        return $this->lft;
    }

    public function getLvl()
    {
        return $this->lvl;
    }

    public function getRgt()
    {
        return $this->rgt;
    }

    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return bool
     */
    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * @param bool $readOnly
     * @return MenuItem
     */
    public function setReadOnly(bool $readOnly): self
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * @param bool $deletable
     * @return MenuItem
     */
    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInternal(): bool
    {
        return $this->internal;
    }

    /**
     * @param bool $internal
     * @return MenuItem
     */
    public function setInternal(bool $internal): self
    {
        $this->internal = $internal;
        return $this;
    }
}
