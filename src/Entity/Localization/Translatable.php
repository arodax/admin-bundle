<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Localization;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Localization\TranslatableRepository;
use Arodax\AdminBundle\Security\Voter\TranslatableVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @method TranslatableTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_TRANSLATABLE\', object)'), new Put(security: 'is_granted(\'UPDATE_TRANSLATABLE\', object)'), new Delete(security: 'is_granted(\'DELETE_TRANSLATABLE\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_TRANSLATABLE\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['translatable:read']], denormalizationContext: ['groups' => ['translatable:write']])]
#[ORM\Entity(repositoryClass: TranslatableRepository::class)]
#[ORM\Table(name: 'aa_translatable')]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['title' => 'partial'])]
class Translatable implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['translatable:read'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['translatable:read', 'translatable:write'])]
    protected ?string $source = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['translatable:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['translatable:write'])]
    protected $newTranslations;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }
}
