<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Geography;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Security\Voter\PointVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_POINT\', object)'), new Put(security: 'is_granted(\'UPDATE_POINT\', object)'), new Delete(security: 'is_granted(\'DELETE_POINT\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_POINT\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['point_read']], denormalizationContext: ['groups' => ['point_write']])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_point')]
class Point
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['point_read', 'point_write'])]
    protected ?int $id = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['point_read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['point_write'])]
    protected $newTranslations;

    /*
     * This property contains coordinates of the point.
     */
    #[ORM\Column(type: 'point', nullable: true)]
    #[Groups(groups: ['point_read', 'point_write'])]
    protected ?\Arodax\Doctrine\Spatial\ValueObject\Point $point = null;

    /**
     * The address of the point.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['point_read', 'point_write'])]
    protected ?string $address = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getPoint(): ?\Arodax\Doctrine\Spatial\ValueObject\Point
    {
        return $this->point ?? new \Arodax\Doctrine\Spatial\ValueObject\Point(0,0);
    }

    /**
     * @todo: Use data transformer or something to tranform \Arodax\Doctrine\Spatial\ValueObject\Point directly
     *
     * @param array<string, float>|null $coords
     */
    public function setPoint(?array $coords): ?self
    {
        if (null === $coords || empty($coords['longitude']) || empty($coords['latitude'])) {
            return $this->point = null;
        }

        $this->point = new \Arodax\Doctrine\Spatial\ValueObject\Point($coords['longitude'] ?? null, $coords['latitude'] ?? null);

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
