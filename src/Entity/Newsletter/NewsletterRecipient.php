<?php

/*
 * This file is part of the seznamdrazeb.cz package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Newsletter;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;

/**
 * This entity represents a newsletter recipient.
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_NEWSLETTER_RECIPIENT\', object)'),
    new Put(security: 'is_granted(\'UPDATE_NEWSLETTER_RECIPIENT\', object)'),
    new Delete(security: 'is_granted(\'DELETE_NEWSLETTER_RECIPIENT\', object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_NEWSLETTER_RECIPIENT\', object)', validationContext: ['groups' => ['']])
],
    normalizationContext: [
        'groups' => ['newsletter:read']
    ],
    denormalizationContext: [
        'groups' => ['newsletter:write']])
]
#[UniqueEntity('email', errorPath: 'email')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_newsletter_recipient')]
#[ORM\Index(columns: ['email'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['email' => 'partial'])]
class NewsletterRecipient
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[Groups(groups: ['newsletter:read', 'newsletter:write'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['newsletter:read', 'newsletter:write'])]
    #[Email]
    #[NotBlank(allowNull: false)]
    private ?string $email = null;

    #[Groups(groups: ['newsletter:read', 'newsletter:write'])]
    #[ORM\Column(type: 'date_immutable')]
    private ?\DateTimeImmutable $createdAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
