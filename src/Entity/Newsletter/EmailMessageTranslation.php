<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Newsletter;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'aa_email_message_translation')]
class EmailMessageTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['email_message:read', 'email_message:write'])]
    protected ?int $id = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['email_message:read', 'email_message:write'])]
    protected ?string $locale = null;

    /*
     * The main email content.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['email_message:read', 'email_message:write'])]
    protected ?string $content = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
}
