<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Newsletter;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TimestampableInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * This entity represent a newsletter email message.
 */
#[ApiResource(
    types: ['http://schema.org/EmailMessage'],
    operations: [
        new Get(security: 'is_granted(\'READ_EMAIL_MESSAGE\', object)'),
        new Put(security: 'is_granted(\'UPDATE_EMAIL_MESSAGE\', object)'),
        new Delete(security: 'is_granted(\'DELETE_EMAIL_MESSAGE\', object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted(\'CREATE_EMAIL_MESSAGE\', object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['email_message:read']],
    denormalizationContext: ['groups' => ['email_message:write']]
)]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_email_message')]
#[ORM\Index(columns: ['status'])]
class EmailMessage implements TranslatableInterface, TimestampableInterface
{
    use TranslatableTrait;
    final public const string STATUS_DRAFT = 'draft';
    final public const string STATUS_PUBLISHED = 'published';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['email_message:read'])]
    private ?int $id = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['email_message:read', 'email_message:write'])]
    private array $context = [];

    #[ORM\Column(type: 'string')]
    #[Groups(groups: ['email_message:read', 'email_message:write'])]
    private ?string $status = self::STATUS_DRAFT;

    #[ORM\Column(type: 'date_immutable')]
    #[Groups(groups: ['email_message:read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'date_immutable', nullable: true)]
    #[Groups(groups: ['email_message:read'])]
    private ?\DateTimeImmutable $updatedAt = null;

    /**
     * @var array
     */
    #[Groups(groups: ['email_message:write'])]
    protected $newTranslations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(array $context): void
    {
        $this->context = $context;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $cretedAt): TimestampableInterface
    {
        $this->createdAt = $cretedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): TimestampableInterface
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
