<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Arodax\AdminBundle\Security\Voter\SettingsVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.SettingsVoter::READ_SETTINGS.'", object)'),
        new Put(security: 'is_granted("'.SettingsVoter::UPDATE_SETTINGS.'", object)'),
        new Delete(security: 'is_granted("'.SettingsVoter::DELETE_SETTINGS.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_SETTING_CONFIGURATION_ADMIN")'),
        new Post(
            securityPostDenormalize: 'is_granted("'.SettingsVoter::CREATE_SETTINGS.'", object)',
            validationContext: ['groups' => ['']]
        )
    ],
    normalizationContext: ['groups' => ['settings:read'], 'swagger_definition_name' => 'Read'],
    denormalizationContext: ['groups' => ['settings:write'], 'swagger_definition_name' => 'Write']
)]
#[ORM\Entity]
#[ORM\Table(name: 'app_settings')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Settings
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['settings:read', 'settings:write'])]
    protected ?int $id = null;

    #[ORM\Column(name: '`key`', type: 'string', length: 191)]
    #[Groups(['settings:read', 'settings:write'])]
    protected string $key;

    #[ORM\Column(name: '`value`', type: 'string', length: 191, nullable: true)]
    #[Groups(['settings:read', 'settings:write'])]
    protected ?string $value = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
