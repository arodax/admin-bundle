<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Gallery;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [new Get(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Put(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Delete(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted("ROLE_ADMINISTRATION_ACCESS")', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['gallery:read', 'media_object:read']], denormalizationContext: ['groups' => ['gallery:write', 'media_object:write']])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_gallery_media_object')]
class GalleryMediaObject
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Gallery::class, inversedBy: 'galleryMediaObjects')]
    #[ORM\JoinColumn(name: 'gallery_id', nullable: false, onDelete: 'CASCADE')]
    protected ?Gallery $gallery = null;

    #[ORM\ManyToOne(targetEntity: MediaObject::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'media_object_id', nullable: false, onDelete: 'CASCADE')]
    #[Groups(groups: ['media_object:read', 'media_object:write', 'article:read', 'article:write', 'event:read', 'event:write'])]
    protected ?MediaObject $mediaObject = null;

    #[ORM\Column(name: 'priority', type: 'integer', options: ['default' => 0])]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected int $priority = 0;

    /*
     * Contains a link, usually when clicked on the object.
     */
    #[Deprecated(reason: 'Since 6.25.0, use "link" property from "GalleryMediaObjectTranslation::class" instead.', since: '6.25.0')]
    #[ORM\Column(name: 'link', type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $link = null;

    /*
    * Array containing all entity translations already persisted into the entity.
    */
    #[Groups(groups: ['gallery:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['gallery:write'])]
    protected $newTranslations;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getGallery(): Gallery
    {
        return $this->gallery;
    }

    public function getMediaObject(): MediaObject
    {
        return $this->mediaObject;
    }

    public function setMediaObject(MediaObject $mediaObject): self
    {
        $this->mediaObject = $mediaObject;

        return $this;
    }

    public function setGallery(Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
