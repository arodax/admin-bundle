<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Gallery;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Gallery\GalleryRepository;
use Arodax\AdminBundle\Security\Voter\GalleryVoter;
use Arodax\AdminBundle\Tree\Mapping\Annotation as Tree;
use Doctrine\Common\Collections\ArrayCollection;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping as ORM;

/**
 * @method GalleryTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.GalleryVoter::READ_GALLERY.'", object)'),
    new Put(security: 'is_granted("'.GalleryVoter::UPDATE_GALLERY.'", object)'),
    new Delete(security: 'is_granted("'.GalleryVoter::DELETE_GALLERY.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(
        securityPostDenormalize: 'is_granted("'.GalleryVoter::CREATE_GALLERY.'", object)',
        validationContext: ['groups' => ['']]
    )],
    normalizationContext: ['groups' => ['gallery:read', 'media_object:read']],
    denormalizationContext: ['groups' => ['gallery:write', 'media_object:write']]
)]
#[Tree\Tree(type: 'nested')]
#[ORM\Entity(repositoryClass: GalleryRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_gallery')]
#[ORM\Index(columns: ['status'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt', 'status'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
class Gallery implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    public const string STATUS_DRAFT = 'draft';
    public const string STATUS_PUBLISHED = 'published';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['gallery:read', 'article:read', 'article:write', 'gallery:tree', 'event:read', 'event:write'])]
    protected ?int $id = null;

    #[Tree\Left]
    #[ORM\Column(name: 'lft', type: 'integer')]
    #[Groups(groups: ['gallery:tree'])]
    protected $lft;

    #[Tree\Level]
    #[ORM\Column(name: 'lvl', type: 'integer')]
    #[Groups(groups: ['gallery:tree'])]
    protected $lvl;

    #[Tree\Right]
    #[ORM\Column(name: 'rgt', type: 'integer')]
    #[Groups(groups: ['gallery:tree'])]
    protected $rgt;

    #[Tree\Root]
    #[ORM\ManyToOne(targetEntity: 'Gallery')]
    #[ORM\JoinColumn(name: 'tree_root', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['gallery:tree'])]
    protected $root;

    #[Tree\ParentNode]
    #[MaxDepth(1)]
    #[ORM\ManyToOne(targetEntity: 'Gallery', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['gallery:write', 'gallery:tree'])]
    protected $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: 'Gallery')]
    #[ORM\OrderBy(value: ['lft' => 'ASC'])]
    #[Groups(groups: ['gallery:read'])]
    protected $children;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['gallery:read', 'gallery:tree'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['gallery:write'])]
    protected $newTranslations;

    /*
     * Property containing information about the state of the galleries.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected string $status = self::STATUS_DRAFT;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['gallery:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['gallery:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when gallery has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /**
     * @var GalleryMediaObject[]
     */
    #[ORM\OneToMany(mappedBy: 'gallery', targetEntity: GalleryMediaObject::class, cascade: ['ALL'], orphanRemoval: true)]
    #[ORM\OrderBy(value: ['priority' => 'ASC'])]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected $galleryMediaObjects;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    private bool $deletable = true;

    public function __construct()
    {
        $this->galleryMediaObjects = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /*
     * Get id of the gallery.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /*
     * Get the parent node of the gallery.
     *
     * If parent is null, this gallery is on the root level.
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /*
     * Set the parent gallery of this gallery.
     *
     * Setting this value will not affect lft, rgt and lvl proprties of the three.
     * These values need to be correctly calculated by the data persister.
     */
    public function setParent(self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get media objects from this gallery.
     *
     * @return GalleryMediaObject[]
     */
    public function getGalleryMediaObjects(): array
    {
        return $this->galleryMediaObjects->toArray();
    }

    /*
     * Add media object to the galleries.
     */
    public function addGalleryMediaObject(GalleryMediaObject $mediaObject): self
    {
        if (!$this->galleryMediaObjects->contains($mediaObject)) {
            $this->galleryMediaObjects->add($mediaObject);
            $mediaObject->setGallery($this);
        }

        return $this;
    }

    /*
     * Remove media object from the gallery.
     */
    public function removeGalleryMediaObject(GalleryMediaObject $mediaObject): self
    {
        if ($this->galleryMediaObjects->contains($mediaObject)) {
            $this->galleryMediaObjects->removeElement($mediaObject);
        }

        return $this;
    }

    /*
     * Check if the gallery has the media object.
     */
    public function hasMediaObject(GalleryMediaObject $mediaObject): bool
    {
        return $this->galleryMediaObjects->contains($mediaObject);
    }

    public function getChildren(): array
    {
        return $this->children->toArray();
    }

    public function getLft()
    {
        return $this->lft;
    }

    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    public function getLvl()
    {
        return $this->lvl;
    }

    public function setLvl($lvl): void
    {
        $this->lvl = $lvl;
    }

    public function getRgt()
    {
        return $this->rgt;
    }

    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root): void
    {
        $this->root = $root;
    }

    #[Groups(groups: ['gallery:tree'])]
    #[Deprecated(reason: 'This method is deprecated to use it as item name, use "getHeadline" instead', replacement: '%class%->getHeadline()')]
    public function getTitle(): ?string
    {
        return $this->translate()->getName();
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): self
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * @param bool $deletable
     * @return Gallery
     */
    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        return $this;
    }
}
