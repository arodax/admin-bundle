<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Gallery;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\GalleryTranslationEntityListener;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;
use Doctrine\ORM\Mapping as ORM;

#[ORM\EntityListeners([GalleryTranslationEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_gallery_translation')]
class GalleryTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?int $id = null;

    /*
     * TThe gallery name.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write', 'gallery:read'])]
    protected ?string $name = null;

    /*
     * The title of the gallery.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $title = null;

    /*
     * The gallery description.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $description = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected $locale;

    /*
     * Gallery keywords.
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?array $metaKeywords = [];

    /*
     * A canonical link for this gallery.
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $canonicalLink = null;

    /*
     * Robots handling for the gallery.
     */
    #[Choice(choices: ['noindex', 'index', 'follow', 'nofollow', 'noimageindex', 'none', 'noarchive', 'nosnippet'])]
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?array $metaRobots = [];

    /*
     * Gallery description.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $metaDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['gallery:read', 'gallery:write'])]
    protected ?string $slug = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMetaKeywords(): ?array
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?array $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getCanonicalLink(): ?string
    {
        return $this->canonicalLink;
    }

    public function setCanonicalLink(?string $canonicalLink): self
    {
        $this->canonicalLink = $canonicalLink;

        return $this;
    }

    public function getMetaRobots(): ?array
    {
        return $this->metaRobots;
    }

    public function setMetaRobots(?array $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    #[Groups(groups: ['gallery:read', 'gallery:tree'])]
    public function getHeadline():?string
    {
        return $this->name;
    }
}
