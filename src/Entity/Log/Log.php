<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Log;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\EntityListener\LogEntityListener;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")')
],
    normalizationContext: ['groups' => ['log_read']]
)]
#[ORM\EntityListeners([LogEntityListener::class])]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_log')]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt'], arguments: ['orderParameterName' => 'order'])]
class Log
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[Groups(groups: ['log_read'])]
    private ?int $id = null;

    #[ORM\Column(name: 'message', type: 'text')]
    #[Groups(groups: ['log_read'])]
    private ?string $message = null;

    /**
     * Contains PSR-3 processor and translated message.
     *
     * @see LogEntityListener for formatted message processor.
     */
    #[Groups(groups: ['log_read'])]
    private ?string $formattedMessage = null;

    #[ORM\Column(name: 'context', type: 'json')]
    #[Groups(groups: ['log_read'])]
    private array $context = [];

    #[ORM\Column(name: 'level', type: 'smallint')]
    #[Groups(groups: ['log_read'])]
    private int $level = 100;

    #[ORM\Column(name: 'level_name', type: 'string', length: 50)]
    #[Groups(groups: ['log_read'])]
    private string $levelName = 'DEBUG';

    #[ORM\Column(name: 'extra', type: 'json')]
    #[Groups(groups: ['log_read'])]
    private array $extra = [];

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['log_read'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['log_read', 'admin:output'])]
    private ?User $user = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getFormattedMessage(): ?string
    {
        return $this->formattedMessage;
    }

    public function setFormattedMessage(?string $formattedMessage): void
    {
        $this->formattedMessage = $formattedMessage;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(array $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getLevelName(): string
    {
        return $this->levelName;
    }

    public function setLevelName(string $levelName): self
    {
        $this->levelName = $levelName;

        return $this;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }

    public function setExtra(array $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function onPrePersist(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
