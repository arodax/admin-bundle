<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\EntityListener\Finance\CreditListener;
use Arodax\AdminBundle\Security\Voter\CreditVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.CreditVoter::READ_CREDIT.'", object)'),
    new Put(security: 'is_granted("'.CreditVoter::UPDATE_CREDIT.'", object)'),
    new Delete(security: 'is_granted("'.CreditVoter::DELETE_CREDIT.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.CreditVoter::CREATE_CREDIT.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['score:read']],
    denormalizationContext: ['groups' => ['score:write']]
)]
#[ORM\Entity]
#[ORM\EntityListeners([
    CreditListener::class
])]
#[ORM\Table(name: 'aa_credit')]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id', 'user.id'])]
class Credit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['score:read', 'score:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'credits')]
    #[Groups(groups: ['score:read', 'score:write'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Assert\NotBlank]
    protected ?User $user = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['score:read', 'score:write'])]
    #[Assert\NotBlank]
    protected int $value = 0;

    #[ORM\ManyToOne(targetEntity: CreditType::class)]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Groups(groups: ['score:read', 'score:write'])]
    #[Assert\NotBlank]
    protected ?CreditType $creditType = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreditType(): ?CreditType
    {
        return $this->creditType;
    }

    public function setCreditType(?CreditType $creditType): self
    {
        $this->creditType = $creditType;

        return $this;
    }
}
