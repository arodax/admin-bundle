<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\EntityListener\InvoiceEntityListener;
use Arodax\AdminBundle\Model\Finance\InvoiceInterface;
use Arodax\AdminBundle\Repository\Finance\InvoiceRepository;
use Arodax\AdminBundle\Security\Voter\InvoiceVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Unique;

#[ApiResource(
    shortName: 'invoice',
    operations: [
        new Get(uriTemplate: '/invoices/{id}', security: 'is_granted("'.InvoiceVoter::READ_INVOICE.'", object)'),
        new Put(uriTemplate: '/invoices/{id}', security: 'is_granted("'.InvoiceVoter::UPDATE_INVOICE.'", object)'),
        new Delete(uriTemplate: '/invoices/{id}', security: 'is_granted("'.InvoiceVoter::DELETE_INVOICE.'", object)'),
        new GetCollection(uriTemplate: '/invoices', security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(uriTemplate: '/invoices', securityPostDenormalize: 'is_granted("'.InvoiceVoter::CREATE_INVOICE.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['invoice:read']],
    denormalizationContext: ['groups' => ['invoice:write']]
)]
#[ORM\EntityListeners([InvoiceEntityListener::class])]
#[ORM\Entity(repositoryClass: InvoiceRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_invoice_czech')]
#[ORM\Index(columns: ['status'])]
#[ORM\Index(columns: ['invoice_number'])]
#[ORM\Index(columns: ['variable_number'])]
#[ORM\Index(columns: ['constant_number'])]
#[ORM\Index(columns: ['specific_number'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'invoiceNumber', 'createdAt', 'paidAt', 'issuedAt', 'dueAt', 'taxableSupplyAt', 'variableNumber', 'constantNumber', 'specificNumber'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact', 'invoiceNumber' => 'partial', 'variableNumber' => 'partial', 'constantNumber' => 'partial', 'specificNumber' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'paidAt', 'issuedAt', 'dueAt', 'taxableSupplyAt'])]
class InvoiceCzech implements InvoiceInterface
{
    use TimestampTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $id = null;

    /*
     * Property containing information about the state of the invoice.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['invoice:read'])]
    #[NotBlank]
    #[NotNull]
    protected ?string $status = 'draft';

    /*
     * A text supplier of the customer address.
     */
    #[ORM\Column(name: 'supplier', type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 191)]
    protected ?string $supplier = null;

    /*
     * A text representation of the customer address.
     */
    #[ORM\Column(name: 'customer', type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 191)]
    protected ?string $customer = null;

    #[ORM\Column(name: 'invoice_number', type: 'bigint', unique: true, nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[NotBlank(allowNull: true)]
    #[Length(max: 10)]
    protected ?string $invoiceNumber = null;

    #[ORM\Column(name: 'order_number', type: 'bigint', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 20)]
    protected ?string $orderNumber = null;

    #[ORM\Column(name: 'variable_number', type: 'bigint', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 10)]
    protected ?string $variableNumber = null;

    #[ORM\Column(name: 'constant_number', type: 'bigint', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 10)]
    protected ?string $constantNumber = null;

    #[ORM\Column(name: 'specific_number', type: 'bigint', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Length(max: 10)]
    protected ?string $specificNumber = null;

    #[ORM\Column(name: 'bank_account_number', type: 'string', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?string $bankAccountNumber = null;

    #[ORM\Column(name: 'iban', type: 'string', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?string $iban = null;

    #[ORM\Column(name: 'swift', type: 'string', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?string $swift = null;

    #[ORM\Column(name: 'issued_at', type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?\DateTimeImmutable $issuedAt = null;

    #[ORM\Column(name: 'taxable_supply_at', type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?\DateTimeImmutable $taxableSupplyAt = null;

    #[ORM\Column(name: 'due_at', type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?\DateTimeImmutable $dueAt = null;

    #[ORM\Column(name: 'paid_at', type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?\DateTimeImmutable $paidAt = null;

    #[ORM\ManyToMany(targetEntity: OrderItem::class, cascade: ['ALL'], orphanRemoval: true)]
    #[ORM\JoinTable(name: 'aa_invoice_czech_order_item')]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[Unique]
    protected $orderItems;

    #[ORM\Column(name: 'payment_info', type: 'text', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?string $paymentInfo = null;

    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[ORM\Column(name: 'vat_invoice', type: 'boolean', nullable: false)]
    protected bool $vatInvoice = false;

    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    #[ORM\Column(name: 'currency', type: 'string', nullable: false)]
    protected string $currency = 'CZK';

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    public function setSupplier(?string $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getCustomer(): ?string
    {
        return $this->customer;
    }

    public function setCustomer(?string $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getInvoiceNumber(): string
    {
        return $this->invoiceNumber;
    }

    public function setInvoiceNumber(string $invoiceNumber): self
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    public function getVariableNumber(): ?string
    {
        return $this->variableNumber;
    }

    public function setVariableNumber(?string $variableNumber): self
    {
        $this->variableNumber = $variableNumber;

        return $this;
    }

    public function getConstantNumber(): ?string
    {
        return $this->constantNumber;
    }

    public function setConstantNumber(?string $constantNumber): self
    {
        $this->constantNumber = $constantNumber;

        return $this;
    }

    public function getSpecificNumber(): ?string
    {
        return $this->specificNumber;
    }

    public function setSpecificNumber(?string $specificNumber): self
    {
        $this->specificNumber = $specificNumber;

        return $this;
    }

    public function getIssuedAt(): ?\DateTimeImmutable
    {
        return $this->issuedAt;
    }

    public function setIssuedAt(?\DateTimeImmutable $issuedAt): self
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    public function getTaxableSupplyAt(): ?\DateTimeImmutable
    {
        return $this->taxableSupplyAt;
    }

    public function setTaxableSupplyAt(?\DateTimeImmutable $taxableSupplyAt): self
    {
        $this->taxableSupplyAt = $taxableSupplyAt;

        return $this;
    }

    public function getDueAt(): ?\DateTimeImmutable
    {
        return $this->dueAt;
    }

    public function setDueAt(?\DateTimeImmutable $dueAt): self
    {
        $this->dueAt = $dueAt;

        return $this;
    }

    public function getPaidAt(): ?\DateTimeImmutable
    {
        return $this->paidAt;
    }

    public function setPaidAt(?\DateTimeImmutable $paidAt): self
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(?string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    public function getPaymentInfo(): ?string
    {
        return $this->paymentInfo;
    }

    public function setPaymentInfo(?string $paymentInfo): self
    {
        $this->paymentInfo = $paymentInfo;

        return $this;
    }

    public function getOrderItems(): array
    {
        return $this->orderItems->toArray();
    }

    public function addOrderItem(OrderItem $item): self
    {
        if (!$this->orderItems->contains($item)) {
            $this->orderItems->add($item);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $item): self
    {
        if ($this->orderItems->contains($item)) {
            $this->orderItems->removeElement($item);
        }

        return $this;
    }

    public function isVatInvoice(): bool
    {
        return $this->vatInvoice;
    }

    public function setVatInvoice(bool $vatInvoice): self
    {
        $this->vatInvoice = $vatInvoice;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getBankAccountNumber(): ?string
    {
        return $this->bankAccountNumber;
    }

    public function setBankAccountNumber(?string $bankAccountNumber): self
    {
        $this->bankAccountNumber = $bankAccountNumber;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getSwift(): ?string
    {
        return $this->swift;
    }

    public function setSwift(?string $swift): self
    {
        $this->swift = $swift;

        return $this;
    }

    #[Groups(groups: ['invoice:read'])]
    public function getSubtotalTax(): ?array
    {
        if (false === $this->vatInvoice) {
            return null;
        }
        $subtotalTax = [];
        /** @var OrderItem $orderItem */
        foreach ($this->orderItems->toArray() as $orderItem) {
            $subtotalTax[$orderItem->getTaxRate()]['taxRate'] = +$orderItem->getTaxRate();
            $subtotalTax[$orderItem->getTaxRate()]['taxPrice'] = +$orderItem->getTaxPrice();
            $subtotalTax[$orderItem->getTaxRate()]['totalPriceWithoutTax'] = +$orderItem->getTotalPriceWithTax();
            $subtotalTax[$orderItem->getTaxRate()]['totalPriceWithTax'] = +$orderItem->getTotalPriceWithTax();
        }

        return array_values($subtotalTax);
    }

    #[Groups(groups: ['invoice:read'])]
    public function getTotalTax(): int
    {
        $total = 0;
        /** @var OrderItem $orderItem */
        foreach ($this->orderItems->toArray() as $orderItem) {
            $total += $orderItem->getTaxPrice();
        }

        return $total;
    }

    #[Groups(groups: ['invoice:read'])]
    public function getTotalPrice(): int
    {
        $total = 0;
        /** @var OrderItem $orderItem */
        foreach ($this->orderItems->toArray() as $orderItem) {
            $total += $this->isVatInvoice() ? $orderItem->getTotalPriceWithTax() : $orderItem->getTotalPriceWithoutTax();
        }

        return $total;
    }
}
