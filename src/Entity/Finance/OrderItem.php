<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'aa_order_item')]
#[ORM\Entity]
class OrderItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $id = null;

    #[ORM\Column(name: 'description', type: 'string', length: 191, nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected string $description = '';

    #[ORM\Column(name: 'quantity', type: 'integer', nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected int $quantity = 1;

    #[ORM\Column(name: 'unit_price', type: 'integer', nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected int $unitPrice = 0;

    #[ORM\Column(name: 'tax_rate', type: 'integer', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $taxRate = null;

    #[ORM\Column(name: 'tax_price', type: 'integer', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $taxPrice = null;

    #[ORM\Column(name: 'discount_price', type: 'integer', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $discountPrice = null;

    #[ORM\Column(name: 'total_price_without_tax', type: 'integer', nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected int $totalPriceWithoutTax = 0;

    #[ORM\Column(name: 'total_price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['invoice:read', 'invoice:write'])]
    protected ?int $totalPriceWithTax = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(int $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getTaxRate(): ?int
    {
        return $this->taxRate;
    }

    public function setTaxRate(int $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function getTaxPrice(): ?int
    {
        return $this->taxPrice;
    }

    public function setTaxPrice(int $taxPrice): self
    {
        $this->taxPrice = $taxPrice;

        return $this;
    }

    public function getDiscountPrice(): ?int
    {
        return $this->discountPrice;
    }

    public function setDiscountPrice(?int $discountPrice): self
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    public function getTotalPriceWithoutTax(): int
    {
        return $this->totalPriceWithoutTax;
    }

    public function setTotalPriceWithoutTax(int $totalPriceWithoutTax): self
    {
        $this->totalPriceWithoutTax = $totalPriceWithoutTax;

        return $this;
    }

    public function getTotalPriceWithTax(): int
    {
        return $this->totalPriceWithTax;
    }

    public function setTotalPriceWithTax(int $totalPriceWithTax): self
    {
        $this->totalPriceWithTax = $totalPriceWithTax;

        return $this;
    }
}
