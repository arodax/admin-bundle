<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Security\Voter\BankAccountVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_BANK_ACCOUNT\', object)'), new Put(security: 'is_granted(\'UPDATE_BANK_ACCOUNT\', object)'), new Delete(security: 'is_granted(\'DELETE_BANK_ACCOUNT\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CRATE_BANK_ACCOUNT\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['bank_account:read']], denormalizationContext: ['groups' => ['bank_account:write']])]
#[UniqueEntity(fields: ['number', 'iban'], message: 'This bank account already exist.', errorPath: 'number')]
#[ORM\Entity]
#[ORM\Table(name: 'aa_bank_account')]
class BankAccount
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['bank_account:read', 'bank_account:write'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', unique: true)]
    #[Groups(groups: ['bank_account:read', 'bank_account:write'])]
    #[NotBlank]
    protected ?string $number = null;

    #[ORM\Column(type: 'string', unique: true)]
    #[Groups(groups: ['bank_account:read', 'bank_account:write'])]
    #[Iban]
    protected ?string $iban = null;

    #[ORM\Column(type: 'string')]
    #[Groups(groups: ['bank_account:read', 'bank_account:write'])]
    #[NotBlank]
    protected ?string $swift = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getSwift(): string
    {
        return $this->swift;
    }

    public function setSwift(string $swift): self
    {
        $this->swift = $swift;

        return $this;
    }
}
