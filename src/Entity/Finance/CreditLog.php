<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Security\Voter\ScoreVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [
    new Get(security: 'is_granted("'.ScoreVoter::READ_SCORE.'", object)'),
    new Put(security: 'is_granted("'.ScoreVoter::UPDATE_SCORE.'", object)'),
    new Delete(security: 'is_granted("'.ScoreVoter::DELETE_SCORE.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.ScoreVoter::CREATE_SCORE.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['score:read']],
    denormalizationContext: ['groups' => ['score:write']]
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_credit_log')]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id', 'user.id'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'user', 'value'], arguments: ['orderParameterName' => 'order'])]
class CreditLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['score:read', 'score:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'scores')]
    #[Groups(groups: ['score:read', 'score:write'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Assert\NotBlank]
    protected ?User $user = null;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['score:read', 'score:write'])]
    protected ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToOne(targetEntity: CreditType::class)]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Groups(groups: ['score:read', 'score:write'])]
    protected ?CreditType $type = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['score:read', 'score:write'])]
    #[Assert\NotBlank]
    protected int $value = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): ?CreditType
    {
        return $this->type;
    }

    public function setType(?CreditType $type): self
    {
        $this->type = $type;
        return $this;
    }
}
