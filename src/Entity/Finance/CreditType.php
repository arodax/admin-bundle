<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Finance;

use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Security\Voter\CreditTypeVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * This entity represent a credit type.
 *
 * @method CreditTypeTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.CreditTypeVoter::READ_CREDIT_TYPE.'", object)'),
    new Put(security: 'is_granted("'.CreditTypeVoter::UPDATE_CREDIT_TYPE.'", object)'),
    new Delete(security: 'is_granted("'.CreditTypeVoter::DELETE_CREDIT_TYPE.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.CreditTypeVoter::CREATE_CREDIT_TYPE.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['score:read']],
    denormalizationContext: ['groups' => ['score:write']]
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_credit_type')]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
class CreditType
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['score:read', 'score:write'])]
    protected ?int $id = null;

    #[Groups(groups: ['score:read'])]
    protected $translations;

    #[Groups(groups: ['score:write'])]
    protected $newTranslations;

    public function getId(): ?int
    {
        return $this->id;
    }


    #[Groups(groups: ['score:read'])]
    public function getTitle(): ?string
    {
        return $this->translate()->getTitle();
    }
}
