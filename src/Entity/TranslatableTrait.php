<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * This trait provides methods and properties for translatable entities.
 */
trait TranslatableTrait
{
    /**
     * Array containing all entity translations already persisted into the entity.
     *
     * @var array
     */
    #[Groups(groups: ['admin_read'])]
    protected $translations;

    /**
     * currentLocale is a non persisted field configured during postLoad event.
     */
    protected $currentLocale;

    protected $defaultLocale = 'en';

    /**
     * Set collection of new translations.
     */
    public function setNewTranslations(array $newTranslations): void
    {
        $diff = array_diff($this->translations ? $this->translations->getKeys() : [], array_keys($newTranslations));
        foreach ($this->getTranslations() as $translation) {
            if (\in_array($translation->getLocale(), $diff, false)) {
                $this->getTranslations()->removeElement($translation);
            }
        }

        if ($newTranslations !== []) {
            foreach ($newTranslations as $locale => $translations) {
                foreach ($translations as $key => $value) {
                    $tr = $this->translate($locale, false);
                    $setter = 'set' . ucfirst((string) $key);

                    if (method_exists($tr, $setter)) {
                        if ($value !== null) {
                            if (is_array($value)) {
                                $tr->{$setter}($value);
                            } else {
                                $tr->{$setter}((string) $value);
                            }
                        } else {
                            $tr->{$setter}(null);
                        }
                    }
                }
            }

            $this->mergeNewTranslations();
        }
    }


    /**
     * Returns collection of translations.
     *
     * @return ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations = $this->translations ?: new ArrayCollection();
    }

    /**
     * Returns collection of new translations.
     *
     * @return ArrayCollection
     */
    public function getNewTranslations()
    {
        return $this->newTranslations = $this->newTranslations ?: new ArrayCollection();
    }

    /**
     * Adds new translation.
     *
     * @return $this
     */
    public function addTranslation($translation)
    {
        $this->getTranslations()->set((string) $translation->getLocale(), $translation);
        $translation->setTranslatable($this);

        return $this;
    }

    /**
     * Removes specific translation.
     */
    public function removeTranslation($translation)
    {
        $this->getTranslations()->removeElement($translation);
    }

    /**
     * Returns translation for specific locale (creates new one if doesn't exists).
     * If requested translation doesn't exist, it will first try to fallback default locale
     * If any translation doesn't exist, it will be added to newTranslations collection.
     * In order to persist new translations, call mergeNewTranslations method, before flush.
     *
     * @param string $locale            The locale (en, ru, fr) | null If null, will try with current locale
     * @param bool   $fallbackToDefault Whether fallback to default locale
     */
    public function translate($locale = null, $fallbackToDefault = true)
    {
        return $this->doTranslate($locale, $fallbackToDefault);
    }

    /**
     * Returns translation for specific locale (creates new one if it doesn't exist).
     * If requested translation doesn't exist, it will first try to fallback default locale
     * If any translation doesn't exist, it will be added to newTranslations collection.
     * In order to persist new translations, call mergeNewTranslations method, before flush.
     *
     * @param string $locale The locale (en, ru, fr) | null If null, will try with current locale
     * @param bool   $fallbackToDefault Whether fallback to default locale
     */
    protected function doTranslate($locale = null, $fallbackToDefault = true)
    {
        if (null === $locale) {
            $locale = $this->getCurrentLocale();
        }

        $translation = $this->findTranslationByLocale($locale);
        if ($translation) {
            return $translation;
        }

        if ($fallbackToDefault) {
            if (($fallbackLocale = $this->computeFallbackLocale($locale))
                && ($translation = $this->findTranslationByLocale($fallbackLocale))) {
                return $translation;
            }

            if ($defaultTranslation = $this->findTranslationByLocale($this->getDefaultLocale(), false)) {
                return $defaultTranslation;
            }
        }

        $class = static::getTranslationEntityClass();
        $translation = new $class();
        $translation->setLocale($locale);

        $this->getNewTranslations()->set((string) $translation->getLocale(), $translation);
        $translation->setTranslatable($this);

        return $translation;
    }

    /**
     * Create a new empty translation for the current locale.
     *
     * @see TranslatableSubscriber which use this method in Doctrine PrePersist event.
     */
    public function createEmptyTranslation(): object
    {
        $class = static::getTranslationEntityClass();
        $translation = new $class();
        $translation->setLocale($this->getCurrentLocale());
        $this->getNewTranslations()->set((string) $translation->getLocale(), $translation);
        $translation->setTranslatable($this);

        return $translation;
    }

    /**
     * Merges newly created translations into persisted translations.
     */
    public function mergeNewTranslations(): void
    {
        foreach ($this->getNewTranslations() as $newTranslation) {
            if (!$this->getTranslations()->contains($newTranslation) && !$newTranslation->isEmpty()) {
                $this->addTranslation($newTranslation);
                $this->getNewTranslations()->removeElement($newTranslation);
            }
        }
    }

    public function setCurrentLocale($locale): void
    {
        $this->currentLocale = $locale;
    }

    public function getCurrentLocale(): string
    {
        return $this->currentLocale ?: $this->getDefaultLocale();
    }

    /**
     * @param mixed $locale the default locale
     */
    public function setDefaultLocale($locale): void
    {
        $this->defaultLocale = $locale;
    }

    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * An extra feature allows you to proxy translated fields of a translatable entity.
     *
     * @param string $method
     *
     * @return mixed The translated value of the field for current locale
     */
    protected function proxyCurrentLocaleTranslation($method, array $arguments = []): mixed
    {
        return \call_user_func_array(
            [$this->translate($this->getCurrentLocale()), $method],
            $arguments
        );
    }

    /**
     * Returns translation entity class name.
     *
     * @return string
     */
    public static function getTranslationEntityClass(): string
    {
        return self::class.'Translation';
    }

    /**
     * Finds specific translation in collection by its locale.
     *
     * @param string $locale The locale (en, ru, fr)
     * @param bool $withNewTranslations searched in new translations too
     */
    protected function findTranslationByLocale($locale, $withNewTranslations = true)
    {
        $translation = $this->getTranslations()->get($locale);

        if ($translation) {
            return $translation;
        }

        if ($withNewTranslations) {
            return $this->getNewTranslations()->get($locale);
        }
    }

    protected function computeFallbackLocale($locale): false|string
    {
        if (false !== strrchr($locale, '_')) {
            return substr($locale, 0, -\strlen(strrchr($locale, '_')));
        }

        return false;
    }
}
