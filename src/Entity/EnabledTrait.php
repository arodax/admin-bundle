<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @deprecated since 6.19.0
 */
trait EnabledTrait
{
    /**
     * @deprecated since 6.19.0
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['read', 'write'])]
    protected bool $enabled = false;

    /**
     * @deprecated since 6.19.0
     */
    public function isEnabled(): bool
    {
        trigger_deprecation('arodax/admin-bundle', '6.19.0', 'Using methods from EnabledTrait trait is deprecated. Implement these methods yourself in your class.');

        return $this->enabled;
    }

    /**
     * @deprecated since 6.19.0
     */
    public function setEnabled(bool $enabled = true): self
    {
        trigger_deprecation('arodax/admin-bundle', '6.19.0', 'Using methods from EnabledTrait trait is deprecated. Implement these methods yourself in your class.');

        $this->enabled = $enabled;

        return $this;
    }
}
