<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Media;

use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Controller\Api\MediaObject\CreateMediaObjectAction;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\EntityListener\MediaObjectEntityListener;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    types: ['https://schema.org/MediaObject'],
    operations: [
        new Get(
            security: 'is_granted(\'READ_MEDIA_OBJECT\', object)'
        ),
        new Delete(
            security: 'is_granted(\'DELETE_MEDIA_OBJECT\', object)'
        ),
        new Post(
            defaults: ['_api_receive' => false],
            controller: CreateMediaObjectAction::class,
            openapiContext: [
                'consumes' => ['multipart/form-data'],
                'parameters' => [
                    [
                        'in' => 'formData',
                        'name' => 'file',
                        'type' => 'file',
                        'description' => 'The file to upload']
                ]
            ],
            security: 'is_granted(\'ROLE_USER\')',
            validationContext: ['groups' => ['Default', 'media_object_create']]
        ),
        new GetCollection(
            security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'
        )],
    normalizationContext: [
        'groups' => ['media_object:read']
    ],
    denormalizationContext: [
        'groups' => ['media_object:write']]
)]
#[Vich\Uploadable]
#[ORM\Entity]
#[ORM\Table(name: 'aa_media_object')]
#[ORM\EntityListeners([MediaObjectEntityListener::class])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['filePath' => 'exact'])]
#[UniqueEntity('filePath')]
class MediaObject implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    #[ORM\Id]
    #[Groups(groups: ['media_object:read', 'media_object:write', 'gallery:write', 'gallery:read'])]
    protected ?int $id = null;

    #[ApiProperty(iris: ['https://schema.org/contentUrl'])]
    #[Groups(groups: ['media_object:read', 'gallery:read'])]
    protected ?string $contentUrl = null;

    #[Vich\UploadableField(mapping: 'media_object', fileNameProperty: 'filePath')]
    #[Assert\NotNull(groups: ['media_object_create'])]
    #[Assert\File(
        maxSize: '128M',
        filenameMaxLength: 128,
        extensions: [
            'jpg' => ['image/jpeg'], 'jpeg' => ['image/jpeg'], 'png' => ['image/png'], 'gif' => ['image/gif'],
            'webp' => ['image/webp'], 'svg' => ['image/svg+xml'], 'mp4' => ['video/mp4'], 'avi' => ['video/x-msvideo'],
            'mov' => ['video/quicktime'], 'mpg' => ['video/mpeg'], 'mpeg' => ['video/mpeg'], 'wmv' => ['video/x-ms-wmv'],
            'flv' => ['video/x-flv'], 'webm' => ['video/webm'], 'mkv' => ['video/x-matroska'], 'mp3' => ['audio/mpeg'],
            'wav' => ['audio/wav'], 'ogg' => ['application/ogg', 'audio/ogg'], 'wma' => ['audio/x-ms-wma'],
            'aac' => ['audio/aac'], 'doc' => ['application/msword'], 'avif' => ['image/avif', 'image/heif'],
            'docx' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
            'xls' => ['application/vnd.ms-excel'],
            'xlsx' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            'ppt' => ['application/vnd.ms-powerpoint'],
            'pptx' => ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
            'txt' => ['text/plain'], 'csv' => ['text/csv'], 'zip' => ['application/zip'],
            'rar' => ['application/x-rar-compressed'], 'gz' => ['application/gzip'], 'tar' => ['application/x-tar'],
            'json' => ['application/json'],
            'pdf' => ['application/pdf']
        ],
    )]
    protected ?File $file = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['media_object:read'])]
    protected ?string $originalFileName = null;

    #[ORM\Column(nullable: true)]
    protected ?string $filePath = null;
    protected ?string $absoluteFilePath = null;
    /*
     * Media object mime type.
     */
    #[ORM\Column(nullable: true)]
    #[Groups(groups: ['media_object:read', 'article:read', 'event:read'])]
    protected ?string $mime = null;

    /*
     * Priority (order) of this entity in the repository.
     */
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['media_object:read', 'media_object:write', 'gallery:read'])]
    protected int $priority = 0;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['media_object:read', 'gallery:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['media_object:write', 'gallery:write'])]
    protected $newTranslations;

    /**
     * Stores additional metadata of the media object.
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['media_object:read', 'media_object:write', 'gallery:read', 'gallery:write'])]
    protected $metadata;

    public function getId(): int
    {
        return $this->id;
    }

    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    public function setContentUrl(?string $contentUrl): self
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getOriginalFileName(): ?string
    {
        return $this->originalFileName;
    }

    public function setOriginalFileName(?string $originalFileName): self
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    #[Groups(groups: ['media_object:read'])]
    public function getOriginalBaseName(): string
    {
        return pathinfo($this->originalFileName, \PATHINFO_FILENAME);
    }

    #[Groups(groups: ['media_object:read'])]
    public function getOriginalExt(): string
    {
        return pathinfo($this->originalFileName, \PATHINFO_EXTENSION);
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getAbsoluteFilePath(): ?string
    {
        return $this->absoluteFilePath;
    }

    public function setAbsoluteFilePath(?string $absoluteFilePath): self
    {
        $this->absoluteFilePath = $absoluteFilePath;
        return $this;
    }

    #[Groups(groups: ['media_object:read', 'gallery:read'])]
    public function getOriginalName(): string
    {
        return $this->filePath;
    }

    public function getMime(): ?string
    {
        return $this->mime;
    }

    public function setMime(?string $mime): self
    {
        $this->mime = $mime;

        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getMetadata(): ?array
    {
        return $this->metadata;
    }

    public function setMetadata(?array $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }
}
