<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Contact;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Arodax\AdminBundle\Repository\Contact\ContactItemRespository;
use Arodax\AdminBundle\Security\Voter\ContactItemVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * This entity represents a contact attached to user.
 */
#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.ContactItemVoter::READ_CONTACT_ITEM.'", object)'),
        new Put(security: 'is_granted("'.ContactItemVoter::UPDATE_CONTACT_ITEM.'", object)'),
        new Delete(security: 'is_granted("'.ContactItemVoter::DELETE_CONTACT_ITEM.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.ContactItemVoter::CREATE_CONTACT_ITEM.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['contact:read']],
    denormalizationContext: ['groups' => ['contact:write']]
)]
#[ORM\Entity(repositoryClass: ContactItemRespository::class)]
#[ORM\Table(name: 'aa_contact_item')]
class ContactItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['contact:read', 'contact:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Contact::class, inversedBy: 'items')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected ?Contact $contact = null;

    /*
     * Type of the contact (email, phone, fax ..).
     */
    #[ORM\Column(type: 'string', length: 191)]
    #[Groups(groups: ['contact:read', 'contact:write', 'user:info'])]
    protected ?string $type = null;

    /*
     * The value of the contact.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['contact:read', 'contact:write', 'user:info'])]
    protected ?string $value = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
