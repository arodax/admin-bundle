<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Contact;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Repository\Contact\ContactRespository;
use Arodax\AdminBundle\Security\Voter\ContactVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * This entity represents a contact attached to user.
 */
#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.ContactVoter::READ_CONTACT.'", object)'),
        new Put(security: 'is_granted("'.ContactVoter::UPDATE_CONTACT.'", object)'),
        new Delete(security: 'is_granted("'.ContactVoter::DELETE_CONTACT.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.ContactVoter::CREATE_CONTACT.'", object)', validationContext: ['groups' => ['']])],
    normalizationContext: ['groups' => ['contact:read']],
    denormalizationContext: ['groups' => ['contact:write']]
)]
#[ORM\Entity(repositoryClass: ContactRespository::class)]
#[ORM\Table(name: 'aa_contact')]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['contact:read', 'contact:write'])]
    protected ?int $id = null;

    // User attached to this contact.
    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EXTRA_LAZY', inversedBy: 'userContacts')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    protected ?User $user = null;

    /**
     * Array of items attached to this contact.
     *
     * @var ArrayCollection
     */
    #[ORM\OneToMany(mappedBy: 'contact', targetEntity: ContactItem::class, cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['contact:read', 'contact:write', 'user:info'])]
    protected $items;

    /**
     * The contact type.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['contact:read', 'contact:write', 'user:info'])]
    protected ?string $type = null;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getItems(): array
    {
        return $this->items->toArray();
    }

    public function removeItem(ContactItem $contactItem): self
    {
        if ($this->items->contains($contactItem)) {
            $this->items->removeElement($contactItem);
            $contactItem->setContact(null);
        }

        return $this;
    }

    public function addItem(ContactItem $contactItem): self
    {
        if (!$this->items->contains($contactItem)) {
            $this->items->add($contactItem);
            $contactItem->setContact($this);
        }

        return $this;
    }

    public function hasItem(ContactItem $contactItem): bool
    {
        return $this->items->contains($contactItem);
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
