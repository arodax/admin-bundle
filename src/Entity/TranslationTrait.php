<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity;

use Arodax\AdminBundle\Model\TranslationMethods;

/**
 * This trait provides method and properties for entity translations entities.
 */
trait TranslationTrait
{
    use TranslationMethods;

    /**
     * Will be mapped to translatable entity
     * by TranslatableSubscriber.
     */
    protected $translatable;
}
