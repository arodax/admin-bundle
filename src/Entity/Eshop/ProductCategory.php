<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\Content\SectionTranslation;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TimestampableInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Eshop\ProductCategoryRepository;
use Arodax\AdminBundle\Security\Voter\ProductCategoryVoter;
use Arodax\AdminBundle\Tree\Mapping\Annotation as Tree;
use Doctrine\Common\Collections\ArrayCollection;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping as ORM;

/**
 * @method SectionTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.ProductCategoryVoter::READ_PRODUCT_CATEGORY.'", object)'),
    new Put(security: 'is_granted("'.ProductCategoryVoter::UPDATE_PRODUCT_CATEGORY.'", object)'),
    new Delete(security: 'is_granted("'.ProductCategoryVoter::DELETE_PRODUCT_CATEGORY.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.ProductCategoryVoter::CREATE_PRODUCT_CATEGORY.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['product_category:read']],
    denormalizationContext: ['groups' => ['product_category:write']]
)]
#[Tree\Tree(type: 'nested')]
#[ORM\Entity(repositoryClass: ProductCategoryRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_product_category')]
#[ORM\Index(columns: ['status'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
class ProductCategory implements TranslatableInterface, TimestampableInterface
{
    use TranslatableTrait;
    use TimestampTrait;

    public const string STATUS_DRAFT = 'draft';
    public const string STATUS_PUBLISHED = 'published';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product_category:read', 'product_category:tree', 'product:read'])]
    protected ?int $id = null;

    #[Tree\Left]
    #[ORM\Column(name: 'lft', type: 'integer')]
    #[Groups(groups: ['product_category:tree'])]

    protected $lft;

    #[Tree\Level]
    #[ORM\Column(name: 'lvl', type: 'integer')]
    #[Groups(groups: ['product_category:tree'])]
    protected $lvl;

    #[Tree\Right]
    #[ORM\Column(name: 'rgt', type: 'integer')]
    #[Groups(groups: ['product_category:tree'])]
    protected $rgt;

    #[Tree\Root]
    #[ORM\ManyToOne(targetEntity: 'ProductCategory')]
    #[ORM\JoinColumn(name: 'tree_root', referencedColumnName: 'id', onDelete: 'CASCADE')]
    protected $root;

    /**
     * @var ProductCategory|null
     */
    #[Tree\ParentNode]
    #[MaxDepth(1)]
    #[ORM\ManyToOne(targetEntity: 'ProductCategory', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['product_category:write', 'product_category:tree'])]
    protected $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: 'ProductCategory')]
    #[ORM\OrderBy(value: ['lft' => 'ASC'])]
    #[Groups(groups: ['product_category:read'])]
    protected $children;

    /**
     * Array containing all entity translations already persisted into the entity.
     *
     * @var ProductCategoryTranslation[]
     */
    #[Groups(groups: ['product_category:read', 'product:read', 'product_category:tree'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['product_category:write'])]
    protected $newTranslations;

    /*
     * Property containing information about the state of the ProductCategories.
     *
     * By default, the productCategory is in the draft state.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['product_category:read', 'product_category:write', 'product_category:tree'])]
    protected string $status = self::STATUS_DRAFT;

    /*
     * DateTime when gallery has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?\DateTimeImmutable $publishedAt = null;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['product_category:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /**
     * The URL of the product category cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected $covers = [];

    /**
     * @var Product[]
     */
    #[ORM\ManyToMany(targetEntity: Product::class, mappedBy: 'categories', fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected $products;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['product_category:read', 'product_category:write', 'product_category:tree'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['product_category:read', 'product_category:write', 'product_category:tree'])]
    private bool $deletable = true;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id of the productCategory.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get the parent node of the productCategory.
     *
     * If parent is null, this productCategory is on the root level.
     *
     * @return ProductCategory|null
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * Set the parent productCategory of this productCategory.
     *
     * Setting this value will not affect lft, rgt and lvl properties of the three.
     * These values need to be correctly calculated by the data persister.
     *
     * @param ProductCategory|null $parent
     *
     * @return ProductCategory
     */
    public function setParent(self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getCovers(): ?array
    {
        return $this->covers;
    }

    public function setCovers(?array $covers): self
    {
        $this->covers = $covers;

        return $this;
    }

    /*
     * Add product to a section.
     */
    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
        }

        $product->addCategory($this);

        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products->toArray();
    }

    /**
     * @return ProductCategory[]
     */
    public function getChildren(): array
    {
        return $this->children->toArray();
    }

    #[Groups(groups: ['product_category:tree', 'product_category:read'])]
    #[Deprecated(reason: 'This method is deprecated to use it as item name, use "getHeadline" instead', replacement: '%class%->getHeadline()')]
    public function getTitle(): ?string
    {
        return $this->translate()->getName();
    }

    #[Groups(groups: ['product_category:tree', 'product_category:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getHeadline();
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): void
    {
        $this->lft = $lft;
    }

    public function getLvl(): ?int
    {
        return $this->lvl;
    }

    public function setLvl(?int $lvl): void
    {
        $this->lvl = $lvl;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(?int $rgt): void
    {
        $this->rgt = $rgt;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): ProductCategory
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    public function setDeletable(bool $deletable): ProductCategory
    {
        $this->deletable = $deletable;
        return $this;
    }
}
