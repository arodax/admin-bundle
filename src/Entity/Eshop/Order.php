<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\EntityListener\OrderEntityListener;
use Arodax\AdminBundle\Model\TimestampableInterface;
use Doctrine\Common\Collections\ArrayCollection;;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GroupSequenceProvider;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\GroupSequenceProviderInterface;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_ORDER\', object)'), new Put(security: 'is_granted(\'UPDATE_ORDER\', object)'), new Delete(security: 'is_granted(\'DELETE_ORDER\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_ORDER\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['order:read', 'order_item:read']], denormalizationContext: ['groups' => ['order:write', 'order_item:write']])]
#[ORM\EntityListeners([OrderEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_order')]
#[GroupSequenceProvider]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'orderNumber'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact', 'orderNumber' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt'])]
class Order implements GroupSequenceProviderInterface, TimestampableInterface
{
    final public const string STATUS_NEW = 'new';
    final public const string STATUS_IN_PROGRESS = 'in_progress';
    final public const string STATUS_PAID = 'paid';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected ?int $id = null;

    #[ORM\Column(name: 'order_number', type: 'bigint', unique: true, nullable: false)]
    #[Groups(groups: ['invoice:read', 'invoice:write', 'order:read', 'order:write', 'shopping_cart:public_read'])]
    #[Length(max: 10)]
    protected ?string $orderNumber = null;

    #[ORM\OneToOne(inversedBy: 'order', targetEntity: ShoppingCart::class, orphanRemoval: true)]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected ?ShoppingCart $shoppingCart = null;

    #[ORM\ManyToOne(targetEntity: ShippingMethod::class)]
    protected ?ShippingMethod $shippingMethod = null;

    #[ORM\ManyToOne(targetEntity: PaymentMethod::class)]
    protected ?PaymentMethod $paymentMethod = null;

    #[ORM\Column(name: 'status', type: 'string', length: 191, nullable: false)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected string $status = self::STATUS_NEW;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $billingCompanyName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $billingCompanyId = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $billingTaxId = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $billingHonorific = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    protected ?string $billingFirstName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    protected ?string $billingLastName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    #[Email(groups: ['Billing'])]
    protected ?string $billingEmail = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    #[Regex(pattern: '/^\+[0-9]{1,3}[0-9]{4,14}(?:x.+)?$/', groups: ['Billing'])]
    protected ?string $billingPhoneNumber = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    protected ?string $billingStreet = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $billingAddressDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    protected ?string $billingCity = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    protected ?string $billingPostCode = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Billing'])]
    #[Country(groups: ['Billing'])]
    protected ?string $billingCountry = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $shippingCompanyName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $shippingHonorific = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Shipping'])]
    protected ?string $shippingFirstName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Shipping'])]
    protected ?string $shippingLastName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Shipping'])]
    #[Email(groups: ['Shipping'])]
    protected ?string $shippingEmail = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(groups: ['Shipping'])]
    #[Regex(pattern: '/^\+[0-9]{1,3}[0-9]{4,14}(?:x.+)?$/', groups: ['Shipping'])]
    protected ?string $shippingPhoneNumber = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: false, groups: ['Shipping'])]
    protected ?string $shippingStreet = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $shippingAddressDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: false, groups: ['Shipping'])]
    protected ?string $shippingCity = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: false, groups: ['Shipping'])]
    protected ?string $shippingPostCode = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[Country(groups: ['Shipping'])]
    #[NotBlank(allowNull: false, groups: ['Shipping'])]
    protected ?string $shippingCountry = null;

    #[ORM\Column(name: 'currency', type: 'string', length: 3, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[Currency]
    protected ?string $currency = null;

    /*
     * The order price without tax not including the shipping cost.
     */
    #[ORM\Column(name: 'price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: true)]
    protected ?int $priceWithoutTax = null;

    /*
     * The order price with tax not including the shipping cost.
     */
    #[ORM\Column(name: 'price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(message: 'Price including tax is not defined.')]
    protected ?int $priceWithTax = null;

    /*
     * The shipping price without tax.
     */
    #[ORM\Column(name: 'shipping_price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: true)]
    protected ?int $shippingPriceWithoutTax = null;

    /*
     * The shipping price with tax.
     */
    #[ORM\Column(name: 'shipping_price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(message: 'Shipping price including tax is not defined.')]
    protected ?int $shippingPriceWithTax = null;


    /**
     * The payment method price without tax.
     */
    #[ORM\Column(name: 'payment_price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: true)]
    protected ?int $paymentPriceWithoutTax = null;

    /**
     * The payment method price without tax.
     */
    #[ORM\Column(name: 'payment_price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[NotBlank(allowNull: true)]
    protected ?int $paymentPriceWithTax = null;

    /**
     * @var ArrayCollection|OrderPayment[]|null
     */
    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderPayment::class, cascade: ['ALL'])]
    #[ORM\OrderBy(value: ['createdAt' => 'DESC'])]
    protected $payments;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    public function getShoppingCart(): ?ShoppingCart
    {
        return $this->shoppingCart;
    }

    public function setShoppingCart(?ShoppingCart $shoppingCart): self
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    public function getShippingMethod(): ?ShippingMethod
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod(?ShippingMethod $shippingMethod): self
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getBillingCompanyName(): ?string
    {
        return $this->billingCompanyName;
    }

    public function setBillingCompanyName(?string $billingCompanyName): self
    {
        $this->billingCompanyName = $billingCompanyName;

        return $this;
    }

    public function getBillingCompanyId(): ?string
    {
        return $this->billingCompanyId;
    }

    public function setBillingCompanyId(?string $billingCompanyId): self
    {
        $this->billingCompanyId = $billingCompanyId;

        return $this;
    }

    public function getBillingTaxId(): ?string
    {
        return $this->billingTaxId;
    }

    public function setBillingTaxId(?string $billingTaxId): self
    {
        $this->billingTaxId = $billingTaxId;

        return $this;
    }

    public function getBillingHonorific(): ?string
    {
        return $this->billingHonorific;
    }

    public function setBillingHonorific(?string $billingHonorific): self
    {
        $this->billingHonorific = $billingHonorific;

        return $this;
    }

    public function getShippingHonorific(): ?string
    {
        return $this->shippingHonorific;
    }

    public function setShippingHonorific(?string $shippingHonorific): self
    {
        $this->shippingHonorific = $shippingHonorific;

        return $this;
    }

    public function getBillingFirstName(): ?string
    {
        return $this->billingFirstName;
    }

    public function setBillingFirstName(?string $billingFirstName): self
    {
        $this->billingFirstName = $billingFirstName;

        return $this;
    }

    public function getBillingLastName(): ?string
    {
        return $this->billingLastName;
    }

    public function setBillingLastName(?string $billingLastName): self
    {
        $this->billingLastName = $billingLastName;

        return $this;
    }

    public function getBillingEmail(): ?string
    {
        return $this->billingEmail;
    }

    public function setBillingEmail(?string $billingEmail): self
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    public function getBillingPhoneNumber(): ?string
    {
        return $this->billingPhoneNumber;
    }

    public function setBillingPhoneNumber(?string $billingPhoneNumber): self
    {
        $this->billingPhoneNumber = $billingPhoneNumber;

        return $this;
    }

    public function getBillingStreet(): ?string
    {
        return $this->billingStreet;
    }

    public function setBillingStreet(?string $billingStreet): self
    {
        $this->billingStreet = $billingStreet;

        return $this;
    }

    public function getBillingAddressDescription(): ?string
    {
        return $this->billingAddressDescription;
    }

    public function setBillingAddressDescription(?string $billingAddressDescription): self
    {
        $this->billingAddressDescription = $billingAddressDescription;

        return $this;
    }

    public function getBillingCity(): ?string
    {
        return $this->billingCity;
    }

    public function setBillingCity(?string $billingCity): self
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    public function getBillingPostCode(): ?string
    {
        return $this->billingPostCode;
    }

    public function setBillingPostCode(?string $billingPostCode): self
    {
        $this->billingPostCode = $billingPostCode;

        return $this;
    }

    public function getBillingCountry(): ?string
    {
        return $this->billingCountry;
    }

    public function setBillingCountry(?string $billingCountry): self
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }

    public function getShippingCompanyName(): ?string
    {
        return $this->shippingCompanyName;
    }

    public function setShippingCompanyName(?string $shippingCompanyName): self
    {
        $this->shippingCompanyName = $shippingCompanyName;

        return $this;
    }

    public function getShippingFirstName(): ?string
    {
        return $this->shippingFirstName;
    }

    public function setShippingFirstName(?string $shippingFirstName): self
    {
        $this->shippingFirstName = $shippingFirstName;

        return $this;
    }

    public function getShippingLastName(): ?string
    {
        return $this->shippingLastName;
    }

    public function setShippingLastName(?string $shippingLastName): self
    {
        $this->shippingLastName = $shippingLastName;

        return $this;
    }

    public function getShippingEmail(): ?string
    {
        return $this->shippingEmail;
    }

    public function setShippingEmail(?string $shippingEmail): self
    {
        $this->shippingEmail = $shippingEmail;

        return $this;
    }

    public function getShippingPhoneNumber(): ?string
    {
        return $this->shippingPhoneNumber;
    }

    public function setShippingPhoneNumber(?string $shippingPhoneNumber): self
    {
        $this->shippingPhoneNumber = $shippingPhoneNumber;

        return $this;
    }

    public function getShippingStreet(): ?string
    {
        return $this->shippingStreet;
    }

    public function setShippingStreet(?string $shippingStreet): self
    {
        $this->shippingStreet = $shippingStreet;

        return $this;
    }

    public function getShippingAddressDescription(): ?string
    {
        return $this->shippingAddressDescription;
    }

    public function setShippingAddressDescription(?string $shippingAddressDescription): self
    {
        $this->shippingAddressDescription = $shippingAddressDescription;

        return $this;
    }

    public function getShippingCity(): ?string
    {
        return $this->shippingCity;
    }

    public function setShippingCity(?string $shippingCity): self
    {
        $this->shippingCity = $shippingCity;

        return $this;
    }

    public function getShippingPostCode(): ?string
    {
        return $this->shippingPostCode;
    }

    public function setShippingPostCode(?string $shippingPostCode): self
    {
        $this->shippingPostCode = $shippingPostCode;

        return $this;
    }

    public function getShippingCountry(): ?string
    {
        return $this->shippingCountry;
    }

    public function setShippingCountry(?string $shippingCountry): self
    {
        $this->shippingCountry = $shippingCountry;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getPriceWithoutTax(): ?int
    {
        return $this->priceWithoutTax;
    }

    public function setPriceWithoutTax(?int $priceWithoutTax): self
    {
        $this->priceWithoutTax = $priceWithoutTax;

        return $this;
    }

    public function getPriceWithTax(): ?int
    {
        return $this->priceWithTax;
    }

    public function setPriceWithTax(?int $priceWithTax): self
    {
        $this->priceWithTax = $priceWithTax;

        return $this;
    }

    public function getShippingPriceWithoutTax(): ?int
    {
        return $this->shippingPriceWithoutTax;
    }

    public function setShippingPriceWithoutTax(?int $shippingPriceWithoutTax): self
    {
        $this->shippingPriceWithoutTax = $shippingPriceWithoutTax;

        return $this;
    }

    public function getShippingPriceWithTax(): ?int
    {
        return $this->shippingPriceWithTax;
    }

    public function setShippingPriceWithTax(?int $shippingPriceWithTax): self
    {
        $this->shippingPriceWithTax = $shippingPriceWithTax;

        return $this;
    }

    public function getPaymentPriceWithoutTax(): ?int
    {
        return $this->paymentPriceWithoutTax;
    }

    public function setPaymentPriceWithoutTax(?int $paymentPriceWithoutTax): self
    {
        $this->paymentPriceWithoutTax = $paymentPriceWithoutTax;
        return $this;
    }

    public function getPaymentPriceWithTax(): ?int
    {
        return $this->paymentPriceWithTax;
    }

    public function setPaymentPriceWithTax(?int $paymentPriceWithTax): self
    {
        $this->paymentPriceWithTax = $paymentPriceWithTax;
        return $this;
    }

    public function getPayments(): array
    {
        return $this->payments->toArray();
    }

    /*
     * Get the latest order payment.
     */
    #[Groups(groups: ['shopping_cart:public_read'])]
    public function getPayment(): ?OrderPayment
    {
        if (!$this->payments->isEmpty()) {
            return $this->payments->first();
        }

        return null;
    }

    public function addPayment(OrderPayment $orderPayment): void
    {
        if (!$this->payments->contains($orderPayment)) {
            $this->payments->add($orderPayment);
        }
    }

    /**
     * This method provides extra validation logic.
     *
     * @internal this method should never be used outside the validator
     */
    #[Callback(groups: ['Billing'])]
    public function validationCallback(ExecutionContextInterface $context): void
    {
        if ($this->getBillingCompanyName() && !$this->getBillingCompanyId()) {
            $context->buildViolation('This value should not be blank.')->atPath('billingCompanyId')->addViolation();
        }
        if (!$this->getBillingCompanyName() && $this->getBillingCompanyId()) {
            $context->buildViolation('This value should not be blank.')->atPath('billingCompanyName')->addViolation();
        }
        if ($this->getBillingHonorific() && !$this->getBillingLastName()) {
            $context->buildViolation('This value should not be blank.')->atPath('billingLastName')->addViolation();
        }
        if ($this->getShippingHonorific() && !$this->getShippingLastName()) {
            $context->buildViolation('This value should not be blank.')->atPath('shippingLastName')->addViolation();
        }
    }

    /*
     * Return the requested validation group sequences.
     */
    public function getGroupSequence(): array
    {
        $groups = ['Default', 'Billing'];

        if ($this->getShippingAddressDescription() || $this->getShippingFirstName() || $this->getShippingLastName() || $this->getShippingCompanyName() || $this->getShippingStreet() || $this->getShippingCity() || $this->getShippingCountry()) {
            $groups[] = 'Shipping';
        }

        return [$groups];
    }

    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    public function getTotalOrderPriceWithTax(): int
    {
        return $this->priceWithTax + $this->shippingPriceWithTax + $this->paymentPriceWithTax;
    }

    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    public function getTotalOrderPriceWithoutTax(): int
    {
        return $this->priceWithoutTax + $this->shippingPriceWithoutTax + $this->paymentPriceWithoutTax;
    }

    public function getBillingFullName(): string
    {
        return $this->billingHonorific. ' '. $this->billingFirstName . ' ' . $this->billingLastName;
    }

    public function getShippingFullName(): string
    {
        return $this->shippingHonorific. ' '. $this->shippingFirstName . ' ' . $this->shippingLastName;
    }

    public function getBillingAddress(): string
    {
        return $this->billingStreet . ', ' . $this->billingCity . ', ' . $this->billingPostCode . ', ' . $this->billingCountry;
    }

    public function getShippingAddress(): string
    {
        return $this->shippingStreet . ', ' . $this->shippingCity . ', ' . $this->shippingPostCode . ', ' . $this->shippingCountry;
    }

    #[Groups(['order:read'])]
    public function getShippingMethodName(): ?string
    {
        return $this->shippingMethod?->translate()?->getTitle();
    }

    #[Groups(['order:read'])]
    public function getPaymentMethodName(): ?string
    {
        return $this->paymentMethod?->translate()?->getName();
    }
}
