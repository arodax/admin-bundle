<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\EntityListener\ShoppingCartEntityListener;
use Arodax\AdminBundle\Model\TimestampableInterface;
use Arodax\AdminBundle\Security\Voter\ShoppingCartVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted("'.ShoppingCartVoter::READ_SHOPPING_CART.'", object)'),
    new Put(security: 'is_granted("'.ShoppingCartVoter::UPDATE_SHOPPING_CART.'", object)'),
    new Delete(security: 'is_granted("'.ShoppingCartVoter::DELETE_SHOPPING_CART.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.ShoppingCartVoter::CREATE_SHOPPING_CART.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['shopping_cart:read', 'shopping_cart_item:read', 'order:read']],
    denormalizationContext: ['groups' => ['shopping_cart:write', 'shopping_cart_item:write']]
)]
#[ORM\EntityListeners([ShoppingCartEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_shopping_cart')]
#[ORM\HasLifecycleCallbacks]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: ExistsFilter::class, properties: ['items', 'order'])]
class ShoppingCart implements TimestampableInterface
{
    public const string STATUS_OPEN = 'open';
    public const string STATUS_CLOSED = 'closed';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read', 'order:read'])]
    protected ?int $id;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['ALL'], fetch: 'LAZY')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write'])]
    protected ?User $user = null;

    /**
     * @var ShoppingCartItem[]
     */
    #[ORM\OneToMany(mappedBy: 'shoppingCart', targetEntity: ShoppingCartItem::class, cascade: ['REMOVE'], orphanRemoval: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read', 'shopping_cart_item:read', 'shopping_cart_item:write', 'order:read'])]
    protected $items;

    #[ORM\Column(name: 'status', type: 'string', length: 191, nullable: false)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected string $status = self::STATUS_OPEN;

    #[ORM\Column(name: 'currency', type: 'string', length: 3, nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected ?string $currency;

    #[ORM\Column(name: 'price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected ?int $priceWithoutTax = null;

    #[ORM\Column(name: 'price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected ?int $priceWithTax = null;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:write', 'shopping_cart:public_read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    #[ORM\OneToOne(mappedBy: 'shoppingCart', targetEntity: Order::class, fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['order:read', 'shopping_cart:public_read'])]
    protected ?Order $order;

    /*
     * The session id, for which the shopping cart has been created.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    protected ?string $sessionId;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getItems(): array
    {
        return $this->items->toArray();
    }

    public function addItem(ShoppingCartItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }

        return $this;
    }

    public function removeItem(ShoppingCartItem $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read'])]
    public function getItemsCount(): int
    {
        return array_reduce($this->items->toArray(), function ($result, $item) {
            // @var ShoppingCartItem $item
            $result += $item->getQuantity();

            return $result;
        }, 0);
    }

    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read'])]
    public function getItemsVariantsCount(): int
    {
        return $this->items->count();
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getPriceWithoutTax(): ?int
    {
        return $this->priceWithoutTax;
    }

    public function setPriceWithoutTax(?int $priceWithoutTax): self
    {
        $this->priceWithoutTax = $priceWithoutTax;

        return $this;
    }

    public function getPriceWithTax(): ?int
    {
        return $this->priceWithTax;
    }

    public function setPriceWithTax(?int $priceWithTax): self
    {
        $this->priceWithTax = $priceWithTax;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(?string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $cretedAt): self
    {
        $this->createdAt = $cretedAt;

        return $this;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
