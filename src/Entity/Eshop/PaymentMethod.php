<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslatableOrderFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\PaymentProvider\DefaultPaymentProvider;
use Arodax\AdminBundle\PaymentProvider\PaymentProviderInterface;
use Arodax\AdminBundle\Security\Voter\PaymentMethodVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

/**
 * This entity represent a payment method.
 *
 * @method PaymentMethodTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.PaymentMethodVoter::READ_PAYMENT_METHOD.'", object)'),
    new Put(security: 'is_granted("'.PaymentMethodVoter::UPDATE_PAYMENT_METHOD.'", object)'),
    new Delete(security: 'is_granted("'.PaymentMethodVoter::DELETE_PAYMENT_METHOD.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(
        securityPostDenormalize: 'is_granted("'.PaymentMethodVoter::CREATE_PAYMENT_METHOD.'", object)',
        validationContext: ['groups' => ['']]
    )],
    normalizationContext: ['groups' => ['payment_method:read']],
    denormalizationContext: ['groups' => ['payment_method:write']]
)]
#[ORM\Table(name: 'aa_payment_method')]
#[ORM\Entity]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: TranslatableOrderFilter::class, properties: ['id', 'translations.*.name'], arguments: ['orderParameterName' => 'order'])]
class PaymentMethod implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private ?int $id = null;

    #[ORM\Column(type: 'text', length: 191, nullable: false)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private string $provider = DefaultPaymentProvider::PROVIDER_NAME;

    /**
     * This column stores the rules in the Symfony expression language for the payment credentials. The payment provider,
     * will evaluate these expressions and create the real payment credentials for the OrderPayment::$credentials.
     *
     * @see PaymentProviderInterface::createPaymentIntent()
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private ?array $credentialExpressions;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['payment_method:read', 'shopping_cart:public_read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['payment_method:write'])]
    protected $newTranslations;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private bool $enabled = false;

    /** @var Collection<int, PaymentMethod>  */
    #[ORM\ManyToMany(targetEntity: ShippingMethod::class, mappedBy: 'paymentMethods')]
    #[ORM\JoinTable(name: 'aa_payment_method_shipping_method')]
    private Collection $shippingMethods;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    private bool $deletable = true;

    #[ORM\Column(name: 'price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    #[PositiveOrZero]
    protected ?int $priceWithoutTax = null;

    #[ORM\Column(name: 'tax_rate', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    #[PositiveOrZero]
    protected ?int $taxRate = null;

    #[ORM\Column(name: 'tax_price', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    #[PositiveOrZero]
    protected ?int $taxPrice = null;

    #[ORM\Column(name: 'price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    #[PositiveOrZero]
    protected ?int $priceWithTax = null;

    #[ORM\Column(type: 'string', length: 3, nullable: true)]
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read', 'payment_method:read', 'payment_method:write'])]
    #[Currency]
    protected ?string $currency = null;

    public function __construct()
    {
        $this->shippingMethods = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /*
     * Get the PaymentProvider PROVIDER_NAME value, which is used to determine, which payment provider
     * will handle this payment method.
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /*
     * Set the PaymentProvider PROVIDER_NAME value, which is used to determine, which payment provider
     * will handle this payment method. This constant might be found in the classes which implements
     * PaymentProviderInterface.
     */
    public function setProvider(string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    /*
     * Get the PaymentMethod name.
     */
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read'])]
    public function getName(): ?string
    {
        return $this->translate()->getName();
    }

    /*
     * Get the PaymentMethod description.
     */
    #[Groups(groups: ['shopping_cart:read', 'shopping_cart:public_read'])]
    public function getDescription(): ?string
    {
        return $this->translate()->getDescription();
    }

    public function getCredentialExpressions(): ?array
    {
        return $this->credentialExpressions;
    }

    public function setCredentialExpressions(?array $credentialExpressions): self
    {
        $this->credentialExpressions = $credentialExpressions;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getShippingMethods(): array
    {
        return $this->shippingMethods->toArray();
    }

    public function addShippingMethod(ShippingMethod $shippingMethod): self
    {
        if (!$this->shippingMethods->contains($shippingMethod)) {
            $this->shippingMethods->add($shippingMethod);
            $shippingMethod->addPaymentMethod($this);
        }

        return $this;
    }

    public function removeShippingMethod(ShippingMethod $shippingMethod): self
    {
        if ($this->shippingMethods->removeElement($shippingMethod)) {
            $shippingMethod->removePaymentMethod($this);
        }

        return $this;
    }

    #[Groups(groups: ['payment_method:read', 'shopping_cart:public_read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }

    public function getPriceWithoutTax(): ?int
    {
        return $this->priceWithoutTax;
    }

    public function setPriceWithoutTax(?int $priceWithoutTax): PaymentMethod
    {
        $this->priceWithoutTax = $priceWithoutTax;
        return $this;
    }

    public function getTaxRate(): ?int
    {
        return $this->taxRate;
    }

    public function setTaxRate(?int $taxRate): self
    {
        $this->taxRate = $taxRate;
        return $this;
    }


    public function getTaxPrice(): ?int
    {
        return $this->taxPrice;
    }

    public function setTaxPrice(?int $taxPrice): self
    {
        $this->taxPrice = $taxPrice;
        return $this;
    }

    public function getPriceWithTax(): ?int
    {
        return $this->priceWithTax;
    }

    public function setPriceWithTax(?int $priceWithTax): self
    {
        $this->priceWithTax = $priceWithTax;
        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;
        return $this;
    }
}
