<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslatableOrderFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Security\Voter\ShippingMethodVoter;
use Arodax\AdminBundle\ShippingProvider\DefaultShippingProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted("'.ShippingMethodVoter::READ_SHIPPING_METHOD.'", object)'),
    new Put(security: 'is_granted("'.ShippingMethodVoter::UPDATE_SHIPPING_METHOD.'", object)'),
    new Delete(security: 'is_granted("'.ShippingMethodVoter::DELETE_SHIPPING_METHOD.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.ShippingMethodVoter::CREATE_SHIPPING_METHOD.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['shipping_method:read', 'shipping_method_item:read']],
    denormalizationContext: ['groups' => ['shipping_method:write', 'shipping_method_item:write']]
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_shipping_method')]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['enabled'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['title' => 'partial'])]
#[ApiFilter(filterClass: TranslatableOrderFilter::class, properties: ['id', 'translations.*.title'], arguments: ['orderParameterName' => 'order'])]
class ShippingMethod implements TranslatableInterface
{
    use TranslatableTrait;
    private const array POSTAGE_PRICE_RULES_KEYS = ['expression', 'currency', 'priceWithTax', 'priceWithoutTax'];

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['shipping_method:read', 'shopping_cart:public_read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['shipping_method:write'])]
    protected $newTranslations;

    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Id]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private ?int $id = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private ?array $postagePriceRules = null;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write'])]
    private bool $enabled = false;

    /*
     * The list of country codes, where the shipping method can be used.
     */
    #[ORM\Column(type: 'json', nullable: false)]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private array $supportedCountries = [];

    /** @var Collection<int, PaymentMethod>  */
    #[ORM\ManyToMany(targetEntity: PaymentMethod::class, inversedBy: 'shippingMethods')]
    #[ORM\JoinTable(name: 'aa_shipping_method_payment_method')]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private Collection $paymentMethods;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private bool $deletable = true;

    #[ORM\Column(type: 'text', length: 191, nullable: false)]
    #[Groups(groups: ['shipping_method:read', 'shipping_method:write', 'shopping_cart:public_read'])]
    private ?string $provider = DefaultShippingProvider::PROVIDER_NAME;

    public function __construct()
    {
        $this->paymentMethods = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPostagePriceRules(): ?array
    {
        return $this->postagePriceRules;
    }

    public function setPostagePriceRules(?array $postagePriceRules): self
    {
        $this->postagePriceRules = $postagePriceRules;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSupportedCountries(): array
    {
        return $this->supportedCountries ?? [];
    }

    public function setSupportedCountries(array $supportedCountries): self
    {
        $this->supportedCountries = $supportedCountries;

        return $this;
    }

    #[Callback]
    public function validatePostagePriceRules(ExecutionContextInterface $context): void
    {
        if ($this->enabled && (!$this->postagePriceRules || [] === $this->postagePriceRules)) {
            $context->buildViolation('Shipping cost expression rules must be defined, when the shipping method is enabled.')
                ->atPath('postagePriceRules')
                ->addViolation();
        }
        if ($this->postagePriceRules && $this->postagePriceRules !== []) {
            foreach ($this->postagePriceRules as $postagePriceRule) {
                if (\count(array_intersect_key(array_flip(self::POSTAGE_PRICE_RULES_KEYS), $postagePriceRule)) !== \count(self::POSTAGE_PRICE_RULES_KEYS)) {
                    $context->buildViolation('The shipping cost expression rule must contain all following keys: '.implode(', ', self::POSTAGE_PRICE_RULES_KEYS))
                        ->atPath('postagePriceRules')
                        ->addViolation();
                    break;
                }
            }
        }
    }

    public function getPaymentMethods(): array
    {
        return $this->paymentMethods->toArray();
    }

    public function addPaymentMethod(PaymentMethod $paymentMethod): self
    {
        if (!$this->paymentMethods->contains($paymentMethod)) {
            $this->paymentMethods->add($paymentMethod);
        }

        return $this;
    }

    public function removePaymentMethod(PaymentMethod $paymentMethod): self
    {
        $this->paymentMethods->removeElement($paymentMethod);

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;
        return $this;
    }

    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): self
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        return $this;
    }
}
