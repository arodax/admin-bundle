<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Doctrine\ORM\Mapping as ORM;

/**
 * This entity represent a product variant price.
 *
 * @method ProductVariantPriceTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_PRODUCT_VARIANT_PRICE\', object)'),
    new Put(security: 'is_granted(\'UPDATE_PRODUCT_VARIANT_PRICE\', object)'),
    new Delete(security: 'is_granted(\'DELETE_PRODUCT_VARIANT_PRICE\', object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_PRODUCT_VARIANT_PRICE\', object)',  validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['product:read']],
    denormalizationContext: ['groups' => ['product:write']]
)]
#[ORM\Entity()]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_product_variant_price')]
#[ORM\Index(columns: ['price_without_tax', 'currency'])]
#[ORM\Index(columns: ['price_with_tax', 'currency'])]
class ProductVariantPrice
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart_item:read'])]
    protected ?int $id = null;

    #[ORM\Column(name: 'price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read', 'shopping_cart_item:read'])]
    #[PositiveOrZero]
    protected ?int $priceWithoutTax = null;

    #[ORM\Column(name: 'tax_rate', type: 'integer', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read', 'shopping_cart_item:read'])]
    #[PositiveOrZero]
    protected ?int $taxRate = null;

    #[ORM\Column(name: 'tax_price', type: 'integer', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read', 'shopping_cart_item:read'])]
    #[PositiveOrZero]
    protected ?int $taxPrice = null;

    #[ORM\Column(name: 'price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read', 'shopping_cart_item:read'])]
    #[PositiveOrZero]
    protected ?int $priceWithTax = null;

    #[ORM\Column(type: 'string', length: 3, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read', 'shopping_cart_item:read'])]
    protected ?string $currency = null;

    #[ORM\ManyToOne(targetEntity: ProductVariant::class, inversedBy: 'prices')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected ProductVariant $productVariant;

    /*
     *  Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['shopping_cart:public_read', 'shopping_cart:public_read', 'product:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['product:write'])]
    protected $newTranslations;

    public function getId(): int
    {
        return $this->id;
    }

    public function getPriceWithoutTax(): ?int
    {
        return $this->priceWithoutTax;
    }

    public function setPriceWithoutTax(?int $priceWithoutTax): self
    {
        $this->priceWithoutTax = $priceWithoutTax;

        return $this;
    }

    public function getTaxRate(): ?int
    {
        return $this->taxRate;
    }

    public function setTaxRate(?int $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function getTaxPrice(): ?int
    {
        return $this->taxPrice;
    }

    public function setTaxPrice(?int $taxPrice): self
    {
        $this->taxPrice = $taxPrice;

        return $this;
    }

    public function getPriceWithTax(): ?int
    {
        return $this->priceWithTax;
    }

    public function setPriceWithTax(?int $priceWithTax): self
    {
        $this->priceWithTax = $priceWithTax;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getProductVariant(): ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }


}
