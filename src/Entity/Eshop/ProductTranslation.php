<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\ProductTranslationEntityListener;
use Arodax\AdminBundle\Model\SluggableInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;
use Doctrine\ORM\Mapping as ORM;

#[ORM\EntityListeners([ProductTranslationEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_product_translation')]
class ProductTranslation implements SluggableInterface
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?int $id = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected $locale;

    /*
     * The title of the product.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $title = null;

    /*
     * The main headline for the product, usually identifies with the heading element.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?string $name = null;

    /*
     * The main product content.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $content = null;

    /**
     * Product keywords.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?array $metaKeywords = [];

    /*
     * A canonical link for this product.
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $canonicalLink = null;

    /**
     * Robots handling for the product.
     *
     * @var array<string>|null
     */
    #[Choice(choices: ['noindex', 'index', 'follow', 'nofollow', 'noimageindex', 'none', 'noarchive', 'nosnippet'])]
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?array $metaRobots = [];

    /*
     * Product description.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $metaDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $slug = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getMetaKeywords(): ?array
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?array $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getCanonicalLink(): ?string
    {
        return $this->canonicalLink;
    }

    public function setCanonicalLink(?string $canonicalLink): self
    {
        $this->canonicalLink = $canonicalLink;

        return $this;
    }

    public function getMetaRobots(): ?array
    {
        return $this->metaRobots;
    }

    public function setMetaRobots(?array $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    #[Groups(groups: ['product:read'])]
    public function getHeadline(): ?string
    {
        return $this->name;
    }
}
