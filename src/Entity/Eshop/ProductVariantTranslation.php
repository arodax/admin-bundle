<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'aa_product_variant_translation')]
class ProductVariantTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?int $id = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected $locale;

    /*
     * The name of the product variant, usually describes the color, or the size.
     * If product has only one variant this might be empty.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?string $name = null;

    /*
     * The additional content of the product variant (i.e. description or more information about the variant).
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?string $content = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
