<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\ProductCategoryTranslationEntityListener;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;
use Doctrine\ORM\Mapping as ORM;

#[ORM\EntityListeners([ProductCategoryTranslationEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_product_category_translation')]
class ProductCategoryTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product_category:read', 'product_category:read'])]
    protected ?int $id = null;

    /*
     * The category name.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write', 'product:read'])]
    protected ?string $name = null;

    /*
     * The title of the category.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?string $title = null;

    /*
     * The category description.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?string $description = null;

    /**
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected $locale;

    /**
     * Category keywords.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?array $metaKeywords = [];

    /*
     * A canonical link for this category.
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?string $canonicalLink = null;

    /**
     * Robots handling for the category.
     *
     * @var array<string>|null
     */
    #[Choice(choices: ['noindex', 'index', 'follow', 'nofollow', 'noimageindex', 'none', 'noarchive', 'nosnippet'])]
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?array $metaRobots = [];

    /*
     * Category description.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?string $metaDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['product_category:read', 'product_category:write'])]
    protected ?string $slug = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMetaKeywords(): ?array
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?array $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getCanonicalLink(): ?string
    {
        return $this->canonicalLink;
    }

    public function setCanonicalLink(?string $canonicalLink): self
    {
        $this->canonicalLink = $canonicalLink;

        return $this;
    }

    public function getMetaRobots(): ?array
    {
        return $this->metaRobots;
    }

    public function setMetaRobots(?array $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    #[Groups(groups: ['product_category:read', 'product_category:tree', 'product:read'])]
    public function getHeadline():?string
    {
        return $this->name;
    }
}
