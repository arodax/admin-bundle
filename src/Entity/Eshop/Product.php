<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\EntityListener\ProductEntityListener;
use Arodax\AdminBundle\Model\Eshop\ProductInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Eshop\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\GroupSequenceProviderInterface;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_PRODUCT\', object)'),
    new Put(security: 'is_granted(\'UPDATE_PRODUCT\', object)', validationContext: ['groups' => ['Default', 'Product']]),
    new Delete(security: 'is_granted(\'DELETE_PRODUCT\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_PRODUCT\', object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['product:read']],
    denormalizationContext: ['groups' => ['product:write']]
)]
#[ORM\EntityListeners([ProductEntityListener::class])]
#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_product')]
#[ORM\Index(columns: ['status'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id', 'categories.id'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt'])]
class Product implements ProductInterface, TranslatableInterface, GroupSequenceProviderInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?int $id = null;

    /*
     * Property containing information about the state of the product.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected string $status = ProductInterface::STATUS_DRAFT;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['product:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['product:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when product has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?\DateTimeImmutable $publishedAt = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['product:read', 'shopping_cart:public_read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['product:write'])]
    protected $newTranslations;

    /**
     * @var ProductCategory[]
     */
    #[ORM\ManyToMany(targetEntity: ProductCategory::class, inversedBy: 'products')]
    #[ORM\JoinTable(name: 'aa_product_product_category')]
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected $categories;

    /*
     * Product variants.
     */
    #[ORM\OneToMany(mappedBy: 'product', targetEntity: ProductVariant::class, cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected $variants;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    #[Valid]
    public function getVariants(): array
    {
        return $this->variants->getValues();
    }

    public function addVariant(ProductVariant $variant): void
    {
        if (!$this->variants->contains($variant)) {
            $variant->setProduct($this);
            $this->variants->add($variant);
        }
    }

    public function removeVariant(ProductVariant $variant): void
    {
        if ($this->variants->contains($variant)) {
            $this->variants->removeElement($variant);
        }
    }

    /**
     * Get productCategories for the article.
     *
     * @return ProductCategory[]
     */
    public function getCategories(): array
    {
        return $this->categories->toArray();
    }

    /*
     * Add product to a category.
     */
    public function addCategory(ProductCategory $productCategory): self
    {
        if (!$this->categories->contains($productCategory)) {
            $this->categories->add($productCategory);
        }

        return $this;
    }

    /*
     * Remove product from the category.
     */
    public function removeCategory(ProductCategory $productCategory): self
    {
        if ($this->categories->contains($productCategory)) {
            $this->categories->removeElement($productCategory);
        }

        return $this;
    }

    /*
     * Return the requested validation group sequences.
     */
    public function getGroupSequence(): array
    {
        return [['Default', 'Product']];
    }


    /*
     * Automatically set createdAt value before persisting an entity.
     */
    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    #[Groups(groups: ['product:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->variants)) {
            return null;
        }

        $firstVariant = $this->variants->first();

        if (null === $firstVariant) {
            return null;
        }

        if (empty($firstVariant->getCovers())) {
            return null;
        }

        $cover = $firstVariant->getCovers()[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }

    #[Groups(groups: ['product:read'])]
    public function getFirstPrice(): ?ProductVariantPrice
    {
        if (empty($this->variants)) {
            return null;
        }

        /** @var ProductVariant $variant */
        $variant = $this->variants[0];

        if (empty($variant->getPrices())) {
            return null;
        }

        return $variant->getPrices()[0];
    }

    public function getLowestPrice(): ?ProductVariantPrice
    {
        if (empty($this->variants)) {
            return null;
        }

        /** @var ProductVariant $variant */
        $variant = $this->variants[0];

        if (empty($variant->getPrices())) {
            return null;
        }

        $lowestPrice = null;

        foreach ($variant->getPrices() as $price) {
            if ($lowestPrice === null || $price->getPriceWithTax() < $lowestPrice->getPriceWithTax()) {
                $lowestPrice = $price;
            }
        }

        return $lowestPrice;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }
}
