<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Security\Voter\ProductVariantParameterVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_PRODUCT_VARIANT_PARAMETER\', object)'), new Put(security: 'is_granted(\'UPDATE_PRODUCT_VARIANT_PARAMETER\', object)'), new Delete(security: 'is_granted(\'DELETE_PRODUCT_VARIANT_PARAMETER\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_PRODUCT_VARIANT_PARAMETER\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['product:read']], denormalizationContext: ['groups' => ['product:write']])]
#[ORM\Entity()]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_product_variant_parameter')]
class ProductVariantParameter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart_item:read'])]
    protected ?int $id = null;

    #[ORM\Column(name: 'name', type: 'string', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    #[NotBlank]
    protected ?string $name = null;

    #[ORM\Column(name: 'value', type: 'string', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    #[NotBlank]
    protected ?string $value = null;

    #[ORM\ManyToOne(targetEntity: ProductVariant::class, inversedBy: 'parameters')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected ProductVariant $productVariant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProductVariant(): ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(?ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }
}
