<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\Eshop\ProductInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Eshop\ProductVariantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Valid;
use Doctrine\ORM\Mapping as ORM;

/**
 * This entity represent a product variant.
 *
 * @method ProductVariantTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_PRODUCT_VARIANT\', object)'), new Put(security: 'is_granted(\'UPDATE_PRODUCT_VARIANT\', object)'), new Delete(security: 'is_granted(\'DELETE_PRODUCT_VARIANT\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_PRODUCT_VARIANT\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['product:read']], denormalizationContext: ['groups' => ['product:write']])]
#[ORM\Entity(repositoryClass: ProductVariantRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_product_variant')]
#[ORM\Index(columns: ['status'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
class ProductVariant implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?int $id = null;

    /*
     * Property containing information about the state of the product.
     *
     * By default the product is in the draft state.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected string $status = ProductInterface::STATUS_DRAFT;

    /**
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['product:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['product:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when product has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['shopping_cart:public_read', 'shopping_cart:public_read', 'product:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['product:write'])]
    protected $newTranslations;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'variants')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[Groups(groups: ['shopping_cart:public_read'])]
    protected Product $product;

    #[ORM\OneToMany(mappedBy: 'productVariant', targetEntity: 'ProductVariantPrice', cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['product:write'])]
    protected $prices;

    /**
     * The URL of the product cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?array $covers = [];

    /*
     * The EAN-13 product code.
     */
    #[ORM\Column(name: 'ean13', type: 'string', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?string $ean13 = null;

    /*
     * Internal product code.
     */
    #[ORM\Column(name: 'code', type: 'string', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?string $code = null;

    /*
     * Available products in stock.
     */
    #[ORM\Column(name: 'quantity_available', type: 'integer', nullable: true)]
    #[Groups(groups: ['product:read', 'product:write', 'shopping_cart:public_read'])]
    protected ?int $quantityAvailable = null;

    /*
     * Individual extended product parameters
     */
    #[ORM\OneToMany(mappedBy: 'productVariant', targetEntity: 'ProductVariantParameter', cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['product:write'])]
    protected $parameters;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->parameters = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    #[Groups(groups: ['product_variant:read'])]
    public function getPrice(): ?ProductVariantPrice
    {
        return $this->prices->count() > 0 ? $this->prices->first() : null;
    }

    /**
     * @return ProductVariantPrice[]
     */
    #[Groups(groups: ['product:read'])]
    #[Valid]
    public function getPrices(): array
    {
        return $this->prices->getValues();
    }

    public function addPrice(ProductVariantPrice $price): self
    {
        if (!$this->prices->contains($price)) {
            $price->setProductVariant($this);
            $this->prices->add($price);
        }

        return $this;
    }

    public function removePrice(ProductVariantPrice $price): self
    {
        if ($this->prices->contains($price)) {
            $this->prices->removeElement($price);
        }

        return $this;
    }

    /**
     * @return ProductVariantParameter[]
     */
    #[Groups(groups: ['product:read'])]
    #[Valid]
    public function getParameters(): array
    {
        return $this->parameters->getValues();
    }

    public function addParameter(ProductVariantParameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $parameter->setProductVariant($this);
            $this->parameters->add($parameter);
        }

        return $this;
    }

    public function removeParameter(ProductVariantParameter $parameter): self
    {
        if ($this->parameters->contains($parameter)) {
            $this->parameters->removeElement($parameter);
            $parameter->setProductVariant(null);
        }

        return $this;
    }

    /*
     * Get product cover URL.
     */
    public function getCovers(): ?array
    {
        return $this->covers;
    }

    /*
     * Set the product cover URL.
     */
    public function setCovers(?array $covers): self
    {
        $this->covers = $covers;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getQuantityAvailable(): ?int
    {
        return $this->quantityAvailable;
    }

    public function setQuantityAvailable(?int $quantityAvailable): self
    {
        $this->quantityAvailable = $quantityAvailable;

        return $this;
    }

    #[Groups(groups: ['product:read', 'product_variant:read', 'shopping_cart:public_read'])]
    public function getName(): ?string
    {
        return $this->translate()?->getName();
    }

    #[Groups(groups: ['product:read', 'product_variant:read', 'shopping_cart:public_read'])]
    public function getContent(): ?string
    {
        return $this->translate()?->getContent();
    }

    #[Groups(['order:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }
}
