<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use Arodax\AdminBundle\EntityListener\OrderPaymentEntityListener;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[UniqueEntity(fields: ['payment_key'], message: 'This payment key is already used.', service: 'payment_key')]
#[ORM\EntityListeners([OrderPaymentEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_order_payment')]
class OrderPayment
{
    final public const string STATUS_PENDING = 'pending';
    final public const string STATUS_PAID = 'paid';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Order::class, cascade: ['ALL'], inversedBy: 'payments')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected Order $order;

    #[ORM\Column(name: 'status', type: 'string', length: 191, nullable: false)]
    #[Groups(groups: ['order:read', 'order:write'])]
    protected string $status = self::STATUS_PENDING;

    /*
     * The unique key for the payment.
     */
    #[ORM\Column(name: 'payment_key', type: 'string', length: 191, unique: true, nullable: true)]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    protected ?string $paymentKey = null;

    /*
     * The payment method.
     */
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    #[Groups(groups: ['order:read', 'order:write', 'shopping_cart:public_read'])]
    #[ORM\ManyToOne(targetEntity: PaymentMethod::class)]
    protected ?PaymentMethod $paymentMethod = null;

    #[ORM\Column(type: 'date_immutable', nullable: false)]
    protected ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'date_immutable', nullable: true)]
    #[Groups(groups: ['shopping_cart:public_read'])]
    protected ?\DateTimeImmutable $paidAt = null;

    /*
     * The payment credentials.
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['shopping_cart:public_read'])]
    protected ?array $credentials = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPaymentKey(): ?string
    {
        return $this->paymentKey;
    }

    public function setPaymentKey(?string $paymentKey): self
    {
        $this->paymentKey = $paymentKey;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPaidAt(): ?\DateTimeImmutable
    {
        return $this->paidAt;
    }

    public function setPaidAt(?\DateTimeImmutable $paidAt): self
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    public function getcredentials(): ?array
    {
        return $this->credentials;
    }

    public function setcredentials(?array $credentials): self
    {
        $this->credentials = $credentials;

        return $this;
    }
}
