<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Eshop;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\EntityListener\ShoppingCartItemEntityListener;
use Arodax\AdminBundle\Repository\Eshop\ShoppingCartItemRepository;
use Arodax\AdminBundle\Security\Voter\ShoppingCartItemVoter;
use Arodax\AdminBundle\Security\Voter\ShoppingCartVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Component\Validator\Constraints\Valid;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted("'.ShoppingCartItemVoter::READ_SHOPPING_CART_ITEM.'", object)'),
    new Put(security: 'is_granted("'.ShoppingCartItemVoter::UPDATE_SHOPPING_CART_ITEM.'", object)'),
    new Delete(security: 'is_granted("'.ShoppingCartItemVoter::DELETE_SHOPPING_CART_ITEM.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted("'.ShoppingCartItemVoter::CREATE_SHOPPING_CART_ITEM.'", object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['shopping_cart_item:read']],
    denormalizationContext: ['groups' => ['shopping_cart_item:write']]
)]
#[ORM\EntityListeners([ShoppingCartItemEntityListener::class])]
#[ORM\Entity(repositoryClass: ShoppingCartItemRepository::class)]
#[ORM\Table(name: 'aa_shopping_cart_item')]
#[ORM\HasLifecycleCallbacks]
class ShoppingCartItem
{
    use TimestampTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'shopping_cart_item:write', 'order:read'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: ShoppingCart::class, inversedBy: 'items')]
    #[ORM\JoinColumn(name: 'shopping_cart', nullable: false, onDelete: 'CASCADE')]
    protected ?ShoppingCart $shoppingCart = null;

    #[ORM\ManyToOne(targetEntity: ProductVariant::class)]
    #[ORM\JoinColumn(name: 'product_variant', onDelete: 'CASCADE')]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[Valid]
    protected ?ProductVariant $productVariant = null;

    #[ORM\ManyToOne(targetEntity: ProductVariantPrice::class)]
    #[ORM\JoinColumn(name: 'product_variant_price', onDelete: 'CASCADE')]
    #[Groups(groups: ['shopping_cart_item:read'])]
    protected ?ProductVariantPrice $productVariantPrice = null;

    #[ORM\Column(name: 'price_without_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[PositiveOrZero]
    protected ?int $priceWithoutTax = null;

    #[ORM\Column(name: 'tax_rate', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[PositiveOrZero]
    protected ?int $taxRate = null;

    #[ORM\Column(name: 'tax_price', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[PositiveOrZero]
    protected ?int $taxPrice = null;

    #[ORM\Column(name: 'price_with_tax', type: 'integer', nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[PositiveOrZero]

    protected ?int $priceWithTax = null;
    #[ORM\Column(type: 'string', length: 3, nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    #[Currency]
    protected ?string $currency = null;

    #[ORM\Column(name: 'quantity', type: 'integer', nullable: false)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'shopping_cart_item:write', 'order:read'])]
    protected int $quantity = 0;

    #[ORM\Column(name: 'note', type: 'text', nullable: true)]
    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'shopping_cart_item:write', 'order:read'])]
    protected ?string $note = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getShoppingCart(): ShoppingCart
    {
        return $this->shoppingCart;
    }

    public function setShoppingCart(ShoppingCart $shoppingCart): self
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    public function getProductVariant(): ProductVariant
    {
        return $this->productVariant;
    }

    public function setProductVariant(ProductVariant $productVariant): self
    {
        $this->productVariant = $productVariant;

        return $this;
    }

    public function getProductVariantPrice(): ProductVariantPrice
    {
        return $this->productVariantPrice;
    }

    public function setProductVariantPrice(ProductVariantPrice $productVariantPrice): self
    {
        $this->productVariantPrice = $productVariantPrice;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPriceWithoutTax(): ?int
    {
        return $this->priceWithoutTax ?? $this->productVariantPrice->getPriceWithoutTax();
    }

    public function setPriceWithoutTax(?int $priceWithoutTax): self
    {
        $this->priceWithoutTax = $priceWithoutTax;

        return $this;
    }

    public function getTaxRate(): ?int
    {
        return $this->taxRate ?? $this->productVariantPrice->getTaxRate();
    }

    public function setTaxRate(?int $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function getTaxPrice(): ?int
    {
        return $this->taxPrice ?? $this->productVariantPrice->getTaxPrice();
    }

    public function setTaxPrice(?int $taxPrice): self
    {
        $this->taxPrice = $taxPrice;

        return $this;
    }

    public function getPriceWithTax(): ?int
    {
        return $this->priceWithTax ?? $this->productVariantPrice->getPriceWithTax();
    }

    public function setPriceWithTax(?int $priceWithTax): self
    {
        $this->priceWithTax = $priceWithTax;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency ?? $this->productVariantPrice->getCurrency();
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read'])]
    public function getCover(): ?string
    {
        /** @var array | null $covers */
        $covers = $this->getProductVariant()->getCovers();
        if ($covers) {
            return $covers[0]['contentUrl'];
        }

        return null;
    }

    #[Groups(groups: ['shopping_cart_item:read', 'shopping_cart:public_read', 'order:read'])]
    public function getName(): ?string
    {
        return implode(' ', [$this->getProductVariant()->getProduct()->translate()->getName() ?? '', $this->getProductVariant()->translate()->getName()]);
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): ShoppingCartItem
    {
        $this->note = $note;
        return $this;
    }


}
