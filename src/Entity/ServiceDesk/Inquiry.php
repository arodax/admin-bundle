<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\ServiceDesk;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\EnabledTrait;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Model\ServiceDesk\ContactFormInterface;
use Arodax\AdminBundle\Security\Voter\InquiryVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(operations: [
    new Get(security: 'is_granted("'.InquiryVoter::READ_INQUIRY.'", object)'),
    new Put(security: 'is_granted("'.InquiryVoter::UPDATE_INQUIRY.'", object)'),
    new Delete(security: 'is_granted("'.InquiryVoter::DELETE_INQUIRY.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(
        securityPostDenormalize: 'is_granted("'.InquiryVoter::CREATE_INQUIRY.'", object)',
        validationContext: ['groups' => ['']]
    )],
    normalizationContext: ['groups' => ['admin_read']],
    denormalizationContext: ['groups' => ['admin_write']]
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_inquiry')]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'status', 'personalName', 'companyName', 'conversion', 'subject'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['enabled', 'conversion'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact', 'subject' => 'partial', 'companyName' => 'partial', 'personalName' => 'partial'])]
class Inquiry implements ContactFormInterface
{
    use EnabledTrait;
    use TimestampTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['admin_read', 'admin_read'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    #[Email]
    #[NotBlank]
    protected ?string $email = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    #[NotBlank]
    protected ?string $phone = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    #[NotBlank]
    protected ?string $personalName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    protected ?string $companyName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    protected ?string $subject = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    #[NotBlank]
    protected ?string $body = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    protected ?array $data = [];

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    protected ?bool $conversion = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['admin_read', 'admin_write'])]
    protected ?string $status = 'unread';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPersonalName(): ?string
    {
        return $this->personalName;
    }

    public function setPersonalName(?string $personalName): self
    {
        $this->personalName = $personalName;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getData(): array
    {
        return $this->data ?? [];
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getConversion(): ?bool
    {
        return $this->conversion;
    }

    public function setConversion(?bool $conversion): self
    {
        $this->conversion = $conversion;

        return $this;
    }
}
