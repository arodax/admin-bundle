<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\User;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Arodax\AdminBundle\Controller\Api\User\RequestResetPassword;
use Arodax\AdminBundle\Entity\Contact\Contact;
use Arodax\AdminBundle\Entity\Finance\Credit;
use Arodax\AdminBundle\Entity\Score\Score;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\EntityListener\User\UserAvatarListener;
use Arodax\AdminBundle\EntityListener\User\UserPasswordListener;
use Arodax\AdminBundle\EntityListener\User\UserRoleListener;
use Arodax\AdminBundle\EntityListener\User\UserTotalScoreListener;
use Arodax\AdminBundle\Model\User\UserInterface;
use Arodax\AdminBundle\Repository\User\UserRepository;
use Arodax\AdminBundle\Security\Voter\UserVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Locale;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;
use Symfony\Component\Validator\Constraints\Timezone;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.UserVoter::READ_USER.'", object)'),
        new Get(uriTemplate: '/users/{id}/lost-password', controller: RequestResetPassword::class, deserialize: false, validate: false),
        new Put(security: 'is_granted("'.UserVoter::UPDATE_USER.'", object)'),
        new Delete(security: 'is_granted("'.UserVoter::DELETE_USER.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(
            securityPostDenormalize: 'is_granted("'.UserVoter::CREATE_USER.'", object)',
            validationContext: ['groups' => ['']]
        ),
        new Patch(
            uriTemplate: '/users/{id}/update-password',
            normalizationContext: ['groups' => ['']],
            denormalizationContext: ['groups' => ['user:password']],
            security: 'is_granted("'.UserVoter::UPDATE_USER.'", object)',
            validationContext: ['groups' => ['user:password']],
            deserialize: true
        )
    ],
    normalizationContext: ['groups' => ['user:read', 'contact:read', 'user:info']],
    denormalizationContext: ['groups' => ['user:write', 'contact:write']],
    validationContext: ['groups' => ['Default', 'user:password']],
)]
#[UniqueEntity(['email'])]
#[ORM\EntityListeners([
    UserPasswordListener::class,
    UserAvatarListener::class,
    UserRoleListener::class,
    UserTotalScoreListener::class
])]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'aa_user')]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['enabled'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'lastActivityAt'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id', 'userGroups.id', 'totalScore'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['displayName' => 'partial', 'email' => 'partial', 'roles' => 'partial'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'enabled', 'createdAt', 'lastActivityAt', 'totalScore'], arguments: ['orderParameterName' => 'order'])]
class User implements UserInterface, EquatableInterface, PasswordAuthenticatedUserInterface, GroupSequenceProviderInterface
{
    use TimestampTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected ?int $id = null;

    #[Email]
    #[ORM\Column(type: 'string', length: 191, unique: true, nullable: true)]
    #[Groups(groups: ['user:read', 'admin_write', 'log_read', 'user:info'])]
    protected ?string $email = null;

    /*
     * Display name of the user, can be real, name, nickname, whatever you want.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'log_read', 'user:info'])]
    protected ?string $displayName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected ?string $firstName = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected ?string $lastName = null;

    /*
     * Url of the user avatar.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'log_read', 'user:info'])]
    protected ?string $avatar = null;

    /*
     * Color for the user.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'user:info', 'log_read'])]
    protected ?string $color = null;

    /*
     * User roles.
     */
    #[ORM\Column(type: 'json')]
    #[Groups(groups: ['user:read', 'admin_write', 'user:info'])]
    protected array $roles = [];

    #[Groups(groups: ['user:read', 'user:info'])]
    protected array $extendedRoles = [];

    /*
     * The hashed password
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $password = null;

    /*
     * The plaintext password for temporary use only, will be not persisted
     */
    #[NotCompromisedPassword(groups: ['user:password'])]
    #[NotBlank(allowNull: true, groups: ['user:password'])]
    #[Groups(groups: ['user:write', 'user:password'])]
    #[Length(min: 8, max: 32, groups: ['user:password'])]
    protected ?string $plainPassword = null;

    /**
     * @var ArrayCollection
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Contact::class, cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    #[MaxDepth(3)]
    protected $userContacts;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['user:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['user:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToMany(targetEntity: UserGroup::class, inversedBy: 'users')]
    #[ORM\JoinTable(name: 'aa_user_user_group')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['user:read', 'admin_write'])]
    #[MaxDepth(1)]
    protected Collection $userGroups;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['user:read', 'user:write'])]
    protected bool $enabled = false;

    /*
     * DateTime when the user has been last seen.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'log_read', 'user:info'])]
    protected ?\DateTimeImmutable $lastActivityAt;

    /*
     * User's timezone, by default all users has UTC timezone.
     */
    #[ORM\Column(type: 'text', nullable: false)]
    #[Timezone]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected string $timezone = 'UTC';

    /**
     * Preferred user's format for date.
     *
     * @see https://moment.github.io/luxon/docs/manual/parsing.html#table-of-tokens for tokens
     */
    #[ORM\Column(type: 'string', length: 191)]
    #[Choice(choices: ['d.M.yyyy', 'M.d.yyyy', 'yyyy-M-dd'])]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected ?string $preferredDateFormat = null;

    /**
     * Preferred user's format for time.
     *
     * @see https://moment.github.io/luxon/docs/manual/parsing.html#table-of-tokens for tokens
     */
    #[ORM\Column(type: 'string', length: 191)]
    #[Choice(choices: ['HH:mm', 'hh:mma'])]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    protected ?string $preferredTimeFormat = null;

    /*
     * Preferred user's locale.
     */
    #[ORM\Column(type: 'string', length: 5, nullable: true)]
    #[Locale]
    #[Groups(groups: ['user:read', 'user:write'])]
    protected ?string $preferredLocale = null;

    /*
     * Timestamp when user requested the login link for the last time.
     *
     * Changing value of this property will cause the invalidation of old login links.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    protected ?\DateTimeImmutable $lastLinkRequestedAt = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user:read', 'user:write'])]
    protected ?string $preferredHome = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(groups: ['user:read', 'user:write', 'user:info'])]
    private ?int $totalScore = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Score::class, cascade: ['ALL'], orphanRemoval: true)]
    private Collection $scores;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Credit::class, cascade: ['ALL'], orphanRemoval: true)]
    private Collection $credits;

    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->userGroups = new ArrayCollection();
        $this->userContacts = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->credits = new ArrayCollection();
        $this->timezone = 'UTC';
        $this->preferredDateFormat = 'd.M.yyyy';
        $this->preferredTimeFormat = 'HH:mm';
    }

    public function __toString(): string
    {
        return (string) $this->email;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /*
     * Get display name for the user.
     */
    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    /*
     * Set display name for the user.
     */
    public function setDisplayName(?string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /*
     * Get user avatar.
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /*
     * Get user color.
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * Set user color.
     *
     * @param string|null $color can be any string including hex or rgba color
     *
     * @return User
     */
    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /*
     * Set user avatar
     */
    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getExtendedRoles(): array
    {
        return $this->extendedRoles;
    }

    public function setExtendedRoles(array $extendedRoles): self
    {
        $this->extendedRoles = $extendedRoles;

        return $this;
    }

    /*
     * Get hashed password.
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /*
     * Set hashed password.
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /*
     * Set not hashed plaintext password.
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /*
     * Set non hashed plaintext password.
     */
    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        // reset password field, to trigger Doctrine onUpdate listener, null is not used because of empty passwords
        $this->password = (string) time();

        return $this;
    }


    #[Deprecated(since: '6.23.4')]
    public function getSalt(): ?string
    {
        return null;
    }

    /*
     * Clear temporary unsecured data such as plain text password.
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /*
     * Get user groups to which user belongs.
     */
    public function getUserGroups(): array
    {
        return $this->userGroups->toArray();
    }

    /*
     * Add user to a user group.
     */
    public function addUserGroup(UserGroup $userGroup): self
    {
        if (!$this->userGroups->contains($userGroup)) {
            $this->userGroups->add($userGroup);
        }

        return $this;
    }

    /*
     * Remove user group from the user.
     */
    public function removeUserGroup(UserGroup $userGroup): self
    {
        if ($this->userGroups->contains($userGroup)) {
            $this->userGroups->removeElement($userGroup);
        }

        return $this;
    }

    /*
     * Check whether user is in specific user group.
     */
    public function hasUserGroup(UserGroup $userGroup): bool
    {
        return $this->userGroups->contains($userGroup);
    }

    /*
     * Get user groups to which user belongs.
     */
    public function getUserContacts(): array
    {
        return $this->userContacts->toArray();
    }

    /*
     * Add user to a user group.
     *
     * @return User
     */
    public function addUserContact(Contact $userContact): self
    {
        if (!$this->userContacts->contains($userContact)) {
            $this->userContacts->add($userContact);
            $userContact->setUser($this);
        }

        return $this;
    }

    /*
     * Remove user group from the user.
     */
    public function removeUserContact(Contact $userContact): self
    {
        if ($this->userContacts->contains($userContact)) {
            $this->userContacts->removeElement($userContact);
            $userContact->setUser(null);
        }

        return $this;
    }

    /*
     * Check whether user is in specific user group.
     */
    public function hasUserContact(Contact $userContact): bool
    {
        return $this->userContacts->contains($userContact);
    }

    public function getLastActivityAt(): ?\DateTimeImmutable
    {
        return $this->lastActivityAt;
    }

    public function setLastActivityAt(?\DateTimeImmutable $lastActivityAt): self
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getPreferredDateFormat(): ?string
    {
        return $this->preferredDateFormat;
    }

    public function setPreferredDateFormat(?string $preferredDateFormat): self
    {
        $this->preferredDateFormat = $preferredDateFormat;

        return $this;
    }

    public function getPreferredTimeFormat(): ?string
    {
        return $this->preferredTimeFormat;
    }

    public function setPreferredTimeFormat(?string $preferredTimeFormat): self
    {
        $this->preferredTimeFormat = $preferredTimeFormat;

        return $this;
    }

    public function getPreferredLocale(): ?string
    {
        return $this->preferredLocale;
    }

    public function setPreferredLocale(?string $preferredLocale): self
    {
        $this->preferredLocale = $preferredLocale;

        return $this;
    }

    public function isEqualTo(\Symfony\Component\Security\Core\User\UserInterface $user): bool
    {
        if ($user instanceof self) {
            return $user->getId() === $this->getId();
        }

        return false;
    }

    public function getLastLinkRequestedAt(): ?\DateTimeImmutable
    {
        return $this->lastLinkRequestedAt;
    }

    public function setLastLinkRequestedAt(?\DateTimeImmutable $lastLinkRequestedAt): self
    {
        $this->lastLinkRequestedAt = $lastLinkRequestedAt;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getPreferredHome(): ?string
    {
        return $this->preferredHome;
    }

    public function setPreferredHome(?string $preferredHome): self
    {
        $this->preferredHome = $preferredHome;
        return $this;
    }

    public function getTotalScore(): ?int
    {
        return $this->totalScore;
    }

    public function setTotalScore(?int $totalScore): self
    {
        $this->totalScore = $totalScore;
        return $this;
    }

    public function getCredits(): array
    {
        return $this->credits->toArray();
    }

    public static function validationGroups(self $book): array
    {
        return ['Default', 'user:password'];
    }

    public function getGroupSequence(): array
    {
        // when returning a simple array, if there's a violation in any group
        // the rest of the groups are not validated. E.g. if 'User' fails,
        // 'Premium' and 'Api' are not validated:
        return ['user:password', 'Default'];
    }
}
