<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\User;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\User\UserGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ApiResource(
    operations: [
        new Get(security: 'is_granted(\'READ_USER_GROUP\', object)'),
        new Put(security: 'is_granted(\'UPDATE_USER_GROUP\', object)'),
        new Delete(security: 'is_granted(\'DELETE_USER_GROUP\', object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted(\'CREATE_USER_GROUP\', object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['user_group:read']],
    denormalizationContext: ['groups' => ['user_group:write']]
)]
#[ORM\Entity(repositoryClass: UserGroupRepository::class)]
#[ORM\Table(name: 'aa_user_group')]
#[ApiFilter(filterClass: BooleanFilter::class, properties: ['enabled'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'enabled'], arguments: ['orderParameterName' => 'order'])]
class UserGroup implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['user_group:read', 'user:read'])]
    protected ?int $id = null;

    /**
     * @var Collection&User<>
     */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'userGroups', cascade: ['ALL'])]
    #[MaxDepth(1)]
    protected Collection $users;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    protected bool $enabled = false;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    #[Assert\CssColor]
    protected ?string $color = null;

    #[Groups(groups: ['user_group:read', 'user_group:tree'])]
    protected $translations;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    #[Assert\Range(min: 0)]
    private ?int $totalScore = null;

    #[ORM\ManyToOne(cascade: ['ALL'], fetch: 'EXTRA_LAZY')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    #[MaxDepth(1)]
    private ?User $teamLeader = null;

    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => 0])]
    private ?bool $gamification = false;

    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean', options: ['default' => 1])]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    private bool $deletable = true;

    /**
     * @var Collection&User<>
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: UserGroupAclUser::class, fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['acl:read'])]
    protected Collection $aclUsers;

    /**
     * When this is enabled, we also apply additional security checks.
     */
    #[Orm\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(groups: ['user_group:read', 'user_group:write'])]
    private bool $aclEnabled = false;

    /**
     * @var array
     */
    #[Groups(groups: ['user_group:write'])]
    protected $newTranslations;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->aclUsers = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsers(): array
    {
        return $this->users->toArray();
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addUserGroup($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeUserGroup($this);
        }

        return $this;
    }

    public function hasUser(User $user): bool
    {
        return $this->users->contains($user);
    }

    public function setEnabled(bool $enabled = true): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    #[Groups(groups: ['user_group:read', 'user:read'])]
    public function getName(): ?string
    {
        return $this->translate()->getName();
    }

    public function getTotalScore(): ?int
    {
        return $this->totalScore;
    }

    public function setTotalScore(?int $totalScore): self
    {
        $this->totalScore = $totalScore;
        return $this;
    }

    public function getTeamLeader(): ?User
    {
        return $this->teamLeader;
    }

    public function setTeamLeader(?User $teamLeader): static
    {
        $this->teamLeader = $teamLeader;

        return $this;
    }

    public function getGamification(): ?bool
    {
        return $this->gamification;
    }

    public function setGamification(?bool $gamification): self
    {
        $this->gamification = $gamification;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getAclUsers(): array
    {
        return $this->aclUsers->toArray();
    }

    public function addSecurityUser(User $user): self
    {
        if (false === $this->aclUsers->contains($user)) {
            $this->aclUsers->add($user);
        }

        return $this;
    }

    public function removeSecurityUser(User $user): self
    {
        if (true === $this->aclUsers->contains($user)) {
            $this->aclUsers->remove($user);
        }

        return $this;
    }

    public function hasAclEnabled(): bool
    {
        return $this->aclEnabled;
    }

    public function setAclEnabled(bool $aclEnabled): self
    {
        $this->aclEnabled = $aclEnabled;
        return $this;
    }
}
