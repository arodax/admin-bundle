<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Event;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents event type.
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_EVENT_TYPE\', object)'),
    new Put(security: 'is_granted(\'UPDATE_EVENT_TYPE\', object)'),
    new Delete(security: 'is_granted(\'DELETE_EVENT_TYPE\', object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_EVENT_TYPE\', object)')
],
    normalizationContext: [
        'groups' => ['event_type:read', 'event:read']
    ],
    denormalizationContext: [
        'groups' => ['event_type:write', 'event:write']
    ]
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_event_type')]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['type' => 'exact'])]
class EventType implements TranslatableInterface
{
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['event_type:read', 'event:read', 'event:write'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true])]
    #[Groups(groups: ['event_type:read', 'event:read', 'event:write'])]
    protected bool $enabled = false;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['event_type:read', 'event:read', 'event:write'])]
    protected ?string $color = null;

    /**
     * @var array
     */
    #[Groups(groups: ['event_type:write'])]
    protected $newTranslations;

    public function getId(): ?int
    {
        return $this->id;
    }

    #[Groups(groups: ['event_type:read', 'event_type:write'])]
    public function getTitle(): ?string
    {
        return $this->translate()->getName();
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;
        return $this;
    }
}
