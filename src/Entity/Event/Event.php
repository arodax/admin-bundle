<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Event;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\Gallery\Gallery;
use Arodax\AdminBundle\Entity\Geography\Point;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\EntityListener\EventListener;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Security\Voter\EventVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Recurr\Exception\InvalidRRule;
use Recurr\Exception\InvalidWeekday;
use Recurr\Recurrence;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents event type.
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted("'.EventVoter::READ_EVENT.'", object)'),
    new Put(security: 'is_granted("'.EventVoter::UPDATE_EVENT.'", object)'),
    new Delete(security: 'is_granted("'.EventVoter::DELETE_EVENT.'", object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(
        securityPostDenormalize: 'is_granted("'.EventVoter::CREATE_EVENT.'", object)',
        validationContext: ['groups' => ['']]
    )],
    normalizationContext: ['groups' => ['event:read', 'point_read', 'user:info', 'acl:read', 'user_group:read', 'gallery:read']],
    denormalizationContext: ['groups' => ['event:write', 'point_write', 'gallery:write']]
)]
#[ORM\EntityListeners([EventListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_event')]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt', 'startAt', 'status'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt', 'startAt'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['headline' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['authors.id', 'id'])]
class Event implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    final public const string STATUS_DRAFT = 'draft';
    final public const string STATUS_PUBLISHED = 'published';
    private const FREQUENCY = [0 => 'DAILY', 1 => 'WEEKLY', 2 => 'MONTHLY', 3 => 'YEARLY'];
    private const WEEKDAYS = [0 => 'SU', 1 => 'MO', 2 => 'TU', 3 => 'WE', 4 => 'TH', 5 => 'FR', 6 => 'SA'];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['event:read'])]
    protected ?int $id = null;

    /**
     * @var User[] authors of this event
     */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'aa_event_author')]
    #[ORM\JoinColumn(name: 'event_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected $authors;

    /*
     * Property containing information about the state of the event.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $status = self::STATUS_DRAFT;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['event:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['event:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when event has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['event:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['event:write'])]
    protected $newTranslations;

    /**
     * The URL of the event cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected array $covers = [];

    /**
     * The first possible occurrence of the event.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?\DateTimeImmutable $startAt = null;
    /**
     * The last possible occurrence of the event.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?\DateTimeImmutable $endAt = null;

    /*
     * Indicates that event is the all day event.
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected bool $allDay = true;

    /*
     * How many times is this event repeated.
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $recurrenceCount = null;

    /*
     *  How often is the event frequency ( 1 - every month, 2 every send month ... 10 every nth month) see also self::$frequency for type of the occurency.
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $recurrenceInterval = null;

    /*
     * What is the frequency of the event occurrence (weekly, monthly, yearly ...).
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $recurrenceFrequency = null;

    /**
     * Indicates every day using self::WEEKDAYS.
     */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    protected array $byDay = [];

    /*
     * Indicates day of the month  (1 first day of the year, -1 last day of the month).
     */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    protected array $byMonthDay = [];

    /*
     * Indicates day of the year  (1 first day of the year, -1 last day of the year).
     */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    protected array $byYearDay = [];

    #[ORM\Column(type: 'simple_array', nullable: true)]
    protected array $byMonth = [];

    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?array $recurrenceCollection = null;

    /**
     * @var Point[]
     */
    #[ORM\ManyToMany(targetEntity: Point::class, cascade: ['ALL'], orphanRemoval: true)]
    #[ORM\JoinTable(name: 'aa_event_point')]
    #[ORM\JoinColumn(name: 'event_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected $geographyPoints;

    /**
     * @var User[] Users who have access to this event.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: EventAclUser::class, cascade: ['PERSIST'], fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUsers;

    /**
     * @var UserGroup[] User groups who have access to this event.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: EventAclUserGroup::class, cascade: ['PERSIST'], fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUserGroups;

    #[Orm\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(groups: ['event:read', 'event:write'])]
    private bool $aclEnabled = false;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $color = null;

    /**
     * @var EventType[]
     */
    #[ORM\ManyToMany(targetEntity: EventType::class, inversedBy: 'events')]
    #[ORM\JoinTable(name: 'aa_event_event_type')]
    #[ORM\JoinColumn(name: 'event_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected $eventTypes;

    /**
     * The gallery attached to this event.
     *
     * @var Collection&Gallery
     */
    #[ORM\ManyToMany(targetEntity: Gallery::class)]
    #[ORM\JoinTable(name: 'aa_event_gallery')]
    #[Groups(groups: ['event:read', 'event:write'])]
    private $galleries;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->geographyPoints = new ArrayCollection();
        $this->aclUsers = new ArrayCollection();
        $this->aclUserGroups = new ArrayCollection();
        $this->eventTypes = new ArrayCollection();
        $this->galleries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /*
     * Get all event authors.
     */
    public function getAuthors(): array
    {
        return $this->authors->toArray();
    }

    /*
     * Add author to the event.
     */
    public function addAuthor(User $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
        }

        return $this;
    }

    /*
     * Remove author from the event.
     */
    public function removeAuthor(User $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /*
     * Automatically set createdAt value before persisting an entity.
     */
    #[ORM\PrePersist]
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeImmutable
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeImmutable $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function isAllDay(): bool
    {
        return $this->allDay;
    }

    public function setAllDay(bool $allDay): self
    {
        $this->allDay = $allDay;

        return $this;
    }

    public function getRecurrenceCount(): int
    {
        return $this->recurrenceCount;
    }

    public function setRecurrenceCount(int $recurrenceCount): self
    {
        $this->recurrenceCount = $recurrenceCount;

        return $this;
    }

    public function getRecurrenceInterval(): ?int
    {
        return $this->recurrenceInterval;
    }

    public function setRecurrenceInterval(int $recurrenceInterval): self
    {
        $this->recurrenceInterval = $recurrenceInterval;

        return $this;
    }

    public function getRecurrenceFrequency(): ?int
    {
        return $this->recurrenceFrequency;
    }

    public function setRecurrenceFrequency(int $recurrenceFrequency): self
    {
        $this->recurrenceFrequency = $recurrenceFrequency;

        return $this;
    }

    public function getByDay(): array
    {
        return $this->byDay;
    }

    public function setByDay(array $byDay): self
    {
        $this->byDay = $byDay;

        return $this;
    }

    public function getByMonthDay(): array
    {
        return $this->byMonthDay;
    }

    public function setByMonthDay(array $byMonthDay): self
    {
        $this->byMonthDay = $byMonthDay;

        return $this;
    }

    public function getByYearDay(): array
    {
        return $this->byYearDay;
    }

    public function setByYearDay(array $byYearDay): self
    {
        $this->byYearDay = $byYearDay;

        return $this;
    }

    public function getByMonth(): array
    {
        return $this->byMonth;
    }

    public function setByMonth(array $byMonth): self
    {
        $this->byMonth = $byMonth;

        return $this;
    }

    /**
     * Return recurrence collection for the event.
     *
     * @throws InvalidRRule
     * @throws InvalidWeekday
     *
     * @return \DateTimeImmutable[]|null
     */
    public function getRecurrenceCollection(): ?array
    {
        if (null !== $this->getStartAt()) {
            return null;
        }

        if (null === $this->getRecurrenceFrequency()) {
            return null;
        }

        $result = [];


        $rulePart = [];

        $rulePart[] = 'FREQ='.self::FREQUENCY[$this->getRecurrenceFrequency()];

        if ($this->getRecurrenceCount() !== 0) {
            $rulePart[] = 'COUNT='.$this->getRecurrenceCount();
        }

        if (null !== $this->getEndAt()) {
            $rulePart[] = 'UNTIL='.$this->getEndAt()->format('Y-m-d');
        }

        $rulePart[] = null === $this->getRecurrenceInterval() ? 'INTERVAL=1' : 'INTERVAL='.$this->getRecurrenceInterval();
        if ($this->getByDay() !== []) {
            $byWeekDayArray = [];
            foreach ($this->getByDay() as $byDay) {
                $byWeekDayArray[] = self::WEEKDAYS[$byDay];
            }

            $rulePart[] = 'BYDAY='.implode(',', $byWeekDayArray);
        }
        if ($this->getByMonth() !== []) {
            $rulePart[] = 'BYMONTH='.implode(',', $this->getByMonth());
        }
        if ($this->getByMonthDay() !== []) {
            $rulePart[] = 'BYMONTHDAY='.implode(',', $this->getByMonthDay());
        }
        if ($this->getByYearDay() !== []) {
            $rulePart[] = 'BYYEARDAY='.implode(',', $this->getByYearDay());
        }

        $ruleString = implode(';', $rulePart);

        $rule = new Rule($ruleString, $this->getStartAt(), null, 'UTC');

        $transformer = new ArrayTransformer();

        /*
         * By default, January 30 + 1 month results on March 30 because February doesn't have 30 days.
         *
         * Enabling this fix tells Recur that +1 month means "last day of next month".
         */
        if (2 === $this->getRecurrenceFrequency()) {
            $transformerConfig = new ArrayTransformerConfig();
            $transformerConfig->enableLastDayOfMonthFix();
            $transformer->setConfig($transformerConfig);
        }

        $elements = $transformer->transform($rule);

        /** @var Recurrence $element */
        foreach ($elements as $element) {
            $result[] = $element->getEnd();
        }

        return $result;
    }

    /**
     * Get all geography points for this event.
     *
     * @return Point[]
     */
    public function getGeographyPoints(): array
    {
        return $this->geographyPoints->toArray();
    }

    /*
     * Add a geography point to this event.
     */
    public function addGeographyPoint(Point $point): self
    {
        if (!$this->geographyPoints->contains($point)) {
            $this->geographyPoints->add($point);
        }

        return $this;
    }

    /*
     * Remove geography points.
     */
    public function removeGeographyPoint(Point $point): self
    {
        if ($this->geographyPoints->contains($point)) {
            $this->geographyPoints->removeElement($point);
        }

        return $this;
    }

    /*
     * Check if geography point is attached to this event.
     *
     * @param Point $point
     *
     * @return bool
     */
    public function hasGeographyPoint(Point $point): bool
    {
        return $this->geographyPoints->contains($point);
    }

    /**
     * @return EventAclUser[]
     */
    public function getAclUsers(): array
    {
        return $this->aclUsers->toArray();
    }

    public function addSecurityUser(EventAclUser $accessor): self
    {
        if (false === $this->aclUsers->contains($accessor)) {
            $this->aclUsers->add($accessor);
        }

        return $this;
    }

    public function removeSecurityUser(EventAclUser $accessor): self
    {
        if (true === $this->aclUsers->contains($accessor)) {
            $this->aclUsers->remove($accessor);
        }

        return $this;
    }

    /**
     * @return UserGroup[]
     */
    public function getAclUserGroups(): array
    {
        return $this->aclUserGroups->toArray();
    }

    public function addSecurityUserGroup(UserGroup $userGroup): self
    {
        if (false === $this->aclUserGroups->contains($userGroup)) {
            $this->aclUserGroups->add($userGroup);
        }

        return $this;
    }

    public function removeSecurityUserGroup(UserGroup $userGroup): self
    {
        if (true === $this->aclUserGroups->contains($userGroup)) {
            $this->aclUserGroups->removeElement($userGroup);
        }

        return $this;
    }

    public function hasAclEnabled(): bool
    {
        return $this->aclEnabled;
    }

    public function setAclEnabled(bool $aclEnabled): self
    {
        $this->aclEnabled = $aclEnabled;
        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Get event types for the event.
     *
     * @return EventType[]
     */
    public function getEventTypes(): array
    {
        return $this->eventTypes->toArray();
    }

    /*
     * Add event type to an event.
     */
    public function addEventType(EventType $eventType): self
    {
        if (!$this->eventTypes->contains($eventType)) {
            $this->eventTypes->add($eventType);
        }

        return $this;
    }

    /**
     * Remove event type from the event.
     */
    public function removeEventType(EventType $eventType): self
    {
        if ($this->eventTypes->contains($eventType)) {
            $this->eventTypes->removeElement($eventType);
        }

        return $this;
    }

    /*
    * Get event cover URL.
    */
    public function getCovers(): ?array
    {
        return $this->covers;
    }

    /**
     * Set the article cover URL.
     *
     * @param ?array $covers
     *
     * @return Event
     */
    public function setCovers(?array $covers): self
    {
        $this->covers = $covers;

        return $this;
    }

    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param Collection|Gallery $galleries
     */
    public function setGalleries($galleries): self
    {
        $this->galleries = $galleries;

        return $this;
    }

    #[Groups(groups: ['event:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getHeadline();
    }

    #[Groups(groups: ['event:read'])]
    public function getLead(): ?string
    {
        return $this->translate()->getLead();
    }

    /**
     * Get first event cover url.
     *
     * @return string|null
     */
    #[Groups(groups: ['event:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }
}
