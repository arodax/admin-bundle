<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Event;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Arodax\AdminBundle\EntityListener\EventTranslationEntityListener;

#[ORM\EntityListeners([EventTranslationEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_event_translation')]
class EventTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $id = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected $locale;

    /*
     * The main headline for the event, usually identifies with the heading element.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $headline = null;

    /*
     * The lead, aka short description of the event purpose.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $lead = null;

    /*
     * The main event content.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $content = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['event:read', 'event:write'])]
    protected ?string $slug = null;

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(?string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getLead(): ?string
    {
        return $this->lead;
    }

    public function setLead(?string $lead): self
    {
        $this->lead = $lead;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
