<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Event;

use Arodax\AdminBundle\Entity\User\UserGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
#[ORM\Table(name: 'aa_event_acl_user_group')]
class EventAclUserGroup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['acl:read', 'acl:write'])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'securityUserGroups')]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    protected ?Event $accessed = null;

    #[ORM\ManyToOne(targetEntity: UserGroup::class)]
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[Groups(['user_group:read', 'acl:read'])]
    protected ?UserGroup $accessor = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['acl:read', 'acl:write'])]
    protected ?array $attributes = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccessed(): ?Event
    {
        return $this->accessed;
    }

    public function setAccessed(?Event $accessed): self
    {
        $this->accessed = $accessed;

        return $this;
    }

    public function getAccessor(): ?UserGroup
    {
        return $this->accessor;
    }

    public function setAccessor(?UserGroup $accessor): self
    {
        $this->accessor = $accessor;

        return $this;
    }

    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    public function setAttributes(?array $attributes): self
    {
        $this->attributes = $attributes;
        return $this;
    }
}
