<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Content;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslatableOrderFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\Gallery\Gallery;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\EntityListener\Content\ArticleEntityListener;
use Arodax\AdminBundle\Model\Content\ArticleInterface;
use Arodax\AdminBundle\Model\TimestampableInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Model\User\UserInterface;
use Arodax\AdminBundle\Repository\Content\ArticleRepository;
use Arodax\AdminBundle\Security\Voter\ArticleVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * This entity represent an article.
 *
 * @method ArticleTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(types: ['http://schema.org/Article'],
    operations: [
        new Get(security: 'is_granted("'.ArticleVoter::READ_ARTICLE.'", object)'),
        new Put(security: 'is_granted("'.ArticleVoter::UPDATE_ARTICLE.'", object)'),
        new Delete(security: 'is_granted("'.ArticleVoter::DELETE_ARTICLE.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.ArticleVoter::CREATE_ARTICLE.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['article:read', 'user:info', 'gallery:read', 'acl:read', 'user_group:read'], 'swagger_definition_name' => 'Read'],
    denormalizationContext: ['groups' => ['article:write', 'gallery:write'], 'swagger_definition_name' => 'Write']
)]
#[ApiFilter(filterClass: TranslatableOrderFilter::class, properties: ['id', 'createdAt', 'publishedAt', 'translations.*.headline'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['headline' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['authors.id', 'sections.id', 'id'])]
#[ORM\EntityListeners([ArticleEntityListener::class])]
#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[ORM\Table(name: 'aa_article')]
#[ORM\Index(columns: ['status'])]
class Article implements ArticleInterface, TranslatableInterface, TimestampableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['article:read', 'article:tree'])]
    protected ?int $id = null;

    /**
     * @var User[] Authors of this article
     */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'aa_article_author')]
    #[ORM\JoinColumn(name: 'article_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected $authors;

    /**
     * @var Section[]
     */
    #[ORM\ManyToMany(targetEntity: Section::class, inversedBy: 'articles')]
    #[ORM\JoinTable(name: 'aa_article_section')]
    #[ORM\JoinColumn(name: 'article_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected $sections;

    /*
     * Property containing information about the state of the article.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?string $status = ArticleInterface::STATUS_DRAFT;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['article:read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['article:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when article has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['article:read', 'article:tree'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['article:write'])]
    protected $newTranslations;

    /**
     * The URL of the article cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected array $covers = [];

    /**
     * The gallery attached to this article.
     *
     * @var Collection&Gallery
     */
    #[ORM\ManyToMany(targetEntity: Gallery::class, cascade: ['ALL'])]
    #[ORM\JoinTable(name: 'aa_article_gallery')]
    #[Groups(groups: ['article:read', 'article:write'])]
    private $galleries;

    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['article:read', 'article:write'])]
    private int $priority = 5;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['article:read', 'article:write'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['article:read', 'article:write'])]
    private bool $deletable = true;

    /**
     * @var User[] Users who have access to this article.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: ArticleAclUser::class, fetch: 'EAGER')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUsers;

    /**
     * @var UserGroup[] User groups who have access to this article.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: ArticleAclUserGroup::class, fetch: 'EAGER')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUserGroups;

    /**
     * When this is enabled, we also apply additional security checks.
     *
     * @var bool
     */
    #[Orm\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(groups: ['article:read', 'article:write'])]
    private bool $aclEnabled = false;

    /*
     * Individual extended article parameters
     */
    #[ORM\OneToMany(mappedBy: 'article', targetEntity: ArticleParameter::class, cascade: ['ALL'], orphanRemoval: true)]
    #[Groups(groups: ['article:write'])]
    protected $parameters;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->sections = new ArrayCollection();
        $this->galleries = new ArrayCollection();
        $this->aclUsers = new ArrayCollection();
        $this->aclUserGroups = new ArrayCollection();
        $this->parameters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /*
     * Get all article authors.
     */
    public function getAuthors(): array
    {
        return $this->authors->toArray();
    }

    /*
     * Add author to the article.
     */
    public function addAuthor(UserInterface $user): self
    {
        if (!$this->authors->contains($user)) {
            $this->authors->add($user);
        }

        return $this;
    }

    /*
     * Remove author from the article.
     */
    public function removeAuthor(UserInterface $author): self
    {
        if ($this->authors->contains($author)) {
            $this->authors->removeElement($author);
        }

        return $this;
    }

    /**
     * Get sections for the article.
     *
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections->toArray();
    }

    /*
     * Add article to a section.
     */
    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections->add($section);
        }

        return $this;
    }

    /**
     * Remove article from the section.
     *
     * @return Article
     */
    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
        }

        return $this;
    }

    /*
     * Get article cover URL.
     */
    public function getCovers(): ?array
    {
        return $this->covers;
    }

    /**
     * Set the article cover URL.
     *
     * @param ?array $covers
     *
     * @return Article
     */
    public function setCovers(?array $covers): self
    {
        $this->covers = $covers;

        return $this;
    }

    /**
     * Get first article cover url.
     *
     * @return string|null
     */
    #[Groups(groups: ['article:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGalleries(): array
    {
        return $this->galleries->toArray();
    }

    public function addGallery(Gallery $gallery): self
    {
        #dd('call is here 1');

        if (!$this->galleries->contains($gallery)) {
            $this->galleries->add($gallery);
            #dd(debug_backtrace());
        }

        return $this;
    }

    public function removeGallery(Gallery $gallery): self
    {
        if ($this->galleries->contains($gallery)) {
            $this->galleries->removeElement($gallery);
        }

        return $this;
    }

    /*
     * Get translated article headline for the request current locale.
     */
    #[Groups(groups: ['article:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getHeadline();
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return Article
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    public function setReadOnly(bool $readOnly): self
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * @param bool $deletable
     * @return Article
     */
    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getAclUsers(): array
    {
        return $this->aclUsers->toArray();
    }

    public function addSecurityUser(User $user): self
    {
        if (false === $this->aclUsers->contains($user)) {
            $this->aclUsers->add($user);
        }

        return $this;
    }

    public function removeSecurityUser(User $user): self
    {
        if (true === $this->aclUsers->contains($user)) {
            $this->aclUsers->remove($user);
        }

        return $this;
    }

    /**
     * @return UserGroup[]
     */
    public function getAclUserGroups(): array
    {
        return $this->aclUserGroups->toArray();
    }

    public function addSecurityUserGroup(UserGroup $UserGroup): self
    {
        if (false === $this->aclUserGroups->contains($UserGroup)) {
            $this->aclUserGroups->add($UserGroup);
        }

        return $this;
    }

    public function removeSecurityUserGroup(UserGroup $UserGroup): self
    {
        if (true === $this->aclUserGroups->contains($UserGroup)) {
            $this->aclUserGroups->remove($UserGroup);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAclEnabled(): bool
    {
        return $this->aclEnabled;
    }

    /**
     * @param bool $aclEnabled
     * @return Article
     */
    public function setAclEnabled(bool $aclEnabled): Article
    {
        $this->aclEnabled = $aclEnabled;
        return $this;
    }

    /*
     * Check whether this article is enabled, we assume this happens when article is published.
     */
    public function isEnabled(): bool
    {
        return $this->status === ArticleInterface::STATUS_PUBLISHED;
    }

    /**
     * @return ArticleParameter[]
     */
    #[Groups(groups: ['article:read'])]
    #[Valid]
    public function getParameters(): array
    {
        return $this->parameters->getValues();
    }

    public function addParameter(ArticleParameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $parameter->setArticle($this);
            $this->parameters->add($parameter);
        }

        return $this;
    }

    public function removeParameter(ArticleParameter $parameter): self
    {
        if ($this->parameters->contains($parameter)) {
            $this->parameters->removeElement($parameter);
            $parameter->setArticle(null);
        }

        return $this;
    }
}
