<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Content;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\EntityListener\Content\SectionEntityListener;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Repository\Content\SectionRepository;
use Arodax\AdminBundle\Security\Voter\SectionVoter;
use Arodax\AdminBundle\Tree\Mapping\Annotation as Tree;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @method SectionTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.SectionVoter::READ_SECTION.'", object)'),
        new Put(security: 'is_granted("'.SectionVoter::UPDATE_SECTION.'", object)'),
        new Delete(security: 'is_granted("'.SectionVoter::DELETE_SECTION.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.SectionVoter::CREATE_SECTION.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['section:read', 'acl:read', 'user:info', 'user_group:read']],
    denormalizationContext: ['groups' => ['section:write']]
)]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'createdAt', 'publishedAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['status' => 'exact'])]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: DateFilter::class, properties: ['createdAt', 'publishedAt'])]

#[ORM\Entity(repositoryClass: SectionRepository::class)]
#[ORM\Table(name: 'aa_section')]
#[ORM\Index(columns: ['status'])]
#[ORM\EntityListeners([SectionEntityListener::class])]
#[Tree\Tree(type: 'nested')]
class Section implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    public const string STATUS_DRAFT = 'draft';
    public const string STATUS_PUBLISHED = 'published';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['section:read', 'article:write', 'article:read', 'section:tree'])]
    protected ?int $id = null;

    #[Tree\Left]
    #[ORM\Column(name: 'lft', type: 'integer')]
    #[Groups(groups: ['section:tree'])]
    protected $lft;

    #[Tree\Level]
    #[ORM\Column(name: 'lvl', type: 'integer')]
    #[Groups(groups: ['section:tree'])]
    protected $lvl;

    #[Tree\Right]
    #[ORM\Column(name: 'rgt', type: 'integer')]
    #[Groups(groups: ['section:tree'])]
    protected $rgt;

    #[Tree\Root]
    #[ORM\ManyToOne(targetEntity: 'Section')]
    #[ORM\JoinColumn(name: 'tree_root', referencedColumnName: 'id', onDelete: 'CASCADE')]
    protected $root;

    #[Tree\ParentNode]
    #[MaxDepth(1)]
    #[ORM\ManyToOne(targetEntity: 'Section', inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['section:write', 'section:tree'])]
    protected $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: 'Section')]
    #[ORM\OrderBy(value: ['lft' => 'ASC'])]
    #[Groups(groups: ['section:read', 'section:tree'])]
    protected $children;

    /**
     * @var Article[]
     */
    #[ORM\ManyToMany(targetEntity: Article::class, mappedBy: 'sections', fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['section:read', 'article:tree'])]
    protected $articles;

    /**
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['section:read', 'article:read', 'section:tree'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['section:write'])]
    protected $newTranslations;

    /*
     * Property containing information about the state of the sections.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['section:read', 'section:write', 'section:tree'])]
    protected ?string $status = self::STATUS_DRAFT;

    /*
     * DateTime when section has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    protected ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['section:read', 'section:write', 'section:tree'])]
    private bool $readOnly = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(groups: ['section:read', 'section:write', 'section:tree'])]
    private bool $deletable = true;

    /**
     * @var SectionAclUser[] Users who have access to this section.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: SectionAclUser::class, fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUsers;

    /**
     * @var SectionAclUserGroup[] User groups who have access to this section.
     */
    #[ORM\OneToMany(mappedBy: 'accessed', targetEntity: SectionAclUserGroup::class, fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['acl:read'])]
    protected $aclUserGroups;

    /**
     * When this is enabled, we also apply additional security checks.
     *
     * @var bool
     */
    #[Orm\Column(type: 'boolean', options: ['default' => 0])]
    #[Groups(groups: ['section:read', 'section:write'])]
    private bool $aclEnabled = false;

    /**
     * The URL of the section cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected ?array $covers = [];

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->aclUsers = new ArrayCollection();
        $this->aclUserGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get the parent node of the section.
     *
     * If parent is null, this section is on the root level.
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * Set the parent section of this section.
     *
     * Setting this value will not affect lft, rgt and lvl properties of the three.
     * These values need to be correctly calculated by the data persister.
     */
    public function setParent(self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Section[]
     */
    public function getChildren(): array
    {
        return $this->children->toArray();
    }

    /*
     * Add article to a section.
     */
    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
        }

        $article->addSection($this);

        return $this;
    }

    /**
     * @return Article[]
     */
    public function getArticles(): array
    {
        return $this->articles->toArray();
    }

    public function getLft()
    {
        return $this->lft;
    }

    public function setLft($lft): void
    {
        $this->lft = $lft;
    }

    public function getLvl()
    {
        return $this->lvl;
    }

    public function setLvl($lvl): void
    {
        $this->lvl = $lvl;
    }

    public function getRgt()
    {
        return $this->rgt;
    }

    public function setRgt($rgt): void
    {
        $this->rgt = $rgt;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root): void
    {
        $this->root = $root;
    }

    #[Groups(groups: ['section:tree', 'section:read'])]
    #[Deprecated(reason: 'This method is deprecated to use it as item name, use "getHeadline" instead', replacement: '%class%->getHeadline()')]
    public function getTitle(): ?string
    {
        return $this->translate()->getName();
    }

    #[Groups(groups: ['section:tree', 'section:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }

    /**
     * @return bool
     */
    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * @param bool $readOnly
     * @return Section
     */
    public function setReadOnly(bool $readOnly): self
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * @param bool $deletable
     * @return Section
     */
    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getAclUsers(): array
    {
        return $this->aclUsers->toArray();
    }

    public function addSecurityUser(User $user): self
    {
        if (false === $this->aclUsers->contains($user)) {
            $this->aclUsers->add($user);
        }

        return $this;
    }

    public function removeSecurityUser(User $user): self
    {
        if (true === $this->aclUsers->contains($user)) {
            $this->aclUsers->remove($user);
        }

        return $this;
    }

    /**
     * @return UserGroup[]
     */
    public function getAclUserGroups(): array
    {
        return $this->aclUserGroups->toArray();
    }

    public function addSecurityUserGroup(SectionAclUserGroup $userGroup): self
    {
        if (false === $this->aclUserGroups->contains($userGroup)) {
            $this->aclUserGroups->add($userGroup);
        }

        return $this;
    }

    public function removeSecurityUserGroup(SectionAclUser $user): self
    {
        if (true === $this->aclUserGroups->contains($user)) {
            $this->aclUserGroups->removeElement($user);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAclEnabled(): bool
    {
        return $this->aclEnabled;
    }

    /**
     * @param bool $aclEnabled
     * @return Section
     */
    public function setAclEnabled(bool $aclEnabled): self
    {
        $this->aclEnabled = $aclEnabled;
        return $this;
    }

    /*
     * Get section cover URL.
     */
    public function getCovers(): array
    {
        return $this->covers ?? [];
    }

    /**
     * Set the section cover URL.
     *
     * @param ?array $covers
     *
     * @return Section
     */
    public function setCovers(?array $covers): self
    {
        $this->covers = $covers;

        return $this;
    }
}
