<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Content;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\Content\SectionTranslationEntityListener;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\EntityListeners([SectionTranslationEntityListener::class])]
#[ORM\Table(name: 'aa_section_translation')]
class SectionTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['section:read'])]
    protected ?int $id = null;

    /*
     * The section name.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['section:read', 'section:write', 'article:read', 'section:tree'])]
    protected ?string $name = null;

    /*
     * The section description.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected ?string $description = null;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected ?string $slug = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['section:read', 'section:write'])]
    protected ?string $path = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;
        $this->path = null;

        return $this;
    }
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    #[Groups(groups: ['section:read', 'section:tree'])]
    public function getHeadline():?string
    {
        return $this->name;
    }
}
