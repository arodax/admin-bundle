<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Content;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\Content\ArticleTranslationEntityListener;
use Arodax\AdminBundle\Model\SluggableInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;

#[ORM\EntityListeners([ArticleTranslationEntityListener::class])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_article_translation')]
class ArticleTranslation implements SluggableInterface
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected $id;

    /*
     * Locale of the current translation.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected $locale;

    /*
     * The title of the article.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected ?string $title = null;

    /**
     * The main headline for the article, usually identifies with the heading element.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected ?string $headline = null;

    /**
     * The lead, aka short description of the article purpose.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?string $lead = null;

    /**
     * The main article content.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?string $content = null;

    /**
     * Article keywords.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?array $metaKeywords = null;

    /*
     * A canonical link for this article.
     */
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?string $canonicalLink = null;

    /**
     * Robots handling for the article.
     *
     * @var array<string>|null
     */
    #[Choice(choices: ['noindex', 'index', 'follow', 'nofollow', 'noimageindex', 'none', 'noarchive', 'nosnippet'])]
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?array $metaRobots = [];

    /*
     * Article description.
     */
    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    protected ?string $metaDescription = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected ?string $slug = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['article:read', 'article:write', 'article:tree'])]
    protected ?string $link = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(?string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getLead(): ?string
    {
        return $this->lead;
    }

    public function getPlainLead(): ?string
    {
        return $this->lead ? strip_tags($this->lead) : null;
    }

    public function setLead(?string $lead): self
    {
        $this->lead = $lead;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getPlainContent(): ?string
    {
        return $this->content ? strip_tags($this->content) : null;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getMetaKeywords(): ?array
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?array $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getCanonicalLink(): ?string
    {
        return $this->canonicalLink;
    }

    public function setCanonicalLink(?string $canonicalLink): self
    {
        $this->canonicalLink = $canonicalLink;

        return $this;
    }

    public function getMetaRobots(): ?array
    {
        return $this->metaRobots;
    }

    public function setMetaRobots(?array $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
