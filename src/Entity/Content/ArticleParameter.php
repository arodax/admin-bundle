<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Content;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use Arodax\AdminBundle\Security\Voter\ArticleVoter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    operations: [
        new Get(security: 'is_granted("'.ArticleVoter::READ_ARTICLE.'", object)'),
        new Put(security: 'is_granted("'.ArticleVoter::UPDATE_ARTICLE.'", object)'),
        new Delete(security: 'is_granted("'.ArticleVoter::DELETE_ARTICLE.'", object)'),
        new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
        new Post(securityPostDenormalize: 'is_granted("'.ArticleVoter::CREATE_ARTICLE.'", object)', validationContext: ['groups' => ['']])
    ],
    normalizationContext: ['groups' => ['article:read']],
    denormalizationContext: ['groups' => ['article:write']]
)]
#[ORM\Entity()]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'aa_article_parameter')]
class ArticleParameter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['article:read', 'article:write', 'shopping_cart_item:read'])]
    protected ?int $id = null;

    #[ORM\Column(name: 'name', type: 'string', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    #[NotBlank]
    protected ?string $name = null;

    #[ORM\Column(name: 'value', type: 'string', nullable: true)]
    #[Groups(groups: ['article:read', 'article:write'])]
    #[NotBlank]
    protected ?string $value = null;

    #[ORM\ManyToOne(targetEntity: Article::class, inversedBy: 'parameters')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected Article $article;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }
}
