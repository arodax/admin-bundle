<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

trait TimestampTrait
{
    /*
     * Datetime of the creation.
     */
    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(groups: ['admin_read'])]
    protected ?\DateTimeImmutable $createdAt = null;

    /*
     * Datetime of the last entity update.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['admin_read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[ORM\PrePersist]
    public function prePersistCreatedAt(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
