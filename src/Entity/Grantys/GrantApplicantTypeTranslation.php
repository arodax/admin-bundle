<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * This entity represent "Grantys" grant applicant type.
 */
#[ORM\Entity()]
#[ORM\Table(name: 'aa_grant_applicant_type_translation')]
class GrantApplicantTypeTranslation implements TranslatableInterface
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}
