<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Range;

/**
 * This entity represent "Grantys" grant call.
 * @method GrantCallTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_GRANT_CALL\', object)'),
    new Put(security: 'is_granted(\'UPDATE_GRANT_CALL\', object)'),
    new Delete(security: 'is_granted(\'DELETE_GRANT_CALL\', object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_GRANT_CALL\', object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['grant_call:read'], 'swagger_definition_name' => 'Read'],
    denormalizationContext: ['groups' => ['grant_call:write'], 'swagger_definition_name' => 'Write']
)]
#[ORM\Entity]
#[ORM\Table(name: 'aa_grant_call')]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: SearchFilter::class)]
#[ApiFilter(filterClass: NumericFilter::class)]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id'], arguments: ['orderParameterName' => 'order'])]
class GrantCall implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_call:read', 'grant_call:write', 'grant_project_group:read'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $status = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $callStatus = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $variableSymbol = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $roundsQuantity = null;

    #[ORM\ManyToOne(targetEntity: GrantApplicantType::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?GrantApplicantType $applicantType = null;

    #[ORM\ManyToOne(targetEntity: GrantFocusType::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?GrantFocusType $focusType = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?\DateTimeInterface $applicationStartAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?\DateTimeInterface $deadlineAt = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    #[Range(min: 0, max: 2147483647)]
    protected ?int $maxAmount = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    /**
     * The URL of the grant calls attachments.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected array $attachments = [];

    /**
     * The URL of grant calls covers.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected array $covers = [];

    #[ORM\OneToMany(mappedBy: 'grantCall', targetEntity: GrantProjectGroup::class)]
    private Collection $grantProjectGroups;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true])]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected bool $onlineLink = true;

    /**
     * @var array
     */
    #[Groups(groups: ['grant_call:write'])]
    protected $newTranslations;

    public function __construct()
    {
        $this->grantProjectGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getVariableSymbol(): ?string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(?string $variableSymbol): void
    {
        $this->variableSymbol = $variableSymbol;
    }

    public function getRoundsQuantity(): ?string
    {
        return $this->roundsQuantity;
    }

    public function setRoundsQuantity(?string $roundsQuantity): void
    {
        $this->roundsQuantity = $roundsQuantity;
    }

    #[Groups(groups: ['grant_call:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }

    #[Groups(groups: ['grant_call:read'])]
    public function getLead(): ?string
    {
        return $this->translate()->getLead();
    }

    #[Groups(groups: ['grant_call:read'])]
    public function getDescription(): ?string
    {
        return $this->translate()->getDescription();
    }

    public function getApplicantType(): ?GrantApplicantType
    {
        return $this->applicantType;
    }

    public function setApplicantType(?GrantApplicantType $applicantType): self
    {
        $this->applicantType = $applicantType;
        return $this;
    }

    public function getFocusType(): ?GrantFocusType
    {
        return $this->focusType;
    }

    public function setFocusType(?GrantFocusType $focusType): self
    {
        $this->focusType = $focusType;
        return $this;
    }

    public function getMaxAmount(): ?int
    {
        return $this->maxAmount;
    }

    public function setMaxAmount(?int $maxAmount): self
    {
        $this->maxAmount = $maxAmount;
        return $this;
    }

    public function getApplicationStartAt(): ?\DateTimeInterface
    {
        return $this->applicationStartAt;
    }

    public function setApplicationStartAt(?\DateTimeInterface $applicationStartAt): self
    {
        $this->applicationStartAt = $applicationStartAt;
        return $this;
    }

    public function getDeadlineAt(): ?\DateTimeInterface
    {
        return $this->deadlineAt;
    }

    public function setDeadlineAt(?\DateTimeInterface $deadlineAt): self
    {
        $this->deadlineAt = $deadlineAt;
        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    public function getCallStatus(): ?string
    {
        return $this->callStatus;
    }

    public function setCallStatus(?string $callStatus): self
    {
        $this->callStatus = $callStatus;
        return $this;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function setAttachments(array $attachments): self
    {
        $this->attachments = $attachments;
        return $this;
    }

    public function getCovers(): array
    {
        return $this->covers;
    }

    public function setCovers(array $covers): self
    {
        $this->covers = $covers;
        return $this;
    }

    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }

    /**
     * @return Collection<int, GrantProjectGroup>
     */
    public function getGrantProjectGroups(): Collection
    {
        return $this->grantProjectGroups;
    }

    public function addGrantProjectGroup(GrantProjectGroup $grantProjectGroup): self
    {
        if (!$this->grantProjectGroups->contains($grantProjectGroup)) {
            $this->grantProjectGroups->add($grantProjectGroup);
            $grantProjectGroup->setGrantCall($this);
        }

        return $this;
    }

    public function removeGrantProjectGroup(GrantProjectGroup $grantProjectGroup): self
    {
        if ($this->grantProjectGroups->removeElement($grantProjectGroup) && $grantProjectGroup->getGrantCall() === $this) {
            $grantProjectGroup->setGrantCall(null);
        }

        return $this;
    }

    public function hasOnlineLink(): bool
    {
        return $this->onlineLink;
    }

    public function setOnlineLink(bool $onlineLink): self
    {
        $this->onlineLink = $onlineLink;

        return $this;
    }
}
