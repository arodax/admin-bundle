<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\Content\ArticleInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Security\Voter\GrantProjectGroupVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use \Arodax\AdminBundle\Entity\Grantys\GrantCall;

/**
 * This entity represent "Grantys" grant project.
 *
 * @method GrantProjectTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [new Get(security: 'is_granted(\'READ_GRANT_PROJECT_GROUP\', object)'), new Put(security: 'is_granted(\'UPDATE_GRANT_PROJECT_GROUP\', object)'), new Delete(security: 'is_granted(\'DELETE_GRANT_PROJECT_GROUP\', object)'), new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'), new Post(securityPostDenormalize: 'is_granted(\'CREATE_GRANT_PROJECT_GROUP\', object)', validationContext: ['groups' => ['']])], normalizationContext: ['groups' => ['grant_project_group:read'], 'swagger_definition_name' => 'Read'], denormalizationContext: ['groups' => ['grant_project_group:write'], 'swagger_definition_name' => 'Write'])]
#[ORM\Entity]
#[ORM\Table(name: 'aa_grant_project_group')]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id', 'approvedAmount', 'year'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'number'], arguments: ['orderParameterName' => 'order'])]
class GrantProjectGroup implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write', 'grant_project:read'])]
    protected ?int $id = null;

    /**
     * The URL of the grant project cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected array $covers = [];

    /**
     * Property containing information about the state of the grant project group.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected ?string $status = ArticleInterface::STATUS_DRAFT;

    /**
    * Datetime of the last entity update.
    */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project_group:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /**
     * DateTime when grant project group has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    #[ORM\ManyToMany(targetEntity: GrantProject::class, mappedBy: 'grantProjectGroups')]
    private Collection $grantProjects;

    #[ORM\ManyToOne(cascade: ['ALL'], inversedBy: 'grantProjectGroups')]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    private ?GrantCall $grantCall = null;

    /**
     * @var array
     */
    #[Groups(groups: ['grant_project_group:write'])]
    protected $newTranslations;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    private ?int $year = null;

    public function __construct()
    {
        $this->grantProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCovers(): array
    {
        return $this->covers;
    }

    public function setCovers(array $covers): self
    {
        $this->covers = $covers;
        return $this;
    }

    #[Groups(groups: ['grant_project_group:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }

    #[Groups(groups: ['grant_project_group:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * @return Collection<int, GrantProject>
     */
    public function getGrantProjects(): Collection
    {
        return $this->grantProjects;
    }

    public function addGrantProject(GrantProject $grantProject): self
    {
        if (!$this->grantProjects->contains($grantProject)) {
            $this->grantProjects->add($grantProject);
            $grantProject->addGrantProjectGroup($this);
        }

        return $this;
    }

    public function removeGrantProject(GrantProject $grantProject): self
    {
        if ($this->grantProjects->removeElement($grantProject)) {
            $grantProject->removeGrantProjectGroup($this);
        }

        return $this;
    }

    public function getGrantCall(): ?GrantCall
    {
        return $this->grantCall;
    }

    public function setGrantCall(?GrantCall $grantCall): self
    {
        $this->grantCall = $grantCall;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): void
    {
        $this->year = $year;
    }
}
