<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\GrantProjectTranslationEntityListener;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\EntityListeners([GrantProjectTranslationEntityListener::class])]
#[ORM\Table(name: 'aa_grant_project_translation')]
class GrantProjectTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $name = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $scopeOfApplication = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $annotation = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $focus = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $technicalArea = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $topic = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $slug = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale): self
    {
        $this->locale = $locale;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getScopeOfApplication(): ?string
    {
        return $this->scopeOfApplication;
    }

    public function setScopeOfApplication(?string $scopeOfApplication): self
    {
        $this->scopeOfApplication = $scopeOfApplication;
        return $this;
    }

    public function getAnnotation(): ?string
    {
        return $this->annotation;
    }

    public function setAnnotation(?string $annotation): self
    {
        $this->annotation = $annotation;
        return $this;
    }

    public function getFocus(): ?string
    {
        return $this->focus;
    }

    public function setFocus(?string $focus): self
    {
        $this->focus = $focus;
        return $this;
    }

    public function getTechnicalArea(): ?string
    {
        return $this->technicalArea;
    }

    public function setTechnicalArea(?string $technicalArea): self
    {
        $this->technicalArea = $technicalArea;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(?string $topic): self
    {
        $this->topic = $topic;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
