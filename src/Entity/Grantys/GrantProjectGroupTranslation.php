<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Arodax\AdminBundle\EntityListener\Grantys\GrantProjectGroupTranslationEntityListener;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\EntityListeners([GrantProjectGroupTranslationEntityListener::class])]
#[ORM\Table(name: 'aa_grant_project_group_translation')]
class GrantProjectGroupTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected ?int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected ?string $name = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_project_type', 'grant_call:read', 'grant_call:write'])]
    protected ?string $slug = null;

    /**
     * The lead, aka short description of the grant project group purpose.
     */
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_project_group:read', 'grant_project_group:write'])]
    protected ?string $lead = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getLocale(): mixed
    {
        return $this->locale;
    }

    public function setLocale($locale): static
    {
        $this->locale = $locale;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }


    public function getLead(): ?string
    {
        return $this->lead;
    }

    public function setLead(?string $lead): self
    {
        $this->lead = $lead;
        return $this;
    }
}
