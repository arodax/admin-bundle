<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Doctrine\Filter\TranslationsSearchFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\Content\ArticleInterface;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Security\Voter\GrantProjectVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This entity represent "Grantys" grant project.
 *
 * @method GrantProjectTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [
    new Get(security: 'is_granted(\'READ_GRANT_PROJECT\', object)'),
    new Put(security: 'is_granted(\'UPDATE_GRANT_PROJECT\', object)'),
    new Delete(security: 'is_granted(\'DELETE_GRANT_PROJECT\', object)'),
    new GetCollection(security: 'is_granted("ROLE_ADMINISTRATION_ACCESS")'),
    new Post(securityPostDenormalize: 'is_granted(\'CREATE_GRANT_PROJECT\', object)', validationContext: ['groups' => ['']])
],
    normalizationContext: ['groups' => ['grant_project:read'], 'swagger_definition_name' => 'Read'],
    denormalizationContext: ['groups' => ['grant_project:write'], 'swagger_definition_name' => 'Write']
)]
#[ORM\Entity()]
#[ORM\Table(name: 'aa_grant_project')]
#[ApiFilter(filterClass: TranslationsSearchFilter::class, properties: ['name' => 'partial', 'focus' => 'partial'])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['applicant' => 'partial', 'projectStatus' => 'partial', 'callNumber' => 'partial', 'number' => 'partial', 'approvedAmount' => 'exact'])]
#[ApiFilter(filterClass: NumericFilter::class, properties: ['id'])]
#[ApiFilter(filterClass: OrderFilter::class, properties: ['id', 'number'], arguments: ['orderParameterName' => 'order'])]
class GrantProject implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $number = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $projectStatus = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $version = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $note = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $email = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $startedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $endedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $signedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $closedAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $applicant = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $dataBox = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $legalForm = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $contractNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $statutory = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $businessNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $address = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $county = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $subjectNote = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $contactPersonName = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $contactPersonEmail = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $contactPersonPhone = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $placeOfRealization = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $districtOfRealization = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $cityOfRealization = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $countyOfRealization = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $callNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $accountNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $iban = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $swift = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $variableNumber = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $amountRequested = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $totalAmount = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $approvedAmount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $currency = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $installment1At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $installment1Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment1Fund = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment1FundCode = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $payment1At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $payment1Amount = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $installment2At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $installment2Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment2Fund = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment2FundCode = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $payment2At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $payment2Amount = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $installment3At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $installment3Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment3Fund = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment3FundCode = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $payment3At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $payment3Amount = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $installment4At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $installment4Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment4Fund = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment4FundCode = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $payment4At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $payment4Amount = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $installment5At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $installment5Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment5Fund = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $installment5FundCode = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $payment5At = null;

    #[ORM\Column(type: 'money', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    #[Assert\Range(min: 0, max: PHP_INT_MAX / 100)]
    protected ?int $payment5Amount = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $message1Type = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message1DueAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message1SubmittedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message1ApprovedAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $message2Type = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message2DueAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message2SubmittedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message2ApprovedAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $message3Type = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message3DueAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message3SubmittedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message3ApprovedAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $message4Type = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message4DueAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message4SubmittedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message4ApprovedAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $message5Type = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message5DueAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message5SubmittedAt = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeImmutable $message5ApprovedAt = null;

    /**
     * The URL of the grant project cover.
     *
     * @var array<string>|null
     */
    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected array $covers = [];

    /*
     * Property containing information about the state of the grant project.
     */
    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?string $status = ArticleInterface::STATUS_DRAFT;

    /*
    * Datetime of the last entity update.
    */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read'])]
    protected ?\DateTimeImmutable $updatedAt = null;

    /*
     * DateTime when grant project has been published.
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?\DateTimeInterface $publishedAt = null;

    #[ORM\ManyToOne(targetEntity: GrantApplicantType::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?GrantApplicantType $applicantType = null;

    #[ORM\ManyToOne(targetEntity: GrantFocusType::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?GrantFocusType $focusType = null;

    #[ORM\ManyToOne(targetEntity: GrantProjectType::class)]
    #[ORM\JoinColumn(nullable: true, onDelete: 'CASCADE')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    protected ?GrantProjectType $projectType = null;

    #[ORM\Column(type: 'integer', options: ['default' => 5])]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    private int $priority = 5;

    /**
     * @return Collection<int, GrantProjectGroup>
     */
    #[ORM\ManyToMany(targetEntity: GrantProjectGroup::class, inversedBy: 'grantProjects', cascade: ['ALL'])]
    #[ORM\JoinTable(name: 'aa_grant_project_grant_project_group')]
    #[Groups(groups: ['grant_project:read', 'grant_project:write'])]
    private Collection $grantProjectGroups;

    /**
     * @var array
     */
    #[Groups(groups: ['grant_project:write'])]
    protected $newTranslations;

    public function __construct()
    {
        $this->grantProjectGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;
        return $this;
    }

    public function getProjectStatus(): ?string
    {
        return $this->projectStatus;
    }

    public function setProjectStatus(?string $projectStatus): self
    {
        $this->projectStatus = $projectStatus;
        return $this;
    }

    public function getDistrictOfRealization(): ?string
    {
        return $this->districtOfRealization;
    }

    public function setDistrictOfRealization(?string $districtOfRealization): self
    {
        $this->districtOfRealization = $districtOfRealization;
        return $this;
    }

    public function getCityOfRealization(): ?string
    {
        return $this->cityOfRealization;
    }

    public function setCityOfRealization(?string $cityOfRealization): self
    {
        $this->cityOfRealization = $cityOfRealization;
        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;
        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getStartedAt(): ?\DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeImmutable $startedAt): self
    {
        $this->startedAt = $startedAt;
        return $this;
    }

    public function getEndedAt(): ?\DateTimeImmutable
    {
        return $this->endedAt;
    }

    public function setEndedAt(?\DateTimeImmutable $endedAt): self
    {
        $this->endedAt = $endedAt;
        return $this;
    }

    public function getSignedAt(): ?\DateTimeImmutable
    {
        return $this->signedAt;
    }

    public function setSignedAt(?\DateTimeImmutable $signedAt): self
    {
        $this->signedAt = $signedAt;
        return $this;
    }

    public function getClosedAt(): ?\DateTimeImmutable
    {
        return $this->closedAt;
    }

    public function setClosedAt(?\DateTimeImmutable $closedAt): self
    {
        $this->closedAt = $closedAt;
        return $this;
    }

    public function getApplicant(): ?string
    {
        return $this->applicant;
    }

    public function setApplicant(?string $applicant): self
    {
        $this->applicant = $applicant;
        return $this;
    }

    public function getDataBox(): ?string
    {
        return $this->dataBox;
    }

    public function setDataBox(?string $dataBox): self
    {
        $this->dataBox = $dataBox;
        return $this;
    }

    public function getLegalForm(): ?string
    {
        return $this->legalForm;
    }

    public function setLegalForm(?string $legalForm): self
    {
        $this->legalForm = $legalForm;
        return $this;
    }

    public function getContractNumber(): ?string
    {
        return $this->contractNumber;
    }

    public function setContractNumber(?string $contractNumber): self
    {
        $this->contractNumber = $contractNumber;
        return $this;
    }

    public function getStatutory(): ?string
    {
        return $this->statutory;
    }

    public function setStatutory(?string $statutory): self
    {
        $this->statutory = $statutory;
        return $this;
    }

    public function getBusinessNumber(): ?string
    {
        return $this->businessNumber;
    }

    public function setBusinessNumber(?string $businessNumber): self
    {
        $this->businessNumber = $businessNumber;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;
        return $this;
    }

    public function getSubjectNote(): ?string
    {
        return $this->subjectNote;
    }

    public function setSubjectNote(?string $subjectNote): self
    {
        $this->subjectNote = $subjectNote;
        return $this;
    }

    public function getContactPersonName(): ?string
    {
        return $this->contactPersonName;
    }

    public function setContactPersonName(?string $contactPersonName): self
    {
        $this->contactPersonName = $contactPersonName;
        return $this;
    }

    public function getContactPersonEmail(): ?string
    {
        return $this->contactPersonEmail;
    }

    public function setContactPersonEmail(?string $contactPersonEmail): self
    {
        $this->contactPersonEmail = $contactPersonEmail;
        return $this;
    }

    public function getContactPersonPhone(): ?string
    {
        return $this->contactPersonPhone;
    }

    public function setContactPersonPhone(?string $contactPersonPhone): self
    {
        $this->contactPersonPhone = $contactPersonPhone;
        return $this;
    }

    public function getPlaceOfRealization(): ?string
    {
        return $this->placeOfRealization;
    }

    public function setPlaceOfRealization(?string $placeOfRealization): self
    {
        $this->placeOfRealization = $placeOfRealization;
        return $this;
    }

    public function getCountyOfRealization(): ?string
    {
        return $this->countyOfRealization;
    }

    public function setCountyOfRealization(?string $countyOfRealization): self
    {
        $this->countyOfRealization = $countyOfRealization;
        return $this;
    }

    public function getCallNumber(): ?string
    {
        return $this->callNumber;
    }

    public function setCallNumber(?string $callNumber): self
    {
        $this->callNumber = $callNumber;
        return $this;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(?string $accountNumber): self
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;
        return $this;
    }

    public function getSwift(): ?string
    {
        return $this->swift;
    }

    public function setSwift(?string $swift): self
    {
        $this->swift = $swift;
        return $this;
    }

    public function getVariableNumber(): ?string
    {
        return $this->variableNumber;
    }

    public function setVariableNumber(?string $variableNumber): self
    {
        $this->variableNumber = $variableNumber;
        return $this;
    }

    public function getAmountRequested(): ?int
    {
        return $this->amountRequested;
    }

    public function setAmountRequested(?int $amountRequested): self
    {
        $this->amountRequested = $amountRequested;
        return $this;
    }

    public function getTotalAmount(): ?int
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(?int $totalAmount): self
    {
        $this->totalAmount = $totalAmount;
        return $this;
    }

    public function getApprovedAmount(): ?int
    {
        return $this->approvedAmount;
    }

    public function setApprovedAmount(?int $approvedAmount): self
    {
        $this->approvedAmount = $approvedAmount;
        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): GrantProject
    {
        $this->currency = $currency;
        return $this;
    }

    public function getInstallment1At(): ?\DateTimeImmutable
    {
        return $this->installment1At;
    }

    public function setInstallment1At(?\DateTimeImmutable $installment1At): self
    {
        $this->installment1At = $installment1At;
        return $this;
    }

    public function getInstallment1Amount(): ?int
    {
        return $this->installment1Amount;
    }

    public function setInstallment1Amount(?int $installment1Amount): self
    {
        $this->installment1Amount = $installment1Amount;
        return $this;
    }

    public function getInstallment1Fund(): ?string
    {
        return $this->installment1Fund;
    }

    public function setInstallment1Fund(?string $installment1Fund): self
    {
        $this->installment1Fund = $installment1Fund;
        return $this;
    }

    public function getInstallment1FundCode(): ?string
    {
        return $this->installment1FundCode;
    }

    public function setInstallment1FundCode(?string $installment1FundCode): self
    {
        $this->installment1FundCode = $installment1FundCode;
        return $this;
    }

    public function getPayment1At(): ?\DateTimeImmutable
    {
        return $this->payment1At;
    }

    public function setPayment1At(?\DateTimeImmutable $payment1At): self
    {
        $this->payment1At = $payment1At;
        return $this;
    }

    public function getPayment1Amount(): ?int
    {
        return $this->payment1Amount;
    }

    public function setPayment1Amount(?int $payment1Amount): self
    {
        $this->payment1Amount = $payment1Amount;
        return $this;
    }

    public function getInstallment2At(): ?\DateTimeImmutable
    {
        return $this->installment2At;
    }

    public function setInstallment2At(?\DateTimeImmutable $installment2At): self
    {
        $this->installment2At = $installment2At;
        return $this;
    }

    public function getInstallment2Amount(): ?int
    {
        return $this->installment2Amount;
    }

    public function setInstallment2Amount(?int $installment2Amount): self
    {
        $this->installment2Amount = $installment2Amount;
        return $this;
    }

    public function getInstallment2Fund(): ?string
    {
        return $this->installment2Fund;
    }

    public function setInstallment2Fund(?string $installment2Fund): self
    {
        $this->installment2Fund = $installment2Fund;
        return $this;
    }

    public function getInstallment2FundCode(): ?string
    {
        return $this->installment2FundCode;
    }

    public function setInstallment2FundCode(?string $installment2FundCode): self
    {
        $this->installment2FundCode = $installment2FundCode;
        return $this;
    }

    public function getPayment2At(): ?\DateTimeImmutable
    {
        return $this->payment2At;
    }

    public function setPayment2At(?\DateTimeImmutable $payment2At): self
    {
        $this->payment2At = $payment2At;
        return $this;
    }

    public function getPayment2Amount(): ?int
    {
        return $this->payment2Amount;
    }

    public function setPayment2Amount(?int $payment2Amount): self
    {
        $this->payment2Amount = $payment2Amount;
        return $this;
    }

    public function getInstallment3At(): ?\DateTimeImmutable
    {
        return $this->installment3At;
    }

    public function setInstallment3At(?\DateTimeImmutable $installment3At): self
    {
        $this->installment3At = $installment3At;
        return $this;
    }

    public function getInstallment3Amount(): ?int
    {
        return $this->installment3Amount;
    }

    public function setInstallment3Amount(?int $installment3Amount): self
    {
        $this->installment3Amount = $installment3Amount;
        return $this;
    }

    public function getInstallment3Fund(): ?string
    {
        return $this->installment3Fund;
    }

    public function setInstallment3Fund(?string $installment3Fund): self
    {
        $this->installment3Fund = $installment3Fund;
        return $this;
    }

    public function getInstallment3FundCode(): ?string
    {
        return $this->installment3FundCode;
    }

    public function setInstallment3FundCode(?string $installment3FundCode): self
    {
        $this->installment3FundCode = $installment3FundCode;
        return $this;
    }

    public function getPayment3At(): ?\DateTimeImmutable
    {
        return $this->payment3At;
    }

    public function setPayment3At(?\DateTimeImmutable $payment3At): self
    {
        $this->payment3At = $payment3At;
        return $this;
    }

    public function getPayment3Amount(): ?int
    {
        return $this->payment3Amount;
    }

    public function setPayment3Amount(?int $payment3Amount): self
    {
        $this->payment3Amount = $payment3Amount;
        return $this;
    }

    public function getInstallment4At(): ?\DateTimeImmutable
    {
        return $this->installment4At;
    }

    public function setInstallment4At(?\DateTimeImmutable $installment4At): self
    {
        $this->installment4At = $installment4At;
        return $this;
    }

    public function getInstallment4Amount(): ?int
    {
        return $this->installment4Amount;
    }

    public function setInstallment4Amount(?int $installment4Amount): self
    {
        $this->installment4Amount = $installment4Amount;
        return $this;
    }

    public function getInstallment4Fund(): ?string
    {
        return $this->installment4Fund;
    }

    public function setInstallment4Fund(?string $installment4Fund): self
    {
        $this->installment4Fund = $installment4Fund;
        return $this;
    }

    public function getInstallment4FundCode(): ?string
    {
        return $this->installment4FundCode;
    }

    public function setInstallment4FundCode(?string $installment4FundCode): self
    {
        $this->installment4FundCode = $installment4FundCode;
        return $this;
    }

    public function getPayment4At(): ?\DateTimeImmutable
    {
        return $this->payment4At;
    }

    public function setPayment4At(?\DateTimeImmutable $payment4At): self
    {
        $this->payment4At = $payment4At;
        return $this;
    }

    public function getPayment4Amount(): ?int
    {
        return $this->payment4Amount;
    }

    public function setPayment4Amount(?int $payment4Amount): self
    {
        $this->payment4Amount = $payment4Amount;
        return $this;
    }

    public function getInstallment5At(): ?\DateTimeImmutable
    {
        return $this->installment5At;
    }

    public function setInstallment5At(?\DateTimeImmutable $installment5At): self
    {
        $this->installment5At = $installment5At;
        return $this;
    }

    public function getInstallment5Amount(): ?int
    {
        return $this->installment5Amount;
    }

    public function setInstallment5Amount(?int $installment5Amount): self
    {
        $this->installment5Amount = $installment5Amount;
        return $this;
    }

    public function getInstallment5Fund(): ?string
    {
        return $this->installment5Fund;
    }

    public function setInstallment5Fund(?string $installment5Fund): self
    {
        $this->installment5Fund = $installment5Fund;
        return $this;
    }

    public function getInstallment5FundCode(): ?string
    {
        return $this->installment5FundCode;
    }

    public function setInstallment5FundCode(?string $installment5FundCode): self
    {
        $this->installment5FundCode = $installment5FundCode;
        return $this;
    }

    public function getPayment5At(): ?\DateTimeImmutable
    {
        return $this->payment5At;
    }

    public function setPayment5At(?\DateTimeImmutable $payment5At): self
    {
        $this->payment5At = $payment5At;
        return $this;
    }

    public function getPayment5Amount(): ?int
    {
        return $this->payment5Amount;
    }

    public function setPayment5Amount(?int $payment5Amount): self
    {
        $this->payment5Amount = $payment5Amount;
        return $this;
    }

    public function getMessage1Type(): ?string
    {
        return $this->message1Type;
    }

    public function setMessage1Type(?string $message1Type): self
    {
        $this->message1Type = $message1Type;
        return $this;
    }

    public function getMessage1DueAt(): ?\DateTimeImmutable
    {
        return $this->message1DueAt;
    }

    public function setMessage1DueAt(?\DateTimeImmutable $message1DueAt): self
    {
        $this->message1DueAt = $message1DueAt;
        return $this;
    }

    public function getMessage1SubmittedAt(): ?\DateTimeImmutable
    {
        return $this->message1SubmittedAt;
    }

    public function setMessage1SubmittedAt(?\DateTimeImmutable $message1SubmittedAt): self
    {
        $this->message1SubmittedAt = $message1SubmittedAt;
        return $this;
    }

    public function getMessage1ApprovedAt(): ?\DateTimeImmutable
    {
        return $this->message1ApprovedAt;
    }

    public function setMessage1ApprovedAt(?\DateTimeImmutable $message1ApprovedAt): self
    {
        $this->message1ApprovedAt = $message1ApprovedAt;
        return $this;
    }

    public function getMessage2Type(): ?string
    {
        return $this->message2Type;
    }

    public function setMessage2Type(?string $message2Type): self
    {
        $this->message2Type = $message2Type;
        return $this;
    }

    public function getMessage2DueAt(): ?\DateTimeImmutable
    {
        return $this->message2DueAt;
    }

    public function setMessage2DueAt(?\DateTimeImmutable $message2DueAt): self
    {
        $this->message2DueAt = $message2DueAt;
        return $this;
    }

    public function getMessage2SubmittedAt(): ?\DateTimeImmutable
    {
        return $this->message2SubmittedAt;
    }

    public function setMessage2SubmittedAt(?\DateTimeImmutable $message2SubmittedAt): self
    {
        $this->message2SubmittedAt = $message2SubmittedAt;
        return $this;
    }

    public function getMessage2ApprovedAt(): ?\DateTimeImmutable
    {
        return $this->message2ApprovedAt;
    }

    public function setMessage2ApprovedAt(?\DateTimeImmutable $message2ApprovedAt): self
    {
        $this->message2ApprovedAt = $message2ApprovedAt;
        return $this;
    }

    public function getMessage3Type(): ?string
    {
        return $this->message3Type;
    }

    public function setMessage3Type(?string $message3Type): self
    {
        $this->message3Type = $message3Type;
        return $this;
    }

    public function getMessage3DueAt(): ?\DateTimeImmutable
    {
        return $this->message3DueAt;
    }

    public function setMessage3DueAt(?\DateTimeImmutable $message3DueAt): self
    {
        $this->message3DueAt = $message3DueAt;
        return $this;
    }

    public function getMessage3SubmittedAt(): ?\DateTimeImmutable
    {
        return $this->message3SubmittedAt;
    }

    public function setMessage3SubmittedAt(?\DateTimeImmutable $message3SubmittedAt): self
    {
        $this->message3SubmittedAt = $message3SubmittedAt;
        return $this;
    }

    public function getMessage3ApprovedAt(): ?\DateTimeImmutable
    {
        return $this->message3ApprovedAt;
    }

    public function setMessage3ApprovedAt(?\DateTimeImmutable $message3ApprovedAt): self
    {
        $this->message3ApprovedAt = $message3ApprovedAt;
        return $this;
    }

    public function getMessage4Type(): ?string
    {
        return $this->message4Type;
    }

    public function setMessage4Type(?string $message4Type): self
    {
        $this->message4Type = $message4Type;
        return $this;
    }

    public function getMessage4DueAt(): ?\DateTimeImmutable
    {
        return $this->message4DueAt;
    }

    public function setMessage4DueAt(?\DateTimeImmutable $message4DueAt): self
    {
        $this->message4DueAt = $message4DueAt;
        return $this;
    }

    public function getMessage4SubmittedAt(): ?\DateTimeImmutable
    {
        return $this->message4SubmittedAt;
    }

    public function setMessage4SubmittedAt(?\DateTimeImmutable $message4SubmittedAt): self
    {
        $this->message4SubmittedAt = $message4SubmittedAt;
        return $this;
    }

    public function getMessage4ApprovedAt(): ?\DateTimeImmutable
    {
        return $this->message4ApprovedAt;
    }

    public function setMessage4ApprovedAt(?\DateTimeImmutable $message4ApprovedAt): self
    {
        $this->message4ApprovedAt = $message4ApprovedAt;
        return $this;
    }

    public function getMessage5Type(): ?string
    {
        return $this->message5Type;
    }

    public function setMessage5Type(?string $message5Type): GrantProject
    {
        $this->message5Type = $message5Type;
        return $this;
    }

    public function getMessage5DueAt(): ?\DateTimeImmutable
    {
        return $this->message5DueAt;
    }

    public function setMessage5DueAt(?\DateTimeImmutable $message5DueAt): GrantProject
    {
        $this->message5DueAt = $message5DueAt;
        return $this;
    }

    public function getMessage5SubmittedAt(): ?\DateTimeImmutable
    {
        return $this->message5SubmittedAt;
    }

    public function setMessage5SubmittedAt(?\DateTimeImmutable $message5SubmittedAt): GrantProject
    {
        $this->message5SubmittedAt = $message5SubmittedAt;
        return $this;
    }

    public function getMessage5ApprovedAt(): ?\DateTimeImmutable
    {
        return $this->message5ApprovedAt;
    }

    public function setMessage5ApprovedAt(?\DateTimeImmutable $message5ApprovedAt): GrantProject
    {
        $this->message5ApprovedAt = $message5ApprovedAt;
        return $this;
    }

    #[Groups(groups: ['grant_project:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }

    #[Groups(groups: ['grant_project:read'])]
    public function getFocusName(): ?string
    {
        return $this->translate()->getFocus();
    }

    public function getCovers(): array
    {
        return $this->covers;
    }

    public function setCovers(array $covers): self
    {
        $this->covers = $covers;
        return $this;
    }

    #[Groups(groups: ['grant_project:read'])]
    public function getFirstCoverUrl(): ?string
    {
        if (empty($this->covers)) {
            return null;
        }

        $cover = $this->covers[0];

        if (empty($cover['contentUrl'])) {
            return null;
        }

        return $cover['contentUrl'];
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    public function getApplicantType(): ?GrantApplicantType
    {
        return $this->applicantType;
    }

    public function setApplicantType(?GrantApplicantType $applicantType): self
    {
        $this->applicantType = $applicantType;
        return $this;
    }

    public function getFocusType(): ?GrantFocusType
    {
        return $this->focusType;
    }

    public function setFocusType(?GrantFocusType $focusType): self
    {
        $this->focusType = $focusType;
        return $this;
    }

    public function getProjectType(): ?GrantProjectType
    {
        return $this->projectType;
    }

    public function setProjectType(?GrantProjectType $projectType): self
    {
        $this->projectType = $projectType;
        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;
        return $this;
    }

    public function getGrantProjectGroups(): array
    {
        return $this->grantProjectGroups->toArray();
    }

    public function addGrantProjectGroup(GrantProjectGroup $grantProjectGroup): self
    {
        if (!$this->grantProjectGroups->contains($grantProjectGroup)) {
            $this->grantProjectGroups->add($grantProjectGroup);
        }

        return $this;
    }

    public function removeGrantProjectGroup(GrantProjectGroup $grantProjectGroup): self
    {
        $this->grantProjectGroups->removeElement($grantProjectGroup);

        return $this;
    }
}
