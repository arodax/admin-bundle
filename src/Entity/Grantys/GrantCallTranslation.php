<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use Arodax\AdminBundle\Entity\TranslationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\Table(name: 'aa_grant_call_translation')]
class GrantCallTranslation
{
    use TranslationTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected $id;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected $locale;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $lead = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $description = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $rules = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $applicationInstructions = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $submissionInstructions = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $link = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $linkText = null;

    #[ORM\Column(type: 'string', length: 191, nullable: true)]
    #[Groups(groups: ['grant_call:read', 'grant_call:write'])]
    protected ?string $applicationDeadline = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getLead(): ?string
    {
        return $this->lead;
    }

    public function setLead(?string $lead): self
    {
        $this->lead = $lead;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getRules(): ?string
    {
        return $this->rules;
    }

    public function setRules(?string $rules): void
    {
        $this->rules = $rules;
    }

    /**
     * @return string|null
     */
    public function getApplicationInstructions(): ?string
    {
        return $this->applicationInstructions;
    }

    /**
     * @param string|null $applicationInstructions
     * @return GrantCallTranslation
     */
    public function setApplicationInstructions(?string $applicationInstructions): self
    {
        $this->applicationInstructions = $applicationInstructions;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubmissionInstructions(): ?string
    {
        return $this->submissionInstructions;
    }

    /**
     * @param string|null $submissionInstructions
     * @return GrantCallTranslation
     */
    public function setSubmissionInstructions(?string $submissionInstructions): self
    {
        $this->submissionInstructions = $submissionInstructions;
        return $this;
    }


    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getLinkText(): ?string
    {
        return $this->linkText;
    }

    public function setLinkText(?string $linkText): self
    {
        $this->linkText = $linkText;

        return $this;
    }

    public function getApplicationDeadline(): ?string
    {
        return $this->applicationDeadline;
    }

    public function setApplicationDeadline(?string $applicationDeadline): self
    {
        $this->applicationDeadline = $applicationDeadline;
        return $this;
    }
}
