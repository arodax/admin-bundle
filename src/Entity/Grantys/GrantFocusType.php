<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * This entity represent "Grantys" grant focus.
 * @method GrantFocusTypeTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [new Get(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Put(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Delete(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new GetCollection(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Post(securityPostDenormalize: 'is_granted(\'ROLE_GRANTYS_ADMIN\')')], normalizationContext: ['groups' => ['grant_focus_type:read']], denormalizationContext: ['groups' => ['grant_focus_type:write']])]
#[ORM\Entity()]
#[ORM\Table(name: 'aa_grant_focus_type')]
class GrantFocusType implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_focus_type', 'grant_call:read', 'grant_project:read'])]
    protected ?int $id = null;

    /*
     * Array containing all entity translations already persisted into the entity.
     */
    #[Groups(groups: ['grant_focus_type:read'])]
    protected $translations;

    /**
     * @var array
     */
    #[Groups(groups: ['grant_focus_type:write'])]
    protected $newTranslations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    #[Groups(groups: ['grant_focus_type:read', 'grant_project:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }
}
