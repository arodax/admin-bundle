<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Entity\Grantys;

use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiFilter;
use Arodax\AdminBundle\Entity\TimestampTrait;
use Arodax\AdminBundle\Entity\TranslatableTrait;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * This entity represent "Grantys" grant applicant type.
 * @method GrantApplicantTypeTranslation translate($locale = null, $fallbackToDefault = true)
 */
#[ApiResource(operations: [new Get(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Put(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Delete(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new GetCollection(security: 'is_granted(\'ROLE_GRANTYS_ADMIN\')'), new Post(securityPostDenormalize: 'is_granted(\'ROLE_GRANTYS_ADMIN\')')], normalizationContext: ['groups' => ['grant_project:read']], denormalizationContext: ['groups' => ['grant_project:write']])]
#[ORM\Entity()]
#[ORM\Table(name: 'aa_grant_applicant_type')]
class GrantApplicantType implements TranslatableInterface
{
    use TimestampTrait;
    use TranslatableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(groups: ['grant_project:read', 'grant_project:read', 'grant_call:read'])]
    protected ?int $id = null;

    /** @var Collection<int, GrantProject>  */
    #[ORM\ManyToMany(targetEntity: GrantProject::class, mappedBy: 'grantProjects', fetch: 'EXTRA_LAZY')]
    #[Groups(groups: ['grant_project:read'])]
    protected Collection $grantProjects;

    /**
     * @var array
     */
    #[Groups(groups: ['grant_project:write'])]
    protected $newTranslations;

    public function __construct()
    {
        $this->grantProjects = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    #[Groups(groups: ['grant_project:read'])]
    public function getHeadline(): ?string
    {
        return $this->translate()->getName();
    }
}
