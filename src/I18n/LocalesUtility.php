<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Intl\Locales;

/**
 * Provides various methods for I18n.
 */
class LocalesUtility implements LocalesUtilityInterface
{

    /**
     * @param array $contentLocales list of locales for the content
     */
    public function __construct(
        #[Autowire(param: 'arodax_admin.content_locales')]
        private readonly array $contentLocales = ['en']
    ) {
    }

    /**
     * Get all locales.
     */
    public function all(): array
    {
        return Locales::getLocales();
    }

    /**
     * Get all locales matching an expression.
     *
     * @param string|null $expression regular expression for matching locales codes
     *
     * @return array of matching regular expression, empty when not matched or missing expression
     */
    public function match(string $expression = null): array
    {
        if ($expression && preg_match_all('/'.$expression.'/', implode('', Locales::getLocales()), $matches) && \is_array($matches[0])) {
            return array_values(array_unique($matches[0]));
        }

        return [];
    }

    /**
     * Get all avaible locales for the content.
     */
    public function getContentLocales(): array
    {
        $locales = [];

        foreach ($this->contentLocales as $locale) {
            $locales[$locale]['code'] = $locale;
            $locales[$locale]['name'] = Locales::getName($locale);
        }

        return $locales;
    }
}
