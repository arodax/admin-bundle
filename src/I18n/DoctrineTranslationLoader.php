<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

use Arodax\AdminBundle\Entity\Localization\Translatable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;

class DoctrineTranslationLoader implements LoaderInterface
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param mixed  $resource
     *
     */
    public function load($resource, string $locale, string $domain = 'messages'): MessageCatalogue
    {
        $i18ns = $this->entityManager->getRepository(Translatable::class)->findByLocale($locale);
        $translations = [];

        /** @var Translatable $i18n */
        foreach ($i18ns as $i18n) {
            $translations[$i18n->getSource()] = $i18n->translate($locale, false)->getTarget();
        }

        return new MessageCatalogue($locale, ['messages' => $translations]);
    }
}
