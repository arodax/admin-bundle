<?php

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\Extractor\ExtractorInterface;
use Symfony\Component\Translation\MessageCatalogue;

final class TranslationsExtractor implements ExtractorInterface
{
    public function __construct(
        #[Autowire('arodax_admin.bundle_dir')]
        private readonly string $bundleDir
    ) {
    }

    private string $prefix = 'arodax_admin_';

    public function extract(iterable|string $resource, MessageCatalogue $catalogue): void
    {
        $messages = $this->getMessages();

        foreach ($messages as $message) {
            $catalogue->set($message, $message, 'arodax_admin');
        }
    }

    public function setPrefix(string $prefix): void
    {
    }

    private function getMessages(): array
    {
        $messages = [];

        $fs = new Filesystem();

        if ($fs->exists(dirname($this->bundleDir) . '/assets/vue')) {
            $finder = new Finder();
            $finder = $finder
                ->files()
                ->in(dirname($this->bundleDir) . '/assets/vue')
                ->ignoreUnreadableDirs()
                ->name('*.vue');

            foreach ($finder as $file) {
                if (preg_match('/\$t\(\'(.*)\'\)/', $file->getContents(), $matches) && isset($matches[1])) {
                    $messages[] = $matches[1];
                }
            }
        }

        return $messages;
    }
}
