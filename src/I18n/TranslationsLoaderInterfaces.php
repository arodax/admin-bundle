<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

/**
 * Extract translations to array.
 */
interface TranslationsLoaderInterfaces
{
    /**
     * @param string $locale Requested locale
     * @return array
     */
    public function loadTranslations(string $locale): array;
}
