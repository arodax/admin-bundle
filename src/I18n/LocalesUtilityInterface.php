<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

interface LocalesUtilityInterface
{
    /**
     * Return  all locales.
     */
    public function all(): array;

    /**
     * Return locales matching an expression.
     */
    public function match(string $expression): array;

    /**
     * Get all avaible locales for the content.
     */
    public function getContentLocales(): array;
}
