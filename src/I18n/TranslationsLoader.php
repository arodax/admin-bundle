<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\I18n;

use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

readonly class TranslationsLoader implements TranslationsLoaderInterfaces
{
    public function __construct(
        private TranslatorInterface $translator
    ) {
    }

    /**
     * Return translations as array.
     *
     * @param string|null $locale Requested locale for translations
     */
    public function loadTranslations(string $locale = null): array
    {
        $this->translator->setLocale($locale);

        $catalogue = $this->translator->getCatalogue()->all();

        if ($this->translator->getCatalogue()->getFallbackCatalogue() instanceof MessageCatalogueInterface) {
            $catalogue = $this->translator->getCatalogue()->getFallbackCatalogue()->all();
        }

        return $catalogue;
    }
}
