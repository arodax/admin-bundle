<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Normalizer;

use ApiPlatform\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final readonly class NestedEntityNormalizer
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private NormalizerInterface $normalizer,
        private IriConverterInterface $iriConverter
    ) {
    }

    /**
     * Converts the nested entity into the tree array.
     *
     * @param $object
     * @param array $context
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, array $context = []): array
    {
        $nodes = [];

        $repository = $this->entityManager->getRepository($object::class);

        /** @var Query $query */
        $query = $repository->getNodesHierarchyQuery($object, false, [], true);

        foreach ($query->getResult() as $node) {
            $data = $this->normalizer->normalize($node, null, $context);
            $data['@id'] = $this->iriConverter->getIriFromResource($node);
            $nodes[] = $data;
        }

        $repositoryUtils = $repository->getRepoUtils();
        $repositoryUtils->setChildrenIndex('children');

        return $repositoryUtils->buildTree($nodes);
    }
}
