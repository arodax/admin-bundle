<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Menu;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

class MenuExtension extends \Knp\Menu\Twig\MenuExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('menu_get', $this->get(...)),
            new TwigFunction('menu_render', $this->render(...), ['is_safe' => ['html']]),
            new TwigFunction('menu_get_breadcrumbs_array', $this->getBreadcrumbsArray(...)),
            new TwigFunction('menu_get_current_item', $this->getCurrentItem(...)),
        ];
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('menu_as_string', $this->pathAsString(...)),
        ];
    }

    /**
     * @return array|TwigTest[]
     */
    public function getTests(): array
    {
        return [
            new TwigTest('menu_current', $this->isCurrent(...)),
            new TwigTest('menu_ancestor', $this->isAncestor(...)),
        ];
    }
}
