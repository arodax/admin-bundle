<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Menu;

use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Arodax\AdminBundle\Normalizer\NestedEntityNormalizer;
use Arodax\AdminBundle\Tree\Hydrator\TreeObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Knp\Menu\NodeInterface as MenuNodeInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Twig\Error\Error;

readonly class MenuBuilder
{
    public function __construct(
        private RequestStack $requestStack,
        private FactoryInterface $factory,
        private EntityManagerInterface $entityManger,
        private RouterInterface $router,
        private Security $security,
        private readonly NestedEntityNormalizer $normalizer,
    ) {
    }

    /**
     * This method will try to find a menu from database and return menu.
     *
     * @param array $options
     *
     * @return MenuItemInterface|null
     * @throws Error
     * @throws ExceptionInterface
     */
    public function generator(array $options): ?MenuItemInterface
    {
        if (!isset($options['id']) || !\is_string($options['id'])) {
            throw new Error('Undefined id of the menu, which should be rendered');
        }

        $treeRepository = $this->entityManger->getRepository(MenuItem::class);

        $this->entityManger->getConfiguration()->addCustomHydrationMode(
            'tree',
            TreeObjectHydrator::class
        );

        /** @var MenuNodeInterface $menu */
        $menu = $treeRepository->findOneBy(['nodeName' => $options['id']]);

        // this eagerly loads the whole tree and save 50% db calls
        $this->normalizer->normalize($menu, ['groups' => 'menu_item:tree']);

        if (null === $menu) {
            throw new Error(sprintf('Menu with id %s could not be found ', $options['id']));
        }

        $criteria = [];
        // By default, we load only enabled items. Set this option too false to load disabled items too.
        if (!isset($options['enabled_only']) || filter_var($options['enabled_only'], \FILTER_VALIDATE_BOOLEAN)) {
            $criteria['enabled'] = true;
        }

        $loader = new NodeLoader(
            factory: $this->factory,
            router: $this->router,
            requestStack: $this->requestStack,
            security: $this->security,
            criteria: $criteria);

        return $loader->load($menu);
    }
}
