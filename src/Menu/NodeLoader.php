<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Menu;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\Loader\LoaderInterface;
use Knp\Menu\NodeInterface;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * This class allows to load routes from the nested tree and handles NodeInterface routes.
 */
readonly class NodeLoader implements LoaderInterface
{
    public function __construct(
        private FactoryInterface $factory,
        private RouterInterface $router,
        private RequestStack $requestStack,
        private Security $security,
        private ?array $criteria = []
    ) {
    }

    /**
     * Load the complete tree.
     *
     * @param $data
     * @return ItemInterface
     */
    public function load($data): ItemInterface
    {
        if (!$data instanceof NodeInterface) {
            throw new \InvalidArgumentException(sprintf('Unsupported data. Expected Knp\Menu\NodeInterface but got %s', get_debug_type($data)));
        }

        return $this->loadItem($data);
    }

    /**
     * Load the single item from the tree.
     *
     * @param NodeInterface $data
     * @return ItemInterface
     */
    private function loadItem(NodeInterface $data): ItemInterface
    {
        $routeName = $data->getOptions()['route'] ?: null;
        $options = [
            'extras' => [
                'icon' => $data->getIcon(),
                'id' => 'menu-' . $data->getId()
            ]
        ];

        if ($routeName) {
            $this->configureRouteOptions($data, $routeName, $options);
        }

        $item = $this->factory->createItem($data->getName(), $options);

        /** @var MenuItem $childNode */
        foreach ($data->getChildren() as $childNode) {
            if ($this->shouldAddChild($childNode)) {
                $item->addChild($this->loadItem($childNode));
            }
        }

        return $item;
    }

    /**
     * Configure route options for the menu item.
     *
     * @param NodeInterface $data
     * @param string $routeName
     * @param array $options
     * @return void
     */
    private function configureRouteOptions(NodeInterface $data, string $routeName, array &$options): void
    {
        $currentRequest = $this->requestStack->getCurrentRequest();

        $locale = $this->router->getContext()->getParameter('_locale');

        // if link type is not route use regular link
        if ($data->translate($locale)?->getLinkType() !== 'route') {
            if ($uri = $data->translate($locale)->getLink()) {
                $options['uri'] = $uri;
                return;
            }
        }

        $routeCollection = $this->router->getRouteCollection();
        $route = $routeCollection->get($routeName.'.'.$locale) ?? $routeCollection->get($routeName) ?? null;
        if ($route !== null) {
            $compiledRoute = $route->compile();
            $routeVars = $compiledRoute->getPathVariables();
            $requestVars = $currentRequest->attributes->get('_route_params', []);
            $predefinedParams = $data->translate()->getRouteParams();
            $predefinedParams = $predefinedParams ? array_merge(...$predefinedParams) : [];

            if (([] === array_diff(array_values($routeVars), array_keys($requestVars))) || ([] === array_diff(array_values($routeVars), array_keys($predefinedParams)))) {
                $options['route'] = $routeName;
                $options['routeParameters'] = $routeVars !== [] ? ($predefinedParams !== [] ? $predefinedParams : $requestVars) : null;
            }

            if (\array_key_exists('_fragment', $predefinedParams)) {
                $options['routeParameters']['_fragment'] = $predefinedParams['_fragment'];
            }
        }
    }

    /**
     * Decide if menu item child should be loaded based od security criteria and "enabled" option.
     *
     * @param MenuItem $childNode
     * @return bool
     */
    private function shouldAddChild(MenuItem $childNode): bool
    {
        if (null !== $childNode->getSecurity() && !$this->security->isGranted(new Expression($childNode->getSecurity()))) {
            return false;
        }

        if (isset($this->criteria['enabled']) && !$childNode->isEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * Decode if this loader supports the data.
     *
     * @param $data
     * @return bool
     */
    public function supports($data): bool
    {
        return $data instanceof NodeInterface;
    }
}
