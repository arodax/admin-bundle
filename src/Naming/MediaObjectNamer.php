<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Naming;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use function Symfony\Component\String\u;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

readonly class MediaObjectNamer implements NamerInterface
{
    private const array DANGEROUS_EXTENSIONS = ['php', 'php3', 'php4', 'php5', 'phtml', 'js', 'html', 'htm', 'exe', 'pl', 'cgi', 'htaccess'];

    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * Return the new name for the uploaded file.
     *
     * @param object $object
     *
     * @return string Name of the file
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $file = $mapping->getFile($object);

        if (null === $file) {
            throw new \RuntimeException('There is no file attached to this object.');
        }

        $originalName = $file->getClientOriginalName();
        $originalExtension = strtolower(pathinfo($originalName, \PATHINFO_EXTENSION));

        // If the extension is dangerous, replace it with 'txt'
        if (in_array($originalExtension, self::DANGEROUS_EXTENSIONS, true)) {
            $originalExtension = 'txt';
        }

        $slugger = new AsciiSlugger();

        $baseName = (string) u((string) $slugger->slug(basename($originalName, '.'.$originalExtension)))->lower();
        $newName = sprintf('%s.%s', $baseName, $originalExtension);

        $mediaObjectsCount = $this->entityManager->getRepository(MediaObject::class)->count(['originalFileName' => $originalName]);

        if ($mediaObjectsCount > 0) {
            return sprintf('%s-%s.%s', $baseName, $mediaObjectsCount, $originalExtension);
        }

        return $newName;
    }
}
