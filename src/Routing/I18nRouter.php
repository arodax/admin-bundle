<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Routing;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class I18nRouter extends Router
{
    public function __construct(
        ContainerInterface $container,
        string $resource,
        array $options, RequestContext $context,
        ContainerInterface $parameters,
        LoggerInterface $logger,
        string $defaultLocale,
        private readonly TranslatedRoutesDecorator $i18nLoader
    ) {
        parent::__construct($container, $resource, $options, $context, $parameters, $logger, $defaultLocale);
    }

    /**
     * Get RouteCollection from the parent router and decorates it with I18n loader.
     */
    public function getRouteCollection(): RouteCollection
    {
        $collection = parent::getRouteCollection();

        return $this->i18nLoader->load($collection);
    }
}
