<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class RouteVoter extends Voter
{
    final public const string ROUTE_OPTION_EXPOSE = 'expose';

    /**
     * Decide if this voter is voter for routes.
     *
     * @param string $attribute
     * @param mixed  $subject
     */
    protected function supports($attribute, $subject): bool
    {
        return $subject instanceof Route;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed  $subject
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::ROUTE_OPTION_EXPOSE => $this->isExposed($subject),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /**
     * Check if the route is exposed.
     *
     * This options is usually used to determine routes which might be used for genering
     * sitemaps, or passed to javascript generator.
     *
     * @param Route $route a route to check
     */
    private function isExposed(Route $route): bool
    {
        if ($route->hasOption(self::ROUTE_OPTION_EXPOSE)) {
            return (bool) $route->getOption(self::ROUTE_OPTION_EXPOSE);
        }

        return false;
    }
}
