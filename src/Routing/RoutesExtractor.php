<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Routing;

use Symfony\Component\Security\Http\Attribute\IsGranted;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class RoutesExtractor implements RoutesExtractorInterface
{
    /**
     * ExposedRoutesExtractor constructor.
     */
    public function __construct(private readonly RouterInterface $router, private readonly AccessDecisionManagerInterface $accessDecisionManager, private readonly TokenStorageInterface $tokenStorage)
    {
    }

    /**
     * Get all exposed routes as collection.
     */
    public function getRoutes(): RouteCollection
    {
        $collection = $this->router->getRouteCollection();
        $routes = new RouteCollection();

        // @var Route
        foreach ($collection->all() as $name => $route) {
            if ($this->isRouteExposed($route)) {
                $routes->add($name, $route);
            }
        }

        return $routes;
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseUrl()
    {
        return $this->router->getContext()->getBaseUrl() ?: '';
    }

    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
        $requestContext = $this->router->getContext();

        $host = $requestContext->getHost();

        if ($this->usesNonStandardPort()) {
            $method = sprintf('get%sPort', ucfirst($requestContext->getScheme()));
            $host .= ':'.$requestContext->$method();
        }

        return $host;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheme(): string
    {
        return $this->router->getContext()->getScheme();
    }

    /**
     * {@inheritdoc}
     */
    public function getResources(): array
    {
        return $this->router->getRouteCollection()->getResources();
    }

    /**
     * Decide whether is route expose or not.
     *
     * @internal
     */
    private function isRouteExposed(Route $route): bool
    {
        if (true === $route->getOption('expose') || 'true' === $route->getOption('expose')) {
            $defaults = $route->getDefaults();
            if (isset($defaults['_controller'])) {
                [$controllerName, $methodName] = explode('::', (string) $defaults['_controller']);
                if ($controllerName && $methodName) {
                    try {
                        $reflectionMethod = new \ReflectionMethod($controllerName, $methodName);

                        if ($reflectionMethod) {
                            /**
                             * In case of use of Symfony\Component\Security\Http\Attribute\IsGranted attribute
                             * we also check, if this route is exposed to the current user token.
                             */
                            $annotationReader = new AnnotationReader();

                            $annotation = $annotationReader->getMethodAnnotation(
                                $reflectionMethod,
                                IsGranted::class
                            );
                            if ($annotation !== null) {
                                return $this->accessDecisionManager->decide(
                                    $this->tokenStorage->getToken(),
                                    [$annotation->getExpression()]
                                );
                            }
                        }
                    } catch (\ReflectionException|AnnotationException) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Check whether server is serving this request from a non-standard port.
     */
    private function usesNonStandardPort(): bool
    {
        return $this->usesNonStandardHttpPort() || $this->usesNonStandardHttpsPort();
    }

    /**
     * Check whether server is serving HTTP over a non-standard port.
     */
    private function usesNonStandardHttpPort(): bool
    {
        return 'http' === $this->getScheme() && '80' != $this->router->getContext()->getHttpPort();
    }

    /**
     * Check whether server is serving HTTPS over a non-standard port.
     */
    private function usesNonStandardHttpsPort(): bool
    {
        return 'https' === $this->getScheme() && '443' != $this->router->getContext()->getHttpsPort();
    }
}
