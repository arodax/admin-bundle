<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Routing;

use Symfony\Component\Config\Resource\ResourceInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Extract routes to array.
 *
 * @param string $locale Requested locale
 */
interface RoutesExtractorInterface
{
    /**
     * Returns a collection of exposed routes.
     *
     * @return RouteCollection
     */
    public function getRoutes();

    /**
     * Return the Base URL.
     *
     * @return string
     */
    public function getBaseUrl();

    /**
     * Get the host and applicable port from RequestContext.
     *
     * @return string
     */
    public function getHost();

    /**
     * Get the scheme from RequestContext.
     *
     * @return string
     */
    public function getScheme();

    /**
     * Return an array of routing resources.
     *
     * @return ResourceInterface[]
     */
    public function getResources();
}
