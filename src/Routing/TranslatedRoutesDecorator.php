<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Routing;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * This class translate routes with Translator.
 */
class TranslatedRoutesDecorator
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        /**
         * @var string domain used for routes translations, default is routes
         */
        private readonly string $translationsDomain = 'routes'
    )
    {
    }

    /**
     * Load RouteCollection and decorates if with translated routes.
     *
     * @param RouteCollection $collection collection with routes for decoration
     *
     * @return RouteCollection decorated routes
     */
    public function load(RouteCollection $collection): RouteCollection
    {
        $i18nCollection = new RouteCollection();

        foreach ($collection->getResources() as $resource) {
            $i18nCollection->addResource($resource);
        }

        foreach ($collection->all() as $name => $route) {
            // check if this route is available for selected locale
            if ($route->hasRequirement('_locale') && $route->hasDefault('_locale') && !preg_match('/'.$route->getRequirement('_locale').'/', (string) $route->getDefault('_locale'))) {
                $collection->remove($name);
                continue;
            }

            // check if this route has available translations
            $translatedPath = $this->translator->trans($route->getDefault('_canonical_route'), [], $this->translationsDomain, $route->getDefault('_locale'));
            if ($route->getDefault('_canonical_route') === $translatedPath || '' === $translatedPath) {
                continue;
            }

            $route->setPath($translatedPath);
        }

        return $collection;
    }
}
