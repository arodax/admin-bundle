<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Geography;

enum Districts : string
{
    case BN = 'Benešov';
    case BE = 'Beroun';
    case BK = 'Blansko';
    case BM = 'Brno-město';
    case BI = 'Brno-venkov';
    case BR = 'Bruntál';
    case BV = 'Břeclav';
    case CL = 'Česká Lípa';
    case CB = 'České Budějovice';
    case CK = 'Český Krumlov';
    case DC = 'Děčín';
    case DO = 'Domažlice';
    case FM = 'Frýdek-Místek';
    case HB = 'Havlíčkův Brod';
    case HO = 'Hodonín';
    case HK = 'Hradec Králové';
    case CH = 'Cheb';
    case CV = 'Chomutov';
    case CR = 'Chrudim';
    case JN = 'Jablonec nad Nisou';
    case JE = 'Jeseník';
    case JC = 'Jičín';
    case JI = 'Jihlava';
    case JH = 'Jindřichův Hradec';
    case KV = 'Karlovy Vary';
    case KI = 'Karviná';
    case KD = 'Kladno';
    case KT = 'Klatovy';
    case KO = 'Kolín';
    case KM = 'Kroměříž';
    case KH = 'Kutná Hora';
    case LI = 'Liberec';
    case LT = 'Litoměřice';
    case LN = 'Louny';
    case ME = 'Mělník';
    case MB = 'Mladá Boleslav';
    case MO = 'Most';
    case NA = 'Náchod';
    case NJ = 'Nový Jičín';
    case NB = 'Nymburk';
    case OC = 'Olomouc';
    case OP = 'Opava';
    case OV = 'Ostrava-město';
    case PU = 'Pardubice';
    case PE = 'Pelhřimov';
    case PI = 'Písek';
    case PJ = 'Plzeň-jih';
    case PS = 'Plzeň-sever';
    case PY = 'Praha-východ';
    case PZ = 'Praha-západ';
    case PT = 'Prachatice';
    case PV = 'Prostějov';
    case PR = 'Přerov';
    case PB = 'Příbram';
    case RA = 'Rakovník';
    case RO = 'Rokycany';
    case RK = 'Rychnov nad Kněžnou';
    case SM = 'Semily';
    case SO = 'Sokolov';
    case ST = 'Strakonice';
    case SY = 'Svitavy';
    case SU = 'Šumperk';
    case TA = 'Tábor';
    case TC = 'Tachov';
    case TP = 'Teplice';
    case TU = 'Trutnov';
    case TR = 'Třebíč';
    case UH = 'Uherské Hradiště';
    case UL = 'Ústní nad Labem';
    case UO = 'Ústí nad Orlicí';
    case VS = 'Vsetín';
    case VY = 'Vyškov';
    case ZL = 'Zlín';
    case ZN = 'Znojmo';
    case ZR = 'Žďár nad Sázavou';
}
