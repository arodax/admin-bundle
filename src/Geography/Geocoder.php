<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Geography;


use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly final class Geocoder
{
    public function __construct(
        private HttpClientInterface $httpClient,
        #[Autowire(env: 'ARODAX_ADMIN_GOOGLE_MAPS_API_KEY')]
        private string $apiKey = ''
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function geocode(string $address): array
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . $this->apiKey;

        $response = $this->httpClient->request(Request::METHOD_GET, $url);

        return json_decode($response->getContent(), true);
    }

    /**
     * Get district enum from the district name
     *
     * @param string $districtName
     * @return Districts|null
     */
    public function getDistrictCode(string $districtName): ?Districts
    {
        try {
            return Districts::from($districtName);
        } catch (\UnexpectedValueException $e) {
            return null;
        }
    }

}
