<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\DataFixtures;

use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MenuFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $repository = $manager->getRepository(MenuItem::class);

        $rootMenuItem = new MenuItem();
        $rootMenuItem->setEnabled(true);
        $rootMenuItem->setNodeName('menu_root');
        $rootMenuItem->setReadOnly(true);

        $repository->persistAsFirstChild($rootMenuItem);

        $manager->flush();
    }
}
