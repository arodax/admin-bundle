<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\ValueObject;

class BankTransferPayment
{
    public function __construct(
        private ?\DateTimeInterface $transactionAt = null,
        private ?string $accountNumber = null,
        private ?int $amount = null,
        private readonly ?string $variableNumber = null,
    ) {
    }

    public function getTransactionAt(): ?\DateTimeInterface
    {
        return $this->transactionAt;
    }

    public function getAccountNumber(): ?string
    {
        return $this->accountNumber;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function getVariableNumber(): ?string
    {
        return $this->variableNumber;
    }
}
