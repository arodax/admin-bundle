<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\ValueObject;

use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\Entity\User\User;

final readonly class UserGroupUser
{
    public function __construct(
        public UserGroup $userGroup,
        public User $user
    ) {
    }
}
