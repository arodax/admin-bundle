<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Reflection;

class ClassAnalyzer
{
    /**
     * Return true if the given object use the given trait, false if not.
     */
    public function hasTrait(\ReflectionClass $class, string $traitName, bool $isRecursive = false): bool
    {
        if (\in_array($traitName, $class->getTraitNames())) {
            return true;
        }

        $parentClass = $class->getParentClass();

        if ((false === $isRecursive) || (false === $parentClass) || (null === $parentClass)) {
            return false;
        }

        return $this->hasTrait($parentClass, $traitName, $isRecursive);
    }

    /**
     * Return true if the given object has the given method, false if not.
     */
    public function hasMethod(\ReflectionClass $class, string $methodName): bool
    {
        return $class->hasMethod($methodName);
    }

    /**
     * Return true if the given object has the given property, false if not.
     */
    public function hasProperty(\ReflectionClass $class, string $propertyName): bool
    {
        if ($class->hasProperty($propertyName)) {
            return true;
        }

        $parentClass = $class->getParentClass();

        if (false === $parentClass) {
            return false;
        }

        return $this->hasProperty($parentClass, $propertyName);
    }
}
