<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Monolog;

use Monolog\LogRecord;
use Symfony\Component\HttpFoundation\RequestStack;

final class ClientIpProcessor
{
    private ?string $cachedClientIp = null;

    public function __construct(
        private readonly RequestStack $requestStack
    ) {
    }

    public function __invoke(LogRecord $record): LogRecord
    {
        if (php_sapi_name() === 'cli') {
            return $record;
        }

        $this->setClientIp();

        $record->extra['request_ip'] = $this->requestStack->getCurrentRequest() !== null ? $this->requestStack->getCurrentRequest()->getClientIp() : 'unavailable';
        $record->extra['client_ip'] = $this->cachedClientIp ?: 'unavailable';

        return $record;
    }

    private function setClientIp(): void
    {
        if ($this->cachedClientIp !== 'unavailable') {
            return;
        }

        $request = $this->requestStack->getCurrentRequest();

        if ($request !== null) {
            $this->cachedClientIp = $request->getClientIp();
        }
    }
}
