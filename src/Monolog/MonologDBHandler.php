<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Monolog;

use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\Log\Log;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;

final class MonologDBHandler extends AbstractProcessingHandler
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Security $security)
    {
        parent::__construct();
    }

    public function write(LogRecord $record): void
    {
        try {
            $this->entityManager->getConnection()->insert($this->entityManager->getClassMetadata(Log::class)->getTableName(), [
                'user_id' => $this->security->getUser() instanceof User ? $this->security->getUser()->getId() : null,
                'message' => $record->message,
                'level' => $record->level->value,
                'level_name' => $record->level->getName(),
                'context' => json_encode($record->context ?? [], JSON_THROW_ON_ERROR),
                'extra' => json_encode($record->extra ?? [], JSON_THROW_ON_ERROR),
                'created_at' => (new \DateTimeImmutable())->format('Y-m-d H:i:s'),
            ]);
        } catch (\Throwable) {
            // silently ignore log errors here
        }
    }
}
