<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Utils\Install;

use Composer\InstalledVersions;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * This class provides various method to read released versions from the ARODAX Admin bundle repository.
 */
final class VersionHelper
{
    public const string INVALID_VERSION = '0.0.0';
    private const string API_URL = 'https://api.bitbucket.org/2.0/repositories/arodax/admin-bundle/refs/tags?sort=-name';

    /**
     * Find the latest available version.
     */
    public function latestVersion(): string
    {
        try {
            return (new FilesystemAdapter())->get('latest_version', function (ItemInterface $item) {
                $item->expiresAfter(180);

                $client = HttpClient::create();
                $response = $client->request('GET', self::API_URL);

                if (200 !== $response->getStatusCode()) {
                    return self::INVALID_VERSION;
                }

                $data = $response->toArray();

                if (!isset($data['values'][0]['name'])) {
                    return self::INVALID_VERSION;
                }

                return $data['values'][0]['name'];
            });
        } catch (\Throwable $exception) {
            return self::INVALID_VERSION;
        }
    }

    /**
     * Find the currently installed version.
     */
    public function installedVersion(): string
    {
        return InstalledVersions::getPrettyVersion('arodax/admin-bundle');
    }
}
