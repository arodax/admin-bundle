<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Utils\Media;

use Arodax\AdminBundle\Exception\PdfPrinterException;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

readonly class PdfPrinter
{
    public function __construct(
        private Environment $twig,
        #[Autowire(param: 'arodax_admin.tmp_dir')]
        private string $tmpDir
    ) {
    }

    public function printToFile(string $template, string $file, array $params = []): void
    {
        try {

            $fs = new Filesystem();

            if (false === $fs->exists($this->tmpDir)) {
                $fs->mkdir($this->tmpDir);
            }

            $html = $this->twig->render($template, $params);
            $mpdf = new Mpdf(['tempDir' => $this->tmpDir]);
            $mpdf->WriteHTML($html);
            $mpdf->output($file, Destination::FILE);
        } catch (\Throwable $exception) {
            throw new PdfPrinterException($exception->getMessage(), $exception->getCode(), $exception->getPrevious());
        }
    }
}
