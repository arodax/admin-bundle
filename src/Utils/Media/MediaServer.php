<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Utils\Media;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Exception\MediaFilterNotFoundException;
use Arodax\AdminBundle\Exception\MediaNotFilterableException;
use Arodax\AdminBundle\Exception\MediaNotFoundException;
use Arodax\AdminBundle\Response\SymfonyResponseFactory;
use Doctrine\ORM\EntityManagerInterface;
use League\Glide\Server;
use League\Glide\ServerFactory;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * This class manipulates with the images.
 */
final readonly class MediaServer
{
    private const array ACCEPT_FOR_FILTER = ['image/jpg', 'image/png', 'image/jpeg', 'image/gif', 'image/webp', 'image/avif',
        'image/heif'
    ];

    public function __construct(
        private EntityManagerInterface $entityManager,
        #[Autowire(param: 'arodax_admin.media_sources')]
        private string $source,
        #[Autowire(param: 'arodax_admin.media_cache')]
        private string $cache,

        /**
         * Filters configured as parameters in arodax_admin.images.filter parameter.
         *
         * These filters are defined as the array, where key is the name of the filter and values
         * are in the array consisting API parameters.
         *
         * @example images.filter.square: {h: 100, w: 100, fit: 'crop' }
         */
        private array $filters
    ) {
    }

    /**
     * Return response from the server containing requested image.
     *
     * @throws MediaNotFoundException
     */
    public function response(Request $request): StreamedResponse
    {
        $path = $request->get(key: 'path');
        $filter = $request->get(key: 'filter');

        $mediaObject = $this->entityManager->getRepository(MediaObject::class)->findOneBy(['filePath' => $path]);

        if (!$mediaObject instanceof MediaObject) {
            throw new MediaNotFoundException();
        }

        // Check if the media object is an SVG
        if ($mediaObject->getMime() === 'image/svg+xml') {
            // Return the SVG as is
            return new StreamedResponse(function () use ($path) {
                readfile($this->source . '/' . $path);
            });
        }

        if (!\in_array($mediaObject->getMime(), self::ACCEPT_FOR_FILTER, true)) {
            throw new MediaNotFilterableException();
        }

        if (!\array_key_exists($filter, $this->filters)) {
            throw new MediaFilterNotFoundException();
        }

        $server = $this->getServer();

        $config = $this->filters[$filter];

        // Set the format based on the media object mime type
        if (false === isset($config['fm'])) {
            $config['fm'] = match ($mediaObject->getMime()) {
                'image/png' => 'png',
                'image/gif' => 'gif',
                'image/webp' => 'webp',
                'image/avif' => 'avif',
                default => 'jpg',
            };
        }

        if (false === in_array(needle: $config['fm'], haystack: ['jpg', 'png', 'gif', 'webp', 'avif'])) {
            throw new MediaFilterNotFoundException();
        }

        $server->setResponseFactory(responseFactory: new SymfonyResponseFactory(request: $request));

        return $server->getImageResponse(path: $path, params: $config);
    }

    public function convertUploadedFileToAvif(UploadedFile $uploadedFile, int $quality = 100): UploadedFile
    {
        return $uploadedFile;
    }

    public function removeFileForMediaObject(MediaObject $mediaObject): void
    {
        $server = $this->getServer();

        $server->deleteCache(path: $mediaObject->getFilePath());
    }

    private function getServer(): Server
    {
        return ServerFactory::create([
            'source' => $this->source,
            'cache' => $this->cache,
            'driver' => extension_loaded('imagick') ? 'imagick' : 'gd'
        ]);
    }
}
