<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Notifier;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * This service provides e-mail notifications to the buyer and seller.
 */
final readonly class EshopNotifier
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private MailerInterface $mailer,
        private LoggerInterface $logger
    ) {
    }

    /**
     * Send notifications to the buyer.
     *
     * @param Order  $order    the order for the notification
     * @param string $subject  the subject of the notification e-mail
     * @param string $template the template of the notification
     */
    public function notifyBuyer(Order $order, string $subject, string $template): void
    {
        $this->sendEmail(
            emailAddress: $order->getBillingEmail(),
            subject: $this->translator->trans($subject, [], 'arodax_admin'),
            template: $template,
            context: ['order' => $order]
        );
    }

    /**
     * Send notification to the seller.
     *
     * @param Order  $order    the order for the notification
     * @param string $subject  the subject of the notification e-mail
     * @param string $template the template of the notification
     */
    public function notifySeller(Order $order, string $subject, string $template): void
    {
        $recipients = $this->entityManager->getRepository(User::class)->findEshopRecipients();

        foreach ($recipients as $recipient) {
            if ($recipient instanceof User && null !== $recipient->getEmail()) {
                $this->sendEmail(
                    emailAddress: $recipient->getEmail(),
                    subject: sprintf('%s - %s', $order->getOrderNumber(), $this->translator->trans($subject, [], 'arodax_admin')),
                    template: $template,
                    context: ['order' => $order]
                );
            }
        }
    }

    /**
     * Send the e-mail to the recipient.
     *
     * @param string $emailAddress the address of the recipient
     * @param string $subject      the e-mail subject
     * @param string $template     the e-mail template
     * @param array  $context      the e-mail context
     */
    private function sendEmail(string $emailAddress, string $subject, string $template, array $context): void
    {
        $email = new TemplatedEmail();
        $email->to($emailAddress);
        $email->subject($subject);
        $email->htmlTemplate($template);
        $email->context($context);

        try {
            $this->mailer->send($email);
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }
}
