<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Media;

use Arodax\AdminBundle\Exception\MediaFilterNotFoundException;
use Arodax\AdminBundle\Utils\Media\MediaServer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MediaController extends AbstractController
{
    #[Route(path: '/media/cache/{filter}/{path}', name: 'arodax_admin_media_object_filter', requirements: ['path' => '.+'], methods: ['GET'])]
    public function filter(Request $request, MediaServer $mediaServer): Response
    {
        try {
            return $mediaServer->response($request);
        } catch (MediaFilterNotFoundException $exception) {
            return new Response($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }
}
