<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Translation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Arodax\AdminBundle\I18n\TranslationsLoaderInterfaces;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Annotation\Route;

class TranslationController extends AbstractController
{
    /**Arodax\AdminBundle\Controller\Admin\SectionController
     * @param Request                        $request
     * @param TranslationsLoaderInterfaces $translationsExtractor
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    #[Route(path: '/translations.json', name: 'translations', methods: ['GET'])]
    #[Cache(expires: 'tomorrow', public: true)]
    public function index(Request $request, TranslationsLoaderInterfaces $translationsExtractor): JsonResponse
    {
        $locale = $request->query->get('_locale') ?? $request->getLocale();

        return $this->json([$locale => $translationsExtractor->loadTranslations($locale)['arodax_admin']]);
    }
}
