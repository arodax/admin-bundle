<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Grantys;

use Arodax\AdminBundle\Manager\GrantysManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;
use Arodax\AdminBundle\Entity\Media\MediaObject;

#[Route(path: '/api/grantys/process/{id}', name: 'api_events_grantys_process', methods: ['POST'])]
#[IsGranted('ROLE_GRANTYS_ADMIN')]
readonly class Process
{
    public function __construct(
        private GrantysManager $grantysManager,
        private SerializerInterface $serializer
    ) {
    }

    /**
     * @throws \Exception when date cannot be parsed from the request.
     */
    public function __invoke(MediaObject $mediaObject): JsonResponse
    {
        $this->grantysManager->parseMediaObject($mediaObject);

        return new JsonResponse($this->serializer->serialize([], 'jsonld'), Response::HTTP_OK, [], true);
    }
}
