<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Security\Authorization\RolesManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController extends AbstractController
{
    /*
     * Return all available non-dynamic user roles in the system.
     */
    #[Route(path: '/api/security/roles', name: 'api_security_roles_index', methods: ['GET'])]
    #[Route('ROLE_ADMINISTRATION_ACCESS')]
    public function roles(SerializerInterface $serializer, RolesManager $roles): JsonResponse
    {
        return new JsonResponse($serializer->serialize($roles->getRoles(), 'jsonld'), Response::HTTP_OK, [], true);
    }
}
