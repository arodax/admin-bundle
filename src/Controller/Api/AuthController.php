<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\Controller\Api;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    /**
     * Return the serialized current user and also act as the authenticator endpoint for the UserAuthenticator.
     */
    #[Route(path: '/api/auth', name: 'api_auth_index', methods: ['POST', 'GET'])]
    public function index(SerializerInterface $serializer, Security $security): JsonResponse
    {
        return new JsonResponse($serializer->serialize($security->getUser(), 'jsonld', ['groups' => 'user:info']), Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/auth', name: 'api_auth_logout', methods: ['DELETE'])]
    public function logout(Security $security): JsonResponse
    {
        if (($token = $security->getToken()) !== null) {
            $token->setAuthenticated(false);
        }

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
