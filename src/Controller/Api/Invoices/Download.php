<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Invoices;

use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/invoices/{invoice}/download', name: 'api_invoices_download_item', methods: ['GET'])]
class Download
{
    public function __invoke(InvoiceCzech $invoice)
    {
        $path = __DIR__.'/../../../../../../var/invoice/invoice.pdf';

        $fs = new Filesystem();

        if (!$fs->exists($path)) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        $response = new BinaryFileResponse($path);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if ($mimeTypeGuesser->isGuesserSupported()) {
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($path));
        } else {
            $response->headers->set('Content-Type', 'text/plain');
        }

        $response->headers->set(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'invoice');

        return $response;
    }
}
