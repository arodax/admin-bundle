<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Section;

use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Normalizer\NestedEntityNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

readonly class TreeController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private NestedEntityNormalizer $normalizer,
        private SerializerInterface $serializer
    ) {
    }

    /**
     * @throws ExceptionInterface
     */
    #[Route(path: '/api/sections/tree', name: 'api_sections_get_tree_index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function index(): JsonResponse
    {
        $repository = $this->entityManager->getRepository(Section::class);
        $rootNodes = $repository->findBy(['parent' => null]);
        $tree = [];
        foreach ($rootNodes as $rootNode) {
            array_push($tree, ...$this->normalizer->normalize($rootNode, ['groups' => 'section:tree']));
        }

        return new JsonResponse($this->serializer->serialize($tree, 'json'), Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/sections/tree', name: 'api_sections_get_tree_update', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function update(Request $request): Response
    {
        $repository = $this->entityManager->getRepository(Section::class);
        $data = json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!isset($data['parent']) || null === $data['parent']) {
            throw new \LogicException('Cannot move the node, when the parent id is not known');
        }
        // find the entity of it's new parent, then find the nth sibling, use persistAsNextSiblingOf
        /** @var Section $node */
        $node = $repository->find($this->entityManager->getRepository(Section::class)->find($data['id'] ?? null));
        $parent = $repository->find($data['parent'] ?? null);

        if ($parent !== null) {
            $repository->persistAsFirstChildOf($node, $parent);
            $this->entityManager->flush();
            $this->entityManager->refresh($parent);
            $this->entityManager->refresh($node);
            if ($data['position'] > 0) {
                $children = $repository->children($parent);
                $repository->persistAsNextSiblingOf($node, $children[$data['position']]);
                $this->entityManager->flush();
            }
        } else {
            $node->setParent(null);
            $repository->persistAsLastChild($node);
            $this->entityManager->flush();
            $this->entityManager->clear();
        }

        return new JsonResponse([], Response::HTTP_OK);
    }

    #[Route(path: '/api/sections/tree', name: 'api_sections_tree_append', methods: ['POST'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function append(Request $request, SerializerInterface $serializer): Response
    {
        $repository = $this->entityManager->getRepository(Section::class);
        $data = json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!isset($data['parent']) || null === $data['parent']) {
            throw new \LogicException('Cannot append the node, when the parent id is not known');
        }
        $parent = $repository->find($data['parent'] ?? null);
        $node = new Section();
        $repository->persistAsLastChildOf($node, $parent);
        $this->entityManager->flush();

        return new JsonResponse($serializer->serialize($node, 'jsonld'), Response::HTTP_OK, [], true);
    }
}
