<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Section;
use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Security\Voter\ArticleVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/api/sections/{id}/article', name: 'api_sections_append_article', methods: ['POST'])]
#[IsGranted(ArticleVoter::CREATE_ARTICLE)]
class ArticleController
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @throws \Exception when date cannot be parsed from the request.
     */
    public function __invoke(Section $section): JsonResponse
    {
        $article = new Article();
        $article->addSection($section);
        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return new JsonResponse($this->serializer->serialize($article, 'jsonld', ['groups' => ['article:read']]), Response::HTTP_OK, [], true);
    }
}
