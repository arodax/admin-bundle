<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Manager\CheckoutManager;
use Arodax\AdminBundle\Manager\PaymentProviderManager;
use Arodax\AdminBundle\Manager\ShoppingCartManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrderController extends AbstractController
{
    /**
     * @throws \Exception
     * @throws \Throwable
     */
    #[Route(path: '/api/order', name: 'api_order_create', methods: ['POST'])]
    public function create(Request $request, SerializerInterface $serializer, CheckoutManager $checkoutManager, PaymentProviderManager $providerManager, ShoppingCartManager $shoppingCartManager): JsonResponse
    {
        // try to create an order
        $order = $checkoutManager->createOrderFromRequest($request, $shoppingCartManager->getShoppingCart());
        // renew a shopping cart by creating a new one
        $shoppingCartManager->createShoppingCart();
        // Payment intents MUST be created after order is persisted into the database
        $providerManager->createPaymentIntent($order, $request->request->getInt('paymentMethod'));

        return new JsonResponse($serializer->serialize($order->getShoppingCart(), 'json', ['groups' => 'shopping_cart:public_read']), Response::HTTP_CREATED, [], true);
    }
}
