<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Orders;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/orders/chart', name: 'api_orders_chart')]
#[IsGranted('ROLE_ESHOP_ORDER_ADMIN')]
class Chart extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function __invoke(): Response
    {
        $qb = $this->entityManager->createQueryBuilder();

        $res = $qb
            ->select('COUNT(o) cnt, MONTH(o.createdAt) orderMonth, DAY(o.createdAt) orderDay, SUM(o.priceWithTax) priceWithTax')
            ->from(Order::class, 'o')
            ->groupBy('orderMonth')
            ->addGroupBy('orderDay')
            ->orderBy('o.createdAt', 'DESC')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();

        $result = array_map(function ($d) use ($res) {
            $ymd = (new \DateTimeImmutable())->modify('- '.$d.' day')->format('Y-m-d');
            $key = array_search($ymd, array_column($res, 'orderDay'));

            if (false !== $key) {
                return ['cnt' => $res[$key]['cnt'], 'date' => $ymd, 'price' => round($res[$key]['priceWithTax'] / 100, 0)];
            } else {
                return ['cnt' => 0, 'date' => $ymd, 'price' => 0];
            }
        }, range(0, 6));

        return new JsonResponse(array_reverse($result), Response::HTTP_OK);
    }
}
