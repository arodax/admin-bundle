<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Security\Acl;

use Arodax\AdminBundle\Security\AclManager;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/security/acl', name: 'api_security_acl_delete', methods: ['DELETE'])]
#[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
class Delete extends AbstractController
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly AclManager $aclManager)
    {
    }

    public function __invoke(): Response
    {
        $this->aclManager->handleRequest($this->requestStack->getCurrentRequest());

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
