<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Security\Acl;

use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Security\AclManager;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/api/security/acl', name: 'api_security_acl_set', methods: ['POST'])]
#[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
class Set extends AbstractController
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly AclManager $aclManager,
        private readonly SerializerInterface $serializer)
    {
    }

    public function __invoke(): JsonResponse
    {
        $object = $this->aclManager->handleRequest($this->requestStack->getCurrentRequest());

        return new JsonResponse($this->serializer->serialize($object, 'jsonld', ['groups' => ['acl:read','user:info', 'user_group:read']]),Response::HTTP_OK, [], true);
    }
}
