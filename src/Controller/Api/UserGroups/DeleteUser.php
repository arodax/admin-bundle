<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\UserGroups;

use Arodax\AdminBundle\Security\Voter\UserGroupVoter;
use Arodax\AdminBundle\ValueObject\UserGroupUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/api/user-groups/{userGroup}/user/{user}', name: 'api_user_groups_delete_user', methods: ['DELETE'])]
#[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
#[IsGranted(attribute: UserGroupVoter::DELETE_USER_FROM_USER_GROUP, subject: 'userGroupUser')]
readonly class DeleteUser
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function __invoke(UserGroupUser $userGroupUser): JsonResponse
    {
        $userGroupUser->userGroup->removeUser($userGroupUser->user);

        $this->entityManager->persist($userGroupUser->userGroup);
        $this->entityManager->flush();

        return new JsonResponse(['success' => 'User has been removed from the user group'], Response::HTTP_OK);
    }
}
