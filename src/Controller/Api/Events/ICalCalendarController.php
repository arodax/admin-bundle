<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Events;

use Arodax\AdminBundle\Manager\CalendarManager;
use Arodax\AdminBundle\Security\Voter\ICalCalendarVoter;
use Eluceo\iCal\Presentation\Factory\CalendarFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


#[Route(path: '/api/events/{token}.ics', name: 'api_events_ics', requirements: ['token' => '[\d\w]+'], methods: ['GET'])]
#[IsGranted(ICalCalendarVoter::SUBSCRIBE_EVENTS, 'token')]
final readonly class ICalCalendarController
{
    public function __construct(
        private CalendarManager $calendarManager,
        private RequestStack    $requestStack
    ) {
    }

    public function __invoke(string $token): Response
    {
        $request = $this->requestStack->getCurrentRequest();
        $locale = $request?->query->get('hl');

        $componentFactory = new CalendarFactory();
        $calendarComponent = $componentFactory->createCalendar($this->calendarManager->getICalCalendar($locale));

        $response = new Response((string)$calendarComponent, Response::HTTP_OK);

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            sprintf('%s.ics', $token)
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
