<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Events;

use Arodax\AdminBundle\Entity\Event\EventType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Manager\CalendarManager;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/api/events/calendar', name: 'api_events_calendar', methods: ['GET'])]
#[IsGranted('ROLE_EVENT_CALENDAR_ADMIN')]
final readonly class CalendarController
{
    public function __construct(
        private RequestStack        $requestStack,
        private CalendarManager     $calendarManager,
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @throws \Exception when date cannot be parsed from the request.
     */
    public function __invoke(): JsonResponse
    {
        $request = $this->requestStack->getMainRequest();

        if (null === $request) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $eventTypes = [];

        if ($request->query->get('eventTypes')) {
            $ids = explode(',', $request->query->get('eventTypes'));

            if ($ids) {
                $qb = $this->entityManager->createQueryBuilder();
                $eventTypes = $qb->select('eventType')
                    ->from(EventType::class, 'eventType')
                    ->where($qb->expr()->in('eventType', $ids))
                    ->getQuery()
                    ->getResult();
                ;
            }
        }

        $events = $this->calendarManager->getEvents(
            (new \DateTimeImmutable($request?->query->get('from')))->modify('-1 week'),
            (new \DateTimeImmutable($request?->query->get('to')))->modify('+1 week'),
            $eventTypes
        );

        return new JsonResponse($this->serializer->serialize($events, 'jsonld'), Response::HTTP_OK, [], true);
    }
}
