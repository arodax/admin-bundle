<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Events;

use Arodax\AdminBundle\Manager\CalendarManager;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/events/calendar/url', name: 'api_events_url', methods: ['GET'])]
#[IsGranted('ROLE_EVENT_CALENDAR_ADMIN')]
final readonly class ICalUrlController
{
    public function __construct(
        private CalendarManager $calendarManager
    ) {
    }

    /**
     * @throws \Exception when date cannot be parsed from the request.
     */
    public function __invoke(): JsonResponse
    {
        return new JsonResponse([
            'url' => $this->calendarManager->getICalCalendarUrl()
        ]);
    }
}
