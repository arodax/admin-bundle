<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\Install;

use Arodax\AdminBundle\Utils\Install\VersionHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/api/install/check-update', name: 'arodax_admin_install_check_update', methods: ['GET'])]
#[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
class CheckUpdate extends AbstractController
{
    public function __construct(
        private readonly VersionHelper $versionHelper
    ) {
    }

    public function __invoke(): JsonResponse
    {
        return new JsonResponse([
            'installedVersion' => $this->versionHelper->installedVersion(),
            'latestVersion' => $this->versionHelper->latestVersion(),
        ]);
    }
}
