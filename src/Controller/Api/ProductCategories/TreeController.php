<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\ProductCategories;

use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Arodax\AdminBundle\Normalizer\NestedEntityNormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TreeController
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly NestedEntityNormalizer $normalizer, private readonly SerializerInterface $serializer)
    {
    }

    #[Route(path: '/api/product-categories/tree', name: 'api_product_categories_get_tree_index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function index(): JsonResponse
    {
        $repository = $this->entityManager->getRepository(ProductCategory::class);
        $rootNodes = $repository->findBy(['parent' => null]);
        $tree = [];
        foreach ($rootNodes as $rootNode) {
            array_push($tree, ...$this->normalizer->normalize($rootNode, ['groups' => 'product_category:tree']));
        }

        return new JsonResponse($this->serializer->serialize($tree, 'json'), Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/product-categories/tree', name: 'api_product_categories_tree_update', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function update(Request $request): Response
    {
        $repository = $this->entityManager->getRepository(ProductCategory::class);
        $data = json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!isset($data['parent']) || null === $data['parent']) {
            throw new \LogicException('Cannot move the node, when the parent id is not known');
        }
        // find the entity of it's new parent, then find the nth sibling, use persistAsNextSiblingOf
        /** @var ProductCategory $node */
        $node = $repository->find($this->entityManager->getRepository(ProductCategory::class)->find($data['id'] ?? null));
        $parent = $repository->find($data['parent'] ?? null);

        if ($parent !== null) {
            $repository->persistAsFirstChildOf($node, $parent);
            $this->entityManager->flush();
            $this->entityManager->refresh($parent);
            $this->entityManager->refresh($node);
            if ($data['position'] > 0) {
                $children = $repository->children($parent);
                $repository->persistAsNextSiblingOf($node, $children[$data['position']]);
                $this->entityManager->flush();
            }
        } else {
            $node->setParent(null);
            $repository->persistAsLastChild($node);
            $this->entityManager->flush();
            $this->entityManager->clear();
        }

        return new JsonResponse([], Response::HTTP_OK);
    }

    #[Route(path: '/api/product-categories/tree', name: 'api_product_categories_tree_append', methods: ['POST'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function append(Request $request, SerializerInterface $serializer): Response
    {
        $repository = $this->entityManager->getRepository(ProductCategory::class);
        $data = json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!isset($data['parent']) || null === $data['parent']) {
            throw new \LogicException('Cannot append the node, when the parent id is not known');
        }
        $parent = $repository->find($data['parent'] ?? null);
        $node = new ProductCategory();
        $repository->persistAsLastChildOf($node, $parent);
        $this->entityManager->flush();

        return new JsonResponse($serializer->serialize($node, 'jsonld'), Response::HTTP_OK, [], true);
    }
}
