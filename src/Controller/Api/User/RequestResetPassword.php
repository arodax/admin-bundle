<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api\User;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Message\LostPasswordMessage;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class RequestResetPassword
{
    public function __construct(
        private MessageBusInterface $bus
    ) {
    }

    public function __invoke(User $user): User
    {
        $this->bus->dispatch(new LostPasswordMessage($user->getEmail()));

        return $user;
    }
}
