<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Manager\PaymentProviderManager;
use Arodax\AdminBundle\Manager\ShippingManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CheckoutController extends AbstractController
{
    /**
     * Return the serialized options available for the checkout.
     */
    #[Route(path: '/api/checkout', name: 'api_checkout_index', methods: ['OPTIONS'])]
    public function index(SerializerInterface $serializer, ShippingManager $shippingManager, PaymentProviderManager $paymentProviderManager): JsonResponse
    {
        $response = [
            'shippingMethods' => $shippingManager->getAvailableShippingMethods(),
            'paymentMethods' => $paymentProviderManager->getAvailablePaymentMethods(),
        ];

        return new JsonResponse($serializer->serialize($response, 'jsonld', ['groups' => 'shopping_cart:public_read']), Response::HTTP_OK, [], true);
    }
}
