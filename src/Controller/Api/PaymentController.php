<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Stripe\Exception\SignatureVerificationException;
use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\PaymentProvider\StripePaymentProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PaymentController extends AbstractController
{
    /**
     * Process the online payment.
     *
     * @throws SignatureVerificationException
     */
    /**
     * Process the online payment.
     *
     * @throws SignatureVerificationException
     */
    #[Route(path: '/api/payment/verify', name: 'api_payment_verify', methods: ['POST', 'GET'])]
    public function verify(SerializerInterface $serializer, StripePaymentProvider $stripePaymentProvider, Request $request): JsonResponse
    {
        $stripePaymentProvider->handleWebhook($request);

        return new JsonResponse($serializer->serialize([], 'jsonld'), Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/payment/verify/{id}', name: 'api_payment_verify_payment', methods: ['GET'])]
    public function verifyPayment(SerializerInterface $serializer, StripePaymentProvider $stripePaymentProvider, ShoppingCart $shoppingCart): JsonResponse
    {
        if (($orderPayment = $shoppingCart->getOrder()->getPayment()) !== null) {
            $stripePaymentProvider->verifyConfirmPayment($orderPayment);
        }

        return new JsonResponse($serializer->serialize($shoppingCart, 'jsonld', ['groups' => 'shopping_cart:public_read']), Response::HTTP_OK, [], true);
    }
}
