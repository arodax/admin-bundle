<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\Manager\ShoppingCartManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ShoppingCartController extends AbstractController
{
    #[Route(path: '/api/shopping-cart/{id}', name: 'api_shopping_cart_read', defaults: ['id' => 'null'], methods: ['GET'])]
    public function read(SerializerInterface $serializer, ShoppingCartManager $shoppingCartManager, Request $request, ?int $id = null): JsonResponse
    {
        // allow to fetch shopping cart by the orderNumber filter
        if ($request->query->get('orderNumber')) {
            $shoppingCart = $shoppingCartManager->getShoppingCart($shoppingCartManager->getShoppingCartIdByOrderNumber($request->query->getInt('orderNumber')));
        } else {
            $shoppingCart = $shoppingCartManager->getShoppingCart($id);
        }

        return new JsonResponse($serializer->serialize($shoppingCart, 'json', ['groups' => 'shopping_cart:public_read']), Response::HTTP_OK, [], true);
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    #[Route(path: '/api/shopping-cart', name: 'api_shopping_cart_update', methods: ['POST'])]
    public function update(ShoppingCartManager $shoppingCartManager, Request $request, SerializerInterface $serializer): JsonResponse
    {
        $shoppingCartManager->addItemById($request->request->getInt('id'), $request->request->getInt('qty'));

        return new JsonResponse($serializer->serialize($shoppingCartManager->getShoppingCart(), 'json', ['groups' => 'shopping_cart:public_read']), Response::HTTP_OK, [], true);
    }
}
