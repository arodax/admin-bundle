<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Api;

use Arodax\AdminBundle\Intl\DeeplTranslatorClient;
use Arodax\AdminBundle\Model\TranslatableInterface;
use Symfony\Component\HttpFoundation\Response;
use Arodax\AdminBundle\I18n\LocalesUtility;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Intl\Locales;
use Symfony\Component\Intl\Timezones;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use ApiPlatform\Api\IriConverterInterface;

class IntlController extends AbstractController
{
    /**
     * Return the available intl configuration for the application instance.
     */
    #[Route(path: '/api/intl', name: 'api_intl_index', methods: ['POST', 'GET'])]
    public function index(SerializerInterface $serializer, LocalesUtility $localesUtility): JsonResponse
    {
        return new JsonResponse($serializer->serialize(['locales' => $localesUtility->getContentLocales()], 'jsonld'), Response::HTTP_OK, [], true);
    }

    /**
     * Return the serialized country list of all countries.
     */
    #[Route(path: '/api/intl/countries', name: 'api_intl_countries', methods: ['POST', 'GET'])]
    public function countries(SerializerInterface $serializer, Request $request): JsonResponse
    {
        return new JsonResponse($serializer->serialize(Countries::getNames($request->getLocale()), 'jsonld'), Response::HTTP_OK, [], true);
    }

    /**
     * Return the serialized list of all system locales.
     */
    #[Route(path: '/api/intl/locales', name: 'api_intl_locales', methods: ['POST', 'GET'])]
    public function locales(SerializerInterface $serializer, Request $request): JsonResponse
    {
        return new JsonResponse($serializer->serialize(Locales::getNames($request->getLocale()), 'jsonld'), Response::HTTP_OK, [], true);
    }

    /**
     * Return the serialized list of all timezones.
     */
    #[Route(path: '/api/intl/timezones', name: 'api_intl_timezones', methods: ['POST', 'GET'])]
    public function timezones(SerializerInterface $serializer, Request $request): JsonResponse
    {
        return new JsonResponse($serializer->serialize(Timezones::getNames($request->getLocale()), 'jsonld'), Response::HTTP_OK, [], true);
    }

    /**
     * Return the serialized list of all timezones.
     */
    #[Route(path: '/api/intl/currencies', name: 'api_intl_currencies', methods: ['POST', 'GET'])]
    public function currencies(SerializerInterface $serializer, Request $request): JsonResponse
    {
        return new JsonResponse($serializer->serialize(Currencies::getNames($request->getLocale()), 'jsonld'), Response::HTTP_OK, [], true);
    }

    #[Route(path: '/api/intl/translate', name: 'api_intl_translate', methods: ['POST', 'GET'])]
    public function translateItem(Request $request, DeeplTranslatorClient $translatorClient, IriConverterInterface $iriConverter): Response
    {
        /** @var TranslatableInterface|null $translatable */
        $translatable = $iriConverter->getResourceFromIri($request->request->get('iri'));

        $translatorClient->translateEntity($translatable, $request->request->get('locale'));

        return new Response(null);
    }
}
