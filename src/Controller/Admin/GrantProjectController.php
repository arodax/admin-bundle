<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Grantys\GrantProject;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\GrantProjectVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/grant-projects')]
class GrantProjectController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_grant_projects_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_GRANTYS_ADMIN')]
    public function index(): Vue
    {
        return new Vue('GrantProjectIndex', '/grant_project/index', 'grant projects');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_grant_projects_edit', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(GrantProjectVoter::READ_GRANT_PROJECT, subject: 'grantProject', message: 'You cannot view this grant project.')]
    public function edit(GrantProject $grantProject): Vue
    {
        return new Vue('GrantProjectEditation', '/grant_project/edit', 'grant project editation');
    }
}
