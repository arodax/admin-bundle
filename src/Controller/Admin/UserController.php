<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\UserVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/users')]
class UserController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_users_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_PEOPLE_USER_ADMIN')]
    public function index(): Vue
    {
        return new Vue('UserIndex', '/user/index', 'users');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_users_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(UserVoter::READ_USER, subject: 'user', message: 'You cannot view this user.')]
    public function edit(User $user): Vue
    {
        return new Vue('UserEditation', '/user/edit', 'user editation');
    }
}
