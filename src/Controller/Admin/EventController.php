<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\EventVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/events')]
class EventController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_events_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_EVENT_CALENDAR_ADMIN')]
    public function index(): Vue
    {
        return new Vue('EventIndex', '/event/index', 'calendar');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_events_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(EventVoter::READ_EVENT, subject: 'event', message: 'You cannot view this event.')]
    public function edit(Event $event): Vue
    {
        return new Vue('EventEditation', '/event/edit', 'event editation');
    }
}
