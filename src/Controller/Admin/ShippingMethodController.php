<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\ShippingMethod;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\ShippingMethodVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/shipping-methods')]
class ShippingMethodController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_shipping_methods_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_SHIPPING_METHOD_ADMIN')]
    public function index(): Vue
    {
        return new Vue('ShippingMethodIndex', '/shipping_method/index', 'shipping methods');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_shipping_methods_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ShippingMethodVoter::READ_SHIPPING_METHOD, subject: 'shippingMethod', message: 'You cannot view this shipping method.')]
    public function edit(ShippingMethod $shippingMethod): Vue
    {
        return new Vue('ShippingMethodEditation', '/shipping_method/edit', 'shipping method editation');
    }
}
