<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Gallery\Gallery;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\GalleryVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/galleries')]
class GalleryController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_galleries_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MULTIMEDIA_GALLERY_ADMIN')]
    public function index(): Vue
    {
        return new Vue('GalleryIndex', '/gallery/index', 'galleries');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_galleries_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(GalleryVoter::READ_GALLERY, subject: 'gallery', message: 'You cannot view this gallery.')]
    public function edit(Gallery $gallery): Vue
    {
        return new Vue('GalleryEditation', '/gallery/edit', 'gallery editation');
    }
}
