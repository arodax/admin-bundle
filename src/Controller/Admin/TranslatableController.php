<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Localization\Translatable;
use Arodax\AdminBundle\Intl\DeeplTranslatorClient;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\TranslatableVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/translatables')]
class TranslatableController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_i18ns_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_SETTING_TRANSLATION_ADMIN')]
    public function index(): Vue
    {
        return new Vue('TranslatableIndex', '/translatable/index', 'translations');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_i18ns_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(TranslatableVoter::READ_TRANSLATABLE, subject: 'translatable', message: 'You cannot view this translation.')]
    public function edit(Translatable $translatable): Vue
    {
        return new Vue('TranslatableEditation', '/translatable/edit', 'translation editation');
    }

    #[Route(path: '/translate', name: 'arodax_admin_i18ns_translate', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function translate(DeeplTranslatorClient $client, Request $request): Response
    {

        if (!$request->request->has('source') || !$request->request->has('target') || !$request->request->has('input')) {
            throw new BadRequestHttpException('Missing parameter "source", target" or "input" in the request.');
        }
        $result = $client->translateString($request->request->getAlpha('source'), $request->request->getAlpha('target'), $request->request->get('input'));

        return $this->json(['locale' => $request->request->getAlpha('target'), 'result' => $result]);
    }
}
