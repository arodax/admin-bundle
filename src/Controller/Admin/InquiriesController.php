<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\ServiceDesk\Inquiry;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\InquiryVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/inquiries')]
class InquiriesController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_inquiries_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_COMMUNICATION_INQUIRY_ADMIN')]
    public function index(): Vue
    {
        return new Vue('InquiryIndex', '/inquiry/index', 'inquiries');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_inquiries_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(InquiryVoter::READ_INQUIRY, subject: 'inquiry', message: 'You cannot view this item.')]
    public function edit(Inquiry $inquiry): Vue
    {
        return new Vue('InquiryEditation', '/inquiry/edit', 'inquiry editation');
    }
}
