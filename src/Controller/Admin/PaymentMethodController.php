<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\PaymentMethodVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/payment-methods')]
class PaymentMethodController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_payment_methods_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_PAYMENT_METHOD_ADMIN')]
    public function index(): Vue
    {
        return new Vue('PaymentMethodIndex', '/payment_method/index', 'payment methods');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_payment_methods_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(PaymentMethodVoter::READ_PAYMENT_METHOD, subject: 'paymentMethod', message: 'You cannot view this payment method.')]
    public function edit(PaymentMethod $paymentMethod): Vue
    {
        return new Vue('PaymentMethodEditation', '/payment_method/edit', 'payment method editation');
    }
}
