<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Response\Vue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/newsletter-recipients')]
class NewsletterRecipientController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_newsletter_recipient_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN')]
    public function index(): Vue
    {
        return new Vue('NewsletterRecipientIndex', '/newsletter_recipient/index', 'newsletter_recipients');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_newsletter_recipient_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN')]
    public function edit(): Vue
    {
        return new Vue('NewsletterRecipientEditation', '/newsletter_recipient/edit', 'newsletter_recipients_editation');
    }
}
