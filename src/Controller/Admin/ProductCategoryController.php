<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\ProductCategoryVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/product-categories')]
class ProductCategoryController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_product_categories_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_CATEGORY_ADMIN')]
    public function index(): Vue
    {
        return new Vue('ProductCategoryIndex', '/product_category/index', 'product categories');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_product_categories_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ProductCategoryVoter::READ_PRODUCT_CATEGORY, subject: 'productCategory', message: 'You cannot view this product category.')]
    public function edit(ProductCategory $productCategory): Vue
    {
        return new Vue('ProductCategoryEditation', '/product_category/edit', 'product category editation');
    }
}
