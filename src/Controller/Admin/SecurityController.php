<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Response\Vue;
use Symfony\Bundle\SecurityBundle\Security;
use Arodax\AdminBundle\Entity\User\ResetPasswordToken;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Message\LostPasswordMessage;
use Arodax\AdminBundle\Security\LoginLinkProvider;
use Arodax\AdminBundle\Security\UserPasswordManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    #[Route(path: '/admin/login', name: 'arodax_admin_security_login', options: ['expose' => true], methods: ['GET', 'POST'])]
    public function login(Request $request): Vue|Response
    {
        if (Request::METHOD_GET === $request->getMethod()) {
            return new Vue(
                rootComponent: 'SecurityLogin',
                encoreEntryPoint: '/security/login',
                title: 'login',
                baseTwigTemplate: '@ArodaxAdmin/security.html.twig'
            );
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/admin/logout', name: 'arodax_admin_security_logout', options: ['expose' => true], methods: ['GET'])]
    public function logout(): never
    {
        throw new \LogicException('This code should not be reached.');
    }

    #[Route(path: '/admin/lost-password', name: 'arodax_admin_security_lost_password', options: ['expose' => true], methods: ['POST'])]
    public function lostPassword(Request $request, MessageBusInterface $bus, EntityManagerInterface $entityManager): Response
    {
        $user = $entityManager->getRepository(User::class)->findOneBy([
            'email' => $request->request->get('email'),
            'enabled' => true,
        ]);
        if ($user !== null) {
            $bus->dispatch(new LostPasswordMessage($user->getEmail()));
        }

        return new Response(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/admin/reset-password', name: 'arodax_admin_security_reset_password', options: ['expose' => true], methods: ['GET', 'PUT'])]
    public function resetPassword(Request $request, EntityManagerInterface $entityManager, UserPasswordManager $userPasswordManager): Vue|Response
    {
        $hash = $request->query->get('hash');
        $password = $request->request->get('password');
        if (Request::METHOD_PUT === $request->getMethod()) {
            $token = $entityManager->getRepository(ResetPasswordToken::class)->findOneBy([
                'hash' => $hash,
            ]);

            if (!$token instanceof ResetPasswordToken) {
                return new Response(null, Response::HTTP_BAD_REQUEST);
            }

            $userPasswordManager->resetLostPassword($token, $password);
        }

        return new Vue(
            rootComponent: 'SecurityResetPassword',
            encoreEntryPoint: '/security/reset_password',
            title: 'password reset',
            baseTwigTemplate: '@ArodaxAdmin/security.html.twig'
        );
    }

    #[Route(path: '/admin/login-check', name: 'arodax_admin_security_check_login_link', options: ['expose' => false], methods: ['GET', 'POST'])]
    public function checkLoginLink(Request $request, Security $security): Vue|Response
    {
        // somehow we already have user, so redirect to the default page
        if ($security->getUser() !== null) {
            return $this->redirectToRoute('arodax_admin_default_index');
        }
        if (!$security->getUser() && Request::METHOD_GET === $request->getMethod()) {
            return new Vue(
                rootComponent: 'SecurityLoginLink',
                encoreEntryPoint: '/security/process_login_link',
                title: 'login',
                baseTwigTemplate: '@ArodaxAdmin/security.html.twig'
            );
        }

        return $this->redirectToRoute('arodax_admin_default_index');
    }

    #[Route(path: '/admin/login-link', name: 'arodax_admin_security_send_login_link', options: ['expose' => true], methods: ['POST'])]
    public function sendLoginLink(LoginLinkProvider $loginLinkProvider, Request $request): JsonResponse
    {
        if (!$request->request->get('email')) {
            throw new BadRequestHttpException('Missing e-mail address.');
        }
        $loginLinkProvider->sendLoginLink($request->request->get('email'));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
