<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectGroup;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\GrantProjectGroupVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/grant-project-groups')]
class GrantProjectGroupController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_grant_project_groups_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_GRANTYS_ADMIN')]
    public function index(): Vue
    {
        return new Vue('GrantProjectGroupIndex', '/grant_project_group/index', 'grant_project_groups');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_grant_project_groups_edit', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(GrantProjectGroupVoter::READ_GRANT_PROJECT_GROUP, subject: 'grantProjectGroup', message: 'You cannot view this grant_project_groups.')]
    public function edit(GrantProjectGroup $grantProjectGroup): Vue
    {
        return new Vue('GrantProjectGroupEditation', '/grant_project_group/edit', 'grant_project_groups_editation');
    }
}
