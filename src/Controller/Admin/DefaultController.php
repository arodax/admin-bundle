<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Utils\Install\VersionHelper;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DefaultController extends AbstractController
{
    #[Route(path: '/admin', name: 'arodax_admin_default_index', options: ['expose' => true])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function index(): RedirectResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        try {
            return $this->redirectToRoute($user->getPreferredHome() ?? 'arodax_admin_dashboard_index');
        } catch (ResourceNotFoundException $exception) {
            return $this->redirectToRoute('arodax_admin_dashboard_index');
        }

    }

    #[Route(path: '/admin/ping', name: 'arodax_admin_default_ping', options: ['expose' => false], methods: ['GET'])]
    public function ping(VersionHelper $versionHelper): JsonResponse
    {
        try {
            $version = $versionHelper->installedVersion();
        } catch (InvalidArgumentException) {
            $version = VersionHelper::INVALID_VERSION;
        }

        return new JsonResponse([
            'pingAt' => date('c'),
            'version' => $version
        ]);
    }
}
