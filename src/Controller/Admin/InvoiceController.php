<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\InvoiceVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/invoices')]
class InvoiceController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_invoices_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_FINANCE_INVOICE_ADMIN')]
    public function index(): Vue
    {
        return new Vue('InvoiceIndex', '/invoice/index', 'invoices');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_invoices_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(InvoiceVoter::READ_INVOICE, subject: 'invoice', message: 'You cannot view this invoice.')]
    public function edit(InvoiceCzech $invoice): Vue
    {
        return new Vue('InvoiceEditation', '/invoice/edit', 'invoice editation');
    }

    #[Route(path: '/download/{id}', name: 'arodax_admin_invoice_download', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(InvoiceVoter::READ_INVOICE, subject: 'invoice', message: 'You cannot view this invoice.')]
    public function download(InvoiceCzech $invoice)
    {
        $path = __DIR__.'/../../../../../var/invoice/'.$invoice->getInvoiceNumber().'.pdf';
        $fs = new Filesystem();
        if (!$fs->exists($path)) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }
        $response = new BinaryFileResponse($path);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        if ($mimeTypeGuesser->isGuesserSupported()) {
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($path));
        } else {
            $response->headers->set('Content-Type', 'text/plain');
        }
        $response->headers->set(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'invoice');

        return $response;
    }
}
