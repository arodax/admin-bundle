<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\ArticleVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/articles')]
class ArticleController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_articles_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CONTENT_ARTICLE_ADMIN')]
    public function index(): Vue
    {
        return new Vue('ArticleIndex', '/article/index', 'articles');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_articles_edit', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ArticleVoter::READ_ARTICLE, subject: 'article', message: 'You cannot view this article.')]
    public function edit(Article $article): Vue
    {
        return new Vue('ArticleEditation', '/article/edit', 'article editation');
    }
}
