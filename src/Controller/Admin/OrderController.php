<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\OrderVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/orders')]
class OrderController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_orders_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_ORDER_ADMIN')]
    public function index(): Vue
    {
        return new Vue('OrderIndex', '/order/index', 'orders');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_orders_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(OrderVoter::READ_ORDER, subject: 'order', message: 'You cannot view this order.')]
    public function edit(Order $order): Vue
    {
        return new Vue('OrderEditation', '/order/edit', 'orders');
    }
}
