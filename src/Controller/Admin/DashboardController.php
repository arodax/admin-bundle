<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Response\Vue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin')]
class DashboardController extends AbstractController
{
    #[Route(path: '/dashboard', name: 'arodax_admin_dashboard_index', options: ['expose' => true])]
    #[IsGranted('ROLE_ADMINISTRATION_ACCESS')]
    public function index(): Vue
    {
        return new Vue('DashboardIndex', '/dashboard/index', 'dashboard');
    }
}
