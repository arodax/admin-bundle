<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\UserGroupVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/user-groups')]
class UserGroupController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_user_groups_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_PEOPLE_USER_GROUP_ADMIN')]
    public function index(): Vue
    {
        return new Vue('UserGroupIndex', '/user_group/index', 'user groups');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_user_groups_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(UserGroupVoter::READ_USER_GROUP, subject: 'userGroup', message: 'You cannot view this user group.')]
    public function edit(UserGroup $userGroup): Vue
    {
        return new Vue('UserGroupEditation', '/user_group/edit', 'user group editation');
    }
}
