<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\SectionVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/sections')]
class SectionController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_sections_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CONTENT_SECTION_ADMIN')]
    public function index(): Vue
    {
        return new Vue('SectionIndex', '/section/index', 'sections');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_sections_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(SectionVoter::READ_SECTION, subject: 'section', message: 'You cannot view this section.')]
    public function edit(Section $section): Vue
    {
        return new Vue('SectionEditation', '/section/edit', 'section editation');
    }
}
