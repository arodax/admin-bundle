<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\ShoppingCartVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/shopping-carts')]
class ShoppingCartController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_shopping_carts_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_SHOPPING_CART_ADMIN')]
    public function index(): Vue
    {
        return new Vue('ShoppingCartIndex', '/shopping_cart/index', 'shopping carts');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_shopping_carts_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ShoppingCartVoter::READ_SHOPPING_CART, subject: 'shoppingCart', message: 'You cannot view this shopping cart.')]
    public function edit(ShoppingCart $shoppingCart): Vue
    {
        return new Vue('ShoppingCartEditation', '/shopping_cart/edit', 'shopping cart editation');
    }
}
