<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\MenuItemVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/menu-items')]
class MenuItemController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_menu_items_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_SETTING_MENU_ADMIN')]
    public function index(): Vue
    {
        return new Vue('MenuItemIndex', '/menu_item/index', 'menu');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_menu_items_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(MenuItemVoter::READ_MENU_ITEM, subject: 'menuItem', message: 'You cannot view this menu item.')]
    public function edit(MenuItem $menuItem): Vue
    {
        return new Vue('MenuItemEditation', '/menu_item/edit', 'menu editation');
    }
}
