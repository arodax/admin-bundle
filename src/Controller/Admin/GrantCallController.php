<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Grantys\GrantCall;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\GrantCallVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/grant-calls')]
class GrantCallController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_grant_calls_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_GRANTYS_ADMIN')]
    public function index(): Vue
    {
        return new Vue('GrantCallIndex', '/grant_call/index', 'grant calls');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_grant_calls_edit', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(GrantCallVoter::READ_GRANT_CALL, subject: 'grantCall', message: 'You cannot view this grant call.')]
    public function edit(GrantCall $grantCall): Vue
    {
        return new Vue('GrantCallEditation', '/grant_call/edit', 'grant calls editation');
    }
}
