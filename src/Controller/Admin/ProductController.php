<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Entity\Eshop\Product;
use Arodax\AdminBundle\Response\Vue;
use Arodax\AdminBundle\Security\Voter\ProductVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/products')]
class ProductController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_products_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ESHOP_PRODUCT_ADMIN')]
    public function index(): Vue
    {
        return new Vue('ProductIndex', '/product/index', 'products');
    }

    #[Route(path: '/{id}', name: 'arodax_admin_products_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ProductVoter::READ_PRODUCT, subject: 'product', message: 'You cannot view this product.')]
    public function edit(Product $product): Vue
    {
        return new Vue('ProductEditation', '/product/edit', 'product editation');
    }
}
