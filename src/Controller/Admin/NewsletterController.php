<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Admin;

use Arodax\AdminBundle\Response\Vue;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: '/admin/newsletter')]
class NewsletterController extends AbstractController
{
    #[Route(path: '/', name: 'arodax_admin_newsletter_index', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_COMMUNICATION_NEWSLETTER_ADMIN')]
    public function index(): Vue
    {
        return new Vue('NewsletterIndex', '/newsletter/index', 'newsletter');
    }
}
