<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Controller\Routes;

use Arodax\AdminBundle\Routing\RoutesExtractorInterface;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RoutesController extends AbstractController
{
    #[Route(path: '/routes.json', name: 'routes', methods: ['GET'])]
    #[Cache(expires: 'tomorrow', public: true)]
    public function index(Request $request, RoutesExtractorInterface $routesExtractor): JsonResponse
    {
        $routesCollection = $routesExtractor->getRoutes();
        $routes = [];
        foreach ($routesCollection->all() as $name => $route) {
            $compiledRoute = $route->compile();

            $defaults = array_intersect_key($route->getDefaults(), array_fill_keys($compiledRoute->getVariables(), null));

            if (!isset($defaults['_locale']) && \in_array('_locale', $compiledRoute->getVariables(), true)) {
                $defaults['_locale'] = $request->getLocale();
            }

            $routes[$name] = [
                'tokens' => $compiledRoute->getTokens(),
                'defaults' => $defaults,
            ];
        }

        return new JsonResponse($routes);
    }
}
