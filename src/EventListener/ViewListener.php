<?php

/*
 * This file is part of the ceecr.cz package.
 *
 * (c) Exekutorská Komora České republiky
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Negotiation\NegotiatorInterface;
use Arodax\AdminBundle\Response\View;
use Arodax\AdminBundle\Response\Vue;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment as Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final readonly class ViewListener implements EventSubscriberInterface
{
    public function __construct(
        protected Twig $twig,
        protected NegotiatorInterface $negotiator,
        protected SerializerInterface $serializer
    ) {
    }

    /**
     * @throws RuntimeError on Twig error
     * @throws SyntaxError  on Twig error
     * @throws LoaderError  on Twig error
     */
    public function onKernelView(ViewEvent $event): void
    {
        $result = $event->getControllerResult();

        if ($result instanceof Vue) {
            $response = new Response($this->twig->render($result->getBaseTwigTemplate(), [
                'vue' => $result,
            ]));

            $event->setResponse($response->setStatusCode(Response::HTTP_OK));
            $event->stopPropagation();

            return;
        }

        if (($result instanceof View) && 'html' === $this->negotiator->getResponseFormat($event->getRequest())) {
            if (empty($result->getTemplate())) {
                throw new \LogicException('There is no HTML template for for this kind of output.');
            }

            $context = array_merge(['data' => $result->getData()], $result->getTemplateContext());
            $response = new Response($this->twig->render($result->getTemplate(), $context));
            $event->setResponse($response->setStatusCode(Response::HTTP_OK));
            $event->stopPropagation();

            return;
        }

        if ($result instanceof View) {
            $data = $this->serializer->serialize($result->getData(), 'json', $result->getSerializationContext());
            $response = new JsonResponse($data, 200, [], true);
            $event->setResponse($response->setStatusCode(Response::HTTP_OK));
            $event->stopPropagation();

            return;
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onKernelView', 2],
        ];
    }
}
