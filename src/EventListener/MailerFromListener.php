<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

/**
 * This listener sets default sender for all emails.
 */
final readonly class MailerFromListener implements EventSubscriberInterface
{
    public function __construct(
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_MAILER_DEFAULT_SENDER')]
        private ?string $mailFrom
    ) {
    }

    /**
     * @throws \Exception when invalid default sender address is specified
     */
    public function onMessageSend(MessageEvent $event): void
    {
        $message = $event->getMessage();

        if (!$message instanceof Email) {
            return;
        }

        if (empty($message->getFrom())) {
            if (false === filter_var($this->mailFrom, \FILTER_VALIDATE_EMAIL)) {
                throw new \RuntimeException(sprintf('Provided default sender "%s" in ARODAX_ADMIN_MAILER_DEFAULT_SENDER environment variable is not a valid e-mal address.', $this->mailFrom));
            }

            $message->from(new Address($this->mailFrom));
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MessageEvent::class => 'onMessageSend',
        ];
    }
}
