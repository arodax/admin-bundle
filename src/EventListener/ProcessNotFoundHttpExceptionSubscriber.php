<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;

/**
 * Handles various kernel exception events.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class ProcessNotFoundHttpExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlMatcherInterface $urlMatcher,
        private RequestStack $requestStack
    ) {
    }

    /**
     * Redirects request to the localized version, if it's available.
     */
    public function processNotFoundHttpException(ExceptionEvent $event): void
    {
        if (!$event->getThrowable() instanceof NotFoundHttpException) {
            return;
        }

        $request = $this->requestStack->getMainRequest();

        if (null === $request) {
            return;
        }

        try {
            $locale = $request->getSession()->get('_locale') ?? $request->getDefaultLocale();

            // try match localized version of the route with locale prefix
            $this->urlMatcher->match('/'.$locale.$request->getPathInfo());

            // redirect to localized version of the route if matcher found one in the route collection
            $event->setResponse(new RedirectResponse('/'.$locale.$request->getRequestUri(), Response::HTTP_TEMPORARY_REDIRECT));
        } catch (ResourceNotFoundException) {
            // continue with the 404 error
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [['processNotFoundHttpException', 10]],
        ];
    }
}
