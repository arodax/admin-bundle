<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Entity\Log\Log;
use Arodax\AdminBundle\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;

#[AsDoctrineListener(event: Events::postPersist)]
#[AsDoctrineListener(event: Events::postUpdate)]
#[AsDoctrineListener(event: Events::postRemove)]
final readonly class EntityLifecycleSubscriber
{
    private ?string $currentRoute;

    public function __construct(
        private LoggerInterface $logger,
        private Security $security,
        private RequestStack $requestStack,
        private EntityManagerInterface $entityManager
    ) {
        $this->currentRoute = $this->requestStack->getCurrentRequest()?->get('_route');
    }

    /**
     * Log entity when it has been created.
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        if ($this->shouldLog()) {
            $this->logChanges($args, 'created');
        }
    }

    /**
     * Log entity when it has been updated.
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        if ($this->shouldLog()) {

            // skip "last activity" updates for User, so log is not filled with useless entries
            $entity = $args->getObject();
            if ($entity instanceof User) {
                $uow = $this->entityManager->getUnitOfWork();
                $changeset = $uow->getEntityChangeSet($entity);

                if (array_key_exists('lastActivityAt', $changeset) && array_key_exists('updatedAt', $changeset) && count($changeset) === 2) {
                    return;
                }
            }

            $this->logChanges($args, 'updated');
        }
    }

    /**
     * Log entity when it has been removed.
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function postRemove(LifecycleEventArgs $args): void
    {
        if ($this->shouldLog()) {
            $this->logChanges($args, 'removed');
        }
    }

    /**
     * Log Doctrine event.
     *
     * @param LifecycleEventArgs $args
     * @param string $action
     * @return void
     */
    private function logChanges(LifecycleEventArgs $args, string $action): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Log) {
            return;
        }

        $className = get_class($entity);

        $this->logger->info('entity.'.$action, [
            'id' => $entity->getId(),
            'action' => $action,
            'entity' => $className,
            'user' => $this->security->getUser()?->getUserIdentifier(),
        ]);
    }


    /**
     * Decide whether this event should be logged.
     *
     * @return bool
     */
    private function shouldLog(): bool
    {
        return $this->currentRoute && (str_starts_with($this->currentRoute, 'arodax_admin_') || str_starts_with($this->currentRoute, 'api_'));
    }
}
