<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Entity\User\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Translation\LocaleSwitcher;

/**
 * Stores the locale of the user in the session after the
 * login. This can be used by the LocaleSubscriber afterward.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class UserLocaleSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private LocaleSwitcher $localeSwitcher,
    ) {
    }

    public function onLoginSuccess(LoginSuccessEvent $event): void
    {
        /** @var User $user */
        $user = $event->getUser();
        $request = $event->getRequest();

        if ($user->getPreferredLocale() !== null && $request->getLocale() !== $user->getPreferredLocale()) {
            $this->localeSwitcher->setLocale($user->getPreferredLocale());

            // Get the current hostname
            $hostname = $request->getHost();

            // Create the cookie name
            $cookieName = $hostname . '_arodax_admin_preferred_locale';

            // Set a cookie with the preferred locale
            $cookie = Cookie::create($cookieName, $user->getPreferredLocale());
            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->sendHeaders();
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onLoginSuccess',
        ];
    }
}
