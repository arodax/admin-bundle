<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Utils\Media\MediaServer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vich\UploaderBundle\Event\Events;
use Vich\UploaderBundle\Event\Event;

/**
 * This listener sets default sender for all Vich uploader events.
 */
final readonly class VichUploaderSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MediaServer $server
    ) {
    }


    public function onPreRemove(Event $event): void
    {
        /** @var MediaObject $mediaObject */
        $mediaObject = $event->getObject();

        $this->server->removeFileForMediaObject($mediaObject);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::PRE_REMOVE => 'onPreRemove',
        ];
    }
}
