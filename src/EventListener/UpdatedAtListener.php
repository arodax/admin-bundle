<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Doctrine\Persistence\Event\LifecycleEventArgs;

/**
 * This listener automatically set createdAt value whenever entity is updated to the current time.
 *
 * Method will perform a check for parameter type and put the correct format as the parameter argument.
 * This listener requires setUpdatedAt setter, if no setter is found it will ignore the property.
 */
final readonly class UpdatedAtListener
{
    /**
     * This method will always be called before persisting entities.
     *
     * @throws \ReflectionException
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        $reflection = new \ReflectionClass($entity);
        if ($reflection->hasMethod('setUpdatedAt')) {
            $parameters = $reflection->getMethod('setUpdatedAt')->getParameters();
            if (1 === \count($parameters)) {
                switch ($parameters[0]->getType()?->getName()) {
                    case 'DateTime':
                    case 'DateTimeImmutable':
                        $entity->setUpdatedAt(new \DateTimeImmutable());
                        break;
                    case 'int':
                        $entity->setUpdatedAt((int) (new \DateTimeImmutable())->format('U'));
                        break;
                    default:
                        $entity->setUpdatedAt((new \DateTimeImmutable())->format('c'));
                }
            }
        }
    }
}
