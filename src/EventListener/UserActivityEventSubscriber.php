<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This event listener updates user activity on every request.
 */
final readonly class UserActivityEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security
    ) {
    }

    /**
     * @throws \Exception
     */
    public function onKernelTerminate(TerminateEvent $event): void
    {
        if (!$this->supports($event)) {
            return;
        }

        $user = $this->security->getUser();

        if ($user instanceof User) {
            $this->entityManager->detach($user);
            $user = $this->entityManager->getRepository(User::class)->find($user->getId());
            $user->setLastActivityAt(new \DateTimeImmutable('now', new \DateTimeZone('UTC')));
            $this->entityManager->flush();
        }
    }

    /**
     * @return array<string, string>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::TERMINATE => 'onKernelTerminate'];
    }

    private function supports(TerminateEvent $event): bool
    {
        if (HttpKernelInterface::MAIN_REQUEST !== $event->getRequestType()) {
            return false;
        }

        if (true === str_starts_with($event->getRequest()->get('_route') ?? '', 'arodax_admin_')) {
            return true;
        }

        return false;
    }
}
