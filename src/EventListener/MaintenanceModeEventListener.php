<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final readonly class MaintenanceModeEventListener implements EventSubscriberInterface
{
    private bool $maintenance;

    public function __construct(
        private Environment $environment,
        private Security $security,
        #[Autowire(env: 'default::resolve:ARODAX_ADMIN_APP_MAITENANCE')]
        ?string $maintenance
    ) {
        $this->maintenance = $maintenance && filter_var($maintenance, \FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$this->maintenance) {
            return;
        }

        if ('dev' === strtolower((string) $_SERVER['APP_ENV'])) {
            return;
        }

        if ($this->security->getUser() && $this->security->isGranted('ROLE_ADMIN')) {
            return;
        }

        $event->setResponse(\in_array('text/html', $event->getRequest()->getAcceptableContentTypes()) ? new Response($this->environment->render('@ArodaxAdmin/maitenance.html.twig'), Response::HTTP_SERVICE_UNAVAILABLE) : new JsonResponse(null, Response::HTTP_SERVICE_UNAVAILABLE));
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => ['onKernelRequest', 6]];
    }
}
