<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Log access to controllers into the logger.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class ControllerSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    /**
     * Redirects request to the localized version, if it's available.
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();

        if (!$this->supports($request)) {
            return;
        }

        $this->logger->debug('Accessed {path}.', [
            'path' => $request->getSchemeAndHttpHost().$request->getRequestUri(),
        ]);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [['onKernelResponse', -2048]],
        ];
    }

    private function supports(Request $request): bool
    {
        // log entries only for "arodax_admin_" routes requested by HTTP get method
        if (Request::METHOD_GET !== $request->getMethod()) {
            return false;
        }

        if (null === $request->get('_route')) {
            return false;
        }

        if (!str_contains((string) $request->get('_route'), 'arodax_admin_')) {
            return false;
        }

        return 'arodax_admin_media_object_filter' !== $request->attributes->get('_route');
    }
}
