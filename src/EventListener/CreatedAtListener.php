<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Doctrine\Persistence\Event\LifecycleEventArgs;

/**
 * This listener automatically set createdAt value whenever entity is created to the current time.
 *
 * Method will perform a check for parameter type and put the correct format as the parameter argument.
 * This listener requires setCreatedAt setter, if no setter is found it will ignore the property.
 */
final readonly class CreatedAtListener
{
    /**
     * This method will always be called before persisting entities.
     *
     * @throws \ReflectionException
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        $reflection = new \ReflectionClass($entity);
        if ($reflection->hasMethod('setCreatedAt')) {
            // allow to pass custom created value, skip this listener if already set.
            if ($reflection->hasMethod('getCreatedAt') && null !== $entity->getCreatedAt()) {
                return;
            }

            $parameters = $reflection->getMethod('setCreatedAt')->getParameters();
            if (1 === \count($parameters)) {
                switch ($parameters[0]->getType()?->getName()) {
                    case 'DateTime':
                    case 'DateTimeImmutable':
                        $entity->setCreatedAt(new \DateTimeImmutable());
                        break;
                    case 'int':
                        $entity->setCreatedAt((int) (new \DateTimeImmutable())->format('U'));
                        break;
                    default:
                        $entity->setCreatedAt((new \DateTimeImmutable())->format('c'));
                }
            }
        }
    }
}
