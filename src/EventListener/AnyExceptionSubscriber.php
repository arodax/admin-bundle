<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Exception\MediaNotFilterableException;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use League\Glide\Filesystem\FileNotFoundException;

/**
 * Handles various kernel exception events.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
final readonly class AnyExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailerInterface $mailer,
        private RequestStack $requestStack,
        private LoggerInterface $logger,
        #[Autowire(param: 'kernel.environment')]
        private string $env,
    ) {
    }

    public function processException(ExceptionEvent $event): void
    {
        if ($this->env !== 'prod') {
            return;
        }

        $exception = $event->getThrowable();

        // Skip 403 and 404 exceptions
        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            if ($statusCode === 403 || $statusCode === 404) {
                return;
            }
        }

        if ($exception instanceof AccessDeniedException) {
            return;
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return;
        }

        if ($exception instanceof MethodNotAllowedException) {
            return;
        }

        if ($exception instanceof NotAcceptableHttpException) {
            return;
        }

        if ($exception instanceof FileNotFoundException) {
            return;
        }

        if ($exception instanceof MediaNotFilterableException) {
            return;
        }

        $exception = $event->getThrowable();
        $request = $this->requestStack->getCurrentRequest();

        // Collect data for the context
        $context = [
            'exceptionClass' => get_class($exception),
            'errorMessage' => $exception->getMessage(),
            'errorFile' => $exception->getFile(),
            'errorLine' => $exception->getLine(),
            'errorTrace' => $exception->getTraceAsString(),
            'requestUri' => $request ? $request->getRequestUri() : 'N/A',
            'requestMethod' => $request ? $request->getMethod() : 'N/A',
            'requestData' => $request ? $request->request->all() : [],
            'queryData' => $request ? $request->query->all() : [],
        ];

        // Prepare the email message
        $email = (new TemplatedEmail())
            ->from(addresses:  'no-reply@arodax.com')
            ->to(addresses: 'podpora@arodax.com')
            ->htmlTemplate(template: '@ArodaxAdmin/emails/arodax_support_exception.html.twig')
            ->context($context)
            ->subject(subject: 'Exception Notification')
        ;

        // Attempt to send the email
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->critical('Unable to send email: ' . $e->getMessage());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [['processException', 10]],
        ];
    }
}
