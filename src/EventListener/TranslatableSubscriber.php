<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Arodax\AdminBundle\Model\TranslatableInterface;
use Arodax\AdminBundle\Reflection\ClassAnalyzer;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Symfony\Component\HttpFoundation\RequestStack;


#[AsDoctrineListener('loadClassMetadata')]
#[AsDoctrineListener('postLoad')]
#[AsDoctrineListener('prePersist')]
final class TranslatableSubscriber
{
    private readonly ClassAnalyzer $classAnalyser;

    public function __construct(
        ClassAnalyzer $classAnalyzer,
        private readonly RequestStack $requestStack,
        private readonly string $translatableTrait,
        private readonly string $translationTrait,
        private int|string $translatableFetchMode,
        private int|string $translationFetchMode,
        private readonly string $defaultLocale = 'en',
    ) {
        $this->classAnalyser = $classAnalyzer;

        $this->translatableFetchMode = $this->convertFetchString($translatableFetchMode);
        $this->translationFetchMode = $this->convertFetchString($translationFetchMode);
    }

    protected function getClassAnalyzer(): ClassAnalyzer
    {
        return $this->classAnalyser;
    }

    /**
     * Adds mapping to the translatable and translations.
     *
     * @throws \ReflectionException
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();

        if (null === $classMetadata->reflClass) {
            return;
        }

        if ($this->isTranslatable($classMetadata)) {
            $this->mapTranslatable($classMetadata);
        }

        if ($this->isTranslation($classMetadata)) {
            $this->mapTranslation($classMetadata);
        }
    }

    public function postLoad(PostLoadEventArgs $eventArgs): void
    {
        $this->setLocales($eventArgs);
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getObject();

        if (!$entity instanceof TranslatableInterface) {
            return;
        }

        $this->setLocales($eventArgs);

        // always create an empty translation, if none have been created
        if (0 === (is_countable($entity->getTranslations()) ? \count($entity->getTranslations()) : 0)) {
            $translationEntity = $entity->createEmptyTranslation();
            $entity->addTranslation($translationEntity);
        }
    }

    /**
     * @throws \ReflectionException
     */
    private function mapTranslatable(ClassMetadata $classMetadata): void
    {
        if (!$classMetadata->hasAssociation('translations')) {
            $classMetadata->mapOneToMany([
                'fieldName' => 'translations',
                'mappedBy' => 'translatable',
                'indexBy' => 'locale',
                'cascade' => ['persist', 'merge', 'remove'],
                'fetch' => $this->translatableFetchMode,
                'targetEntity' => $classMetadata->getReflectionClass()?->getMethod('getTranslationEntityClass')->invoke(null),
                'orphanRemoval' => true,
            ]);
        }
    }

    /**
     * @throws \ReflectionException
     * @throws MappingException
     */
    private function mapTranslation(ClassMetadata $classMetadata): void
    {
        if (!$classMetadata->hasAssociation('translatable')) {
            $classMetadata->mapManyToOne([
                'fieldName' => 'translatable',
                'inversedBy' => 'translations',
                'cascade' => ['persist', 'merge'],
                'fetch' => $this->translationFetchMode,
                'joinColumns' => [[
                    'name' => 'translatable_id',
                    'referencedColumnName' => 'id',
                    'onDelete' => 'CASCADE',
                ]],
                'targetEntity' => $classMetadata->getReflectionClass()?->getMethod('getTranslatableEntityClass')->invoke(null),
            ]);
        }

        $name = $classMetadata->getTableName().'_unique_translation';
        if (!$this->hasUniqueTranslationConstraint($classMetadata, $name)) {
            $classMetadata->table['uniqueConstraints'][$name] = [
                'columns' => ['translatable_id', 'locale'],
            ];
        }

        if (!$classMetadata->hasField('locale') && !$classMetadata->hasAssociation('locale')) {
            $classMetadata->mapField([
                'fieldName' => 'locale',
                'type' => 'string',
            ]);
        }
    }

    // Convert string FETCH mode to required string.
    private function convertFetchString(string|int $fetchMode): int
    {
        if (\is_int($fetchMode)) {
            return $fetchMode;
        }

        $fetchMode = strtoupper($fetchMode);

        return match ($fetchMode) {
            'EAGER' => ClassMetadataInfo::FETCH_EAGER,
            'EXTRA_LAZY' => ClassMetadataInfo::FETCH_EXTRA_LAZY,
            default => ClassMetadataInfo::FETCH_LAZY,
        };
    }

    private function hasUniqueTranslationConstraint(ClassMetadata $classMetadata, string $name): bool
    {
        if (!isset($classMetadata->table['uniqueConstraints'])) {
            return false;
        }

        return isset($classMetadata->table['uniqueConstraints'][$name]);
    }

    // Checks if entity is translatable.
    private function isTranslatable(ClassMetadata $classMetadata): bool
    {
        return $this->getClassAnalyzer()->hasTrait($classMetadata->reflClass, $this->translatableTrait);
    }

    // Checks if entity is a translation.
    private function isTranslation(ClassMetadata $classMetadata): bool
    {
        return $this->getClassAnalyzer()->hasTrait($classMetadata->reflClass, $this->translationTrait);
    }

    private function setLocales(LifecycleEventArgs $eventArgs): void
    {
        $em = $eventArgs->getObjectManager();
        $entity = $eventArgs->getObject();
        $classMetadata = $em->getClassMetadata($entity::class);

        if (!$this->isTranslatable($classMetadata)) {
            return;
        }

        if ($locale = $this->getCurrentLocale()) {
            $entity->setCurrentLocale($locale);
        }

        if (($locale = $this->getDefaultLocale()) !== '' && ($locale = $this->getDefaultLocale()) !== '0') {
            $entity->setDefaultLocale($locale);
        }
    }

    private function getCurrentLocale(): ?string
    {
        return null !== $this->requestStack->getCurrentRequest() ? $this->requestStack->getCurrentRequest()->getLocale() : null;
    }

    private function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }
}
