<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

final readonly class LogoutSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private RouterInterface $router,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [LogoutEvent::class => 'onLogout'];
    }

    public function onLogout(LogoutEvent $event): void
    {
        $referer = $event->getRequest()->headers->get('referer');

        $route = null;

        if (null !== $referer) {
            $path = parse_url($referer, \PHP_URL_PATH);
            if ($path) {
                try {
                    $result = $this->router->match($path);
                    $route = $result['_route'] ?? null;
                } catch (ResourceNotFoundException) {
                    // do nothing
                }
            }
        }

        if (null !== $route && str_starts_with($route, 'arodax_admin_')) {
            $response = new RedirectResponse(
                $this->urlGenerator->generate('arodax_admin_security_login'),
                Response::HTTP_SEE_OTHER
            );

            $event->setResponse($response);
        }
    }
}
