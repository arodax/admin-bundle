<?php

/*
 * This file is part of the ceecr.cz package.
 *
 * (c) Exekutorská Komora České republiky
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;

final readonly class ViewFormListener implements EventSubscriberInterface
{
    public function onKernelView(ViewEvent $event): void
    {
        $data = $event->getControllerResult();

        if (!($data instanceof FormInterface)) {
            return;
        }

        if (!$data->isSubmitted()) {
            throw new BadRequestHttpException('The form is not submitted.');
        }

        if ($data->isValid()) {
            $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
                ->disableExceptionOnInvalidPropertyPath()
                ->getPropertyAccessor();

            if (is_object($data->getData())) {
                $id = $propertyAccessor->getValue($data->getData(), 'id');
                $event->setResponse(new JsonResponse(['id' => $id], Response::HTTP_OK, [], false));
            } else {
                $event->setResponse(new JsonResponse(null));
            }


            $event->stopPropagation();

            return;
        }

        $errors = [];
        foreach ($data->getErrors(true, true) as $error) {
            $errors[] = [
                'propertyPath' => $error->getOrigin()?->getName(),
                'fullPath' => $data->getName().'.'.$error->getOrigin()?->getName(),
                'title' => $error->getMessage(),
            ];
        }

        $event->setResponse(new JsonResponse(['violations' => $errors], Response::HTTP_BAD_REQUEST));
        $event->stopPropagation();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onKernelView', 1],
        ];
    }
}
