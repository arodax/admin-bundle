<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ApiResourceEventSubscriber implements EventSubscriberInterface
{
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if ($request->query->has('_download')) {
            $request->headers->set('Accept', 'text/csv');
        }
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if (\in_array('text/csv', $event->getRequest()->getAcceptableContentTypes())) {
            $event->getResponse()->headers->set('Content-Disposition', 'attachment; filename=download.csv');
            $event->getResponse()->headers->set('Content-Type', 'application/octet-stream');
        }
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::REQUEST => ['onKernelRequest', 8], KernelEvents::RESPONSE => ['onKernelResponse', -2]];
    }
}
