<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


declare(strict_types=1);

namespace Arodax\AdminBundle\Message\Grantys;

/**
 * This class represents a request to geocode a Grantys grant project entity.
 */
final readonly class GeocodeGrantProject
{
    public function __construct(
        private int $id
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }
}
