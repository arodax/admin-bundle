<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\Message\Eshop;

/**
 * Dispatched after order is created.
 */
final class OrderPaidMessage extends AbstractOrderMessage
{
}
