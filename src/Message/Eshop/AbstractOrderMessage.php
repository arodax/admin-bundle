<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Message\Eshop;

/**
 * This class is a base class for all Order related messages.
 */
abstract class AbstractOrderMessage
{
    public function __construct(
        /**
         * Id of the order.
         */
        private readonly int $orderId
    )
    {
    }

    /**
     * Get order id.
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }
}
