<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


declare(strict_types=1);

namespace Arodax\AdminBundle\Message\Translator;

/**
 * This class represents a request to translate an entity.
 */
final readonly class TranslateEntity
{
    public function __construct(
        private string $class,
        private int $id,
        private string $source,
    ) {
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }
}
