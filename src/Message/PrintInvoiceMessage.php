<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Message;

/**
 * This message contains the request to print the invoice.
 */
final readonly class PrintInvoiceMessage
{
    public function __construct(private int $invoiceId)
    {
    }

    public function getInvoiceId(): int
    {
        return $this->invoiceId;
    }
}
