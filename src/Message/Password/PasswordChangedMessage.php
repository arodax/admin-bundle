<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Message\Password;

/**
 * This message is sent when user password is changed.
 */
readonly class PasswordChangedMessage
{
    public function __construct(
        private int $userId,
        private ?int $updatedByUserId = null,
        private ?string $currentPasswordHash = null,
    ) {
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getUpdatedByUserId(): ?int
    {
        return $this->updatedByUserId;
    }

    public function getCurrentPasswordHash(): ?string
    {
        return $this->currentPasswordHash;
    }
}
