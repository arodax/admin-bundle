<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model\User;

use Symfony\Component\Security\Core\User\UserInterface as SymfonyUserInterface;

/**
 * This interface represents an user.
 */
interface UserInterface extends SymfonyUserInterface
{
    public function getEmail();
}
