<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model;

/**
 * This interface defines Doctrine ORM entities with tracking fields
 * for createdAt and updatedAt.
 */
interface TimestampableInterface
{
    /**
     * Get an entity creation DateTime.
     */
    public function getCreatedAt(): ?\DateTimeImmutable;

    /**
     * Set an entity creation DateTime.
     *
     * @param ?\DateTimeImmutable $cretedAt
     */
    public function setCreatedAt(?\DateTimeImmutable $cretedAt);

    /**
     * Get an entity update DateTime.
     */
    public function getUpdatedAt(): ?\DateTimeImmutable;

    /**
     * Set an entity update DateTime.
     *
     * @param ?\DateTimeImmutable $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?\DateTimeImmutable $updatedAt);
}
