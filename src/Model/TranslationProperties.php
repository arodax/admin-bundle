<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model;

/**
 * Translation trait.
 *
 * Should be used inside translation entity.
 */
trait TranslationProperties
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $locale;

    /**
     * Will be mapped to translatable entity
     * by TranslatableSubscriber.
     */
    protected $translatable;
}
