<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model\Finance;

use Arodax\AdminBundle\Entity\Finance\OrderItem;

interface InvoiceInterface
{
    public function getInvoiceNumber(): string;

    public function setInvoiceNumber(string $invoiceNumber): self;

    public function getOrderItems(): array;

    public function addOrderItem(OrderItem $item): self;

    public function removeOrderItem(OrderItem $item): self;
}
