<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model\Eshop;

/**
 * This interface represent an product.
 *
 * @since 6.0.0
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
interface ProductInterface
{
    public const string STATUS_DRAFT = 'draft';
    public const string STATUS_PUBLISHED = 'published';

    /**
     * Get identificator of the article.
     * Usually this should be unique generated ID, either UUID or auto increment.
     *
     * @return mixed
     */
    public function getId();

    /**
     * Get date of publication of the article.
     *
     * @return mixed
     */
    public function getPublishedAt(): ?\DateTimeInterface;

    /**
     * Set date of publication of the article.
     *
     * @return mixed
     */
    public function setPublishedAt(\DateTimeInterface $dateTime);

    /**
     * Get current status of the article.
     */
    public function getStatus(): string;

    /**
     * Set current status of the article.
     *
     * @return mixed
     */
    public function setStatus(string $status);
}
