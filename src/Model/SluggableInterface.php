<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model;

use Arodax\AdminBundle\EntityListener\SluggableListener;

/**
 * This interface describes an entity, which can be sluggale.
 *
 * @see SluggableListener for the Doctrine event listener, which autogenerate slugs for this interface.
 */
interface SluggableInterface
{
    public function getSlug(): ?string;

    /**
     * @return mixed
     */
    public function setSlug(?string $slug);
}
