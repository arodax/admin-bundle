<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Model\Content;

use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Model\User\UserInterface;

/**
 * This interface represent an article.
 *
 * @since 6.0.0
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
interface ArticleInterface
{
    public const string STATUS_DRAFT = 'draft';
    public const string STATUS_PUBLISHED = 'published';

    /**
     * Get identification of the article.
     * Usually this should be unique generated ID, either UUID or auto increment.
     *
     * @return mixed
     */
    public function getId();

    /**
     * Get authors of the article
     * It is possible to have multiple authors for every article.
     *
     * @return UserInterface[]
     */
    public function getAuthors(): array;

    /**
     * Add the author to the article.
     *
     * @return mixed
     */
    public function addAuthor(UserInterface $user);

    /**
     * Remove the author from the article.
     *
     * @return mixed
     */
    public function removeAuthor(UserInterface $user);

    /**
     * Get sections of the article.
     * It is possible for article to be in multiple sections at the same time.
     *
     * @return Section[]
     */
    public function getSections(): array;

    /**
     * Add the section to the article.
     *
     * @return mixed
     */
    public function addSection(Section $section);

    /**
     * Remove the section from the article.
     *
     * @return mixed
     */
    public function removeSection(Section $section);

    /**
     * Get the article covers
     * The return array might contain the array of URLs with the covers (photos, videos ...).
     *
     * @return mixed
     */
    public function getCovers(): ?array;

    /**
     * Set covers of the article.
     * The array contains usually URLs of the covers (photos, videos ...).
     *
     * @return mixed
     */
    public function setCovers(array $covers);

    /**
     * Get date of publication of the article.
     *
     * @return mixed
     */
    public function getPublishedAt(): ?\DateTimeInterface;

    /**
     * Set date of publication of the article.
     *
     * @return mixed
     */
    public function setPublishedAt(\DateTimeInterface $dateTime);

    /**
     * Get current status of the article.
     */
    public function getStatus(): string;

    /**
     * Set current status of the article.
     *
     * @return mixed
     */
    public function setStatus(string $status);
}
