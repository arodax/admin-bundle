<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Factory;

use Arodax\AdminBundle\Entity\User\User;

final class UserFactory
{
    public function createAdmin(string $email, string $password): User
    {
        $user = new User();
        $user->setDisplayName($email);
        $user->setEmail($email);
        $user->setColor('#fff');
        $user->setAvatar(null);
        $user->setPlainPassword($password);
        $user->setRoles(['ROLE_ADMIN', 'ROLE_ADMIN_RECIPIENT']);
        $user->setEnabled(true);

        return $user;
    }
}
