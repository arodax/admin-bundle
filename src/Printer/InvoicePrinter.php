<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\Printer;

use Arodax\AdminBundle\Model\Finance\InvoiceInterface;
use Arodax\AdminBundle\Utils\Media\PdfPrinter;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Filesystem\Filesystem;

readonly class InvoicePrinter
{
    public function __construct(
        private PdfPrinter $pdPrinter,
        #[Autowire(param: 'arodax_admin.invoices_dir')]
        private string $outputPath
    ) {
    }

    public function print(InvoiceInterface $invoice): void
    {
        $fs = new Filesystem();

        if (false === $fs->exists($this->outputPath)) {
            $fs->mkdir($this->outputPath);
        }

        $this->pdPrinter->printToFile('@ArodaxAdmin/invoice/template.pdf.twig', $this->outputPath.'/'.$invoice->getInvoiceNumber().'.pdf', [
            'invoice' => $invoice,
            'logo' => $this->getLogoDataUrl(),
        ]);
    }

    /**
     * Try ti find a logo in one of the following paths and return it as base 64 encoded string.
     */
    public function getLogoDataUrl(): string
    {
        $paths = [
            __DIR__.'/../../../../public/build/images/logo/invoice.svg',
            __DIR__.'/../../../../public/build/images/logo.svg',
            __DIR__.'/../../../../public/bundles/arodaxadmin/images/logo/invoice.svg',
        ];

        $content = '';

        foreach ($paths as $path) {
            if (file_exists($path)) {
                $content = file_get_contents($path);
                break;
            }
        }

        return 'data:image/svg;base64,'.base64_encode($content);
    }
}
