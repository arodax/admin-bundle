<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\Filter;

use ApiPlatform\Doctrine\Common\Filter\SearchFilterInterface;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Arodax\AdminBundle\I18n\LocalesUtility;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Exception\InvalidArgumentException;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryBuilderHelper;

/**
 * This filter for API platform handles search for nested entity translations.
 */
final class TranslationsSearchFilter extends AbstractFilter implements SearchFilterInterface
{

    protected const string TRANSLATION_COLLECTION = 'translations';
    protected const string TRANSLATION_LOCALE_FIELD = 'locale';
    protected const string LOCALE_WILDCARD = '*';

    public function __construct(
        ManagerRegistry $managerRegistry,
        private readonly LocalesUtility $localesUtility,
        LoggerInterface $logger = null,
        array $properties = null,
        NameConverterInterface $nameConverter = null,
    ) {
        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);
    }

    /**
     * @throws \ReflectionException
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
    {
        if (!preg_match('/^'.self::TRANSLATION_COLLECTION.'\.[\w\*]+\.[\w+]+$/', $property)) {
            return;
        }

        [$translationCollection, $locale, $translationProperty] = explode('.', $property);

        if (!$this->isTranslationsCollection($translationCollection)) {
            return;
        }

        if (self::LOCALE_WILDCARD !== $locale && !$this->isTranslationLocaleAvailable($locale)) {
            return;
        }

        if (!$this->isTranslationPropertyMapped($translationProperty, $resourceClass)) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];

        $metadata = $this->getClassMetadata($this->getTranslationClass($resourceClass));
        $values = $this->normalizeValues((array) $value, $translationProperty);

        if (null === $values) {
            return;
        }

        $caseSensitive = true;

        if (!$metadata->hasField($translationProperty)) {
            return;
        }

        if (!$this->hasValidValues($values, $this->getDoctrineFieldType($translationProperty, $resourceClass))) {
            $this->getLogger()->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $translationProperty)),
            ]);

            return;
        }

        $strategy = $this->properties[$translationProperty] ?? self::STRATEGY_EXACT;

        // prefixing the strategy with "i" makes it case-insensitive
        if (str_starts_with((string) $strategy, 'i')) {
            $strategy = substr((string) $strategy, 1);
            $caseSensitive = false;
        }

        $associationAlias = QueryBuilderHelper::addJoinOnce($queryBuilder, $queryNameGenerator, $alias, self::TRANSLATION_COLLECTION);

        if (1 === \count($values)) {
            $this->addAndWhereByStrategyWithLocale($strategy, $queryBuilder, $queryNameGenerator, $associationAlias, $translationProperty, $values[0], $caseSensitive, $locale);
        }
    }

    /**
     * Get descriptions for property filters mapping.
     *
     * @throws \ReflectionException
     */
    public function getDescription(string $resourceClass): array
    {
        $properties = $this->getProperties();
        $description = [];
        foreach ($properties as $property => $strategy) {
            if ($this->isTranslationPropertyMapped($property, $resourceClass)) {
                foreach ($this->localesUtility->getContentLocales() as $localeCode => $localeName) {
                    $description += $this->getFilterDescription($localeCode, $property, $strategy);
                    unset($localeName);
                }

                $description += $this->getFilterDescription(self::LOCALE_WILDCARD, $property, $strategy);
            }
        }

        return $description;
    }

    /**
     * Get the reflection class for entity translation.
     *
     * @throws \ReflectionException
     */
    protected function getTranslationsReflectionClass(string $resourceClass): \ReflectionClass
    {
        return new \ReflectionClass($this->getTranslationClass($resourceClass));
    }

    /**
     * Check if requested translation property is mapped.
     *
     * @param string $property      Translation property
     * @param string $resourceClass Translatable class
     *
     * @throws \ReflectionException
     */
    protected function isTranslationPropertyMapped(string $property, string $resourceClass): bool
    {
        return $this->getTranslationsReflectionClass($resourceClass)->hasProperty($property);
    }

    /**
     * Get name of the translation class.
     *
     * @throws \ReflectionException
     */
    protected function getTranslationClass(string $resourceClass): ?string
    {
        $translatableClass = new \ReflectionClass($resourceClass);

        if (!$translatableClass->hasMethod('getTranslationEntityClass')) {
            $this->getLogger()->notice('No translations entity associated', [
                'exception' => new InvalidArgumentException(sprintf('Class %s has no translations entity associated. Expected getTranslationEntityClass method, but there is not one.', $resourceClass)),
            ]);

            return null;
        }

        return $translatableClass->getMethod('getTranslationEntityClass')->invoke(null);
    }


    /**
     * Get description for the filter.
     *
     * @param string $locale   locale code
     * @param string $property property of the translation
     * @param string $strategy search strategy
     */
    protected function getFilterDescription(string $locale, string $property, string $strategy): array
    {
        return [
            sprintf('%s.%s.%s[%s]', self::TRANSLATION_COLLECTION, $locale, $property, $strategy) => [
                'property' => $property,
                'type' => 'string',
                'required' => false,
            ],
        ];
    }

    /**
     * Check if translations collection is requested.
     *
     * @param string $collection collection name
     */
    protected function isTranslationsCollection(string $collection): bool
    {
        if (self::TRANSLATION_COLLECTION !== $collection) {
            $this->getLogger()->notice('Translation filter used on non invalid collection', [
                'exception' => new InvalidArgumentException(sprintf('Translation filter was used on invalid collection. Expected translations but %s was requested.', $collection)),
            ]);

            return false;
        }

        return true;
    }

    /**
     * Check if requested locale is available for translations.
     *
     * @param string $locale locale code
     */
    protected function isTranslationLocaleAvailable(string $locale): bool
    {
        if (!array_key_exists($locale, $this->localesUtility->getContentLocales())) {
            $this->getLogger()->notice('Invalid locale requested', [
                'exception' => new InvalidArgumentException(sprintf('Invalid locale %s requested for translations. Only %s are available.', $locale, implode(', ', array_keys($this->localesUtility->getContentLocales())))),
            ]);

            return false;
        }

        return true;
    }

    /**
     * Normalize the values array.
     */
    protected function normalizeValues(array $values, string $property): ?array
    {
        foreach ($values as $key => $value) {
            if (!\is_string($value)) {
                unset($values[$key]);
            }
        }

        if ($values === []) {
            $this->getLogger()->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('At least one value is required for property %s.', $property)),
            ]);

            return null;
        }

        return array_values($values);
    }

    /**
     * When the field should be an integer, check that the given value is a valid one.
     *
     * @param mixed|null $type
     */
    protected function hasValidValues(array $values, $type = null): bool
    {
        foreach ($values as $key => $value) {
            if (null !== $value && \in_array($type, (array) Types::INTEGER, true) && false === filter_var($value, \FILTER_VALIDATE_INT)) {
                return false;
            }
        }

        return true;
    }

    protected function addAndWhereByStrategyWithLocale(string $strategy, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, mixed $value, bool $caseSensitive, string $locale): void
    {
        $wrapCase = $this->createWrapCase($caseSensitive);
        $valueParameter = $queryNameGenerator->generateParameterName($field);
        $localeParameter = $queryNameGenerator->generateParameterName($locale);

        switch ($strategy) {
            case null:
            case self::STRATEGY_EXACT:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                if (self::LOCALE_WILDCARD !== $locale) {
                    $queryBuilder->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, self::TRANSLATION_LOCALE_FIELD, $localeParameter))
                        ->setParameter($localeParameter, $locale);
                }
                break;
            case self::STRATEGY_PARTIAL:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(\'%%\', :%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                if (self::LOCALE_WILDCARD !== $locale) {
                    $queryBuilder->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, self::TRANSLATION_LOCALE_FIELD, $localeParameter))
                        ->setParameter($localeParameter, $locale);
                }
                break;
            case self::STRATEGY_START:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(:%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                if (self::LOCALE_WILDCARD !== $locale) {
                    $queryBuilder->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, self::TRANSLATION_LOCALE_FIELD, $localeParameter))
                        ->setParameter($localeParameter, $locale);
                }
                break;
            case self::STRATEGY_END:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(\'%%\', :%s)'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                if (self::LOCALE_WILDCARD !== $locale) {
                    $queryBuilder->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, self::TRANSLATION_LOCALE_FIELD, $localeParameter))
                        ->setParameter($localeParameter, $locale);
                }
                break;
            case self::STRATEGY_WORD_START:
                $queryBuilder
                    ->andWhere(sprintf($wrapCase('%1$s.%2$s').' LIKE '.$wrapCase('CONCAT(:%3$s, \'%%\')').' OR '.$wrapCase('%1$s.%2$s').' LIKE '.$wrapCase('CONCAT(\'%% \', :%3$s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                if (self::LOCALE_WILDCARD !== $locale) {
                    $queryBuilder->andWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, self::TRANSLATION_LOCALE_FIELD, $localeParameter))
                        ->setParameter($localeParameter, $locale);
                }
                break;
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    /**
     * Creates a function that will wrap a Doctrine expression according to the
     * specified case sensitivity.
     *
     * For example, "o.name" will get wrapped into "LOWER(o.name)" when $caseSensitive
     * is false.
     */
    protected function createWrapCase(bool $caseSensitive): \Closure
    {
        return static function (string $expr) use ($caseSensitive): string {
            if ($caseSensitive) {
                return $expr;
            }

            return sprintf('LOWER(%s)', $expr);
        };
    }
}
