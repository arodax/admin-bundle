<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\Filter;

use Arodax\AdminBundle\Doctrine\Annotation\UserAware;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use ReflectionAttribute;

/**
 * Doctrine filter for querying user's entities only, using PHP native attributes.
 */
class UserAwareFilter extends SQLFilter
{
    /**
     * Add filter constraint to query only entities related to a specific user.
     *
     * @param ClassMetadata $targetEntity The target entity metadata.
     * @param string $targetTableAlias The alias for the target entity table in the query.
     * @return string The SQL condition to add to the query or an empty string if not applicable.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        $reflectionClass = $targetEntity->getReflectionClass();
        $attributes = $reflectionClass->getAttributes(UserAware::class, ReflectionAttribute::IS_INSTANCEOF);

        if (count($attributes) === 0) {
            return '';
        }

        $attributeInstance = $attributes[0]->newInstance();
        $fieldName = $attributeInstance->userFieldName;

        try {
            $userId = $this->getParameter('id');
        } catch (\InvalidArgumentException) {
            return '';
        }

        if (empty($fieldName) || empty($userId)) {
            return '';
        }

        return sprintf('%s.%s = %s', $targetTableAlias, $fieldName, $userId);
    }
}
