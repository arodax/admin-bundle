<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\Filter;

use ApiPlatform\Doctrine\Common\Filter\OrderFilterInterface;
use ApiPlatform\Doctrine\Common\Filter\OrderFilterTrait;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Arodax\AdminBundle\I18n\LocalesUtility;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class TranslatableOrderFilter extends AbstractFilter implements OrderFilterInterface
{
    use OrderFilterTrait;

    public function __construct(
        private LocalesUtility $localesUtility,
        ManagerRegistry $managerRegistry,
        private readonly RequestStack $requestStack,
        string $orderParameterName = 'order',
        ?LoggerInterface $logger = null,
        ?array $properties = null,
        ?NameConverterInterface  $nameConverter = null,
        private readonly ?string $orderNullsComparison = null
    ) {
        if (null !== $properties) {
            $properties = array_map(static function ($propertyOptions) {
                // shorthand for default direction
                if (\is_string($propertyOptions)) {
                    $propertyOptions = [
                        'default_direction' => $propertyOptions,
                    ];
                }

                return $propertyOptions;
            }, $properties);
        }

        parent::__construct($managerRegistry, $logger, $properties, $nameConverter);

        $this->orderParameterName = $orderParameterName;
    }

    /**
     * Applies the filter.
     *
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param Operation|null $operation
     * @param array $context
     * @return void
     * @throws \ReflectionException
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {

        if (isset($context['filters']) && !isset($context['filters'][$this->orderParameterName])) {
            return;
        }

        if (!isset($context['filters'][$this->orderParameterName]) || !\is_array($context['filters'][$this->orderParameterName])) {
            parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operation, $context);

            return;
        }

        foreach ($context['filters'][$this->orderParameterName] as $property => $value) {
            $this->filterProperty($this->denormalizePropertyName($property), $value, $queryBuilder, $queryNameGenerator, $resourceClass, $operation, $context);
        }
    }

    /**
     * Passes a property through the filter.
     *
     * @param string $property
     * @param $value
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param Operation|null $operation
     * @param array $context
     * @return void
     * @throws \ReflectionException
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        if (!$this->isPropertyEnabled($property, $resourceClass)) {
            return;
        }

        $value = $this->normalizeValue($value, $property);

        if (null === $value) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $field = $property;

        if ($this->isPropertyNested($property, $resourceClass)) {
            [$alias, $field] = $this->addJoinsForNestedProperty($property, $alias, $queryBuilder, $queryNameGenerator, $resourceClass, Join::LEFT_JOIN);
        }

        if (null !== $nullsComparison = $this->properties[$property]['nulls_comparison'] ?? $this->orderNullsComparison) {
            $nullsDirection = self::NULLS_DIRECTION_MAP[$nullsComparison][$value];

            $nullRankHiddenField = sprintf('_%s_%s_null_rank', $alias, str_replace('.', '_', $field));

            $queryBuilder->addSelect(sprintf('CASE WHEN %s.%s IS NULL THEN 0 ELSE 1 END AS HIDDEN %s', $alias, $field, $nullRankHiddenField));
            $queryBuilder->addOrderBy($nullRankHiddenField, $nullsDirection);
        }


        if ($this->isPropertyTranslation($property, $resourceClass))  {
            $translationAlias = $queryNameGenerator->generateJoinAlias('t');
            $queryBuilder->leftJoin(sprintf('%s.translations', $alias), $translationAlias, Join::WITH, sprintf('%s.locale = :locale', $translationAlias));
            $queryBuilder->setParameter(key: 'locale', value: $this->requestStack->getCurrentRequest()->query->get(key: 'locale', default: 'cs'));
            $queryBuilder->addOrderBy(sprintf('%s.%s', $translationAlias, $field), $value);
        } else {
            $queryBuilder->addOrderBy(sprintf('%s.%s', $alias, $field), $value);
        }
    }

    /**
     * Get name of the translation class.
     *
     * @throws \ReflectionException
     */
    protected function getTranslationClass(string $resourceClass): ?string
    {
        $translatableClass = new \ReflectionClass($resourceClass);

        if (!$translatableClass->hasMethod('getTranslationEntityClass')) {
            $this->getLogger()->notice('No translations entity associated', [
                'exception' => new InvalidArgumentException(sprintf('Class %s has no translations entity associated. Expected getTranslationEntityClass method, but there is not one.', $resourceClass)),
            ]);

            return null;
        }

        return $translatableClass->getMethod('getTranslationEntityClass')->invoke(null);
    }

    /**
     * Get the reflection class for entity translation.
     *
     * @throws \ReflectionException
     */
    protected function getTranslationsReflectionClass(string $resourceClass): \ReflectionClass
    {
        return new \ReflectionClass($this->getTranslationClass($resourceClass));
    }

    /**
     * Check if requested translation property is mapped.
     *
     * @param string $property      Translation property
     * @param string $resourceClass Translatable class
     *
     * @throws \ReflectionException
     */
    protected function isTranslationPropertyMapped(string $property, string $resourceClass): bool
    {
        return $this->getTranslationsReflectionClass($resourceClass)->hasProperty($property);
    }

    /**
     * Check if this property is enabled or not.
     *
     * @param string $property
     * @param string $resourceClass
     * @return bool
     * @throws \ReflectionException
     */
    protected function isPropertyEnabled(string $property, string $resourceClass): bool
    {
        if (null === $this->properties) {
            // to ensure sanity, nested properties must still be explicitly enabled
            return !$this->isPropertyNested($property, $resourceClass);
        }

        if (true === \array_key_exists($property, $this->properties)) {
            return true;
        }

        $translationPropertyName = 'translations.*.'.$property;

        if (true === \array_key_exists($translationPropertyName, $this->properties)) {

            if ($this->getTranslationsReflectionClass($resourceClass)->hasProperty($property)) {
                return true;
            }


            return false;
        }

        return false;
    }

    /**
     * Check if property is nested
     *
     * @param string $property
     * @param string $resourceClass
     * @return bool
     */
    protected function isPropertyNested(string $property, string $resourceClass): bool
    {
        $pos = strpos($property, '.');

        if (false === $pos) {
            return false;
        }

        return $this->getClassMetadata($resourceClass)->hasAssociation(substr($property, 0, $pos));
    }

    /**
     * Check if property is translation property.
     *
     * @throws \ReflectionException
     */
    protected function isPropertyTranslation(string $property, string $resourceClass): bool
    {
        $translationPropertyName = 'translations.*.'.$property;

        if (true === \array_key_exists($translationPropertyName, $this->properties)) {

            if ($this->getTranslationsReflectionClass($resourceClass)->hasProperty($property)) {
                return true;
            }


            return false;
        }

        return false;
    }
}
