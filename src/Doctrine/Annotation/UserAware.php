<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\Annotation;

use Attribute;

/**
 * This attribute allows marking the class as user-aware.
 * It indicates which field in the class represents the user.
 */
#[Attribute(Attribute::TARGET_CLASS)]
final class UserAware
{
    /**
     * Declares the field name which holds information about the user.
     */
    public function __construct(public string $userFieldName)
    {
    }
}
