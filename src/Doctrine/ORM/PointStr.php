<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\ORM;

use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Query\SqlWalker;

/**
 * POINT_STR function for querying using Point objects as parameters.
 *
 * @example: POINT_STR(:param) where param should be mapped to $point where $point is Point
 * without any special typing provided (eg. so that it gets converted to string)
 *
 * $query = $em->createQuery('SELECT e FROM ExampleEntity e WHERE e.somePoint = POINT_STR(:point)');
 * $point = new Point(1, 2);
 * $query->setParameter('point', $point);
 * $query->execute();
 */
class PointStr extends FunctionNode
{
    private Node|string|null $arg = null;

    /**
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return 'GeomFromText('.$this->arg->dispatch($sqlWalker).')';
    }

    /**
     * @throws QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->arg = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
