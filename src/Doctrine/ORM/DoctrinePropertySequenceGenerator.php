<?php

declare(strict_types=1);

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arodax\AdminBundle\Doctrine\ORM;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use function Symfony\Component\String\u;

final readonly class DoctrinePropertySequenceGenerator
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * Generate the sequence number increased by 1 from the path given in the property path of the entity.
     *
     * This generator is usefully for generating custom "AUTO INCREMENT" values for entities.
     *
     * @param string $entityClass The FQCN of the entity
     * @param string $propertyPath The property path which will be used for fetching max number. The default is "number".
     * @param int|null $length The required length of the generated sequence number. This will pad "0" between prefix and the number.
     * @param bool $resetOnPrefixChange Whether to reset the sequence number when the prefix changes
     *
     * @throws NonUniqueResultException when non-unique result is found, but this should never happen
     */
    public function generate(
        string $entityClass,
        string $propertyPath = 'number',
        string $prefix = null,
        int $length = null,
        bool $resetOnPrefixChange = false
    ): string
    {
        $qb = $this->entityManager->createQueryBuilder();

        $entity = $qb
            ->select('entity')
            ->from($entityClass, 'entity')
            ->orderBy(sprintf('entity.%s', $propertyPath), 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $propertyAccessor = new PropertyAccessor();
        $latestNumber = $entity !== null ? (string)$propertyAccessor->getValue($entity, $propertyPath) : '0';

        // Here we calculate the prefix length, falling back to 0 if it's null or empty
        $prefixLength = $prefix !== null && $prefix !== '' ? strlen($prefix) : 0;

        if ('0' !== $latestNumber && $prefixLength > 0) {
            // Convert $latestNumber to a UnicodeString object for manipulation
            $latestNumberUnicodeString = u($latestNumber);

            // Assume the prefix is the first part of the latestNumber based on the prefix length
            $existingPrefix = $latestNumberUnicodeString->slice(0, $prefixLength)->toString();

            // The numeric part starts after the prefix
            $numericPart = $latestNumberUnicodeString->slice($prefixLength)->trimStart('0');

            // Check if the current prefix is different from the existing one and if we need to reset
            if ($resetOnPrefixChange && $prefix !== $existingPrefix) {
                // Reset sequence number since the prefix has changed
                $latestNumber = '1';
            } else {
                // Increment the sequence number
                $latestNumber = (string)((int)$numericPart->toString() + 1);
            }
        } else {
            // This handles cases where there's either no previous number or no prefix provided, resetting to '1'
            $latestNumber = '1';
        }

        // Padding logic remains the same
        if (null !== $length) {
            $padLength = $length - $prefixLength - strlen($latestNumber);
            $latestNumber = str_pad($latestNumber, $padLength + strlen($latestNumber), '0', STR_PAD_LEFT);
        }

        // Ensure the final length does not exceed the specified length
        if (null !== $length && strlen($prefix . $latestNumber) > $length) {
            $latestNumber = substr($latestNumber, -($length - strlen($prefix)));
        }

        return $prefix . $latestNumber;
    }
}
