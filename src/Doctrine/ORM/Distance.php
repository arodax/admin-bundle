<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\ORM;

use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Query\SqlWalker;

/**
 * DQL function for calculating distances between two points.
 *
 * This class allows us to use a new DQL function called DISTANCE to calculate distances between points.
 * It’s converted into SQL as GLength(LineString(arg1, arg2)) because it achieves the same result as the SQL DISTANCE function, which for some reason doesn’t seem to function in MySQL.
 * Using this function is simple, for example to get all entities within a certain distance of another point:
 *
 * @example: SELECT e FROM ExampleEntity e WHERE DISTANCE(e.somePoint, POINT_STR(:point)) < 5
 */
class Distance extends FunctionNode
{
    private Node|string|null $firstArg = null;

    private Node|string|null $secondArg = null;

    /**
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        return 'GLength(LineString('.$this->firstArg->dispatch($sqlWalker).', '.$this->secondArg->dispatch($sqlWalker).'))';
    }

    /**
     * @throws QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->firstArg = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->secondArg = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
