<?php

/*

 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Doctrine\Migrations;

use Doctrine\Migrations\Version\Comparator;
use Doctrine\Migrations\Version\Version;

final readonly class VersionComparator implements Comparator
{
    public function compare(Version $a, Version $b) : int
    {
        // Extract the version numbers from the class names
        $versionNumberA = $this->extractVersionNumber((string)$a);
        $versionNumberB = $this->extractVersionNumber((string)$b);

        // Convert the version numbers to semantic version strings
        $versionStringA = $this->convertToVersionString($versionNumberA);
        $versionStringB = $this->convertToVersionString($versionNumberB);

        // Use PHP's version_compare function to compare the versions
        return version_compare($versionStringA, $versionStringB);
    }

    private function extractVersionNumber(string $className) : string
    {
        // Extract the version number from the class name
        $parts = explode('Version', $className);
        return end($parts);
    }

    private function convertToVersionString(string $versionNumber) : string
    {
        // Convert the version number to a semantic version string
        // Major version is always "6", minor version is variable, patch version is "0"
        return '6.' . substr($versionNumber, 0, -1) . '.0';
    }
}
