<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version6550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration from ^6.54 to 6.55.0.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aa_grant_project CHANGE covers covers LONGTEXT DEFAULT NULL COMMENT "(DC2Type:json)"');
        $this->addSql('ALTER TABLE aa_grant_call CHANGE attachments attachments LONGTEXT DEFAULT NULL COMMENT "(DC2Type:json)", CHANGE covers covers LONGTEXT DEFAULT NULL COMMENT "(DC2Type:json)"');
        $this->addSql(sql: 'ALTER TABLE aa_grant_call_translation ADD application_deadline VARCHAR(191) DEFAULT NULL');
        $this->addSql(sql: 'ALTER TABLE aa_grant_project_group CHANGE covers covers LONGTEXT DEFAULT NULL COMMENT "(DC2Type:json)"');
    }
}
