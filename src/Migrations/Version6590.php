<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version6590 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration from ^6.58 to 6.59.0.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(sql: 'CREATE TABLE aa_total_score_log (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT "(DC2Type:datetime_immutable)", value INT NOT NULL, INDEX IDX_4EEC7720A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE aa_total_score_log ADD CONSTRAINT FK_4EEC7720A76ED395 FOREIGN KEY (user_id) REFERENCES aa_user (id) ON DELETE CASCADE;');
        $this->addSql('CREATE TABLE aa_credit_log (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, type_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT "(DC2Type:datetime_immutable)", value INT NOT NULL, INDEX IDX_476E686BA76ED395 (user_id), INDEX IDX_476E686BC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE aa_credit_log ADD CONSTRAINT FK_476E686BA76ED395 FOREIGN KEY (user_id) REFERENCES aa_user (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE aa_credit_log ADD CONSTRAINT FK_476E686BC54C8C93 FOREIGN KEY (type_id) REFERENCES aa_credit_type (id) ON DELETE CASCADE;');
    }
}
