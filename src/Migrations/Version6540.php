<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version6540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration from ^6.53 to 6.54.0.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(sql: 'ALTER TABLE aa_grant_call_translation ADD lead LONGTEXT DEFAULT NULL');
    }
}
