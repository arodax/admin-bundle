<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version6570 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration from ^6.56 to 6.57.0.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(sql: 'ALTER TABLE aa_menu_item CHANGE icon icon LONGTEXT DEFAULT NULL');
        $this->addSql(sql: 'CREATE TABLE aa_user_password_log (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, updated_by_id INT NOT NULL, current_password_hash VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT "(DC2Type:datetime_immutable)", INDEX IDX_DA1CF714A76ED395 (user_id), INDEX IDX_DA1CF714896DBBDE (updated_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql(sql: 'ALTER TABLE aa_user_password_log ADD CONSTRAINT FK_DA1CF714A76ED395 FOREIGN KEY (user_id) REFERENCES aa_user (id) ON DELETE CASCADE');
        $this->addSql(sql: 'ALTER TABLE aa_user_password_log ADD CONSTRAINT FK_DA1CF714896DBBDE FOREIGN KEY (updated_by_id) REFERENCES aa_user (id) ON DELETE CASCADE');
    }
}
