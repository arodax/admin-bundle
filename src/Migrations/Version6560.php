<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version6560 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration from ^6.55 to 6.56.0.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(sql: 'CREATE TABLE aa_credit_type_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, locale VARCHAR(191) DEFAULT NULL, title VARCHAR(191) DEFAULT NULL, INDEX IDX_C4DABC1D2C2AC5D3 (translatable_id), UNIQUE INDEX aa_credit_type_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id))');
        $this->addSql(sql: 'ALTER TABLE aa_credit_type_translation ADD CONSTRAINT FK_C4DABC1D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES aa_credit_type (id) ON DELETE CASCADE;');
    }
}
