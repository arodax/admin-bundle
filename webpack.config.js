/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

const Encore = require('@symfony/webpack-encore')
const path = require('path')
const entries = require('./webpack.entrypoints')

Object.entries(entries).map((entry) => {
  Encore.addEntry(entry[0], path.resolve([__dirname, entry[1]].join('/')))
  return true
})

Encore.addStyleEntry('main', path.resolve([__dirname, 'assets/css/main.css'].join('/')))

Encore
  .cleanupOutputBeforeBuild()
  .enableVersioning(true)
  .enableSingleRuntimeChunk()
  .setPublicPath('/bundles/arodaxadmin/')
  .setManifestKeyPrefix('')
  .enableSourceMaps(!Encore.isProduction())
  .enableVueLoader(() => {}, { runtimeCompilerBuild: true })
  .enableTypeScriptLoader(function(typeScriptConfigOptions) {
    typeScriptConfigOptions.transpileOnly = true

  })
  .enableForkedTypeScriptTypesChecking()
  .copyFiles([{ from: [__dirname, '/assets/images'].join('/'), to: 'images/[path][name].[ext]' }
  ])
  .enableSassLoader(options => {
    options.implementation = require('sass')
  })
  .splitEntryChunks()
  .configureTerserPlugin((options) => {
    options.parallel = true
    options.terserOptions = {
      output: {
        comments: false
      }
    }
  })
  .configureFilenames({
    js: '[name].[contenthash].js',
    css: '[name].[contenthash].css',
    assets: '[name].[ext]'
  })

// dev
if (Encore.isProduction() === false) {
  Encore
    .setOutputPath([__dirname, '../../public/bundles/arodaxadmin'].join('/'))
    .enableVersioning(true)
}

// prod
if (Encore.isProduction() === true) {
  Encore
    .setOutputPath([__dirname, 'public'].join('/'))
    .enableVersioning(true)
}

module.exports = Encore.getWebpackConfig()
