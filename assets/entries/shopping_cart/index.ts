/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createVueApp } from '../../composables/vueAppFactory'
import Breadcrumb from '../../components/Breadcrumb.vue'
import ShoppingCartIndex from '../../views/ShoppingCartIndex.vue'

createVueApp([Breadcrumb, ShoppingCartIndex])
