/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createVueApp } from '../../composables/vueAppFactory'
import UserGroupIndex from '../../views/UserGroupIndex.vue'
import Breadcrumb from '../../components/Breadcrumb.vue'

createVueApp([Breadcrumb, UserGroupIndex])
