/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { crudPayload, crudParams } from '../interfaces/crudInterface'
import { PersistedStateOptions } from 'pinia-plugin-persistedstate'

interface DataTableOptions {
  id?: string;
}

declare module 'pinia' {
  interface DefineStoreOptionsBase<S extends StateTree, Store> {
    state?: S;
    actions?: Partial<Store>;
    persist?: boolean | PersistedStateOptions | PersistedStateOptions[];
  }

  interface PiniaCustomProperties {
    $hydrate: (opts?: { runHooks?: boolean; }) => void;
    $persist: () => void;
  }
}

/**
 * Create a new data table store for the given collection. The collection should
 * be API endpoint of the collection from the API platform which could be read.
 *
 * @param collection API endpoint of the collection
 * @param options Options for the data table store.
 */
export const useDataTableStore = (collection: string, options: DataTableOptions = {}) => defineStore(options.id ?? collection, {
  state: () => ({
    items: [] as any[],
    totalItems: 0,
    params: {
      page: 1,
      itemsPerPage: 50,
      order: {
        // never add anything here
      }
    },
    filters: {
      // never add anything here
    },
    menu: {
      filters: 0
    }
  }),

  actions: {
    /**
     * Create an item in the collection with the optional payload.
     *
     * @param endpoint
     * @param payload
     */
    async createItem (endpoint: string, payload: crudPayload = {}) {
      return await crud.createItem(endpoint, payload)
    },

    /**
     * Read a single item from the collection.
     * @param iri
     */
    async readItem (iri: string) {
      return await crud.readItem(iri)
    },

    /**
     * Read all items from collections collection of items from the collection.
     * This method is usually called when creating store ot fetch the store of all items
     * of the certain types.
     *
     * @param params
     */
    async readCollection (params: crudParams = {}) {
      const mergedParams = { ...this.params, ...params }
      const response = await crud.readCollection(collection, mergedParams)
      this.items = response.items
      this.totalItems = response.totalItems
    },

    /**
     * Delete an item from the collection.
     *
     * @param iri
     */
    async deleteItem (iri: string) {
      return await crud.deleteItem(iri)
    },

    /**
     * Update an item in the collection with the optional payload.
     *
     * @param iri
     * @param payload
     */
    async updateItem (iri: string, payload: crudPayload) {
      return await crud.updateItem(iri, payload)
    },

    /**
     * Append an item to the collection on the specific position. This usually
     * means inserting item into the tree to create another level of hierarchy.
     *
     * @param iri
     * @param payload
     */
    async appendItem (iri: string, payload: crudPayload) {
      return await crud.appendItem(iri, payload)
    },

    /**
     * Move an item in the tree to the specific position.
     *
     * @param iri
     * @param payload
     */
    async moveItemTree (iri: string, payload: crudPayload) {
      return await crud.moveItemTree(iri, payload)
    },

    /**
     * Set the filter parameters to filters for this collection. This method
     * is basically usefully only for saving state of the current filters and does
     * not filter results in the collection itself. For that, use the readCollection
     * with payload of params, which will filter the results by API resource filter
     * parameters.
     *
     * @param filter
     * @param value
     */
    setFilter (filter: string, value: any): void {
      // Create a shallow copy of the existing filters object
      this.filters = { ...this.filters, [filter]: value }
      this.logFilterQueryString()
    },
    /**
     * Set current filter in URL query string.
     */
    logFilterQueryString (): void {
      // Filter out entries with null values, empty arrays, or a 'status' of -1
      const filteredFilters = Object.entries(this.filters).reduce((acc, [key, value]) => {
        if (value !== null && !(Array.isArray(value) && value.length === 0) && !(key === 'status' && value === -1)) {
          acc[key] = value
        }
        return acc
      }, {})

      // Serialize the filtered filters object into a JSON string
      const filtersJson = JSON.stringify(filteredFilters)

      // Only append 'filters' parameter if there are any filters to show
      const queryParam = filtersJson !== '{}' ? `?filters=${(filtersJson)}` : ''

      // Build the new URL by appending or replacing the 'filters' parameter
      const newUrl = `${window.location.protocol}//${window.location.host}${window.location.pathname}${queryParam}`

      // Replace the current URL without adding a new history entry
      window.history.replaceState({ path: newUrl }, '', newUrl)
    },

    /**
     * Get the filter value from the store's stat. This is useful for getting
     * the filter value from the store and recreating form inputs with the
     * specific state.
     *
     * @param filter
     * @param defaultValue
     */
    getFilter (filter: string, defaultValue: any = null): any {
      // Now return the value from the store's state
      return typeof this.filters[filter] === 'undefined' ? defaultValue : this.filters[filter]
    },

    /**
     * Set param to the store. Params are usefully for filtering items or applying
     * any other logic to the collection. Do not confuse filters and params, since
     * filters only keep state of the filters to recreate saved filter state for
     * the form, while params are used to filter the collection itself.
     *
     * To find out how to set and use params, see the documentation of the API
     * resources of each collection.
     *
     * @param param
     * @param value
     */
    setParam (param: string, value: any): void {
      // Check if param is in the format 'xxx.id' and value is an array
      if (param.includes('.id') && Array.isArray(value)) {
        // Map each string in the array to the last numeric part of the string
        value = value.map(item => {
          if (typeof item === 'string') {
            const numericParts = item.match(/\d+/g)
            return numericParts ? numericParts[numericParts.length - 1] : item
          }
          return item
        })
      }

      // Create a shallow copy of the existing params object
      this.params = { ...this.params, [param]: value }
    },

    /**
     * Get the param value from the store's stat. This is useful for getting
     * order of the data table. This method also convert data table sorting
     * to backend sorting format, which is different.
     *
     * @param order
     * @param direction
     */
    setOrder (order: string, direction: 'asc' | 'desc'): void {
      // Update the order property in the store's state with the new order and direction
      this.params.order = { [order]: direction }
    },

    /**
     * Get the current order sort for the data table.
     */
    getOrder () {
      return this.params.order
    },

    /**
     * Set number of pages per data table page.
     * @param itemsPerPage
     */
    setItemsPerPage (itemsPerPage: number): void {
      // Update the itemsPerPage property in the store's state with the new value
      this.params.itemsPerPage = itemsPerPage
    },

    /**
     * Get the number of items per page for the data table.
     */
    getItemsPerPage (): number {
      return this.params.itemsPerPage
    },

    /**
     * Set current page of the data table.
     * @param page
     */
    setPage (page: number): void {
      // Update the page property in the store's state with the new value
      this.params.page = page
    },

    /**
     * Get the current page of the data table.
     */
    getPage (): number {
      return this.params.page
    },

    /**
     * Find out whether the filter (or any other) menu is opened or not, this
     * is usefully to determine the state of Vuetify accordions.
     * @param menuItem
     */
    isMenuOpened (menuItem: string): number | undefined {
      return this.menu[menuItem]
    },

    /**
     * Set the menu state to the given value. This is usefully for saving the
     * state of Vuetify accordions. Read the core documentation for more info
     * of Vuetify accordions, since value should represent the index of the
     * accordion.
     *
     * @param menuItem
     * @param value
     */
    setMenu (menuItem: string, value: 0 | undefined): void {
      // Synchronize the store's state with the given value
      this.menu = { ...this.menu, [menuItem]: value }
    }
  },

  /**
   * Persist the state of the store in the session storage. This is usefully
   * for saving the state of the data table filters and params.
   */
  persist: {
    storage: sessionStorage,
    paths: ['filters', 'params', 'menu']
  }
})()
