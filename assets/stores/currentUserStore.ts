/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { User } from '../interfaces/user'

export const useCurrentUserStore = defineStore('currentUser', {
  state: () => ({
    user: {} as Partial<User>
  }),

  getters: {
    getAdmin (): Partial<User> {
      return this.user
    }
  },

  actions: {
    setAdmin (payload: Partial<User>) {
      this.user = payload
    },

    loadFromJson (payload: Partial<User>) {
      this.setAdmin(payload)
    }
  }
})
