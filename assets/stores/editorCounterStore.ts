/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// Import necessary functions from Pinia and your custom composable
import { defineStore } from 'pinia'
import { useLocalStorage } from '../composables/useLocalStorage'

// Define the interface for your store's state if needed
interface CounterState {
  simpleMode: boolean;
}

// Define and export the store
export const useEditorCounterStore = defineStore('editorCounter', {
  // Define the initial state of the store
  state: () => ({
    simpleMode: true as boolean
  }),
  // Define getters for the store state if necessary
  getters: {
    isSimpleMode (state: CounterState): boolean {
      return state.simpleMode
    }
  },
  // Define actions to modify the store state
  actions: {
    initializeMode () {
      // Use the custom useLocalStorage composable to load the simpleMode state
      const storedMode = useLocalStorage<boolean>('arodax_admin_editor_counter', true)
      this.simpleMode = storedMode.value
    },
    toggleMode () {
      // Toggle the simpleMode state and persist it using useLocalStorage
      const storedMode = useLocalStorage<boolean>('arodax_admin_editor_counter', this.simpleMode)
      storedMode.value = !storedMode.value // Toggle and save to LocalStorage
      this.simpleMode = storedMode.value // Update the store's state
    },
    setMode (newMode: boolean) {
      // Set the simpleMode state to a specific value and persist it
      const storedMode = useLocalStorage<boolean>('arodax_admin_editor_counter', this.simpleMode)
      storedMode.value = newMode // Save the new mode to LocalStorage
      this.simpleMode = newMode // Update the store's state
    }
  }
})
