/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { has, set, get, map, unset } from 'lodash'
import { apiErrorInterface } from '../interfaces/apiErrorInterface'

export const useErrorStore = defineStore('errors', {
  state: () => ({
    violations: []
  }),
  actions: {
    clearViolations () {
      this.violations = []
    },
    clearViolation (propertyPath: string) {
      unset(this.violations, propertyPath)
    },
    hasViolation (propertyPath: string): boolean {
      if (!propertyPath) {
        return false
      }

      return has(this.violations, propertyPath)
    },
    getViolation (propertyPath: string | []): string | null {
      if (!propertyPath) {
        return null
      }

      if (Array.isArray(propertyPath)) {
        let violation = null

        propertyPath.forEach((path) => {
          if (this.hasViolation(path)) {
            violation = get(this.violations, path)
          }
        })

        return violation
      } else {
        return get(this.violations, propertyPath)
      }
    },
    setViolation (message: string, propertyPath: string): void {
      if (!has(this.violations, propertyPath)) {
        set(this.violations, propertyPath, [])
      }

      this.violations[propertyPath].push(message)
    },
    buildViolations (response: apiErrorInterface): void {
      if (!response.data) {
        return
      }

      const violations: any = {}

      map(response.data.violations, (value) => {
        if (has(value, 'title')) {
          set(violations, value.propertyPath, value.title)
        }

        if (has(value, 'message')) {
          set(violations, value.propertyPath, value.message)
        }
      })

      this.violations = violations

      console.log(violations)
    }

  }
})
