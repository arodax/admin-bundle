/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { useLocalStorage } from '../composables/useLocalStorage'

type Width = 'fluid' | 'fixed';

interface ContainerWidthState {
  width: Width;
}

export const useContainerWidthStore = defineStore('containerWidth', {
  state: () => ({
    width: 'fluid' as Width
  }),
  getters: {
    widthName (this: ContainerWidthState) {
      return this.width
    }
  },
  actions: {
    initializeWidth () {
      const storedWidth = useLocalStorage<Width>('width', 'fluid')
      this.width = storedWidth.value
    },
    setWidth (newWidth: Width) {
      const storedWidth = useLocalStorage<Width>('width', 'fluid')
      storedWidth.value = newWidth
      this.width = newWidth
    }
  }
})
