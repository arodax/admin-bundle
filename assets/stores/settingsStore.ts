/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'

export const useSettingsStore = defineStore('settings', {
  state: () => ({
    settings: {}
  }),

  actions: {
    createItem (payload: object) {
      return crud.createItem('/api/settings', payload)
    },
    readItem (iri: string) {
      return crud.readItem(iri)
    },
    async readCollection () {
      this.settings = (await crud.readCollection('/api/settings?itemsPerPage=999')).items
    },
    deleteItem (iri: string) {
      return crud.deleteItem(iri)
    },
    updateItem (payload: object) {
      return crud.updateItem(payload['@id'], payload)
    }
  }
})
