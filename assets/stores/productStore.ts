/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { isEmpty, map } from 'lodash'
import { convertToIri } from '../utils/iri_converter'

interface ItemType {
  translations?: { [key: string]: any }
  [key: string]: any
}

export const useProductStore = (iri: string) => defineStore(iri, {
  state: () => ({
    item: null as ItemType | null
  }),

  actions: {

    getItem () {
      return this.item
    },

    setItem (item: ItemType) {
      this.item = item
    },

    async readItem () {
      this.item = null
      const response = await crud.readItem(iri)
      this.item = response.data
    },

    async updateItem () {
      if (this.item === null) {
        throw new Error('Item is null')
      }

      let item = this.item

      // Function to deep traverse and update the object
      const deepTraverseAndUpdate = (obj: ItemType): ItemType => {
        if (typeof obj !== 'object' || obj === null) {
          return obj
        }

        // Copy the current object, including 'translations' if present
        const newObj: ItemType = Array.isArray(obj) ? [] : {}
        if (Object.prototype.hasOwnProperty.call(obj, 'translations')) {
          newObj.newTranslations = { ...obj.translations }
        }

        for (const key in obj) {
          if (Object.prototype.hasOwnProperty.call(obj, key)) {
            newObj[key] = deepTraverseAndUpdate(obj[key])
          }
        }

        return newObj
      }

      // Perform deep traversal and update
      item = deepTraverseAndUpdate(item)

      if (!isEmpty(item.categories)) {
        item.categories = map(item.categories, (value: any) => {
          return convertToIri(value.id, 'product-categories') ? convertToIri(value.id, 'product-categories') : value
        })
      }

      item = JSON.parse(JSON.stringify(item), (k, v) => {
        if (k === 'ean13') {
          return v
        }
        return (typeof v === 'object' || isNaN(v)) ? v : parseInt(v)
      })

      return await crud.updateItem(iri, item)
    },

    async deleteItem () {
      return await crud.deleteItem(iri)
    }
  },

  persist: {
    storage: sessionStorage,
    paths: ['item']
  }
})()
