/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { isEmpty, map } from 'lodash'
import { convertToIri } from '../utils/iri_converter'

export const useArticleStore = (iri: string) => defineStore(iri, {
  state: () => ({
    item: null as any
  }),

  actions: {

    getItem () {
      return this.item
    },

    setItem (item: any) {
      this.item = item
    },

    async readItem () {
      this.item = null
      const response = await crud.readItem(iri)
      this.item = response.data
    },

    async updateItem () {
      const item = JSON.parse(JSON.stringify(this.item))
      item.newTranslations = item.translations

      if (!isEmpty(item.authors)) {
        item.authors = map(this.item.authors, (value: any) => {
          return convertToIri(value.id, 'users') ? convertToIri(value.id, 'users') : value
        })
      }


      if (!isEmpty(item.galleries)) {
        item.galleries = map(this.item.galleries, (value: any) => {
          return convertToIri(value.id, 'galleries') ? convertToIri(value.id, 'galleries') : value
        })
      }

      console.log(item)

      return await crud.updateItem(iri, item)
    },

    async deleteItem () {
      return await crud.deleteItem(iri)
    }
  },

  persist: {
    storage: sessionStorage,
    paths: ['item']
  }
})()
