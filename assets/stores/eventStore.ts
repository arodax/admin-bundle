/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { isEmpty, map } from 'lodash'
import { convertToIri } from '../utils/iri_converter'

export const useEventStore = (iri: string) => defineStore(iri, {
  state: () => ({
    item: null as any
  }),

  actions: {

    getItem () {
      return this.item
    },

    setItem (item: any) {
      this.item = item
    },

    async readItem () {
      this.item = null
      const response = await crud.readItem(iri)
      this.item = response.data
    },

    async updateItem () {
      const item = this.item
      item.newTranslations = item.translations

      if (!isEmpty(item.authors)) {
        item.authors = map(this.item.authors, (value: any) => {
          return convertToIri(value.id, 'users') ? convertToIri(value.id, 'users') : value
        })
      }

      if (!isEmpty(item.eventTypes)) {
        item.eventTypes = map(this.item.eventTypes, (value: any) => {
          return convertToIri(value.id, 'event-types') ? convertToIri(value.id, 'event-types') : value
        })
      }

      if (!isEmpty(item.securityUsers)) {
        item.securityUsers = map(this.item.securityUsers, (value: any) => {
          return convertToIri(value.id, 'event-acl-users') ? convertToIri(value.id, 'event-acl-users') : value
        })
      }

      if (!isEmpty(item.geographyPoints)) {
        item.geographyPoints = map(this.item.geographyPoints, (value: any) => {
          if (value.point === null) {
            value.point = {
              latitude: 0,
              longitude: 0
            }

            return value
          }

          const point = value.point
          if (point.latitude === '' || !point.latitude) {
            point.latitude = 0
          } else {
            point.latitude = parseFloat(point.latitude)
          }

          if (point.longitude === '' || !point.longitude) {
            point.longitude = 0
          } else {
            point.longitude = parseFloat(point.longitude)
          }

          value.point = point

          return value
        })
      }

      return await crud.updateItem(iri, item)
    },

    async deleteItem () {
      return await crud.deleteItem(iri)
    }
  },

  persist: {
    storage: sessionStorage,
    paths: ['item']
  }
})()
