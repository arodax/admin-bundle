/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { isEmpty } from 'lodash'
import { convertToIri } from '../utils/iri_converter'

export const useOrderStore = (iri: string) => defineStore(iri, {
  state: () => ({
    item: null as any
  }),

  actions: {

    getItem () {
      return this.item
    },

    setItem (item: any) {
      this.item = item
    },

    async readItem () {
      this.item = null
      const response = await crud.readItem(iri)
      this.item = response.data
    },

    async updateItem () {
      const item = JSON.parse(JSON.stringify(this.item))

      if (!isEmpty(item)) {
        if (item.shoppingCart) {
          item.shoppingCart = convertToIri(item.shoppingCart.id, 'shopping-carts') ? convertToIri(item.shoppingCart.id, 'shopping-carts') : item.shoppingCart
        }
      }

      return await crud.updateItem(iri, item)
    },

    async deleteItem () {
      return await crud.deleteItem(iri)
    }
  },

  persist: {
    storage: sessionStorage,
    paths: ['item']
  }
})()
