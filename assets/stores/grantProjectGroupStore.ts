/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { isEmpty } from 'lodash'
import { convertToIri } from '../utils/iri_converter'

export const useGrantProjectGroupStore = (iri: string) => defineStore(iri, {
  state: () => ({
    item: null as any
  }),

  actions: {

    getItem () {
      return this.item
    },

    setItem (item: any) {
      this.item = item
    },

    async readItem () {
      this.item = null
      const response = await crud.readItem(iri)
      this.item = response.data
    },

    async updateItem () {
      console.log(this.item)
      const item = JSON.parse(JSON.stringify(this.item))
      item.newTranslations = item.translations

      console.log(item)

      if (!isEmpty(item.grantCall)) {
        item.grantCall = convertToIri(item.grantCall.id, 'grant-calls') ? convertToIri(item.grantCall.id, 'grant-calls') : item.grantCall
      }

      return await crud.updateItem(iri, item)
    },

    async deleteItem () {
      return await crud.deleteItem(iri)
    }
  },

  persist: {
    storage: sessionStorage,
    paths: ['item']
  }
})()
