/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import Axios from '../composables/ajax'

export const useRolesStore = defineStore('roles', {
  state: () => ({
    roles: {}
  }),

  actions: {
    async readRoles () {
      const response = await Axios.get('/api/security/roles')
      this.roles = response.data
    }
  }
})
