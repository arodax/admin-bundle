/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { defineStore } from 'pinia'
import { crud } from '../composables/crud'
import { crudPayload, crudParams } from '../interfaces/crudInterface'

export const useUserGroupsStore = defineStore('userGroupsStore', {
  state: () => ({
    items: [] as any[],
    totalItems: 0,
    params: {
      page: 1,
      itemsPerPage: 50,
      order: {
        id: 'asc'
      }
    }
  }),

  actions: {
    async createItem (endpoint: string, payload: crudPayload) {
      return await crud.createItem(endpoint, payload)
    },

    async readItem (iri: string) {
      return await crud.readItem(iri)
    },

    async readCollection (params: crudParams = {}) {
      const mergedParams = { ...this.params, ...params }
      const response = await crud.readCollection('/api/user-groups', mergedParams)
      this.items = response.items
      this.totalItems = response.totalItems
    },

    async deleteItem (iri: string) {
      return await crud.deleteItem(iri)
    },

    async updateItem (iri: string, payload: crudPayload) {
      return await crud.updateItem(iri, payload)
    }
  }
})
