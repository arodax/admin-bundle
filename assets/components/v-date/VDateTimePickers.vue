<!--
  - This file is part of the ARODAX Admin package.
  -
  -  (c) ARODAX a.s.
  -
  -  For the full copyright and license information, please view the LICENSE
  -  file that was distributed with this source code.
  -->

<template>
  <v-row
    no-gutters
    class="ma-n2"
  >
    <!-- date picker -->
    <v-col
      v-show="showDatePicker"
      :cols="12"
      :md="6"
      class="pa-2"
    >
      <v-menu
        v-model="menu.date"
        :close-on-content-click="false"
        min-width="290px"
        :nudge-right="40"
        offset-y
      >
        <template #activator="{ on }">
          <v-text-field
            v-model="dateFormatted"
            outlined
            hide-details
            prepend-icon="event"
            :label="labelDate"
            :disabled="disabled"
            v-on="on"
          >
            <template #prepend>
              <v-tooltip bottom>
                <template #activator="{ on }">
                  <v-icon
                    :disabled="disabled"
                    v-on="on"
                  >
                    fa-light fa-fw fa-calendar-day
                  </v-icon>
                </template>
                <span>{{ dayName }}</span>
              </v-tooltip>
            </template>
          </v-text-field>
        </template>
        <v-date-picker
          v-model="date"
          scrollable
        />
      </v-menu>
    </v-col>

    <!-- time picker -->
    <v-col
      v-show="showTimePicker"
      :cols="12"
      :md="6"
      class="pa-2"
    >
      <v-menu
        v-model="menu.time"
        :close-on-content-click="false"
        min-width="290px"
        :nudge-right="40"
        offset-y
      >
        <template #activator="{ on }">
          <v-text-field
            v-model="timeFormatted"
            outlined
            hide-details
            prepend-icon="schedule"
            :label="labelTime"
            :disabled="disabled"
            v-on="on"
          >
            <template #prepend>
              <v-tooltip bottom>
                <template #activator="{ on }">
                  <v-icon
                    :disabled="disabled"
                    v-on="on"
                  >
                    fa-light fa-fw fa-calendar-clock
                  </v-icon>
                </template>
                <span>{{ $t('timezone') }}: {{ getTimeZone() }}</span>
              </v-tooltip>
            </template>
          </v-text-field>
        </template>
        <v-time-picker
          v-model="time"
          scrollable
          :format="timePickerHoursFormat"
        />
      </v-menu>
    </v-col>
  </v-row>
</template>

<script>
import { DateTime } from 'luxon'
import isEmpty from 'lodash/isEmpty'
import split from 'lodash/split'
import { useCurrentUserStore } from '../../stores/currentUserStore'

export default {
  name: 'VDateTimePickers',
  props: {
    status: {
      type: String,
      default: null
    },
    value: {
      default: null,
      required: false
    },
    labelDate: {
      type: String,
      required: false,
      default: null
    },
    labelTime: {
      type: String,
      required: false,
      default: null
    },
    disabled: {
      type: Boolean,
      default: false
    },
    showDatePicker: {
      type: Boolean,
      required: false,
      default: true
    },
    showTimePicker: {
      type: Boolean,
      required: false,
      default: true
    }
  },
  data: () => {
    return {
      menu: {
        time: false,
        date: false,
        timezone: false
      },
      values: {
        date: null,
        time: null
      }
    }
  },
  computed: {
    admin: {
      get () {
        const currentUserStore = useCurrentUserStore()
        return currentUserStore.getAdmin
      },
      set (value) {
        const currentUserStore = useCurrentUserStore()
        currentUserStore.setAdmin(value)
      }
    },
    date: {
      get () {
        // v-date-picker component expected date in ISO format
        return this.parseDateTime(this.value, this.getDateFormat('yyyy-MM-dd'))
      },
      set (value) {
        // v-date-picker always return yyyy-MM-dd ISO date format
        this.values.date = value
      }
    },
    dateFormatted: {
      get () {
        // display date in user preferred format
        return this.parseDateTime(this.value, this.getDateFormat(), this.getTimeZone())
      },
      set (value) {
        // parse date in preferred user format to ISO date format YYYY-MM-dd
        const date = DateTime.fromFormat(value, this.getDateFormat(), { locale: this.locale })
        this.values.date = !date.invalid ? date.toFormat('yyyy-MM-dd') : null
      }
    },
    time: {
      get () {
        // v-time-picker component expected time in 24h format HH:mm
        return this.parseDateTime(this.value, this.getTimeFormat('HH:mm'))
      },
      set (value) {
        // v-time-picker always return 24hour formatted date in HH:mm
        this.values.time = value
      }
    },
    timeFormatted: {
      get () {
        // display time in user preferred format
        return this.parseDateTime(this.value, this.getTimeFormat())
      },
      set (value) {
        // parse date in preferred user format to 24hrs HH:mm format
        const time = DateTime.fromFormat(value, this.getTimeFormat(), { locale: this.locale })
        this.values.time = !time.invalid ? time.toFormat('HH:mm') : null
      }
    },
    /**
       * Decide whether the time picker will be displayed in AMPM or 24hours mode
       * @return {String} format for the TimePicker
       */
    timePickerHoursFormat () {
      return this.admin.preferredTimeFormat === 'hh:mma' ? 'ampm' : '24hr'
    },
    /**
       * Get user's preffered locale either from user configuration or from the store
       * @return {String} locale
       */
    locale () {
      return !isEmpty(this.admin.preferredLocale) ? this.admin.preferredLocale : document.documentElement.lang
    },
    /**
       * Display day name
       * @return {String} Localized name of the day
       */
    dayName () {
      return this.parseDateTime(this.value, 'EEEE')
    }
  },
  watch: {
    /**
       * Return computed value for v-model
       * If value is not null the UTC zone will be set.
       * @param value
       */
    values: {
      deep: true,
      handler (values) {
        // we need at least something
        if (isEmpty(values.time) && isEmpty(values.date)) {
          return
        }

        // emit crated date back to parent component
        this.$emit('input', this.createDateTime())
      }
    },
    showTimePicker: {
      handler: function (val) {
        if (val === false) {
          this.time = '00:00'
        }
      }
    }
  },
  methods: {
    /**
       * Try to parse dateTime string and return the formatted string
       * @param {String} dateTime ISO formatted dateTime string
       * @param {String|null} tokens for the date output, if empty token is given full ISO date will be returned
       * @param {String|null} timezone
       */
    parseDateTime (dateTime, tokens = null, timezone = null) {
      let dt = DateTime.fromISO(dateTime)

      // check for dt validity, if no valid date found return null and log error
      if (dt.invalid) {
        return null
      }

      dt = dt.setZone(this.getTimeZone(timezone))
      dt = dt.setLocale(this.locale)

      return tokens ? dt.toFormat(tokens) : dt.toISO()
    },
    /**
       * Decide which timezone to use
       * @param {String|null} timezone override
       * @return {String} timezone
       *
       */
    getTimeZone (timezone = null) {
      const utc = 'utc'

      if (isEmpty(this.admin.timezone) && isEmpty(timezone)) {
        return utc
      }

      if (isEmpty(this.admin.timezone) && !isEmpty(timezone)) {
        return timezone
      }

      if (!isEmpty(this.admin.timezone) && isEmpty(timezone)) {
        return this.admin.timezone
      }

      if (!isEmpty(this.admin.timezone) && !isEmpty(timezone)) {
        return timezone
      }

      return utc
    },
    /**
       * Decide which date format to use
       * @param {String|null} format overrie
       * @return {String} format
       */
    getDateFormat (format = null) {
      const iso = 'yyyy-MM-dd'

      if (isEmpty(this.admin.preferredDateFormat) && isEmpty(format)) {
        return iso
      }

      if (isEmpty(this.admin.preferredDateFormat) && !isEmpty(format)) {
        return format
      }

      if (!isEmpty(this.admin.preferredDateFormat) && isEmpty(format)) {
        return this.admin.preferredDateFormat
      }

      if (!isEmpty(this.admin.preferredDateFormat) && !isEmpty(format)) {
        return format
      }

      return iso
    },
    /**
       * Decide which time format to use
       * @param {String|null} format overrie
       * @return {String} format
       */
    getTimeFormat (format = null) {
      const hrs24 = 'HH:mm'

      if (isEmpty(this.admin.preferredTimeFormat) && isEmpty(format)) {
        return hrs24
      }

      if (isEmpty(this.admin.preferredTimeFormat) && !isEmpty(format)) {
        return format
      }

      if (!isEmpty(this.admin.preferredTimeFormat) && isEmpty(format)) {
        return this.admin.preferredTimeFormat
      }

      if (!isEmpty(this.admin.preferredTimeFormat) && !isEmpty(format)) {
        return format
      }

      return hrs24
    },
    createDateTime () {
      let dt = null
      // find the date part, either from the new picked value or the original one from v-model
      if (!isEmpty(this.values.date)) {
        dt = DateTime.fromFormat(this.values.date, 'yyyy-MM-dd', { locale: this.locale, zone: this.getTimeZone() })
      } else if (!isEmpty(this.value)) {
        dt = DateTime.fromISO(this.value, { locale: this.locale, zone: 'utc' }).setZone(this.getTimeZone())
      }

      // if date is invalid or null nothing much to do
      if (isEmpty(dt) || dt.invalid) {
        return
      }

      // set the time part only if we picked some new time here
      if (!isEmpty(this.values.time)) {
        const HHmm = split(this.values.time, ':', 2)
        dt = dt.set({ hours: HHmm[0] ? HHmm[0] : 0, minutes: HHmm[1] ? HHmm[1] : 0 })
      }

      // covert datetime to UTC
      return dt.setZone('utc').toISO()
    }
  }
}
</script>
