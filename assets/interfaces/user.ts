/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UserGroup } from './userGroup'

export interface User {
    id: number;
    email: string | null;
    displayName: string | null;
    firstName: string | null;
    lastName: string | null;
    avatar: string | null;
    color: string;
    roles: string[];
    extendedRoles: string[];
    userContacts: any[];
    createdAt: string;
    updatedAt: string;
    userGroups: UserGroup[];
    enabled: boolean;
    lastActivityAt: string | null; // Type adjusted to allow null
    timezone: string | null;
    preferredDateFormat: string | null;
    preferredTimeFormat: string | null;
    preferredLocale: string | null;
    preferredHome: string | null;
    totalScore: number | null;
}
