/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
interface crudPayload {
  [key: string]: any;
}
interface crudParams {
  [key: string]: any;
}
interface CollectionResponse {
  items: any[];
  totalItems: number;
}
interface crudInterface {
  createItem(endpoint: string, payload: crudPayload): Promise<any>;

  readItem(iri: string): Promise<any>;

  readCollection(url: string, params?: crudParams): Promise<CollectionResponse>;

  deleteItem(iri: string): Promise<any>;

  updateItem(iri: string, payload: crudPayload): Promise<any>;

  patchItem(iri: string, payload: crudPayload): Promise<any>;

  appendItem(iri: string, payload: crudPayload): Promise<any>;

  moveItemTree(iri: string, payload: crudPayload): Promise<any>;
}

export { crudInterface, crudPayload, crudParams, CollectionResponse }
