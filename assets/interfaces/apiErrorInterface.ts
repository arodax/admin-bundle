/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface apiErrorInterface {
  data?: {
    '@context'?: string
    '@type'?: string
    'hydra:title'?: string
    'hydra:description'?: string
    violations?: Array<{
      propertyPath?: string
      message?: string
      code?: string
    }>
  }
  status?: number
  statusText?: string
  headers?: {
    'cache-control'?: string
    'content-type'?: string
    'date'?: string
    'expires'?: string
    'link'?: string
    'server'?: string
    'x-content-type-options'?: string
    'x-debug-token'?: string
    'x-debug-token-link'?: string
    'x-frame-options'?: string
    'x-powered-by'?: string
    'x-robots-tag'?: string
  };
  config?: {
    url?: string
    method?: string
    data?: string
    headers?: {
      Accept?: string
      'Content-Type'?: string
    };
    transformRequest?: any[]
    transformResponse?: any[]
    timeout?: number
    xsrfCookieName?: string
    xsrfHeaderName?: string
    maxContentLength?: number
    maxBodyLength?: number
    transitional?: {
      silentJSONParsing?: boolean
      forcedJSONParsing?: boolean
      clarifyTimeoutError?: boolean
    };
  };
  request?: any
}
