/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface UserGroup {
    '@id'?: string
    '@type'?: string
    id: number
    enabled?: boolean
    gamification?: boolean
}
