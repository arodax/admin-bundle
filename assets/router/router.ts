/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { reactive } from 'vue'
import localforage from 'localforage'
import Axios from '../composables/ajax'

interface Route {
  tokens: Array<[string, string, string?, string?]>;
  defaults?: { [key: string]: any };
}

const useUrlGenerator = () => {
  let routes = reactive<{ [key: string]: Route }>({})

  const loadRoutes = async (): Promise<{ [key: string]: Route } | null> => {
    const cachedRoutes = await localforage.getItem<{ [key: string]: Route }>('routes')

    if (cachedRoutes === null) {
      const response = await Axios.get('/routes.json')
      await localforage.setItem('routes', response.data)
      return response.data
    }

    return cachedRoutes
  }

  loadRoutes().then((r) => { if (r) routes = r })

  const generateUrl = (routeName: string, parameters: { [key: string]: any } = {}, locale: string | null = null): string => {
    if (!locale) {
      locale = document.documentElement.lang
    }

    const route = routes[routeName] || routes[routeName + '.' + locale] || null

    if (!route) {
      console.warn(`Route ${routeName} does not exist`)
      return ''
    }

    if (typeof route.tokens === 'undefined') {
      console.warn(`Route ${routeName} has no valid tokens`)
      return ''
    }

    let url = ''

    const unusedParams: { [key: string]: any } = { ...parameters }

    let optional = true

    route.tokens.forEach((token) => {
      if (token[0] === 'text') {
        url = token[1] + url
        return
      }

      if (token[0] === 'variable') {
        const hasDefault = route.defaults && (token[3]! in route.defaults)

        if (!optional || !hasDefault || (token[3]! in parameters && parameters[token[3]!] !== route.defaults![token[3]!])) {
          let value: any

          if (token[3]! in parameters) {
            value = parameters[token[3]!]
            delete unusedParams[token[3]!]
          } else if (hasDefault) {
            value = route.defaults![token[3]!]
          } else if (optional) {
            return
          } else {
            throw new Error(`The route "${routeName}" requires the parameter "${token[3]}".`)
          }

          const empty = value === true || value === false || value === ''

          if (!empty || !optional) {
            let encodedValue = encodeURIComponent(value).replace(/%2F/g, '/')

            if (encodedValue === 'null' && value === null) {
              encodedValue = ''
            }

            url = token[1] + encodedValue + url
          }

          optional = false
        } else if (hasDefault && (token[3]! in unusedParams)) {
          delete unusedParams[token[3]!]
        }

        return
      }

      throw new Error(`The token type "${token[0]}" is not supported.`)
    })

    if (url === '') {
      url = '/'
    }

    if (unusedParams._nocache === true) {
      unusedParams._nocache = (new Date()).getTime()
    }

    if (Object.keys(unusedParams).length > 0) {
      let prefix: string
      const queryParams: string[] = []
      const add = (key: string, value: any) => {
        value = (typeof value === 'function') ? value() : value

        value = (value === null) ? '' : value

        queryParams.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
      }

      for (prefix in unusedParams) {
        buildQueryParams(prefix, unusedParams[prefix], add)
      }

      url = url + '?' + queryParams.join('&').replace(/%20/g, '+')
    }

    return url
  }

  const buildQueryParams = (prefix: string, obj: any, add: (key: string, value: any) => void) => {
    if (Array.isArray(obj)) {
      obj.forEach((v, i) => {
        if (/\[\]$/.test(prefix)) {
          add(prefix, v)
        } else {
          buildQueryParams(`${prefix}[${typeof v === 'object' && v !== null ? i : ''}]`, v, add)
        }
      })
    } else if (typeof obj === 'object') {
      for (const name in obj) {
        buildQueryParams(`${prefix}[${name}]`, obj[name], add)
      }
    } else {
      add(prefix, obj)
    }
  }

  return { generateUrl }
}

export default {
  install (Vue: any) {
    Vue.prototype.$generateUrl = function (routeName: string, parameters: { [key: string]: any } = {}, locale: string | null = null): string {
      return useUrlGenerator().generateUrl(routeName, parameters, locale)
    }
  }
}
