/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import Vue from 'vue'
import { vuetifyTheme } from '../composables'

/**
 * themeMixin provides a computed property `themeName` that returns 'dark' or 'light'
 * based on the current theme of the application. It supports both Vuetify 2 and Vuetify 3.
 *
 * Usage:
 * In your Vue component, include this mixin to access the `themeName` computed property.
 *
 * Example:
 * <template>
 *   <div :class="themeName">
 *     <!-- Your component template -->
 *   </div>
 * </template>
 *
 * <script>
 * import { themeMixin } from '@/assets/mixins/themeMixin'
 *
 * export default {
 *   mixins: [themeMixin],
 *   // Other component options...
 * }
 * </script>
 */

export const themeMixin = Vue.extend({
  computed: {
    /**
     * Returns the current theme name ('dark' or 'light').
     * @returns {string} - The name of the current theme.
     */
    themeName (): string {
      return this.isDarkTheme() ? 'dark' : 'light'
    }
  },
  created () {
    vuetifyTheme().initializeTheme()
  },
  methods: {
    /**
     * Determines if the current theme is dark.
     * Supports Vuetify 2 and Vuetify 3.
     * @returns {boolean} - True if the theme is dark, false otherwise.
     */
    isDarkTheme (): boolean {
      // Check if $vuetify is available (Vuetify 2)
      if ((this as any).$vuetify) {
        return (this as any).$vuetify.theme.dark
      }

      // Attempt to use Vuetify 3's useTheme if available
      try {
        // eslint-disable-next-line
        const { useTheme } = require('vuetify')
        const vTheme = useTheme()
        return vTheme.current.value.dark
      } catch (e) {
        console.error(e)
        // If useTheme is not available, assume Vuetify 2 or no Vuetify
      }

      // Default to light theme if nothing is found
      return false
    }
  }
})
