/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { apiErrorInterface } from '../interfaces/apiErrorInterface'
import { useErrorStore } from '../stores'

export const errorMixin = {
  computed: {
    errorStore () {
      return useErrorStore()
    },
    getViolations () {
      return (this as any).errorStore.violations
    }
  },
  methods: {
    clearViolations () {
      (this as any).errorStore.clearViolations()
    },
    clearViolation (propertyPath: string) {
      (this as any).errorStore.clearViolation(propertyPath)
    },
    hasViolation (propertyPath: string): boolean {
      return (this as any).errorStore.hasViolation(propertyPath)
    },
    getViolation (propertyPath: string | []): string | null {
      return (this as any).errorStore.getViolation(propertyPath)
    },
    setViolation (message: string, propertyPath: string): void {
      (this as any).errorStore.setViolation(message, propertyPath)
    },
    buildViolations (response: apiErrorInterface): void {
      (this as any).errorStore.buildViolations(response)
    }
  }
}
