/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

/* eslint-disable camelcase */
import { isEmpty } from 'lodash'

const mediaMixin = {
  methods: {
    /**
     * Return path for the content url with specific image filter.
     * @param {string} path Content url path
     * @param {string | null} filter Filter name
     * @returns {string | null}
     */
    $_imageFilter (path: string, filter: string | null = null): string | null {
      if (isEmpty(path)) {
        return null
      }

      if (isEmpty(filter)) {
        return path
      }

      return path.replace('/media/', `/media/cache/${filter}/`)
    }
  }
}

export { mediaMixin }
