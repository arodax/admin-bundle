/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { isNil, truncate } from 'lodash'
import prettysize from 'prettysize'

interface MediaObject {
  mime?: string;
  metadata?: {
    filesize?: number;
  };
  originalName?: string;
}

const mediaObjectMixin = {
  methods: {
    /**
     * Return the type of the MediaObject based on its MIME content-type.
     *
     * @param {MediaObject} mediaObject The MediaObject
     * @returns {string | null}
     */
    $_mediaType (mediaObject: MediaObject): string | null {
      if (isNil(mediaObject)) {
        return null
      }

      const mime = mediaObject.mime ?? ''

      if (mime.match(/^image\/svg/)) {
        return 'svg'
      }

      if (mime.match(/^image\//)) {
        return 'image'
      }

      if (mime.match(/^video\//)) {
        return 'video'
      }

      if (mime.match(/^audio\//)) {
        return 'audio'
      }

      return 'file'
    },
    /**
     * Return the file size of the media object in a human-readable format.
     *
     * @param {MediaObject} mediaObject
     * @returns {string | null}
     */
    $_mediaObjectFileSize (mediaObject: MediaObject): string | null {
      if (isNil(mediaObject.metadata?.filesize)) {
        return null
      }

      return prettysize(mediaObject.metadata?.filesize)
    },
    /**
     * Return the mediaObject name, eventually truncated like macOS style file in finder.
     * @param {MediaObject} mediaObject
     * @param {number | null} length The maximum length of the media object (note, that another 6 chars will be deducted)
     * @return {string | null}
     */
    $_mediaObjectFileName (mediaObject: MediaObject, length: number | null = null): string | null {
      const originalName = mediaObject.originalName ?? null
      if (originalName === null) {
        return null
      }

      if (!length || originalName.length < length) {
        return originalName
      }

      const parts = originalName.split('.')
      let extension = parts.pop() as string
      // if no extension exists we set it as the empty string
      if (extension === originalName) {
        extension = ''
      }

      length = length - 6

      const truncatedFileName = truncate(parts.join('.'), {
        length
      })

      return truncatedFileName + extension.substr(-3) + '.' + extension
    }
  }
}

export { mediaObjectMixin }
