/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

const restoreDataTableFiltersMixin = {
  created () {
    const params = new URLSearchParams(window.location.search)
    const filters = params.get('filters')
    if (filters) {
      try {
        const filtersObject = JSON.parse(decodeURIComponent(filters))
        // Set each filter value if it exists in the parsed object
        Object.entries(filtersObject).forEach(([key, value]) => {
          if (this[key] !== undefined) {
            this[key] = value // Use the setter of the computed properties
          }
        })
      } catch (e) {
        console.error('Error parsing filters:', e)
      }
    }
  },
  methods: {
    reset () {
      // @ts-expect-error - this is a Vue mixin
      const filters = (this.store.filters)
      Object.keys(filters).forEach((key) => {
        this[key] = null
      })

      // @ts-expect-error - this is a Vue mixin
      this.submit()
    }
  }
}

export { restoreDataTableFiltersMixin }
