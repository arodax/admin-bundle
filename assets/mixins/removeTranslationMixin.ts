/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable camelcase */

import { isNil, has } from 'lodash'
import Vue from 'vue'

const $_mixinName = 'removeTranslationMixin'

interface Translatable {
  translations?: Record<string, any>;
}

const removeTranslationMixin = {
  methods: {
    /**
     * Check whether the object has a translation at the given path
     * @param {Translatable} object Object with the translations
     * @param {string|null} locale Short locale name
     * @return {void}
     */
    removeTranslation (object: Translatable, locale: string | null): void {
      if (object === null || typeof object !== 'object') {
        throw new Error(`${$_mixinName}: The given object is not an object type`)
      }

      if (locale === null) {
        throw new Error(`${$_mixinName}: Unknown locale for the translation check. Have you provided "locale" argument?`)
      }

      if (isNil(object.translations) || !has(object.translations, locale)) {
        throw new Error(`${$_mixinName}: The required locale does not exist in the object translations, nothing to remove.`)
      }

      // Ensure translations is not undefined
      if (object.translations) {
        Vue.delete(object.translations, locale)
      }
    }
  }
}

export { removeTranslationMixin }
