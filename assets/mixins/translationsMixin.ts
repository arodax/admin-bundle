/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { isNil, size } from 'lodash'

interface Translatable {
  translations?: Record<string, any>;
}

const translationsMixin = {
  methods: {
    /**
     * Check if the translatable object has translations
     *
     * @param {Translatable} object The "translatable" object
     * @returns {boolean}
     */
    $_hasTranslations (object: Translatable): boolean {
      return !isNil(object.translations) && size(object.translations) > 0
    }
  }
}

export { translationsMixin }
