/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable camelcase */

import { size, orderBy } from 'lodash'

const collectionsMixin = {
  methods: {
    /**
     * Get the size of the countable object (array or collection)
     * @param {any[] | Record<string, any>} object The countable object
     * @return {number}
     */
    size (object: any[] | Record<string, any>): number {
      return size(object)
    },

    /**
     * Order a collection by a given key and order.
     *
     * @param {Array} collection - Collection to be ordered
     * @param {string} key - Key to order by
     * @param {string} order - Order direction
     * @returns {Array} Ordered collection
     */
    orderBy (collection, key, order) {
      return orderBy(collection, key, order)
    }
  }
}

export { collectionsMixin }
