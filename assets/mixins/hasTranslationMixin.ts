/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable camelcase */

import { has, size } from 'lodash'

const $_mixinName = 'collectionsMixin'

interface Translatable {
  translations?: Record<string, Record<string, any>>;
}

const hasTranslationMixin = {
  methods: {
    /**
     * Check whether the object has a translation at the given path
     * @param {Translatable} object Object with the translations
     * @param {string|null} locale Short locale name
     * @param {string|null} path The path of the object in "translations" of the given object.
     * @return {boolean}
     */
    hasTranslation (object: Translatable, locale: string | null = null, path: string | null = null): boolean {
      if (object === null || typeof object !== 'object') {
        throw new Error(`${$_mixinName}: The given object is not an object type`)
      }

      if (locale === null) {
        throw new Error(`${$_mixinName}: Unknown locale for the translation check. Have you provided "locale" argument?`)
      }

      if (path === null) {
        throw new Error(`${$_mixinName}: Unknown path for the translation. Have you provided the "path" argument?`)
      }

      if (!object.translations || !object.translations[locale]) {
        return false
      }

      const localeTranslations = object.translations[locale]
      return has(localeTranslations, path) && localeTranslations[path] && localeTranslations[path].replace(/\s/g, '') !== ''
    },
    /**
     * Check if the object has "any" translations
     * @param {Translatable} object Object with the supposed translations
     * @return {boolean}
     */
    $_hasTranslations (object: Translatable): boolean {
      return has(object, 'translations') && size(object.translations) > 0
    }
  }
}

export { hasTranslationMixin }
