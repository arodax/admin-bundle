/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

const refreshItemMixin = {
  created () {
    // @ts-expect-error since it's a mixin
    this.$eventHub.$on('item-translated', this.refreshItem)
  },
  methods: {
    refreshItem (): void {
      // @ts-expect-error since it's a mixin
      this.store.readItem()
    }
  }
}
export { refreshItemMixin }
