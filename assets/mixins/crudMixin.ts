/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import Axios from '../composables/ajax'

const crudMixin = {
  methods: {
    /**
     * @param {string} endpoint
     * @param {object} payload
     * @return {Promise}
     */
    $_createItem (endpoint: string, payload: Record<string, any>): Promise<any> {
      return Axios.post(endpoint, payload)
    },
    /**
     * @param {string} iri
     * @return {Promise}
     */
    $_readItem (iri: string): Promise<any> {
      return Axios.get(iri)
    },
    /**
     * Read the collection from the given API endpoint and return it as an array
     * @param {string} url
     * @param {Record<string, any>} params The query params
     * @return {Promise<any[]>}
     */
    $_readCollection (url: string, params: Record<string, any> | null = null): Promise<any[]> {
      return new Promise((resolve, reject) => {
        Axios.get(url, { params })
          .then((response) => {
            // check if the response contains a collection
            if (response.data['hydra:member'] !== undefined) {
              resolve(response.data['hydra:member'])
            } else {
              // if no collection found reject a promise
              reject(new Error('Response does not contain a "hydra:member" object.'))
            }
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    /**
     * @param {string} iri
     * @return {Promise}
     */
    $_deleteItem (iri: string): Promise<any> {
      return Axios.delete(iri)
    },
    /**
     * @param {string} iri
     * @param {object} payload
     * @return {Promise}
     */
    $_updateItem (iri: string, payload: Record<string, any>): Promise<any> {
      return Axios.put(iri, payload)
    }
  }
}

export { crudMixin }
