/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { useContainerWidthStore } from '../stores'

export const containerWidthMixin = {
  computed: {
    containerFluid () {
      const store = useContainerWidthStore()
      return store.width === 'fluid'
    }
  },
  created () {
    useContainerWidthStore().initializeWidth()
  }
}
