/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { DateTime } from 'luxon'
import { isNil, isNull, trim, isEmpty } from 'lodash'
import { useCurrentUserStore } from '../stores'

interface DateTimeOptions {
  date?: boolean;
  time?: boolean;
}

interface SubtractOptions {
  days?: number;
  hours?: number;
  minutes?: number;
  seconds?: number;
}

const dateTimeMixin = {
  methods: {
    /**
     * Format time to the specified format or user's preferred format.
     * @param {string|null} dateTime ISO formatted datetime string
     * @param {string|null} format Luxon format tokens
     * @param {DateTimeOptions} options Additional options
     * @return {string|null} Formatted datetime string or null on invalid format.
     */
    $_strftime (
      dateTime: string | null = null,
      format: string | null = null,
      options: DateTimeOptions = { date: true, time: true }
    ): string | null {
      const currentUserStore = useCurrentUserStore()
      const me = currentUserStore.getAdmin

      if (isNull(me)) {
        console.warn('No user found in store. Ignoring preferred timezone, the browser local will be used')
      }

      const dt = DateTime.fromISO(dateTime ?? '')
      const pd = isNil(me?.preferredDateFormat) ? null : me.preferredDateFormat
      const pt = isNil(me?.preferredTimeFormat) ? null : me.preferredTimeFormat
      const tz = isNil(me?.timezone) ? null : me.timezone
      const pf = trim(`${options.date ? pd + ' ' : ''}${options.time ? pt + ' ' : ''}`)

      if (dt.invalid) {
        console.warn(`Provided datetime ${dateTime} is not valid.`)
        return null
      }

      if (!isNull(format)) {
        return dt.setZone(tz).toFormat(format)
      }

      if (!isNull(pd) && !isNull(format)) {
        return dt.setZone(tz).toFormat(pf)
      }

      return dt.setZone(tz).toISO()
    },
    /**
     * Subtract datetime from given datetime object
     * @param {string|null} dateTime ISO formatted datetime string
     * @param {SubtractOptions} options Duration object to be subtracted
     * @return {string|null} returned ISO string with subtracted date
     */
    $_substractDateTime (
      dateTime: string | null = null,
      options: SubtractOptions = { days: undefined, hours: undefined, minutes: undefined, seconds: undefined }
    ): string | null {
      const dt = DateTime.fromISO(dateTime ?? '')

      if (dt.invalid) {
        console.warn(`Provided datetime ${dateTime} is not valid.`)
        return null
      }

      return dt.minus(options).toISO()
    },
    /**
     * Format date with the user preferred date format.
     * @param {string} value - Date value to format
     * @returns {string|null} Formatted date or null if no value
     */
    formatDate (value: string): string | null {
      if (!value) {
        return null
      }

      const currentUserStore = useCurrentUserStore()
      const admin = currentUserStore.getAdmin

      return DateTime.fromISO(value).toFormat(isEmpty(admin?.preferredDateFormat) ? 'D' : admin.preferredDateFormat)
    },
    /**
     * Format date time with the user preferred date and time format.
     * @param {string} value - Date time value to format
     * @returns {string|null} Formatted date time or null if no value
     */
    formatDateTime (value: string): string | null {
      if (!value) {
        return null
      }

      const currentUserStore = useCurrentUserStore()
      const admin = currentUserStore.getAdmin

      return DateTime.fromISO(value).toFormat(
        `${isEmpty(admin?.preferredDateFormat) ? 'D' : admin.preferredDateFormat} ${
          isEmpty(admin?.preferredTimeFormat) ? 'T' : admin.preferredTimeFormat
        }`
      )
    }
  }
}

export { dateTimeMixin }
