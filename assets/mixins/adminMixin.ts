/*
 * This file is part of the ARODAX Admin package.
 *
 *  (c) ARODAX a.s.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

import { useCurrentUserStore } from '../stores'
import parseData from '../utils/parse_data'
import Vue from 'vue'

interface AdminMixin extends Vue {
  admin: any
}

export const adminMixin = {
  computed: {
    admin: {
      get () {
        const currentUserStore = useCurrentUserStore()
        return currentUserStore.getAdmin
      },
      set (value) {
        const currentUserStore = useCurrentUserStore()
        currentUserStore.setAdmin(value)
      }
    }
  },
  created (this: AdminMixin) {
    this.admin = parseData('admin')
  }
}
