/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { useCurrentUserStore } from '../stores'
import { User } from '../interfaces/user'

const securityMixin = {
  methods: {
    /**
     * Check if the current user has specific permission(s).
     *
     * @param {string | string[]} permission - The permission or list of permissions to check.
     * @returns {boolean} - True if the user has at least one of the specified permissions, otherwise false.
     */
    isGranted (permission: string | string[]): boolean {
      const currentUserStore = useCurrentUserStore()
      const user: Partial<User> | null = currentUserStore.getAdmin

      if (!user || !user.extendedRoles) {
        console.warn('Permission check failed: User or extendedRoles not found.')
        return false
      }

      const permissions = Array.isArray(permission) ? permission : [permission]
      const isGranted = permissions.some(perm => user.extendedRoles!.includes(perm))

      console.debug(
        isGranted
          ? `Permission granted for: ${JSON.stringify(permission)}`
          : `Permission denied for: ${JSON.stringify(permission)}`
      )

      return isGranted
    }
  }
}

export { securityMixin }
