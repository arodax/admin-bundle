import { Node, mergeAttributes } from '@tiptap/core'

export interface SectionOptions {
  HTMLAttributes: Record<string, any>,
}

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    section: {
      /**
       * Set a section node
       */
      setSection: () => ReturnType,
      /**
       * Toggle a section node
       */
      toggleSection: () => ReturnType,
      /**
       * Unset a section node
       */
      unsetSection: () => ReturnType,
    }
  }
}

export const Section = Node.create<SectionOptions>({
  name: 'section',

  group: 'block',

  content: 'block+',

  addOptions() {
    return {
      HTMLAttributes: {},
    }
  },

  parseHTML() {
    return [
      {
        tag: 'section',
      },
    ]
  },

  renderHTML({ HTMLAttributes }) {
    return ['section', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes), 0]
  },

  addCommands() {
    return {
      setSection: () => ({ commands }) => {
        return commands.setNode(this.name)
      },
      toggleSection: () => ({ commands }) => {
        return commands.toggleNode(this.name, 'paragraph')
      },
      unsetSection: () => ({ commands }) => {
        return commands.lift(this.name)
      },
    }
  },
})
