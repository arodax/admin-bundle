import { Node as TipTapNode } from '@tiptap/core'

export interface SvgOptions {
  HTMLAttributes: Record<string, any>;
}

export const Svg = TipTapNode.create<SvgOptions>({
  name: 'svg',
  group: 'block',
  draggable: true,
  content: 'block+',
  selectable: true,

  parseHTML() {
    return [
      {
        tag: 'svg'
      }
    ]
  },

  renderHTML({ node, HTMLAttributes }) {
    // Ignore existing classes and add only 'svg-placeholder'
    return ['span', { ...HTMLAttributes, class: 'svg-placeholder' }]
  },

  addOptions() {
    return {
      HTMLAttributes: {}
    }
  }
})
