import { Mark, mergeAttributes } from '@tiptap/core'

export interface SpanOptions {
  HTMLAttributes: Record<string, any>,
}

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    span: {
      /**
       * Set a span mark
       */
      setSpan: () => ReturnType,
      /**
       * Toggle a span mark
       */
      toggleSpan: () => ReturnType,
      /**
       * Unset a span mark
       */
      unsetSpan: () => ReturnType,
    }
  }
}

export const Span = Mark.create<SpanOptions>({
  name: 'span',

  addOptions () {
    return {
      HTMLAttributes: {}
    }
  },

  parseHTML () {
    return [
      {
        tag: 'span'
      }
    ]
  },

  renderHTML ({ HTMLAttributes }) {
    return ['span', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes), 0]
  },

  addCommands () {
    return {
      setSpan: () => ({ commands }) => {
        return commands.setMark(this.name)
      },
      toggleSpan: () => ({ commands }) => {
        return commands.toggleMark(this.name)
      },
      unsetSpan: () => ({ commands }) => {
        return commands.unsetMark(this.name)
      }
    }
  }
})
