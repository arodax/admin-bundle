import { Node, mergeAttributes } from '@tiptap/core'

export interface ArticleOptions {
  HTMLAttributes: Record<string, any>,
}

declare module '@tiptap/core' {
  interface Commands<ReturnType> {
    article: {
      /**
       * Set an article node
       */
      setArticle: () => ReturnType,
      /**
       * Toggle an article node
       */
      toggleArticle: () => ReturnType,
      /**
       * Unset an article node
       */
      unsetArticle: () => ReturnType,
    }
  }
}

export const Article = Node.create<ArticleOptions>({
  name: 'article',

  group: 'block',

  content: 'block+',

  addOptions() {
    return {
      HTMLAttributes: {},
    }
  },

  parseHTML() {
    return [
      {
        tag: 'article',
      },
    ]
  },

  renderHTML({ HTMLAttributes }) {
    return ['article', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes), 0]
  },

  addCommands() {
    return {
      setArticle: () => ({ commands }) => {
        return commands.setNode(this.name)
      },
      toggleArticle: () => ({ commands }) => {
        return commands.toggleNode(this.name, 'paragraph')
      },
      unsetArticle: () => ({ commands }) => {
        return commands.lift(this.name)
      },
    }
  },
})
