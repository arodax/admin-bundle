'use strict'

class QrCode {
  static readonly MIN_VERSION = 1
  static readonly MAX_VERSION = 40
  static readonly PENALTY_N1 = 3
  static readonly PENALTY_N2 = 3
  static readonly PENALTY_N3 = 40
  static readonly PENALTY_N4 = 10
  static readonly ECC_CODEWORDS_PER_BLOCK: number[][] = [
    [-1, 7, 10, 15, 20, 26, 18, 20, 24, 30, 18, 20, 24, 26, 30, 22, 24, 28, 30, 28, 28, 28, 28, 30, 30, 26, 28, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30],
    [-1, 10, 16, 26, 18, 24, 16, 18, 22, 22, 26, 30, 22, 22, 24, 24, 28, 28, 26, 26, 26, 26, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28],
    [-1, 13, 22, 18, 26, 18, 24, 18, 22, 20, 24, 28, 26, 24, 20, 30, 24, 28, 28, 26, 30, 28, 30, 30, 30, 30, 28, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30],
    [-1, 17, 28, 22, 16, 22, 28, 26, 26, 24, 28, 24, 28, 22, 24, 24, 30, 28, 28, 26, 28, 30, 24, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]
  ]

  static readonly NUM_ERROR_CORRECTION_BLOCKS: number[][] = [
    [-1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 4, 4, 6, 6, 6, 6, 7, 8, 8, 9, 9, 10, 12, 12, 12, 13, 14, 15, 16, 17, 18, 19, 19, 20, 21, 22, 24, 25],
    [-1, 1, 1, 1, 2, 2, 4, 4, 4, 5, 5, 5, 8, 9, 9, 10, 10, 11, 13, 14, 16, 17, 17, 18, 20, 21, 23, 25, 26, 28, 29, 31, 33, 35, 37, 38, 40, 43, 45, 47, 49],
    [-1, 1, 1, 2, 2, 4, 4, 6, 6, 8, 8, 8, 10, 12, 16, 12, 17, 16, 18, 21, 20, 23, 23, 25, 27, 29, 34, 34, 35, 38, 40, 43, 45, 48, 51, 53, 56, 59, 62, 65, 68],
    [-1, 1, 1, 2, 4, 4, 4, 5, 6, 8, 8, 11, 11, 16, 16, 18, 16, 19, 21, 25, 25, 25, 34, 30, 32, 35, 37, 40, 42, 45, 48, 51, 54, 57, 60, 63, 66, 70, 74, 77, 81]
  ]

  version: number
  errorCorrectionLevel: any
  mask: number
  modules: boolean[][]
  isFunction: boolean[][]
  size: number

  constructor (t: number, s: any, o: any, r: number) {
    if (t < QrCode.MIN_VERSION || t > QrCode.MAX_VERSION) throw 'Version value out of range'
    if (r < -1 || r > 7) throw 'Mask value out of range'

    this.version = t
    this.errorCorrectionLevel = s
    this.mask = r
    this.modules = []
    this.isFunction = []
    this.size = 4 * t + 17

    const i: boolean[] = []
    for (let t = 0; t < this.size; t++) i.push(false)
    for (let t = 0; t < this.size; t++) this.modules.push(i.slice()), this.isFunction.push(i.slice())

    this.drawFunctionPatterns()
    const n = this.addEccAndInterleave(o)
    this.drawCodewords(n)

    if (r == -1) {
      let t = 1e9
      for (let e = 0; e < 8; e++) {
        this.applyMask(e)
        this.drawFormatBits(e)
        const s = this.getPenaltyScore()
        s < t && ((r = e), (t = s)), this.applyMask(e)
      }
    }

    if (r < 0 || r > 7) throw 'Assertion error'

    this.mask = r
    this.applyMask(r)
    this.drawFormatBits(r)
    this.isFunction = []
  }

  static encodeText (s: string, o: any) {
    const r = QrSegment.makeSegments(s)
    return QrCode.encodeSegments(r, o)
  }

  static encodeBinary (s: Uint8Array, o: any) {
    const r = QrSegment.makeBytes(s)
    return QrCode.encodeSegments([r], o)
  }

  static encodeSegments (t: any[], o: any, i = 1, n = 40, h = -1, a = true) {
    if (!(QrCode.MIN_VERSION <= i && i <= n && n <= QrCode.MAX_VERSION) || h < -1 || h > 7) throw 'Invalid value'

    let l: number, u: number
    for (l = i; ; l++) {
      const s = 8 * QrCode.getNumDataCodewords(l, o)
      const i = QrSegment.getTotalBits(t, l)
      if (i <= s) {
        u = i
        break
      }
      if (l >= n) throw 'Data too long'
    }

    for (const t of [Ecc.MEDIUM, Ecc.QUARTILE, Ecc.HIGH]) {
      a && u <= 8 * QrCode.getNumDataCodewords(l, t) && (o = t)
    }

    const c: number[] = []
    for (const e of t) {
      QrCode.appendBits(e.mode.modeBits, 4, c)
      QrCode.appendBits(e.numChars, e.mode.numCharCountBits(l), c)
      for (const t of e.getData()) c.push(t)
    }

    if (c.length != u) throw 'Assertion error'

    const d = 8 * QrCode.getNumDataCodewords(l, o)
    if (c.length > d) throw 'Assertion error'

    QrCode.appendBits(0, Math.min(4, d - c.length), c)
    QrCode.appendBits(0, (8 - (c.length % 8)) % 8, c)
    if (c.length % 8 != 0) throw 'Assertion error'

    for (let t = 236; c.length < d; t ^= 253) QrCode.appendBits(t, 8, c)

    const f: number[] = []
    for (; 8 * f.length < c.length;) f.push(0)

    c.forEach((t, e) => (f[e >>> 3] |= t << (7 - (7 & e))))

    return new QrCode(l, o, f, h)
  }

  getModule (t: number, e: number) {
    return t >= 0 && t < this.size && e >= 0 && e < this.size && this.modules[e][t]
  }

  drawCanvas (t: number, e: number, s: HTMLCanvasElement) {
    if (t <= 0 || e < 0) throw 'Value out of range'

    const o = (this.size + 2 * e) * t
    s.width = o
    s.height = o

    const r = s.getContext('2d')
    if (!r) throw 'Failed to get 2D context'

    for (let s = -e; s < this.size + e; s++) {
      for (let o = -e; o < this.size + e; o++) {
        r.fillStyle = this.getModule(o, s) ? '#000000' : '#FFFFFF'
        r.fillRect((o + e) * t, (s + e) * t, t, t)
      }
    }
  }

  toSvgString (t: number) {
    if (t < 0) throw 'Border must be non-negative'

    const e: string[] = []
    for (let s = 0; s < this.size; s++) {
      for (let o = 0; o < this.size; o++) {
        if (this.getModule(o, s)) {
          e.push(`M${o + t},${s + t}h1v1h-1z`)
        }
      }
    }

    return `<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 ${
      this.size + 2 * t
    } ${this.size + 2 * t}" stroke="none">\n\t<rect width="100%" height="100%" fill="#FFFFFF"/>\n\t<path d="${e.join(' ')}" fill="#000000"/>\n</svg>\n`
  }

  private drawFunctionPatterns () {
    for (let t = 0; t < this.size; t++) {
      this.setFunctionModule(6, t, t % 2 == 0)
      this.setFunctionModule(t, 6, t % 2 == 0)
    }

    this.drawFinderPattern(3, 3)
    this.drawFinderPattern(this.size - 4, 3)
    this.drawFinderPattern(3, this.size - 4)

    const t = this.getAlignmentPatternPositions()
    const e = t.length
    for (let s = 0; s < e; s++) {
      for (let o = 0; o < e; o++) {
        if ((s == 0 && o == 0) || (s == 0 && o == e - 1) || (s == e - 1 && o == 0)) continue
        this.drawAlignmentPattern(t[s], t[o])
      }
    }

    this.drawFormatBits(0)
    this.drawVersion()
  }

  private drawFormatBits (t: number) {
    const e = (this.errorCorrectionLevel.formatBits << 3) | t
    let s = e
    for (let t = 0; t < 10; t++) s = (s << 1) ^ (1335 * (s >>> 9))

    const r = 21522 ^ ((e << 10) | s)
    if (r >>> 15 != 0) throw 'Assertion error'

    for (let t = 0; t <= 5; t++) this.setFunctionModule(8, t, QrCode.getBit(r, t))
    this.setFunctionModule(8, 7, QrCode.getBit(r, 6))
    this.setFunctionModule(8, 8, QrCode.getBit(r, 7))
    this.setFunctionModule(7, 8, QrCode.getBit(r, 8))
    for (let t = 9; t < 15; t++) this.setFunctionModule(14 - t, 8, QrCode.getBit(r, t))

    for (let t = 0; t < 8; t++) this.setFunctionModule(this.size - 1 - t, 8, QrCode.getBit(r, t))
    for (let t = 8; t < 15; t++) this.setFunctionModule(8, this.size - 15 + t, QrCode.getBit(r, t))

    this.setFunctionModule(8, this.size - 8, true)
  }

  private drawVersion () {
    if (this.version < 7) return

    let t = this.version
    for (let e = 0; e < 12; e++) t = (t << 1) ^ (7973 * (t >>> 11))

    const e = (this.version << 12) | t
    if (e >>> 18 != 0) throw 'Assertion error'

    for (let t = 0; t < 18; t++) {
      const s = QrCode.getBit(e, t)
      const r = this.size - 11 + (t % 3)
      const i = Math.floor(t / 3)
      this.setFunctionModule(r, i, s)
      this.setFunctionModule(i, r, s)
    }
  }

  private drawFinderPattern (t: number, e: number) {
    for (let s = -4; s <= 4; s++) {
      for (let o = -4; o <= 4; o++) {
        const r = Math.max(Math.abs(o), Math.abs(s))
        const i = t + o
        const n = e + s
        if (i >= 0 && i < this.size && n >= 0 && n < this.size) {
          this.setFunctionModule(i, n, r != 2 && r != 4)
        }
      }
    }
  }

  private drawAlignmentPattern (t: number, e: number) {
    for (let s = -2; s <= 2; s++) {
      for (let o = -2; o <= 2; o++) {
        this.setFunctionModule(t + o, e + s, Math.max(Math.abs(o), Math.abs(s)) != 1)
      }
    }
  }

  private setFunctionModule (t: number, e: number, s: boolean) {
    this.modules[e][t] = s
    this.isFunction[e][t] = true
  }

  private addEccAndInterleave (t: number[]): number[] {
    const s = this.version
    const o = this.errorCorrectionLevel
    if (t.length != QrCode.getNumDataCodewords(s, o)) throw 'Invalid argument'

    const r = QrCode.NUM_ERROR_CORRECTION_BLOCKS[o.ordinal][s]
    const i = QrCode.ECC_CODEWORDS_PER_BLOCK[o.ordinal][s]
    const n = Math.floor(QrCode.getNumRawDataModules(s) / 8)
    const h = r - (n % r)
    const a = Math.floor(n / r)
    const l: number[][] = []
    const u = QrCode.reedSolomonComputeDivisor(i)

    for (let s = 0, o = 0; s < r; s++) {
      const r = t.slice(o, o + a - i + (s < h ? 0 : 1))
      o += r.length
      const n = QrCode.reedSolomonComputeRemainder(r, u)
      s < h && r.push(0)
      l.push(r.concat(n))
    }

    const c: number[] = []
    for (let t = 0; t < l[0].length; t++) {
      l.forEach((e, s) => {
        (t != a - i || s >= h) && c.push(e[t])
      })
    }

    if (c.length != n) throw 'Assertion error'
    return c
  }

  private drawCodewords (t: number[]) {
    if (t.length != Math.floor(QrCode.getNumRawDataModules(this.version) / 8)) throw 'Invalid argument'

    let s = 0
    for (let e = this.size - 1; e >= 1; e -= 2) {
      e == 6 && (e = 5)
      for (let r = 0; r < this.size; r++) {
        for (let i = 0; i < 2; i++) {
          const n = e - i
          const h = ((e + 1) & 2) == 0 ? this.size - 1 - r : r
          if (!this.isFunction[h][n] && s < 8 * t.length) {
            this.modules[h][n] = QrCode.getBit(t[s >>> 3], 7 - (7 & s))
            s++
          }
        }
      }
    }

    if (s != 8 * t.length) throw 'Assertion error'
  }

  private applyMask (t: number) {
    if (t < 0 || t > 7) throw 'Mask value out of range'

    for (let e = 0; e < this.size; e++) {
      for (let s = 0; s < this.size; s++) {
        let o: boolean
        switch (t) {
        case 0:
          o = (s + e) % 2 == 0
          break
        case 1:
          o = e % 2 == 0
          break
        case 2:
          o = s % 3 == 0
          break
        case 3:
          o = (s + e) % 3 == 0
          break
        case 4:
          o = (Math.floor(s / 3) + Math.floor(e / 2)) % 2 == 0
          break
        case 5:
          o = ((s * e) % 2) + ((s * e) % 3) == 0
          break
        case 6:
          o = (((s * e) % 2) + ((s * e) % 3)) % 2 == 0
          break
        case 7:
          o = (((s + e) % 2) + ((s * e) % 3)) % 2 == 0
          break
        default:
          throw 'Assertion error'
        }

        !this.isFunction[e][s] && o && (this.modules[e][s] = !this.modules[e][s])
      }
    }
  }

  private getPenaltyScore (): number {
    let t = 0

    for (let s = 0; s < this.size; s++) {
      let o = false
      let r = 0
      const i: number[] = [0, 0, 0, 0, 0, 0, 0]

      for (let n = 0; n < this.size; n++) {
        if (this.modules[s][n] == o) {
          r++
          r == 5 ? (t += QrCode.PENALTY_N1) : r > 5 && t++
        } else {
          this.finderPenaltyAddHistory(r, i)
          o || (t += this.finderPenaltyCountPatterns(i) * QrCode.PENALTY_N3)
          o = this.modules[s][n]
          r = 1
        }
      }

      t += this.finderPenaltyTerminateAndCount(o, r, i) * QrCode.PENALTY_N3
    }

    for (let s = 0; s < this.size; s++) {
      let o = false
      let r = 0
      const i: number[] = [0, 0, 0, 0, 0, 0, 0]

      for (let n = 0; n < this.size; n++) {
        if (this.modules[n][s] == o) {
          r++
          r == 5 ? (t += QrCode.PENALTY_N1) : r > 5 && t++
        } else {
          this.finderPenaltyAddHistory(r, i)
          o || (t += this.finderPenaltyCountPatterns(i) * QrCode.PENALTY_N3)
          o = this.modules[n][s]
          r = 1
        }
      }

      t += this.finderPenaltyTerminateAndCount(o, r, i) * QrCode.PENALTY_N3
    }

    for (let s = 0; s < this.size - 1; s++) {
      for (let o = 0; o < this.size - 1; o++) {
        const r = this.modules[s][o]
        if (r == this.modules[s][o + 1] && r == this.modules[s + 1][o] && r == this.modules[s + 1][o + 1]) {
          t += QrCode.PENALTY_N2
        }
      }
    }

    let s = 0
    for (const t of this.modules) s = t.reduce((t, e) => t + (e ? 1 : 0), s)

    const o = this.size * this.size
    return (t += (Math.ceil(Math.abs(20 * s - 10 * o) / o) - 1) * QrCode.PENALTY_N4), t
  }

  private getAlignmentPatternPositions (): number[] {
    if (this.version == 1) return []

    const t = Math.floor(this.version / 7) + 2
    const e = this.version == 32 ? 26 : 2 * Math.ceil((this.size - 13) / (2 * t - 2))
    const s: number[] = [6]

    for (let o = this.size - 7; s.length < t; o -= e) s.splice(1, 0, o)

    return s
  }

  static getNumRawDataModules (t: number): number {
    if (t < QrCode.MIN_VERSION || t > QrCode.MAX_VERSION) throw 'Version number out of range'

    let s = (16 * t + 128) * t + 64
    if (t >= 2) {
      const e = Math.floor(t / 7) + 2
      s -= (25 * e - 10) * e - 55
      t >= 7 && (s -= 36)
    }

    if (!(s >= 208 && s <= 29648)) throw 'Assertion error'

    return s
  }

  static getNumDataCodewords (t: number, s: any): number {
    return Math.floor(QrCode.getNumRawDataModules(t) / 8) - QrCode.ECC_CODEWORDS_PER_BLOCK[s.ordinal][t] * QrCode.NUM_ERROR_CORRECTION_BLOCKS[s.ordinal][t]
  }

  static reedSolomonComputeDivisor (t: number): number[] {
    if (t < 1 || t > 255) throw 'Degree out of range'

    const s: number[] = []
    for (let e = 0; e < t - 1; e++) s.push(0)
    s.push(1)

    let o = 1
    for (let r = 0; r < t; r++) {
      for (let t = 0; t < s.length; t++) {
        s[t] = QrCode.reedSolomonMultiply(s[t], o)
        t + 1 < s.length && (s[t] ^= s[t + 1])
      }
      o = QrCode.reedSolomonMultiply(o, 2)
    }

    return s
  }

  static reedSolomonComputeRemainder (t: number[], s: number[]): number[] {
    const o = s.map(() => 0)
    for (const r of t) {
      const t = r ^ o.shift()!
      o.push(0)
      s.forEach((s, r) => (o[r] ^= QrCode.reedSolomonMultiply(s, t)))
    }
    return o
  }

  static reedSolomonMultiply (t: number, e: number): number {
    if (t >>> 8 != 0 || e >>> 8 != 0) throw 'Byte out of range'

    let s = 0
    for (let o = 7; o >= 0; o--) {
      s = (s << 1) ^ (285 * (s >>> 7))
      s ^= ((e >>> o) & 1) * t
    }

    if (s >>> 8 != 0) throw 'Assertion error'
    return s
  }

  private finderPenaltyCountPatterns (t: number[]): number {
    const e = t[1]
    if (e > 3 * this.size) throw 'Assertion error'

    const s = e > 0 && t[2] == e && t[3] == 3 * e && t[4] == e && t[5] == e
    return (s && t[0] >= 4 * e && t[6] >= e ? 1 : 0) + (s && t[6] >= 4 * e && t[0] >= e ? 1 : 0)
  }

  private finderPenaltyTerminateAndCount (t: boolean, e: number, s: number[]): number {
    t && this.finderPenaltyAddHistory(e, s)
    e += this.size
    this.finderPenaltyAddHistory(e, s)
    return this.finderPenaltyCountPatterns(s)
  }

  private finderPenaltyAddHistory (t: number, e: number[]) {
    e[0] == 0 && (t += this.size)
    e.pop()
    e.unshift(t)
  }

  private static appendBits (t: number, e: number, s: number[]) {
    if (e < 0 || e > 31 || t >>> e != 0) throw 'Value out of range'
    for (let o = e - 1; o >= 0; o--) s.push((t >>> o) & 1)
  }

  private static getBit (t: number, e: number): boolean {
    return ((t >>> e) & 1) != 0
  }
}

class QrSegment {
  static readonly NUMERIC_REGEX = /^[0-9]*$/
  static readonly ALPHANUMERIC_REGEX = /^[A-Z0-9 $%*+.\/:-]*$/
  static readonly ALPHANUMERIC_CHARSET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:'

  mode: any
  numChars: number
  bitData: number[]

  constructor (t: any, e: number, s: number[]) {
    if (e < 0) throw 'Invalid argument'
    this.mode = t
    this.numChars = e
    this.bitData = s.slice()
  }

  static makeBytes (t: Uint8Array) {
    const e: number[] = []
    for (const o of t) QrSegment.appendBits(o, 8, e)
    return new QrSegment(Mode.BYTE, t.length, e)
  }

  static makeNumeric (t: string) {
    if (!this.NUMERIC_REGEX.test(t)) throw 'String contains non-numeric characters'
    const e: number[] = []
    for (let o = 0; o < t.length;) {
      const r = Math.min(t.length - o, 3)
      QrSegment.appendBits(parseInt(t.substr(o, r), 10), 3 * r + 1, e)
      o += r
    }
    return new QrSegment(Mode.NUMERIC, t.length, e)
  }

  static makeAlphanumeric (t: string) {
    if (!this.ALPHANUMERIC_REGEX.test(t)) throw 'String contains unencodable characters in alphanumeric mode'
    let e
    const o: number[] = []
    for (e = 0; e + 2 <= t.length; e += 2) {
      let i = 45 * QrSegment.ALPHANUMERIC_CHARSET.indexOf(t.charAt(e))
      i += QrSegment.ALPHANUMERIC_CHARSET.indexOf(t.charAt(e + 1))
      QrSegment.appendBits(i, 11, o)
    }
    e < t.length && QrSegment.appendBits(QrSegment.ALPHANUMERIC_CHARSET.indexOf(t.charAt(e)), 6, o)
    return new QrSegment(Mode.ALPHANUMERIC, t.length, o)
  }

  static makeSegments (t: string) {
    if (t === '') return []
    return this.NUMERIC_REGEX.test(t) ? [QrSegment.makeNumeric(t)] : this.ALPHANUMERIC_REGEX.test(t) ? [QrSegment.makeAlphanumeric(t)] : [QrSegment.makeBytes(QrSegment.toUtf8ByteArray(t))]
  }

  static makeEci (t: number) {
    const e: number[] = []
    if (t < 0) throw 'ECI assignment value out of range'
    if (t < 128) QrSegment.appendBits(t, 8, e)
    else if (t < 16384) {
      QrSegment.appendBits(2, 2, e)
      QrSegment.appendBits(t, 14, e)
    } else {
      if (!(t < 1e6)) throw 'ECI assignment value out of range'
      QrSegment.appendBits(6, 3, e)
      QrSegment.appendBits(t, 21, e)
    }
    return new QrSegment(Mode.ECI, 0, e)
  }

  getData () {
    return this.bitData.slice()
  }

  static getTotalBits (t: any[], e: number) {
    let s = 0
    for (const o of t) {
      const t = o.mode.numCharCountBits(e)
      if (o.numChars >= 1 << t) return Infinity
      s += 4 + t + o.bitData.length
    }
    return s
  }

  private static toUtf8ByteArray (t: string): Uint8Array {
    t = encodeURI(t)
    const e: number[] = []
    for (let s = 0; s < t.length; s++) {
      if (t.charAt(s) != '%') {
        e.push(t.charCodeAt(s))
      } else {
        e.push(parseInt(t.substr(s + 1, 2), 16))
        s += 2
      }
    }
    return new Uint8Array(e)
  }

  private static appendBits (t: number, e: number, s: number[]) {
    if (e < 0 || e > 31 || t >>> e != 0) throw 'Value out of range'
    for (let o = e - 1; o >= 0; o--) s.push((t >>> o) & 1)
  }
}

class Ecc {
  static readonly LOW = new Ecc(0, 1)
  static readonly MEDIUM = new Ecc(1, 0)
  static readonly QUARTILE = new Ecc(2, 3)
  static readonly HIGH = new Ecc(3, 2)

  ordinal: number
  formatBits: number

  private constructor (t: number, e: number) {
    this.ordinal = t
    this.formatBits = e
  }
}

class Mode {
  static readonly NUMERIC = new Mode(1, [10, 12, 14])
  static readonly ALPHANUMERIC = new Mode(2, [9, 11, 13])
  static readonly BYTE = new Mode(4, [8, 16, 16])
  static readonly KANJI = new Mode(8, [8, 10, 12])
  static readonly ECI = new Mode(7, [0, 0, 0])

  modeBits: number
  numBitsCharCount: number[]

  private constructor (t: number, e: number[]) {
    this.modeBits = t
    this.numBitsCharCount = e
  }

  numCharCountBits (t: number) {
    return this.numBitsCharCount[Math.floor((t + 7) / 17)]
  }
}

// Export classes
export { QrCode, QrSegment, Ecc, Mode }
