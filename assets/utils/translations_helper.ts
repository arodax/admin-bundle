/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { unset, set, get, map } from 'lodash'
import deepForEach from 'deep-for-each'

/**
 * This method will handle translations for the model before persist.
 *
 * Since currently KNP Translations require newTranslations property containing new translations
 * we need to handle the translations' property.
 *
 * @param obj object to be handled
 * @returns {object} The modified object with handled translations
 */
export const handleTranslations = (obj: { [key: string]: any }): { [key: string]: any } => {
  deepForEach(obj, (value: any, key: string, subject: any, path: string) => {
    if (typeof value === 'object' && key === 'translations') {
      const translations = get(obj, path)

      map(translations, (value, key) => {
        unset(translations[key], 'translatable')
        unset(translations[key], 'empty')
      })

      set(subject, 'newTranslations', translations)
    }
  })

  return obj
}
