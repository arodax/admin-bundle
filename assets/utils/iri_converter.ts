/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This method will convert id to the API IRI path
 * @param id Object id
 * @param path Path of the API object
 * @returns {string | null}
 */
export const convertToIri = (id: string | number | null | undefined, path: string): string | null => {
  return id ? `/api/${path}/${id}` : null
}
