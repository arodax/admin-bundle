/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { attempt, isNull } from 'lodash'

/**
 * Parse serialized data from the selector data attribute.
 * @param data The data attribute without data-prefix
 * @param selector The element selector, if selector is omitted [data-*] selector from the data parameter will be used.
 * @returns {Object|Error} Return object from the JSON or Error if JSON could not be parsed.
 */
export default (data: string, selector: string | null = null): object | Error => {
  const actualSelector = selector === null ? `[data-${data}]` : selector
  const element = document.querySelector(actualSelector)
  if (!element) {
    throw new Error(`Element with selector "${actualSelector}" not found`)
  }
  const dataAttribute = element.getAttribute(`data-${data}`)
  if (!dataAttribute) {
    throw new Error(`Data attribute "data-${data}" not found on element with selector "${actualSelector}"`)
  }
  return attempt(() => JSON.parse(dataAttribute))
}
