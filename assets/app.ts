import { PiniaVuePlugin } from 'pinia'
import { errorMixin, mediaObjectMixin, adminMixin, themeMixin, containerWidthMixin, dateTimeMixin } from './mixins'
import { initializeErrorHandler, initializeWindowEventListeners } from './composables'
import { pinia, vuetify } from './plugins'
import Router from './router/router'
import i18n from './i18n/i18n'
import DateTimeComponent from './components/v-date/VDateTime.vue'
import VMenuLocale from './components/v-menu/VMenuLocale.vue'
import VAutocompleteSearch from './components/v-autocomplete/VAutocompleteSearch.vue'
import VBtnContainerWidth from './components/v-btn/VBtnContainerWidth.vue'
import VBtnTheme from './components/v-btn/VBtnTheme.vue'
import VSnackbarBus from './components/v-snackbar/VSnackbarBus.vue'
import VDialogBus from './components/v-dialog/VDialogBus.vue'
import VUser from './components/v-menu/VUser.vue'
import Vue from 'vue'
import Vuetify from 'vuetify'

// Use Vue plugins
Vue.use(Vuetify)
Vue.use(Router)
Vue.use(PiniaVuePlugin)

Vue.config.productionTip = false
Vue.prototype.$eventHub = new Vue()

/**
 * Create the Arodax Admin app instance.
 *
 * @return {Object} Arodax Admin app instance
 */
const AppFactory = () => {
  return {
    pinia,
    vuetify,
    i18n,
    el: '#app',
    mixins: [
      errorMixin,
      mediaObjectMixin,
      adminMixin,
      themeMixin,
      containerWidthMixin,
      dateTimeMixin
    ],
    components: {
      VMenuLocale,
      VUser,
      DateTime: DateTimeComponent,
      VAutocompleteSearch,
      VSnackbarBus,
      VDialogBus,
      VBtnTheme,
      VBtnContainerWidth
    },
    data () {
      return {
        drawer: null,
        dialog: false,
        changeLocaleMenu: false
      }
    },
    async created () {
      const messages = await import(/* webpackChunkName: "lang-[request]" */ `../translations/arodax_admin+intl-icu.${document.documentElement.lang}.json`)
      const translations = {}
      translations[document.documentElement.lang] = messages
      i18n.setLocaleMessage(document.documentElement.lang, messages)
      i18n.locale = document.documentElement.lang

      // Initialize the error handler
      initializeErrorHandler()

      // Initialize window event listeners
      initializeWindowEventListeners()
    }
  }
}

const App = AppFactory()

export { App, AppFactory }
