/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import Vuetify from 'vuetify'
import en from 'vuetify/src/locale/en'
import cs from '../i18n/cs'
import colors from 'vuetify/lib/util/colors'

const createVuetify = function (): Vuetify {
  return new Vuetify({
    lang: {
      locales: { en, cs },
      current: document.documentElement.lang as 'en' | 'cs'
    },
    theme: {
      dark: true,
      themes: {
        light: {
          primary: '#283593',
          secondary: '#B71C1C',
          accent: '#5C6BC0'
        },
        dark: {
          primary: colors.blue.darken4,
          secondary: colors.red.lighten2,
          grey: colors.grey.darken4,
          lighten2: colors.grey.darken3,
          accent: '#616161'
        }
      }
    },
    icons: {
      iconfont: 'mdiSvg'
    }
  })
}

const vuetify = createVuetify()

export { vuetify }
