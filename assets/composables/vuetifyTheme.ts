/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { useLocalStorage } from './useLocalStorage'
import { useCookies } from './useCookies'
import { computed, Ref, watchEffect } from 'vue'
import { vuetify } from '../plugins/vuetify'
import { DateTime } from 'luxon'

type Theme = 'dark' | 'light' | 'adaptive' | 'system';

type VuetifyThemeResult = {
  theme: Ref<Theme>
  initializeTheme(): void
  setTheme: (newTheme: Theme) => void
  themeName: Ref<'dark' | 'light'>
}

/**
 * Manages the theme in a Vue.js application using Vuetify.
 *
 * @returns {VuetifyThemeResult} An object containing theme-related properties and methods.
 */
export function vuetifyTheme (): VuetifyThemeResult {
  const defaultTheme = 'light'

  const theme = useLocalStorage<Theme>('theme', defaultTheme)
  const themeCookie = useCookies<Theme>('theme', defaultTheme)

  const initializeTheme = () => {
    if (theme.value === 'adaptive') {
      handleAdaptiveTheme()
    } else if (theme.value === 'system') {
      handleSystemTheme()
    } else {
      vuetify.framework.theme.dark = theme.value === 'dark'
    }
  }

  const handleSystemTheme = () => {
    // Check if the user's system prefers a dark theme
    vuetify.framework.theme.dark = window.matchMedia(
      '(prefers-color-scheme: dark)'
    ).matches
  }

  const setTheme = (newTheme: Theme) => {
    console.debug('Theme changed to', newTheme)

    // set theme in local storage and cookie
    theme.value = newTheme
    themeCookie.value = newTheme

    if (newTheme === 'adaptive') {
      handleAdaptiveTheme()
    } else if (newTheme === 'system') {
      handleSystemTheme()
    } else {
      vuetify.framework.theme.dark = newTheme === 'dark'
    }
  }

  const handleAdaptiveTheme = () => {
    const now = DateTime.local()
    const sunrise = getSunrise()
    const sunset = getSunset()

    // Check if current time is between sunrise and sunset
    const isDaytime = now >= sunrise && now <= sunset

    vuetify.framework.theme.dark = !isDaytime
  }

  const getSunrise = () => {
    return DateTime.local()
      .setZone('Europe/Prague')
      .set({ hour: 6, minute: 0, second: 0 })
  }

  const getSunset = () => {
    return DateTime.local()
      .setZone('Europe/Prague')
      .set({ hour: 18, minute: 0, second: 0 })
  }

  const themeName = computed(() => {
    if (theme.value === 'dark' || theme.value === 'light') {
      return theme.value
    } else {
      return vuetify.framework.theme.dark ? 'dark' : 'light'
    }
  })

  watchEffect(() => {
    if (theme.value === 'system') {
      const mediaQueryList = window.matchMedia('(prefers-color-scheme: dark)')

      const listener = (e) => {
        vuetify.framework.theme.dark = e.matches
      }

      mediaQueryList.addListener(listener)

      // Cleanup function for the effect
      return () => {
        mediaQueryList.removeListener(listener)
      }
    }
  })

  return { theme, setTheme, themeName, initializeTheme }
}
