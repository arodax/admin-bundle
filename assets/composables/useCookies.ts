/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ref, watchEffect, Ref } from 'vue'
import { generateStorageKey } from './keyGenerator'
import { Storable } from '../types'

/**
 * A Vue composable that provides a reactive interface to a cookie.
 *
 * @param key - The key under which the value is stored in the cookie.
 * @param defaultValue - The default value to use if there is no item in the cookie with the given key.
 * @param days - The number of days until the cookie should expire.
 * @returns A Ref of the value stored in the cookie. This Ref is reactive and can be used just like any other Ref in Vue.
 */
export function useCookies<T extends Storable> (key: string, defaultValue: T, days?: number): Ref<T> {
  // Generate the full key for the cookie
  const cookieKey = generateStorageKey(key)

  // Initialize the value from the cookie, or use the default value
  const value = ref(defaultValue) as Ref<T>

  // Get the stored value from the cookie
  const storedValue = getCookie(cookieKey)

  // If a stored value exists, parse it and assign to our reactive Ref
  if (storedValue) {
    value.value = JSON.parse(storedValue) as T
  }

  // Watch for changes to the reactive Ref and update the cookie when it changes
  watchEffect(() => {
    setCookie(cookieKey, JSON.stringify(value.value), days)
  })

  // Return the reactive Ref
  return value
}

/**
 * Sets a cookie with a given name, value, and expiration date.
 * @param name - The name of the cookie.
 * @param value - The value to store in the cookie.
 * @param days - The number of days until the cookie should expire.
 */
function setCookie (name: string, value: string, days = 365): void {
  const date = new Date()
  date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
  const expires = '; expires=' + date.toUTCString()

  // Set the cookie with the provided name, value, and expiration date
  document.cookie = name + '=' + value + expires + '; path=/'
}

/**
 * Gets the value of a cookie with a given name.
 * @param name - The name of the cookie.
 * @returns The value of the cookie, or null if no cookie with the given name exists.
 */
function getCookie (name: string): string | null {
  const value = '; ' + name + '='
  const parts = document.cookie.split(value)

  // If a cookie with the provided name exists, return its value
  if (parts.length === 2) return parts.pop()?.split(';').shift() || null

  // If no cookie with the provided name exists, return null
  return null
}
