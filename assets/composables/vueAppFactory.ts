/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import Vue from 'vue'
import { App } from '../app'

export function createVueApp (components) {
  const componentMap = {}
  components.forEach(component => {
    componentMap[component.name] = component
  })

  /* eslint-disable no-new */
  new Vue({
    components: componentMap,
    extends: App
  })
}
