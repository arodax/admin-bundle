/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import axios from 'axios/index'

axios.interceptors.response.use((response) => {
  return response
}, error => {
  return Promise.reject(error.response)
})

export default axios
