/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

interface EventListeners {
  removeListeners: () => void
}

/**
 * Initializes global window event listeners for the ARODAX Admin application.
 *
 * This function sets up several important event listeners on the window object:
 *
 * 1. `dragover`: Prevents the default behavior during drag operations over the window. This is useful to avoid
 *    unexpected behaviors or styles that might occur when dragging items over the application window.
 *
 * 2. `dragenter`: Prevents the default behavior when a dragged element enters the window. This helps to maintain
 *    the intended user interface and user experience when elements are dragged into the application window.
 *
 * 3. `drop`: Prevents the default behavior when an element is dropped within the window. This ensures that the drop
 *    actions do not trigger unintended consequences, allowing the application to handle these events in a controlled manner.
 *
 * @returns {EventListeners} An object with a method to remove the event listeners.
 *
 * Usage:
 *
 * ```typescript
 * import { initializeWindowEventListeners } from './composables/windowEventListeners';
 *
 * const listeners = initializeWindowEventListeners();
 *
 * // To remove the listeners later
 * listeners.removeListeners()
 * ```
 *
 * The drag and drop event listeners (`dragover`, `dragenter`, and `drop`) are useful for preventing unexpected behaviors
 * when files or other elements are dragged and dropped onto the window. This can help ensure that the ARODAX Admin application's
 * drag-and-drop functionality works as intended without interference from the default browser behaviors.
 *
 * Removing Listeners:
 *
 * The returned object from `initializeWindowEventListeners` contains a method called `removeListeners` which can be called to remove
 * all the event listeners that were added. This is useful for cleanup purposes, such as when your component is destroyed or when you
 * no longer need these event listeners active.
 *
 * Example:
 *
 * ```typescript
 * const listeners = initializeWindowEventListeners();
 *
 * // Later in the code, when you want to remove the listeners:
 * listeners.removeListeners();
 * ```
 */
export function initializeWindowEventListeners (): EventListeners {
  const preventDefault = (e: Event) => e.preventDefault()

  // Add listeners
  window.addEventListener('dragover', preventDefault, false)
  window.addEventListener('dragenter', preventDefault, false)
  window.addEventListener('drop', preventDefault, false)

  return {
    removeListeners: () => {
      // Remove listeners
      window.removeEventListener('dragover', preventDefault, false)
      window.removeEventListener('dragenter', preventDefault, false)
      window.removeEventListener('drop', preventDefault, false)
    }
  }
}
