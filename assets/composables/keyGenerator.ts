/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// Cache object to store the generated storage keys
const cache: Record<string, string> = {}

/**
 * Generates a unique storage key based on the provided key.
 *
 * If a cached storage key exists for the provided key, it returns the cached value.
 * Otherwise, it generates a new storage key using the provided key and caches it for future use.
 *
 * @param {string} key - The key used to generate the storage key.
 * @returns {string} The generated storage key.
 */
export function generateStorageKey (key: string): string {
  if (cache[key]) {
    return cache[key]
  }

  const storageKey = `${window.location.hostname}_arodax_admin_${key}`
  cache[key] = storageKey

  return storageKey
}
