/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import Axios from './ajax'
import { crudInterface, crudPayload, crudParams, CollectionResponse } from '../interfaces/crudInterface'
import Qs from 'qs'

export const crud: crudInterface = {
  /**
   * Creates a new item at the given endpoint.
   *
   * @param endpoint The URL of the endpoint.
   * @param payload The payload to send to the endpoint.
   */
  createItem (endpoint: string, payload: crudPayload) {
    return Axios.post(endpoint, payload)
  },

  /**
   * Reads an item from the given IRI.
   * @param iri The IRI of the item.
   */
  readItem (iri: string) {
    return Axios.get(iri)
  },

  /**
   * Reads a collection from the given URL.
   * @param url The URL of the collection.
   * @param params The parameters to send to the endpoint.
   */
  async readCollection (url: string, params: crudParams = {}): Promise<CollectionResponse> {
    const response = await Axios.get(url + '?' + Qs.stringify(params, { arrayFormat: 'brackets', encodeValuesOnly: true, skipNulls: true }))
    if (typeof response.data['hydra:member'] !== 'undefined' && typeof response.data['hydra:totalItems'] !== 'undefined') {
      return {
        items: response.data['hydra:member'],
        totalItems: response.data['hydra:totalItems']
      }
    } else {
      throw new Error('Response does not contain a "hydra:member" object and/or "hydra:totalItems" property.')
    }
  },

  /**
   * Deletes an item from the given IRI.
   * @param iri
   */
  deleteItem (iri: string) {
    return Axios.delete(iri)
  },

  /**
   * Updates an item at the given IRI.
   * @param iri
   * @param payload
   */
  updateItem (iri: string, payload: crudPayload) {
    return Axios.put(iri, payload, { headers: { 'Content-Type': 'application/ld+json' } })
  },

  /**
   * Patch a partial item at the given IRI.
   * @param iri
   * @param payload
   */
  patchItem (iri: string, payload: crudPayload) {
    return Axios.patch(iri, payload, { headers: { 'Content-Type': 'application/merge-patch+json' } })
  },

  /**
   * Appends an item to the given IRI.
   * @param iri
   * @param payload
   */
  appendItem (iri: string, payload: crudPayload) {
    return Axios.post(iri, { ...payload }, { headers: { 'Content-Type': 'application/ld+json' } })
  },

  /**
   * Moves an item in the tree.
   * @param iri
   * @param payload
   */
  moveItemTree (iri: string, payload: crudPayload) {
    return Axios.put(iri, { ...payload }, { headers: { 'Content-Type': 'application/ld+json' } })
  }
}
