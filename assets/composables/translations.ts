/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { keys } from 'lodash'

export function usedTranslationKeys (item: any) {
  if (item === null || typeof item.translations === 'undefined' || item.translations === null) {
    return []
  }
  return keys(item.translations)
}
