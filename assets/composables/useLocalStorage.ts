/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ref, watchEffect, Ref } from 'vue'
import { generateStorageKey } from './keyGenerator'
import { Storable } from '../types'

/**
 * A Vue composable that provides a reactive interface to a localStorage item.
 *
 * @param key - The key under which the value is stored in localStorage.
 * @param defaultValue - The default value to use if there is no item in localStorage with the given key.
 * @returns A Ref of the value stored in localStorage. This Ref is reactive and can be used just like any other Ref in Vue.
 */
export function useLocalStorage<T extends Storable> (key: string, defaultValue: T): Ref<T> {
  // Validate the key
  if (key.trim().length === 0) {
    throw new Error('Invalid key. Key must be a non-empty string.')
  }

  // Generate the full key for the localStorage item
  const storageKey = generateStorageKey(key)

  // Initialize the value from localStorage, or use the default value
  const storedValue = localStorage.getItem(storageKey)
  const initialValue = storedValue !== null ? parseValue(storedValue) : defaultValue
  const value = ref(initialValue) as Ref<T>

  // Check for availability of localStorage
  if (typeof localStorage !== 'undefined') {
    // Get the stored value from localStorage
    if (storedValue !== null) {
      // Assign the parsed stored value to our reactive Ref
      value.value = parseValue(storedValue)
    }
  } else {
    throw new Error('localStorage is not available.')
  }

  // Define a function to update the value in localStorage when the reactive Ref changes
  const updateStorage = () => {
    if (typeof localStorage !== 'undefined') {
      const currentValue = stringifyValue(value.value)
      // Check if the stored value has changed
      if (currentValue !== localStorage.getItem(storageKey)) {
        try {
          // Update the value in localStorage
          localStorage.setItem(storageKey, currentValue)
        } catch (error) {
          console.error('Error setting value in localStorage:', error)
        }
      }
    }
  }

  // Watch for changes to the reactive Ref and call updateStorage when it changes
  watchEffect(updateStorage)

  // Return the reactive Ref
  return value
}

function parseValue<T> (value: string): T {
  try {
    return JSON.parse(value) as T
  } catch {
    return value as unknown as T
  }
}

function stringifyValue (value: unknown): string {
  if (typeof value === 'string') {
    return value
  } else {
    return JSON.stringify(value)
  }
}
