/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import Vue from 'vue'

/**
 * Initializes global error handling for the ARODAX Admin application.
 *
 * This function sets up a global error handler for Vue.js as well as an event listener for unhandled promise rejections.
 *
 * 1. `Vue.config.errorHandler`: Captures all errors in Vue components and handles them using a custom logic.
 *    This ensures that any errors related to form submissions or other Vue components are centrally managed,
 *    providing a consistent error handling mechanism.
 *
 * 2. `unhandledrejection`: Captures all unhandled promise rejections and emits a snackbar error notification.
 *    This ensures that any unhandled promise rejections are centrally managed, which can help in logging errors and
 *    providing feedback to the user.
 *
 * @returns {EventListeners} An object with a method to remove the event listeners.
 *
 * Usage:
 *
 * ```typescript
 * import { initializeErrorHandler } from './composables/errorHandler';
 *
 * const listeners = initializeErrorHandler();
 *
 * // To remove the listeners later
 * listeners.removeListeners();
 * ```
 *
 * The `errorHandler` function is defined to handle errors globally within Vue components. This includes specific logic
 * for handling form-related errors (HTTP status 422) by displaying a snackbar error and building violation details.
 *
 * The unhandled promise rejection listener is useful for capturing unexpected promise rejections and providing user feedback
 * through a snackbar notification, ensuring the application handles these cases gracefully.
 *
 * Removing Listeners:
 *
 * The returned object from `initializeErrorHandler` contains a method called `removeListeners` which can be called to remove
 * the `unhandledrejection` event listener that was added. This is useful for cleanup purposes, such as when your component
 * is destroyed or when you no longer need these event listeners active.
 *
 * Example:
 *
 * ```typescript
 * const listeners = initializeErrorHandler();
 *
 * // Later in the code, when you want to remove the listener:
 * listeners.removeListeners();
 * ```
 */
interface CustomError extends Error {
  status?: number;
}

declare module 'vue/types/vue' {
  interface Vue {
    clearViolations?: () => void;
    buildViolations?: (err: CustomError) => void;
  }
}

export function initializeErrorHandler () {
  Vue.config.errorHandler = (err: CustomError, vm, info) => {
    // Handle all form related errors with global error handler
    if (typeof err.status !== 'undefined' && err.status === 422) {
      Vue.prototype.$eventHub.$emit('snackbar-error', vm.$t('error_general_form_error'))
      console.log(err)
      if (typeof vm.clearViolations === 'function') {
        vm.clearViolations()
      }
      if (typeof vm.buildViolations === 'function') {
        vm.buildViolations(err)
      }
    } else {
      console.error(err)
      console.error(vm)
      console.error(info)
    }
  }

  // Add listener to all unhandledrejection events
  const handleUnhandledRejection = (event: PromiseRejectionEvent) => {
    Vue.prototype.$eventHub.$emit('snackbar-error', 'An unexpected error occurred')
    console.error(event.reason)
    event.preventDefault()
  }

  window.addEventListener('unhandledrejection', handleUnhandledRejection)

  return {
    removeListeners: () => {
      // Remove listener
      window.removeEventListener('unhandledrejection', handleUnhandledRejection)
    }
  }
}
