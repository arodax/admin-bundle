<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

use Symfony\Component\Dotenv\Dotenv;

/**
 * Initial function to run before the PHPUnit tests.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 *
 * @throws Exception
 */
function bootstrap(): void
{
    $autoloadFile = dirname(__DIR__).'/vendor/autoload.php';

    if (!file_exists($autoloadFile)) {
        $autoloadFile = dirname(__DIR__) . '/../../vendor/autoload.php';
        if (!file_exists($autoloadFile)) {
            throw new RuntimeException('Install dependencies to run test suite.');
        }
    }

    require_once $autoloadFile;

    if (!class_exists(Dotenv::class)) {
        throw new RuntimeException('Please run "composer require symfony/dotenv" to load the ".env" files configuring the application.');
    } else {
        (new Dotenv())->overload(__DIR__ . '/../.env.test');
    }

    $kernel = new \Arodax\AdminBundle\Tests\Kernel();
    $kernel->boot();

    $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
    $application->setAutoExit(false);

    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
        'command' => 'doctrine:schema:drop',
    ]));

    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
        'command' => 'doctrine:schema:update',
        '--force' => null,
    ]));

    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
        'command' => 'doctrine:fixtures:load',
        '--no-interaction' => null,
    ]));

    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
        'command' => 'arodax_admin:install:menu',
        '--no-interaction' => null,
    ]));

    $kernel->shutdown();
}

bootstrap();
