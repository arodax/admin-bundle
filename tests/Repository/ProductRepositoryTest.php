<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Repository;

use Arodax\AdminBundle\Entity\Eshop\Product;
use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Arodax\AdminBundle\Repository\Eshop\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
class ProductRepositoryTest extends WebTestCase
{

    /**
     * @covers \Arodax\AdminBundle\Repository\Eshop\ProductRepository::findByAvailable
     */
    public function testFindByAvailable()
    {
        static::createClient();

        $entityManager = static::getContainer()->get(EntityManagerInterface::class);

        $category = new ProductCategory();
        $entityManager->persist($category);

        $productAvailable = new Product();
        $productAvailable->setPublishedAt((new \DateTimeImmutable())->modify('- 1 hour'));
        $productAvailable->setStatus(Product::STATUS_PUBLISHED);
        $productAvailable->addCategory($category);

        $entityManager->persist($productAvailable);

        $productNotAvailable = new Product();
        $productNotAvailable->setPublishedAt(null);
        $productNotAvailable->setStatus(Product::STATUS_PUBLISHED);
        $productNotAvailable->addCategory($category);
        $entityManager->persist($productNotAvailable);

        $entityManager->flush();

        $productRepository = static::getContainer()->get(ProductRepository::class);

        $products = $productRepository->findByAvailable();

        $this->assertContains($productAvailable, $products, sprintf('Expected to find product "%s" in available products, but product could not be found.', $productAvailable->getId()));
        $this->assertNotContains($productNotAvailable, $products, sprintf('Expected to not find product "%s" in available products, but product has been found.', $productAvailable->getId()));
    }

    /**
     * @covers \Arodax\AdminBundle\Repository\Eshop\ProductRepository::findByAvailableCategoryId
     */
    public function testFindByAvailableCategoryId()
    {
        static::createClient();

        $entityManager = static::getContainer()->get(EntityManagerInterface::class);

        $category = new ProductCategory();
        $entityManager->persist($category);

        $productAvailable = new Product();
        $productAvailable->setPublishedAt((new \DateTimeImmutable())->modify('- 1 hour'));
        $productAvailable->setStatus(Product::STATUS_PUBLISHED);
        $productAvailable->addCategory($category);

        $entityManager->persist($productAvailable);

        $productNotAvailable = new Product();
        $productNotAvailable->setPublishedAt(null);
        $productNotAvailable->setStatus(Product::STATUS_PUBLISHED);
        $productNotAvailable->addCategory($category);
        $entityManager->persist($productNotAvailable);

        $entityManager->flush();

        $productRepository = static::getContainer()->get(ProductRepository::class);

        $products = $productRepository->findByAvailableCategoryId($category->getId());

        $this->assertContains($productAvailable, $products, sprintf('Expected to find product "%s" in available products, but product could not be found.', $productAvailable->getId()));
        $this->assertNotContains($productNotAvailable, $products, sprintf('Expected to not find product "%s" in available products, but product has been found.', $productAvailable->getId()));
    }
}
