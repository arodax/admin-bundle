<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/**
 * Modified kernel file for the application testing.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const string CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function __construct()
    {
        parent::__construct('test', true);
    }

    public function registerBundles(): \Generator
    {
        $bundles = [
            \Symfony\Bundle\FrameworkBundle\FrameworkBundle::class,
            \Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class,
            \Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class,
            \Symfony\Bundle\SecurityBundle\SecurityBundle::class,
            \Symfony\Bundle\TwigBundle\TwigBundle::class,
            \Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class,
            \Symfony\Bundle\MonologBundle\MonologBundle::class,
            \Symfony\Bundle\DebugBundle\DebugBundle::class,
            \Symfony\Bundle\MakerBundle\MakerBundle::class,
            \Symfony\WebpackEncoreBundle\WebpackEncoreBundle::class,
            \Knp\Bundle\MenuBundle\KnpMenuBundle::class,
            \Arodax\AdminBundle\ArodaxAdminBundle::class,
            \Nelmio\CorsBundle\NelmioCorsBundle::class,
            \ApiPlatform\Symfony\Bundle\ApiPlatformBundle::class,
            \Vich\UploaderBundle\VichUploaderBundle::class,
            \Twig\Extra\TwigExtraBundle\TwigExtraBundle::class,
            \Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class,
        ];

        foreach ($bundles as $class) {
            yield new $class();
        }
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->setParameter('kernel.environment', 'test');
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = __DIR__.'/../config';
        $loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{packages}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $confDir = __DIR__.'/../config';

        $routes->import($confDir.'/routing.yml');
    }
}
