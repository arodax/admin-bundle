<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;

class MissingTranslationsTest extends TestCase
{
    private string $vueComponentsDir = __DIR__.'/../../assets/components';
    private string $vueViewsDir = __DIR__.'/../../assets/views';
    private string $masterTranslationFile = __DIR__.'/../../translations/arodax_admin+intl-icu.cs.json';

    public function testMissingTranslations(): void
    {
        $translationKeys = $this->extractTranslationKeysFromVueFiles();
        $masterTranslations = $this->loadMasterTranslations();

        foreach ($translationKeys as $key) {
            $this->assertArrayHasKey($key, $masterTranslations, "The translation key '{$key}' is missing in the master translation file.");
        }
    }

    private function extractTranslationKeysFromVueFiles(): array
    {
        $finder = new Finder();
        $translationKeys = [];

        $finder->files()->in($this->vueComponentsDir)->name('*.vue');
        $finder->files()->in($this->vueViewsDir)->name('*.vue');

        foreach ($finder as $file) {
            $content = $file->getContents();
            preg_match_all('/\$t\([\'"](\w+)[\'"]\)/', $content, $matches);
            $translationKeys = array_merge($translationKeys, $matches[1]);
        }

        return array_unique($translationKeys);
    }

    private function loadMasterTranslations(): array
    {
        if (!file_exists($this->masterTranslationFile)) {
            $this->fail("The master translation file does not exist.");
        }

        $masterTranslations = json_decode(file_get_contents($this->masterTranslationFile), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->fail('Failed to decode JSON from the master translation file.');
        }

        return $masterTranslations;
    }

}
