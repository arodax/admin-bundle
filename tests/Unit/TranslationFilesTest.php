<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Unit;
use PHPUnit\Framework\TestCase;
class TranslationFilesTest extends TestCase

{
    private string $translationDir = __DIR__ . '/../../translations'; // Adjust the path as necessary

    public function testAllTranslationFilesContainSameKeys(): void
    {
        $files = glob($this->translationDir . '/arodax_admin+intl-icu.*.json');
        $allKeys = [];

        foreach ($files as $file) {
            $content = json_decode(file_get_contents($file), true);
            $allKeys[basename($file)] = array_keys($content);
        }

        $referenceKeys = $allKeys[array_key_first($allKeys)];

        foreach ($allKeys as $fileName => $keys) {
            sort($keys);
            sort($referenceKeys);

            $this->assertEquals($referenceKeys, $keys, sprintf('File %s has different keys.', $fileName));
        }
    }
}
