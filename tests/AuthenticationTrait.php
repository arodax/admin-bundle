<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests;

use Arodax\AdminBundle\Repository\User\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Provides method for authenticating user in the functional tests.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 */
trait AuthenticationTrait
{
    /**
     * Authenticate user in the test kernel browser.
     *
     * @param KernelBrowser $client   the functional test kernel browser, which will provide the request
     * @param string        $email    E-mail of the user which should be authenticated
     * @param string        $firewall optional firewall name, the default is "admin"
     */
    public function loginUser(KernelBrowser $client, string $email, string $firewall = 'admin'): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail($email);

        // simulate $testUser being logged in
        $client->loginUser($testUser, $firewall);
    }
}
