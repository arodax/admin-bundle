<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Doctrine\ORM;

use Arodax\AdminBundle\Doctrine\ORM\DoctrinePropertySequenceGenerator;
use Arodax\AdminBundle\Entity\Eshop\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DoctrinePropertySequenceGeneratorTest extends WebTestCase
{

    public function testSequenceGenerationWithResetOnPrefixChange(): void
    {
        $doctrinePropertySequenceGenerator = static::getContainer()->get(DoctrinePropertySequenceGenerator::class);
        $entityManager = static::getContainer()->get(EntityManagerInterface::class);

        $entityClass = Order::class;
        $propertyPath = 'orderNumber';

        // Test resetting sequence with prefix change
        $prefix = date('Y'); // Use current year as prefix
        $length = 10;
        $resetOnPrefixChange = true;

        // Generate a number
        $number = $doctrinePropertySequenceGenerator->generate(
            entityClass: $entityClass,
            propertyPath: $propertyPath,
            prefix: $prefix,
            length: $length,
            resetOnPrefixChange: $resetOnPrefixChange
        );


        // Assertions here would typically check the format and length of $number
        // This is a placeholder assertion. Replace with your actual conditions.
        $this->assertMatchesRegularExpression('/^\d{10}$/', $number, 'Generated number matches expected format and length.');
    }
}
