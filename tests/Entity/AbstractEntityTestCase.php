<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity;

use Arodax\AdminBundle\Model\TranslatableInterface;
use PHPUnit\Framework\TestCase;

abstract class AbstractEntityTestCase extends TestCase
{
    /**
     * Test expected properties of the given entity.
     *
     * @param object $entity             the entity which will be tested
     * @param array  $expectedProperties expected properties
     */
    protected function assertProperties(object $entity, array $expectedProperties = []): void
    {
        $reflectedEntity = new \ReflectionClass($entity);

        // the entity must not contain property which is not listed among expected properties
        foreach ($reflectedEntity->getProperties() as $property) {
            $this->assertContains($property->getName(), $expectedProperties, sprintf('Entity \'%s\' must not contain property \'%s\' because it\'s not listed in expected properties.', $reflectedEntity->getName(), $property->getName()));
        }

        // the entity must contain property which is listed in expected properties
        foreach ($expectedProperties as $expectedProperty) {
            $this->assertClassHasAttribute($expectedProperty, \get_class($entity), sprintf('Entity \'%s\' must contain property \'%s\' because it\'s listed in expected properties.', $reflectedEntity->getName(), $expectedProperty));
        }
    }

    protected function assertBeingTranslatable(object $entity): void
    {
        $reflectedEntity = new \ReflectionClass($entity);

        $this->assertTrue($reflectedEntity->implementsInterface(TranslatableInterface::class), sprintf('Entity \'%s\' must contain property implement \'%s\' interface.', $reflectedEntity->getName(), TranslatableInterface::class));
        $this->assertTrue(class_exists($entity->getTranslationEntityClass()), sprintf('Expected to find \'%s\' class for the translatable entity \'%s\'', $entity->getTranslationEntityClass(), get_class($entity)));

    }
}
