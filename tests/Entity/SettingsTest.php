<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

use Arodax\AdminBundle\Entity\Settings;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class SettingsTest extends AbstractEntityTestCase
{
    protected Settings $object;

    public function setUp(): void
    {
        $this->object = new Settings();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'key',
            'value'
        ]);
    }
}
