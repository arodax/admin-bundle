<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Contact;

use Arodax\AdminBundle\Entity\Contact\ContactItem;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ContactItemTest extends AbstractEntityTestCase
{
    protected ContactItem $object;

    public function setUp(): void
    {
        $this->object = new ContactItem();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'type',
            'contact',
            'value'
        ]);
    }
}
