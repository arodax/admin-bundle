<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Contact;

use Arodax\AdminBundle\Entity\Contact\Contact;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ContactTest extends AbstractEntityTestCase
{
    protected Contact $object;

    public function setUp(): void
    {
        $this->object = new Contact();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'items',
            'type'
        ]);
    }
}
