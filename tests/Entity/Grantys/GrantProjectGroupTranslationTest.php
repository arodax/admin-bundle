<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectGroupTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantProjectGroupTranslationTest extends AbstractEntityTestCase
{
    protected GrantProjectGroupTranslation $object;

    public function setUp(): void
    {
        $this->object = new GrantProjectGroupTranslation();
    }

    public function testProperites()
    {
        $this->assertTrue(true);
    }

    public function testGettersAndSetters()
    {
        $this->assertTrue(true);
    }
}
