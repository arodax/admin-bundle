<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantCall;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantCallTest extends AbstractEntityTestCase
{
    protected GrantCall $object;

    protected function setUp(): void
    {
        $this->object = new GrantCall();
    }

    public function test__construct(): void
    {
        $object = new GrantCall();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'createdAt',
            'updatedAt',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'status',
            'callStatus',
            'variableSymbol',
            'roundsQuantity',
            'applicantType',
            'focusType',
            'applicationStartAt',
            'deadlineAt',
            'maxAmount',
            'publishedAt',
            'attachments',
            'covers',
            'grantProjectGroups',
            'onlineLink'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
