<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectTypeTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantProjectTypeTranslationTest extends AbstractEntityTestCase
{
    protected GrantProjectTypeTranslation $object;

    public function setUp(): void
    {
        $this->object = new GrantProjectTypeTranslation();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'name'
        ]);
    }
}
