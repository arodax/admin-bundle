<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantFocusTypeTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantFocusTypeTranslationTest extends AbstractEntityTestCase
{
    protected GrantFocusTypeTranslation $object;

    public function setUp(): void
    {
        $this->object = new GrantFocusTypeTranslation();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'name'
        ]);
    }
}
