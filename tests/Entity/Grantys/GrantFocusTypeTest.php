<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantFocusType;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantFocusTypeTest extends AbstractEntityTestCase
{
    protected GrantFocusType $object;

    protected function setUp(): void
    {
        $this->object = new GrantFocusType();
    }

    public function test__construct(): void
    {
        $object = new GrantFocusType();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'createdAt',
            'updatedAt',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
