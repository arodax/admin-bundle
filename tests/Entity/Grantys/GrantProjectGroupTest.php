<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Grantys;

use Arodax\AdminBundle\Entity\Grantys\GrantProjectGroup;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GrantProjectGroupTest extends AbstractEntityTestCase
{
    protected GrantProjectGroup $object;

    protected function setUp(): void
    {
        $this->object = new GrantProjectGroup();
    }

    public function test__construct()
    {
        $object = new GrantProjectGroup();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertTrue(true);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }

    public function testGettersAndSetters()
    {
        $this->assertTrue(true);
    }
}
