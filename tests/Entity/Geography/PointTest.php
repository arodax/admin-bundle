<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Geography;

use Arodax\AdminBundle\Entity\Geography\Point;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class PointTest extends AbstractEntityTestCase
{
    protected Point $object;

    public function setUp(): void
    {
        $this->object = new Point();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'translations',
            'newTranslations',
            'point',
            'address',
            'currentLocale',
            'defaultLocale'
        ]);
    }
}
