<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Geography;

use Arodax\AdminBundle\Entity\Geography\PointTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class PointTranslationTest extends AbstractEntityTestCase
{
    protected PointTranslation $object;

    public function setUp(): void
    {
        $this->object = new PointTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'description',
            'translatable'
        ]);
    }
}
