<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Gallery\Gallery;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GalleryTest extends AbstractEntityTestCase
{
    protected Gallery $object;

    protected function setUp(): void
    {
        $this->object = new Gallery();
    }

    public function test__construct()
    {
        $object = new Gallery();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'lft',
            'lvl',
            'rgt',
            'root',
            'children',
            'publishedAt',
            'galleryMediaObjects',
            'defaultLocale',
            'status',
            'translations',
            'newTranslations',
            'currentLocale',
            'createdAt',
            'updatedAt',
            'parent',
            'readOnly',
            'deletable'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::setCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::getCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::setUpdatedAt
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::getUpdatedAt
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::setStatus
     * @covers \Arodax\AdminBundle\Entity\Gallery\Gallery::getStatus
     */
    public function testGettersAndSetters()
    {
        // status
        $this->object->setStatus(Gallery::STATUS_PUBLISHED);
        $this->assertEquals(Gallery::STATUS_PUBLISHED, $this->object->getStatus());

        // created at
        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getCreatedAt()->format('Y-m-d H:i:s'));

        // updated at
        $now = new \DateTimeImmutable();
        $this->object->setUpdatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getUpdatedAt()->format('Y-m-d H:i:s'));

        // parent
        $parent = new Gallery();
        $this->object->setParent($parent);
        $this->assertEquals($parent, $this->object->getParent());
    }
}
