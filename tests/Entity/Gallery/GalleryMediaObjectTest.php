<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Contact;

use Arodax\AdminBundle\Entity\Gallery\GalleryMediaObject;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GalleryMediaObjectTest extends AbstractEntityTestCase
{
    protected GalleryMediaObject $object;

    public function setUp(): void
    {
        $this->object = new GalleryMediaObject();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'gallery',
            'mediaObject',
            'priority',
            'link',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale'
        ]);
    }
}
