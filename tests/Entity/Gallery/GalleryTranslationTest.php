<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Gallery\GalleryTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Gallery\GalleryTranslation
 */
class GalleryTranslationTest extends AbstractEntityTestCase
{
    protected GalleryTranslation $object;

    public function setUp(): void
    {
        $this->object = new GalleryTranslation();
    }

    public function testTranslatableProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'name',
            'description',
            'title',
            'metaKeywords',
            'canonicalLink',
            'metaRobots',
            'metaDescription',
            'slug',
        ]);
    }
}
