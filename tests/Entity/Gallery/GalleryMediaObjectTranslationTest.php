<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Gallery;

use Arodax\AdminBundle\Entity\Gallery\GalleryMediaObjectTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class GalleryMediaObjectTranslationTest extends AbstractEntityTestCase
{
    protected GalleryMediaObjectTranslation $object;

    public function setUp(): void
    {
        $this->object = new GalleryMediaObjectTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'link'
        ]);
    }
}
