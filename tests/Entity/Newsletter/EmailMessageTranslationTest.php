<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Newsletter;


use Arodax\AdminBundle\Entity\Newsletter\EmailMessageTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class EmailMessageTranslationTest extends AbstractEntityTestCase
{
    protected EmailMessageTranslation $object;

    protected function setUp(): void
    {
        $this->object = new EmailMessageTranslation();
    }

    public function test__construct()
    {
        $object = new EmailMessageTranslation();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'content',
            'translatable'
        ]);
    }

    public function testGettersAndSetters()
    {
        $this->object->setContent('foo');
        $this->assertEquals('foo', $this->object->getContent());
    }
}
