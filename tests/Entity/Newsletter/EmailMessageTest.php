<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Newsletter;

use Arodax\AdminBundle\Entity\Newsletter\EmailMessage;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class EmailMessageTest extends AbstractEntityTestCase
{
    protected EmailMessage $object;

    protected function setUp(): void
    {
        $this->object = new EmailMessage();
    }

    public function test__construct()
    {
        $object = new EmailMessage();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'context',
            'status',
            'createdAt',
            'updatedAt',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }

    public function testGettersAndSetters()
    {
        $this->object->setContext(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $this->object->getContext());

        $this->object->setStatus('foo');
        $this->assertEquals('foo', $this->object->getStatus());

        $now = new \DateTimeImmutable();

        $this->object->setCreatedAt($now);
        $this->assertEquals($now, $this->object->getCreatedAt());

        $this->object->setUpdatedAt($now);
        $this->assertEquals($now, $this->object->getUpdatedAt());
    }
}
