<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Newsletter;

use Arodax\AdminBundle\Entity\Newsletter\NewsletterRecipient;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class NewsletterRecipientTest extends AbstractEntityTestCase
{
    protected NewsletterRecipient $object;

    protected function setUp(): void
    {
        $this->object = new NewsletterRecipient();
    }

    public function test__construct()
    {
        $object = new NewsletterRecipient();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'email',
            'createdAt'
        ]);
    }

    public function testGettersAndSetters()
    {
        $this->object->setEmail('foo@example.com');
        $this->assertEquals('foo@example.com', $this->object->getEmail());

        $now =  new \DateTimeImmutable();

        $this->object->setCreatedAt($now);
        $this->assertEquals($now, $this->object->getCreatedAt());
    }
}
