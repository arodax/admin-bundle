<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Log;

use Arodax\AdminBundle\Entity\Log\Log;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class LogTest extends AbstractEntityTestCase
{
    protected Log $object;

    protected function setUp(): void
    {
        $this->object = new Log();
    }

    public function test__construct()
    {
        $object = new Log();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'message',
            'formattedMessage',
            'context',
            'level',
            'levelName',
            'extra',
            'createdAt',
            'user',
        ]);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Log\Log::setMessage
     * @covers \Arodax\AdminBundle\Entity\Log\Log::getMessage
     * @covers \Arodax\AdminBundle\Entity\Log\Log::setContext
     * @covers \Arodax\AdminBundle\Entity\Log\Log::getContext
     * @covers \Arodax\AdminBundle\Entity\Log\Log::setLevel
     * @covers \Arodax\AdminBundle\Entity\Log\Log::getLevel
     * @covers \Arodax\AdminBundle\Entity\Log\Log::setLevelName
     * @covers \Arodax\AdminBundle\Entity\Log\Log::getLevelName
     * @covers \Arodax\AdminBundle\Entity\Log\Log::setExtra
     * @covers \Arodax\AdminBundle\Entity\Log\Log::getExtra
     */
    public function testGettersAndSetters()
    {
        $this->object->setMessage('foo');
        $this->assertEquals('foo', $this->object->getMessage());

        $this->object->setContext(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $this->object->getContext());

        $this->object->setLevel(1);
        $this->assertEquals(1, $this->object->getLevel());

        $this->object->setLevelName('notice');
        $this->assertEquals('notice', $this->object->getLevelName());

        $this->object->setExtra(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $this->object->getExtra());
    }
}
