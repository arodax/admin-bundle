<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Localization;

use Arodax\AdminBundle\Entity\Localization\Translatable;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Localization\Translatable
 */
class TranslatableTest extends AbstractEntityTestCase
{
    protected Translatable $object;

    protected function setUp(): void
    {
        $this->object = new Translatable();
    }

    public function test__construct()
    {
        $object = new Translatable();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
           'id',
           'source',
           'translations',
           'newTranslations',
           'currentLocale',
           'defaultLocale',
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Localization\Translatable::setSource
     */
    public function testGettersAndSetters()
    {
        // status
        $this->object->setSource('translation_source');
        $this->assertEquals('translation_source', $this->object->getSource());
    }
}
