<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Localization\TranslatableTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Localization\TranslatableTranslation
 */
class TranslatableTranslationTest extends AbstractEntityTestCase
{
    protected TranslatableTranslation $object;

    public function setUp(): void
    {
        $this->object = new TranslatableTranslation();
    }

    public function testTranslatableProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'target',
        ]);
    }
}
