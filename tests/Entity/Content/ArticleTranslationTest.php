<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\Content\ArticleTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ArticleTranslationTest extends AbstractEntityTestCase
{
    protected ArticleTranslation $object;

    public function setUp(): void
    {
        $this->object = new ArticleTranslation();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'headline',
            'lead',
            'translatable',
            'content',
            'title',
            'metaKeywords',
            'canonicalLink',
            'metaRobots',
            'metaDescription',
            'slug',
            'link'
        ]);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setLead
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getLead
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setLocale
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getLocale
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setTranslatable
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getTranslatable
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::isEmpty
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getTitle
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setTitle
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setMetaKeywords
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getMetaKeywords
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getContent
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setContent
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getCanonicalLink
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setCanonicalLink
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getMetaRobots
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setMetaRobots
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getMetaDescription
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setMetaDescription
     */
    public function testGettersAndSetters(): void
    {
        $article = new Article();

        $this->object->setLead('lead');
        $this->assertEquals('lead', $this->object->getLead());

        $this->object->setHeadline('headline');
        $this->assertEquals('headline', $this->object->getHeadline());

        $this->object->setLocale('en');
        $this->assertEquals('en', $this->object->getLocale());

        $this->object->setTranslatable($article);
        $this->assertEquals($article, $this->object->getTranslatable());
        $this->assertFalse($this->object->isEmpty());

        $this->object->setTitle('title');
        $this->assertEquals('title', $this->object->getTitle());

        $this->object->setMetaKeywords(['foo', 'bar']);
        $this->assertEquals(['foo', 'bar'], $this->object->getMetaKeywords());

        $this->object->setContent('content');
        $this->assertEquals('content', $this->object->getContent());

        $this->object->setCanonicalLink('https://example.com/foo');
        $this->assertEquals('https://example.com/foo', $this->object->getCanonicalLink());

        $this->object->setMetaRobots(['index', 'follow']);
        $this->assertEquals(['index', 'follow'], $this->object->getMetaRobots());

        $this->object->setMetaDescription('description');
        $this->assertEquals('description', $this->object->getMetaDescription());
    }
}
