<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\Article;
use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ArticleTest extends AbstractEntityTestCase
{
    protected Article $object;

    protected function setUp(): void
    {
        $this->object = new Article();
    }

    public function test__construct(): void
    {
        $object = new Article();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'authors',
            'sections',
            'status',
            'createdAt',
            'updatedAt',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'publishedAt',
            'covers',
            'galleries',
            'readOnly',
            'priority',
            'deletable',
            'aclUsers',
            'aclUserGroups',
            'aclEnabled',
            'parameters'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::setLead
     * @covers \Arodax\AdminBundle\Entity\Content\ArticleTranslation::getLead
     * @covers \Arodax\AdminBundle\Entity\Content\Article::addAuthor
     * @covers \Arodax\AdminBundle\Entity\Content\Article::getAuthors
     * @covers \Arodax\AdminBundle\Entity\Content\Article::addSection
     * @covers \Arodax\AdminBundle\Entity\Content\Article::getSections
     * @covers \Arodax\AdminBundle\Entity\Content\Article::removeSection
     * @covers \Arodax\AdminBundle\Entity\Content\Article::setCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Article::getCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Article::getPublishedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Article::setPublishedAt
     */
    public function testGettersAndSetters(): void
    {
        // headline
        $this->object->translate('en')->setHeadLine('headline en');
        $this->object->translate('cs')->setHeadLine('headline cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('headline en', $this->object->translate('en')->getHeadline());
        $this->assertEquals('headline cs', $this->object->translate('cs')->getHeadline());

        // lead
        $this->object->translate('en')->setLead('lead en');
        $this->object->translate('cs')->setLead('lead cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('lead en', $this->object->translate('en')->getLead());
        $this->assertEquals('lead cs', $this->object->translate('cs')->getLead());

        // content
        $this->object->translate('en')->setContent('content en');
        $this->object->translate('cs')->setContent('content cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('content en', $this->object->translate('en')->getContent());
        $this->assertEquals('content cs', $this->object->translate('cs')->getContent());

        // authors
        $author = new User();
        $this->object->addAuthor($author);
        $this->assertContains($author, $this->object->getAuthors());
        $this->object->removeAuthor($author);
        $this->assertEquals([], $this->object->getAuthors());

        // sections
        $section = new Section();
        $this->object->addSection($section);
        $this->assertContains($section, $this->object->getSections());
        $this->object->removeSection($section);
        $this->assertEquals([], $this->object->getSections());

        // created at
        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getCreatedAt()->format('Y-m-d H:i:s'));

        // updated at
        $now = new \DateTimeImmutable();
        $this->object->setUpdatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getUpdatedAt()->format('Y-m-d H:i:s'));

        // published at
        $now = new \DateTimeImmutable();
        $this->object->setPublishedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getPublishedAt()->format('Y-m-d H:i:s'));

        // covers
        $covers = [['contentUrl' => 'https://picsum.photos/200/300'], ['contentUrl' => 'https://picsum.photos/200/300']];
        $this->object->setCovers($covers);
        $this->assertEquals($covers, $this->object->getCovers());
    }
}
