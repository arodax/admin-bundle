<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\Section;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class SectionTest extends AbstractEntityTestCase
{
    protected Section $object;

    protected function setUp(): void
    {
        $this->object = new Section();
    }

    public function test__construct(): void
    {
        $object = new Section();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'lft',
            'lvl',
            'rgt',
            'root',
            'parent',
            'children',
            'articles',
            'translations',
            'newTranslations',
            'status',
            'publishedAt',
            'createdAt',
            'updatedAt',
            'currentLocale',
            'defaultLocale',
            'readOnly',
            'deletable',
            'aclUsers',
            'aclUserGroups',
            'aclEnabled',
            'covers'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Content\Section::setCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Section::getCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Section::setUpdatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Section::getUpdatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Section::setEnabled
     * @covers \Arodax\AdminBundle\Entity\Content\Section::isEnabled
     */
    public function testGettersAndSetters()
    {
        // enabled
        $this->object->setStatus(Section::STATUS_DRAFT);
        $this->assertEquals(Section::STATUS_DRAFT, $this->object->getStatus());

        // created at
        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getCreatedAt()->format('Y-m-d H:i:s'));

        // updated at
        $now = new \DateTimeImmutable();
        $this->object->setUpdatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getUpdatedAt()->format('Y-m-d H:i:s'));

        // published at
        $now = new \DateTimeImmutable();
        $this->object->setPublishedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getPublishedAt()->format('Y-m-d H:i:s'));

        // parent
        $parent = new Section();
        $this->object->setParent($parent);
        $this->assertEquals($parent, $this->object->getParent());

        // covers
        $covers = [['contentUrl' => 'https://picsum.photos/200/300'], ['contentUrl' => 'https://picsum.photos/200/300']];
        $this->object->setCovers($covers);
        $this->assertEquals($covers, $this->object->getCovers());
    }
}
