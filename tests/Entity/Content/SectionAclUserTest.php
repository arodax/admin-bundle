<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\SectionAclUser;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class SectionAclUserTest extends AbstractEntityTestCase
{
    protected SectionAclUser $object;

    protected function setUp(): void
    {
        $this->object = new SectionAclUser();
    }

    public function test__construct(): void
    {
        $object = new SectionAclUser();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'accessed',
            'accessor',
            'attributes'
        ]);
    }
}
