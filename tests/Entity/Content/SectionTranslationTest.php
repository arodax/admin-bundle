<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\SectionTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Content\SectionTranslation
 */
class SectionTranslationTest extends AbstractEntityTestCase
{
    protected SectionTranslation $object;

    public function setUp(): void
    {
        $this->object = new SectionTranslation();
    }

    public function testTranslatableProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'name',
            'description',
            'path',
            'slug'
        ]);
    }
}
