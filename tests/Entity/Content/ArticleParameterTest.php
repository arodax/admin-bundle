<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\Content\ArticleParameter;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ArticleParameterTest extends AbstractEntityTestCase
{
    protected ArticleParameter $object;

    public function setUp(): void
    {
        $this->object = new ArticleParameter();
    }

    /**
     * @throws \ReflectionException
     */
    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'name',
            'value',
            'article'
        ]);
    }
}
