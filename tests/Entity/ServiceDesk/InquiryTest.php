<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Content;

use Arodax\AdminBundle\Entity\ServiceDesk\Inquiry;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class InquiryTest extends AbstractEntityTestCase
{
    protected Inquiry $object;

    protected function setUp(): void
    {
        $this->object = new Inquiry();
    }

    public function test__construct()
    {
        $object = new Inquiry();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'phone',
            'personalName',
            'companyName',
            'subject',
            'body',
            'email',
            'enabled',
            'createdAt',
            'updatedAt',
            'status',
            'data',
            'conversion'
        ]);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::setEmail
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::getEmail
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::getPhone
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::setPhone
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::getPersonalName
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::setPersonalName
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::getCompanyName
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::setCompanyName
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::getSubject
     * @covers \Arodax\AdminBundle\Entity\ServiceDesk\Inquiry::setSubject
     */
    public function testGettersAndSetters()
    {
        $this->object->setEmail('foo@example.com');
        $this->assertEquals('foo@example.com', $this->object->getEmail());

        $this->object->setPhone('+420 123 456 789');
        $this->assertEquals('+420 123 456 789', $this->object->getPhone());

        $this->object->setPersonalName('John Doe');
        $this->assertEquals('John Doe', $this->object->getPersonalName());

        $this->object->setCompanyName('ACME Ltd.');
        $this->assertEquals('ACME Ltd.', $this->object->getCompanyName());

        $this->object->setSubject('Subject');
        $this->assertEquals('Subject', $this->object->getSubject());

        $this->object->setBody('Body');
        $this->assertEquals('Body', $this->object->getBody());

        // created at
        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getCreatedAt()->format('Y-m-d H:i:s'));

        // updated at
        $now = new \DateTimeImmutable();
        $this->object->setUpdatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getUpdatedAt()->format('Y-m-d H:i:s'));
    }
}
