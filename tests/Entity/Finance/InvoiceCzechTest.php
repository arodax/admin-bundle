<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Finance\InvoiceCzech;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class InvoiceCzechTest extends AbstractEntityTestCase
{
    protected InvoiceCzech $object;

    public function setUp(): void
    {
        $this->object = new InvoiceCzech();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'createdAt',
            'updatedAt',
            'status',
            'supplier',
            'customer',
            'invoiceNumber',
            'variableNumber',
            'constantNumber',
            'specificNumber',
            'issuedAt',
            'taxableSupplyAt',
            'dueAt',
            'paidAt',
            'orderNumber',
            'paymentInfo',
            'orderItems',
            'vatInvoice',
            'currency',
            'bankAccountNumber',
            'iban',
            'swift'
        ]);
    }
}
