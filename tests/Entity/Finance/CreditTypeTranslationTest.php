<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Finance;

use Arodax\AdminBundle\Entity\Finance\CreditType;
use Arodax\AdminBundle\Entity\Finance\CreditTypeTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Finance\CreditTypeTranslation
 */
class CreditTypeTranslationTest extends AbstractEntityTestCase
{
    protected CreditTypeTranslation $object;

    protected function setUp(): void
    {
        $this->object = new CreditTypeTranslation();
    }

    public function testTranslatableProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'title',
            'translatable',
        ]);
    }
}
