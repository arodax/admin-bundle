<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Finance\OrderItem;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class OrderItemTest extends AbstractEntityTestCase
{
    protected OrderItem $object;

    public function setUp(): void
    {
        $this->object = new OrderItem();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'description',
            'quantity',
            'unitPrice',
            'taxRate',
            'taxPrice',
            'discountPrice',
            'totalPriceWithoutTax',
            'totalPriceWithTax'
        ]);
    }
}
