<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Finance;

use Arodax\AdminBundle\Entity\Finance\Credit;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Finance\Credit
 */
class CreditTest extends AbstractEntityTestCase
{
    protected Credit $object;

    protected function setUp(): void
    {
        $this->object = new Credit();
    }

    public function testTranslatableProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'value',
            'creditType',
        ]);
    }
}
