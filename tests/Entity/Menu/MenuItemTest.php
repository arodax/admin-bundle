<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Menu;

use Arodax\AdminBundle\Entity\Menu\MenuItem;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem
 */
class MenuItemTest extends AbstractEntityTestCase
{
    protected MenuItem $object;

    protected function setUp(): void
    {
        $this->object = new MenuItem();
    }

    public function testTranslatableProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'nodeName',
            'lft',
            'lvl',
            'rgt',
            'root',
            'parent',
            'icon',
            'children',
            'translations',
            'newTranslations',
            'enabled',
            'currentLocale',
            'defaultLocale',
            'security',
            'readOnly',
            'deletable',
            'internal'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::getNodeName
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::setNodeName
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::getParent
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::setParent
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::getChildren
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::getIcon
     * @covers \Arodax\AdminBundle\Entity\Menu\MenuItem::setIcon
     */
    public function testGettersAndSetters()
    {
        $this->object->setNodeName('test');
        $this->assertEquals('test', $this->object->getNodeName());

        $parent = new MenuItem();
        $this->object->setParent($parent);
        $this->assertEquals($parent, $this->object->getParent());

        $this->object->setIcon('foo');
        $this->assertEquals('foo', $this->object->getIcon());
    }
}
