<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Menu;

use Arodax\AdminBundle\Entity\Menu\MenuItemTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Menu\MenuItemTranslation
 */
class MenuItemTranslationTest extends AbstractEntityTestCase
{
    protected MenuItemTranslation $object;

    protected function setUp(): void
    {
        $this->object = new MenuItemTranslation();
    }

    public function testTranslatableProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'title',
            'link',
            'routeParams',
            'linkType',
        ]);
    }
}
