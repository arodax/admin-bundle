<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Score;

use Arodax\AdminBundle\Entity\Score\Score;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\Score\Score
 */
class ScoreTest extends AbstractEntityTestCase
{
    protected Score $object;

    protected function setUp(): void
    {
        $this->object = new Score();
    }

    public function testTranslatableProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'createdAt',
            'value',
            'type',
            'reason',
            'createdBy'
        ]);
    }
}
