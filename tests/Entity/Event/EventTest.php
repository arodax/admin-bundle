<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Event;

use Arodax\AdminBundle\Entity\Event\Event;
use Arodax\AdminBundle\Entity\Event\EventType;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class EventTest extends AbstractEntityTestCase
{
    protected Event $object;

    protected function setUp(): void
    {
        $this->object = new Event();
    }

    public function test__construct(): void
    {
        $object = new Event();
        $this->assertEquals($this->object, $object);
    }

    /**
     * @throws \ReflectionException
     */
    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'authors',
            'status',
            'createdAt',
            'updatedAt',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'publishedAt',
            'startAt',
            'endAt',
            'allDay',
            'recurrenceCount',
            'recurrenceInterval',
            'recurrenceFrequency',
            'byDay',
            'byMonthDay',
            'byYearDay',
            'byMonth',
            'recurrenceCollection',
            'geographyPoints',
            'aclUsers',
            'aclUserGroups',
            'aclEnabled',
            'color',
            'eventTypes',
            'covers',
            'galleries'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\Content\EventTranslation::setHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\EventTranslation::getHeadline
     * @covers \Arodax\AdminBundle\Entity\Content\EventTranslation::setLead
     * @covers \Arodax\AdminBundle\Entity\Content\EventTranslation::getLead
     * @covers \Arodax\AdminBundle\Entity\Content\Event::setLead
     * @covers \Arodax\AdminBundle\Entity\Content\Event::getLead
     * @covers \Arodax\AdminBundle\Entity\Content\Event::removeChapter
     * @covers \Arodax\AdminBundle\Entity\Content\Event::addAuthor
     * @covers \Arodax\AdminBundle\Entity\Content\Event::getAuthors
     * @covers \Arodax\AdminBundle\Entity\Content\Event::addSection
     * @covers \Arodax\AdminBundle\Entity\Content\Event::getSections
     * @covers \Arodax\AdminBundle\Entity\Content\Event::removeSection
     * @covers \Arodax\AdminBundle\Entity\Content\Event::setCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Event::getCreatedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Event::getPublishedAt
     * @covers \Arodax\AdminBundle\Entity\Content\Event::setPublishedAt
     */
    public function testGettersAndSetters(): void
    {
        // headline
        $this->object->translate('en')->setHeadLine('headline en');
        $this->object->translate('cs')->setHeadLine('headline cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('headline en', $this->object->translate('en')->getHeadline());
        $this->assertEquals('headline cs', $this->object->translate('cs')->getHeadline());

        // lead
        $this->object->translate('en')->setLead('lead en');
        $this->object->translate('cs')->setLead('lead cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('lead en', $this->object->translate('en')->getLead());
        $this->assertEquals('lead cs', $this->object->translate('cs')->getLead());

        // content
        $this->object->translate('en')->setContent('content en');
        $this->object->translate('cs')->setContent('content cs');
        $this->object->mergeNewTranslations();
        $this->assertEquals('content en', $this->object->translate('en')->getContent());
        $this->assertEquals('content cs', $this->object->translate('cs')->getContent());

        // authors
        $author = new User();
        $this->object->addAuthor($author);
        $this->assertContains($author, $this->object->getAuthors());
        $this->object->removeAuthor($author);
        $this->assertEquals([], $this->object->getAuthors());

        // created at
        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getCreatedAt()->format('Y-m-d H:i:s'));

        // updated at
        $now = new \DateTimeImmutable();
        $this->object->setUpdatedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getUpdatedAt()->format('Y-m-d H:i:s'));

        // published at
        $now = new \DateTimeImmutable();
        $this->object->setPublishedAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getPublishedAt()->format('Y-m-d H:i:s'));

        // start date
        $now = new \DateTimeImmutable();
        $this->object->setStartAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getStartAt()->format('Y-m-d H:i:s'));

        // end date
        $now = new \DateTimeImmutable();
        $this->object->setEndAt($now);
        $this->assertEquals($now->format('Y-m-d H:i:s'), $this->object->getEndAt()->format('Y-m-d H:i:s'));

        // all day
        $this->object->setAllDay(true);
        $this->assertTrue($this->object->isAllDay());

        // count
        $this->object->setRecurrenceCount(10);
        $this->assertEquals(10, $this->object->getRecurrenceCount());

        // interval
        $this->object->setRecurrenceInterval(1);
        $this->assertEquals(1, $this->object->getRecurrenceInterval());

        // frequency
        $this->object->setRecurrenceFrequency(1);
        $this->assertEquals(1, $this->object->getRecurrenceFrequency());

        // by day
        $this->object->setByDay([1, 2, 3]);
        $this->assertEquals([1, 2, 3], $this->object->getByDay());

        // by month day
        $this->object->setByMonthDay([1, -1]);
        $this->assertEquals([1, -1], $this->object->getByMonthDay());

        // by year day
        $this->object->setByYearDay([1, -1]);
        $this->assertEquals([1, -1], $this->object->getByYearDay());

        // by month
        $this->object->setByMonth([1, 6]);
        $this->assertEquals([1, 6], $this->object->getByMonth());

        // event types
        $eventType = new EventType();
        $this->object->addEventType($eventType);
        $this->assertContains($eventType, $this->object->getEventTypes());
        $this->object->removeEventType($eventType);
        $this->assertEquals([], $this->object->getEventTypes());

        // covers
        $covers = [['contentUrl' => 'https://picsum.photos/200/300'], ['contentUrl' => 'https://picsum.photos/200/300']];
        $this->object->setCovers($covers);
        $this->assertEquals($covers, $this->object->getCovers());
    }
}
