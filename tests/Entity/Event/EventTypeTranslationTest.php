<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Event;

use Arodax\AdminBundle\Entity\Event\EventTypeTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class EventTypeTranslationTest extends AbstractEntityTestCase
{
    protected EventTypeTranslation $object;

    public function setUp(): void
    {
        $this->object = new EventTypeTranslation();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'name',
            'translatable'
        ]);
    }
}
