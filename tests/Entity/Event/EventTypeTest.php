<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Event;

use Arodax\AdminBundle\Entity\Event\EventType;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class EventTypeTest extends AbstractEntityTestCase
{
    protected EventType $object;

    public function setUp(): void
    {
        $this->object = new EventType();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'enabled',
            'color'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
