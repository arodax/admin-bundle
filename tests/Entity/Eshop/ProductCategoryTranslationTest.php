<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductCategoryTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductCategoryTranslationTest extends AbstractEntityTestCase
{
    protected ProductCategoryTranslation $object;

    public function setUp(): void
    {
        $this->object = new ProductCategoryTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'name',
            'title',
            'description',
            'locale',
            'metaKeywords',
            'canonicalLink',
            'metaRobots',
            'metaDescription',
            'slug',
            'translatable',
        ]);
    }
}
