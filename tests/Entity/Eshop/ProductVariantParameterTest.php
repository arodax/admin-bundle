<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariantParameter;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductVariantParameterTest extends AbstractEntityTestCase
{
    protected ProductVariantParameter $object;

    public function setUp(): void
    {
        $this->object = new ProductVariantParameter();
    }

    /**
     * @throws \ReflectionException
     */
    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'name',
            'value',
            'productVariant'
        ]);
    }
}
