<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariantTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductVariantTranslationTest extends AbstractEntityTestCase
{
    protected ProductVariantTranslation $object;

    public function setUp(): void
    {
        $this->object = new ProductVariantTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'name',
            'content',
            'translatable'
        ]);
    }
}
