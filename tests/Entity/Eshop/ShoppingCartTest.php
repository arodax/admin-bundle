<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ShoppingCart;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ShoppingCartTest extends AbstractEntityTestCase
{
    protected ShoppingCart $object;

    public function setUp(): void
    {
        $this->object = new ShoppingCart();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'items',
            'createdAt',
            'updatedAt',
            'status',
            'currency',
            'priceWithoutTax',
            'priceWithTax',
            'order',
            'sessionId'
        ]);
    }
}
