<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\Product;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductTest extends AbstractEntityTestCase
{
    protected Product $object;

    public function setUp(): void
    {
        $this->object = new Product();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'status',
            'createdAt',
            'updatedAt',
            'publishedAt',
            'translations',
            'newTranslations',
            'categories',
            'variants',
            'currentLocale',
            'defaultLocale'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }
}
