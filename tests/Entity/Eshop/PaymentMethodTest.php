<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\PaymentMethod;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class PaymentMethodTest extends AbstractEntityTestCase
{
    protected PaymentMethod $object;

    public function setUp(): void
    {
        $this->object = new PaymentMethod();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'provider',
            'credentialExpressions',
            'enabled',
            'shippingMethods',
            'deletable',
            'readOnly',
            'priceWithoutTax',
            'taxRate',
            'taxPrice',
            'priceWithTax',
            'currency'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
