<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\PaymentMethodTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class PaymentMethodTranslationTest extends AbstractEntityTestCase
{
    protected PaymentMethodTranslation $object;

    public function setUp(): void
    {
        $this->object = new PaymentMethodTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'translatable',
            'locale',
            'name',
            'description'
        ]);
    }
}
