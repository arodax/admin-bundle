<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariantPrice;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductVariantPriceTest extends AbstractEntityTestCase
{
    protected ProductVariantPrice $object;

    public function setUp(): void
    {
        $this->object = new ProductVariantPrice();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'priceWithoutTax',
            'taxRate',
            'taxPrice',
            'priceWithTax',
            'currency',
            'productVariant',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
        ]);
    }
}
