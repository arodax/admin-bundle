<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductCategory;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductCategoryTest extends AbstractEntityTestCase
{
    protected ProductCategory $object;

    public function setUp(): void
    {
        $this->object = new ProductCategory();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'lft',
            'lvl',
            'rgt',
            'root',
            'parent',
            'children',
            'translations',
            'newTranslations',
            'status',
            'publishedAt',
            'createdAt',
            'covers',
            'products',
            'currentLocale',
            'defaultLocale',
            'updatedAt',
            'readOnly',
            'deletable'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }
}
