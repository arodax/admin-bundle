<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Entity\Eshop\ShippingMethod;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ShippingMethodTest extends AbstractEntityTestCase
{
    protected ShippingMethod $object;

    public function setUp(): void
    {
        $this->object = new ShippingMethod();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'enabled',
            'postagePriceRules',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'supportedCountries',
            'paymentMethods',
            'deletable',
            'readOnly',
            'provider'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
