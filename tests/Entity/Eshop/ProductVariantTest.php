<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ProductVariant;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ProductVariantTest extends AbstractEntityTestCase
{
    protected ProductVariant $object;

    public function setUp(): void
    {
        $this->object = new ProductVariant();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'status',
            'createdAt',
            'updatedAt',
            'publishedAt',
            'translations',
            'newTranslations',
            'product',
            'prices',
            'covers',
            'currentLocale',
            'defaultLocale',
            'ean13',
            'code',
            'quantityAvailable',
            'parameters'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }
}
