<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\ShoppingCartItem;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class ShoppingCartItemTest extends AbstractEntityTestCase
{
    protected ShoppingCartItem $object;

    public function setUp(): void
    {
        $this->object = new ShoppingCartItem();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'shoppingCart',
            'createdAt',
            'updatedAt',
            'productVariant',
            'productVariantPrice',
            'quantity',
            'priceWithoutTax',
            'taxRate',
            'taxPrice',
            'priceWithTax',
            'currency',
            'note'
        ]);
    }
}
