<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Eshop;

use Arodax\AdminBundle\Entity\Eshop\Order;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class OrderTest extends AbstractEntityTestCase
{
    protected Order $object;

    public function setUp(): void
    {
        $this->object = new Order();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'status',
            'createdAt',
            'updatedAt',
            'shoppingCart',
            'billingCompanyName',
            'billingCompanyId',
            'billingTaxId',
            'billingFirstName',
            'billingLastName',
            'billingStreet',
            'billingCity',
            'billingPostCode',
            'billingCountry',
            'shippingCompanyName',
            'shippingFirstName',
            'shippingLastName',
            'shippingEmail',
            'shippingPhoneNumber',
            'shippingStreet',
            'shippingCity',
            'shippingPostCode',
            'shippingCountry',
            'currency',
            'priceWithoutTax',
            'priceWithTax',
            'billingEmail',
            'billingPhoneNumber',
            'billingHonorific',
            'billingAddressDescription',
            'shippingHonorific',
            'shippingAddressDescription',
            'orderNumber',
            'shippingMethod',
            'paymentMethod',
            'shippingPriceWithoutTax',
            'shippingPriceWithTax',
            'payments',
            'paymentPriceWithoutTax',
            'paymentPriceWithTax',
        ]);
    }
}
