<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\User;

use Arodax\AdminBundle\Entity\User\UserPasswordLog;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class UserPasswordLogTest extends AbstractEntityTestCase
{
    protected UserPasswordLog $object;

    public function setUp(): void
    {
        $this->object = new UserPasswordLog();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'updatedBy',
            'currentPasswordHash',
            'createdAt'
        ]);
    }
}
