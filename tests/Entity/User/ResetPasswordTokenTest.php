<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Menu;

use Arodax\AdminBundle\Entity\User\ResetPasswordToken;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken
 */
class ResetPasswordTokenTest extends AbstractEntityTestCase
{
    protected ResetPasswordToken $object;

    protected function setUp(): void
    {
        $this->object = new ResetPasswordToken();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'user',
            'hash',
            'createdAt',
            'validTill',
        ]);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::getUser
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::setUser
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::getHash
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::setHash
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::getCreatedAt
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::setCreatedAt
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::getValidTill
     * @covers \Arodax\AdminBundle\Entity\User\ResetPasswordToken::setValidTill
     */
    public function testGettersAndSetters()
    {
        $this->object = new ResetPasswordToken();

        $user = new User();
        $this->object->setUser($user);
        $this->assertEquals($user, $this->object->getUser());

        $hash = 'choh1jahng6eibei6pohghah4eiveicie1aeX2aeTooMaiTae';
        $this->object->setHash($hash);
        $this->assertEquals($hash, $this->object->getHash());

        $now = new \DateTimeImmutable();
        $this->object->setCreatedAt($now);
        $this->assertEquals($now, $this->object->getCreatedAt());

        $this->object->setValidTill($now);
        $this->assertEquals($now, $this->object->getValidTill());
    }
}
