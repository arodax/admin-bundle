<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Menu;

use Arodax\AdminBundle\Entity\User\UserGroupTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\User\UserGroupTranslation
 */
class UserGroupTranslationTest extends AbstractEntityTestCase
{
    protected UserGroupTranslation $object;

    public function setUp(): void
    {
        $this->object = new UserGroupTranslation();
    }

    public function testTranslatableProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'translatable',
            'name',
            'description',
        ]);
    }
}
