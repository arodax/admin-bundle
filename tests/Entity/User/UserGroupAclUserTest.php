<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\User;

use Arodax\AdminBundle\Entity\User\UserGroupAclUser;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class UserGroupAclUserTest extends AbstractEntityTestCase
{
    protected UserGroupAclUser $object;

    protected function setUp(): void
    {
        $this->object = new UserGroupAclUser();
    }

    public function test__construct(): void
    {
        $object = new UserGroupAclUser();
        $this->assertEquals($this->object, $object);
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'accessed',
            'accessor',
            'attributes'
        ]);
    }
}
