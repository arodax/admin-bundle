<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\User;

use Arodax\AdminBundle\Entity\Contact\Contact;
use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class UserTest extends AbstractEntityTestCase
{
    protected User $object;

    public function setUp(): void
    {
        $this->object = new User();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'email',
            'displayName',
            'avatar',
            'color',
            'roles',
            'password',
            'plainPassword',
            'userContacts',
            'createdAt',
            'updatedAt',
            'userGroups',
            'enabled',
            'lastActivityAt',
            'timezone',
            'preferredDateFormat',
            'preferredTimeFormat',
            'preferredHome',
            'preferredLocale',
            'lastLinkRequestedAt',
            'firstName',
            'lastName',
            'extendedRoles',
            'totalScore',
            'scores',
            'credits'
        ]);
    }
}
