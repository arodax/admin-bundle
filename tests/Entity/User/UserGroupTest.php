<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\User;

use Arodax\AdminBundle\Entity\User\User;
use Arodax\AdminBundle\Entity\User\UserGroup;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

/**
 * @covers \Arodax\AdminBundle\Entity\User\UserGroup
 */
class UserGroupTest extends AbstractEntityTestCase
{
    protected UserGroup $object;

    protected function setUp(): void
    {
        $this->object = new UserGroup();
    }

    public function testProperties(): void
    {
        $this->assertProperties($this->object, [
            'id',
            'users',
            'enabled',
            'translations',
            'newTranslations',
            'currentLocale',
            'defaultLocale',
            'color',
            'totalScore',
            'teamLeader',
            'gamification',
            'readOnly',
            'deletable',
            'aclUsers',
            'aclEnabled'
        ]);
    }

    public function testTranslatable(): void
    {
        $this->assertBeingTranslatable($this->object);
    }

    /**
     * @covers \Arodax\AdminBundle\Entity\User\UserGroup::addUser
     * @covers  \Arodax\AdminBundle\Entity\User\UserGroup::getUsers
     * @covers \Arodax\AdminBundle\Entity\User\UserGroup::setEnabled
     * @covers \Arodax\AdminBundle\Entity\User\UserGroup::isEnabled
     * @covers \Arodax\AdminBundle\Entity\User\UserGroupTranslation::setName
     * @covers \Arodax\AdminBundle\Entity\User\UserGroupTranslation::getName
     */
    public function testGettersAndSetters(): void
    {
        $user = new User();
        $this->object->addUser($user);
        $this->assertContains($user, $this->object->getUsers());

        $this->object->setEnabled(true);
        $this->assertTrue($this->object->isEnabled());

        $this->object->translate('cs')->setName('name cs');
        $this->object->translate('en')->setName('name en');
        $this->object->mergeNewTranslations();

        $this->assertEquals('name cs', $this->object->translate('cs')->getName());
        $this->assertEquals('name en', $this->object->translate('en')->getName());
    }
}
