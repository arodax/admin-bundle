<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Media;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class MediaObjectTest extends AbstractEntityTestCase
{
    protected MediaObject $object;

    public function setUp(): void
    {
        $this->object = new MediaObject();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'contentUrl',
            'file',
            'originalFileName',
            'filePath',
            'absoluteFilePath',
            'mime',
            'priority',
            'translations',
            'newTranslations',
            'metadata',
            'currentLocale',
            'defaultLocale'
        ]);
    }

    public function testTranslatable()
    {
        $this->assertBeingTranslatable($this->object);
    }
}
