<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Entity\Media;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Arodax\AdminBundle\Entity\Media\MediaObjectTranslation;
use Arodax\AdminBundle\Tests\Entity\AbstractEntityTestCase;

class MediaObjectTranslationTest extends AbstractEntityTestCase
{
    protected MediaObjectTranslation $object;

    public function setUp(): void
    {
        $this->object = new MediaObjectTranslation();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'id',
            'locale',
            'description',
            'translatable'
        ]);
    }
}
