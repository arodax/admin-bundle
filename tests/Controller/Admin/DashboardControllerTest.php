<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Admin;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @todo: Implement controller tests
 */
class DashboardControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testIndex(): void
    {
        $client = static::createClient();
        $uri = '/cs/admin/';

        $this->loginUser($client, 'admin@example.com');

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertEquals(301, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 301, $response->getStatusCode()));
        $this->assertEquals('text/html; charset=utf-8', $response->headers->get('Content-Type'), sprintf('Failed to receive "text/html" content-type header, got "%s.', $response->headers->get('content-type')));
    }
}
