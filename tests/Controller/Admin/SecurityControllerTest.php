<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Mailer\DataCollector\MessageDataCollector;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SecurityControllerTest extends WebTestCase
{
    /**
     * @covers \Arodax\AdminBundle\Controller\Admin\SecurityController::login
     */
    public function testLogin(): void
    {
        $client = static::createClient();
        $client->request('GET','/en/admin/login');

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 200, $response->getStatusCode()));

        preg_match('/data-csrf="(.*)"/', $response->getContent(), $matches);

        $client->request('POST','/en/admin/login', [
            'email' => 'admin@example.com',
            'password' => 'test',
            '_token' => $matches[1] ?? null
        ]);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 200, $response->getStatusCode()));

        $client->request('POST','/en/admin/login', [
            'email' => 'admin@example.com',
            'password' => 'invalid-password',
            '_token' => $matches[1] ?? null
        ]);

        $response = $client->getResponse();

        $this->assertEquals(401, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 401, $response->getStatusCode()));
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Admin\SecurityController::checkLoginLink
     */
    public function testCheckLoginLink(): void
    {
        $client = static::createClient();
        $client->request('GET','/en/admin/login-check');

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 200, $response->getStatusCode()));
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Admin\SecurityController::sendLoginLink
     */
    public function testSendLoginLink(): void
    {
        $client = static::createClient();
        $client->enableProfiler();

        $client->request('POST','/en/admin/login-link', ['email' => 'admin@example.com']);
        $response = $client->getResponse();

        $this->assertEquals(204, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 204, $response->getStatusCode()));

        self::assertEmailCount(1);
        /** @var MessageDataCollector  $mailCollector */

        $mailCollector = $client->getProfile()->getCollector('mailer');

        $message = $mailCollector->getEvents()->getMessages()[0] ?? null;

        $this->assertNotNull($message, 'Expected to find a sent message, but null found.');

        $link = $message->getContext()['action_url'] ?? null;

        $this->assertNotNull($message, 'Expected to find a login link, but none found.');

        $client->request('GET', $link);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 200, $response->getStatusCode()));

        /** @var TokenInterface $token */
        $token = $client->getContainer()->get('security.token_storage')->getToken();

        $this->assertNull($token, 'Expected to not find a token, but token has been found');

        $client->request('POST', $link);
        $response = $client->getResponse();

        $token = $client->getContainer()->get('security.token_storage')->getToken();

        $this->assertNotNull($token->getUser(), 'Expected to log in user, but not found');

        $this->assertEquals(302, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 302, $response->getStatusCode()));

        $client->getContainer()->get('security.token_storage')->setToken(null);

        $client->request('POST','/en/admin/login-link', ['email' => 'invalid-user@example.com']);

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode(), sprintf('Expected "%s" to be a reponse code but "%s" given', 400, $response->getStatusCode()));
    }
}
