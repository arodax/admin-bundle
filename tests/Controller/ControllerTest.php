<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Finder\Finder;

class ControllerTest extends WebTestCase
{
    public function testController(): void
    {
        $finder = new Finder();
        $files = $finder->in(dirname(__DIR__).'/../src/Controller')->name('*.php');
        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            $relativeControllerPath = str_replace(dirname(__DIR__).'/../src/Controller', '', $file->getPathname());
            $classFileName = $file->getBasename('.'.$file->getExtension());
            $testFileName = '/Controller/'.ltrim(rtrim(str_replace($file->getBasename(), '', $relativeControllerPath), '/').'/'.$classFileName.'Test.php','/');
            $exist = dirname(__DIR__).$testFileName;
            $this->assertTrue(file_exists($exist), sprintf('Expected to find test in \'%s\' but this file does not exist.', $exist));
        }
    }
}
