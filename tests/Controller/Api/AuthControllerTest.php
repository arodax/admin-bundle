<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthControllerTest extends WebTestCase
{
    /**
     * @covers \Arodax\AdminBundle\Controller\Api\AuthController::index
     */
    public function testIndex()
    {
        $uri = '/api/auth';
        $client = static::createClient();

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));
        $this->assertEquals('null', $response->getContent(), 'Expected "null" to be returned');
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\AuthController::index
     */
    public function testLogout()
    {
        $uri = '/api/auth';
        $client = static::createClient();

        $client->request('DELETE', $uri);
        $response = $client->getResponse();

        $this->assertEquals(204, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 204, $response->getStatusCode()));
    }
}
