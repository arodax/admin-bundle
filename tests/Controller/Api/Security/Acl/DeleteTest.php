<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api\Security\Acl;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \Arodax\AdminBundle\Controller\Api\Set
 */
class DeleteTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testInvoke(): void
    {
        $client = static::createClient();

        $uri = '/api/security/acl';

        $client->request('DELETE', $uri);
        $response = $client->getResponse();
        $this->assertEquals(302, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 302, $response->getStatusCode()));

        $this->loginUser($client, 'admin@example.com');

        $client->request('DELETE', $uri);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 400, $response->getStatusCode()));
    }
}
