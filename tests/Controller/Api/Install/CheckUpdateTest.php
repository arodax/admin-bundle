<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api\Install;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CheckUpdateTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testCheckUpdate(): void
    {
        // Create a client to make requests
        $client = static::createClient();

        // Make a request to the /api/install/check-update route
        $client->request(method: 'GET', uri: '/api/install/check-update');

        $this->assertEquals(expected: Response::HTTP_FOUND, actual: $client->getResponse()->getStatusCode());

        // next run with authentication
        $this->loginUser($client, email: 'admin@example.com');

        $client->request(method: 'GET', uri: '/api/install/check-update');
        $response = $client->getResponse();

        $this->assertEquals(expected: Response::HTTP_OK, actual:  $client->getResponse()->getStatusCode(), message: sprintf('Failed to make a GET request to the "/api/install/check-update" route, got "%s" status code.', $client->getResponse()->getStatusCode()));
        $this->assertEquals(expected: 'application/json', actual: $response->headers->get('Content-Type'), message: sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));


        // Decode the JSON response
        $responseData = json_decode($response->getContent(), true);

        $this->assertArrayHasKey(key: 'installedVersion', array: $responseData, message: 'Failed to receive "installedVersion" key in the response.');
        $this->assertArrayHasKey(key: 'latestVersion', array: $responseData, message: 'Failed to receive "latestVersion" key in the response.');
    }
}
