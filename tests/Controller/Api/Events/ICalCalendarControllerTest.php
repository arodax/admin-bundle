<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api\Events;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \Arodax\AdminBundle\Controller\Api\Events\ICalCalendarController
 * @todo: Implement controller tests
 */
class ICalCalendarControllerTest extends WebTestCase
{
    use AuthenticationTrait;
}
