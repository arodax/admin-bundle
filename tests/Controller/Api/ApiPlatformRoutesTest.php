<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiPlatformRoutesTest extends WebTestCase
{
    use AuthenticationTrait;
        private array $apiRoutes = [
            '/api/contacts',
            '/api/contact-items',
            '/api/articles',
            '/api/article-parameters',
            '/api/sections',
            '/api/orders',
            '/api/payment-methods',
            '/api/products',
            '/api/product-categories',
            '/api/product-variants',
            '/api/product-variant-parameters',
            '/api/product-variant-prices',
            '/api/shipping-methods',
            '/api/shopping-carts',
            '/api/shopping-cart-items',
            '/api/events',
            '/api/event-types',
            '/api/bank-accounts',
            '/api/invoices',
            '/api/galleries',
            '/api/gallery-media-objects',
            '/api/points',
            '/api/grant-applicant-types',
            '/api/grant-calls',
            '/api/grant-focus-types',
            '/api/grant-projects',
            '/api/grant-project-groups',
            '/api/grant-project-types',
            '/api/translatables',
            '/api/logs',
            '/api/media-objects',
            '/api/menu-items',
            '/api/email-messages',
            '/api/newsletter-recipients',
            '/api/scores',
            '/api/inquiries',
            '/api/settings',
            '/api/users',
            '/api/user-groups',
            '/api/credits',
            '/api/credit-types',
            '/api/credit-logs',
            '/api/total-score-logs',
        ];


    public function testRoutesCoverage(): void
    {
        $client = static::createClient();
        $router = $client->getContainer()->get('router');
        $allRoutes = $router->getRouteCollection();

        $apiRoutePatterns = [];
        foreach ($allRoutes as $route => $details) {
            if (strpos($route, '_api_') === 0 && strpos($route, '_get_collection') !== false) {
                $apiRoutePatterns[] = $this->formatRouteForTest($details->getPath());
            }
        }

        sort($this->apiRoutes);
        sort($apiRoutePatterns);

        $this->assertEquals($this->apiRoutes, $apiRoutePatterns, 'API routes in the test do not match the actual API routes');
    }

    public function testCollectionRoutes(): void
    {
        $client = static::createClient();

        // first run without authentication
        foreach ($this->apiRoutes as $route) {
            $client->request('GET', $route);
            $response = $client->getResponse();

            $this->assertEquals(401, $response->getStatusCode(), sprintf('Expected HTTP status code 401 for unauthorized access to %s, but got "%s".', $route, $response->getStatusCode()));
        }

        // next run with authentication
        $this->loginUser($client, 'admin@example.com');

        foreach ($this->apiRoutes as $route) {
            $client->request('GET', $route);
            $response = $client->getResponse();
            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code for %s, expected "%s" but got "%s".', $route, 200, $response->getStatusCode()));
            $this->assertEquals('application/ld+json; charset=utf-8', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/ld+json; charset=utf-8" content-type header, got "%s.', $response->headers->get('content-type')));
        }
    }

    private function formatRouteForTest(string $routePath): string
    {
        // Remove the '.{_format}' part from the route if present
        return str_replace('.{_format}', '', $routePath);
    }
}
