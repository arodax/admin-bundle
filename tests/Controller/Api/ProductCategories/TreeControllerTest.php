<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api\ProductCategories;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @covers \Arodax\AdminBundle\Controller\Api\ProductCategories\TreeController
 */
class TreeControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testTree(): void
    {
        $client = static::createClient();

        $uri = '/api/product-categories/tree';

        $client->request('GET', $uri);
        $response = $client->getResponse();
        $this->assertEquals(302, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 302, $response->getStatusCode()));

        $this->loginUser($client, 'admin@example.com');

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));
    }

    public function testAppend(): void
    {
        $client = static::createClient();

        $uri = '/api/product-categories/tree';

        $client->request('POST', $uri);
        $response = $client->getResponse();
        $this->assertEquals(302, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 302, $response->getStatusCode()));

        $this->loginUser($client, 'admin@example.com');

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));
    }
}
