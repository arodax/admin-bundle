<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Intl\Locales;
use Symfony\Component\Intl\Timezones;

class IntlControllerTest extends WebTestCase
{
    /**
     * @covers \Arodax\AdminBundle\Controller\Api\IntlController::index
     */
    public function testIndex(): void
    {
        $client = static::createClient();
        $uri = '/api/intl';

        array_map(function ($method) use ($client, $uri) {
            $client->request($method, $uri);
            $response = $client->getResponse();

            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
            $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

            $serializer = self::getContainer()->get('serializer');
            $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize(['locales' => ['en' => ['code' => 'en', 'name' => 'English'], 'cs' => ['code' => 'cs', 'name' => 'Czech']]], 'json'), sprintf('Invalid JSON response from "%s". Expected serilized list of available application locales.', $uri));
        }, ['GET', 'POST']);
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\IntlController::countries
     */
    public function testCountries(): void
    {
        $client = static::createClient();
        $uri = '/api/intl/countries';

        array_map(function ($method) use ($client, $uri) {
            $client->request($method, $uri);
            $response = $client->getResponse();

            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
            $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

            $serializer = self::getContainer()->get('serializer');
            $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize(Countries::getNames(), 'json'), sprintf('Invalid JSON response from "%s". Expected serialized countries list from symfony/intl pacakge.', $uri));
        }, ['GET', 'POST']);
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\IntlController::locales
     */
    public function testLocales(): void
    {
        $client = static::createClient();
        $uri = '/api/intl/locales';

        array_map(function ($method) use ($client, $uri) {
            $client->request($method, $uri);
            $response = $client->getResponse();

            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
            $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

            $serializer = self::getContainer()->get('serializer');
            $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize(Locales::getNames(), 'json'), sprintf('Invalid JSON response from "%s". Expected serialized countries list from symfony/intl pacakge.', $uri));
        }, ['GET', 'POST']);
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\IntlController::timezones
     */
    public function testTimezones(): void
    {
        $client = static::createClient();
        $uri = '/api/intl/timezones';

        array_map(function ($method) use ($client, $uri) {
            $client->request($method, $uri);
            $response = $client->getResponse();

            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
            $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

            $serializer = self::getContainer()->get('serializer');
            $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize(Timezones::getNames(), 'json'), sprintf('Invalid JSON response from "%s". Expected serialized countries list from symfony/intl pacakge.', $uri));
        }, ['GET', 'POST']);
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\IntlController::currencies
     */
    public function testCurrencies(): void
    {
        $client = static::createClient();
        $uri = '/api/intl/currencies';

        array_map(function ($method) use ($client, $uri) {
            $client->request($method, $uri);
            $response = $client->getResponse();

            $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
            $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

            $serializer = self::getContainer()->get('serializer');
            $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize(Currencies::getNames(), 'json'), sprintf('Invalid JSON response from "%s". Expected serialized countries list from symfony/intl pacakge.', $uri));
        }, ['GET', 'POST']);
    }
}
