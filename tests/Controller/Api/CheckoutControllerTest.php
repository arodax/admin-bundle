<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api;

use Arodax\AdminBundle\Manager\PaymentProviderManager;
use Arodax\AdminBundle\Manager\ShippingManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CheckoutControllerTest extends WebTestCase
{
    /**
     * @covers \Arodax\AdminBundle\Controller\Api\CheckoutController::index
     */
    public function testIndex()
    {
        $uri = '/api/checkout';
        $client = static::createClient();

        $container = self::getContainer();

        $serializer = $container->get('serializer');

        $client->request('OPTIONS', $uri);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

        /**
         * @todo: this test should be re-created, serializing available methods from service will cause missing session call
         *
         *
         *  $this->assertEquals($serializer->serialize([
         *      'shippingMethods' => $container->get(ShippingManager::class)->getAvailableShippingMethods(),
         *      'paymentMethods' => $container->get(PaymentProviderManager::class)->getAvailablePaymentMethods(),
         *  ], 'jsonld', ['groups' => 'shopping_cart:public_read']), $response->getContent(), 'Unexpected output returned.');
         */
    }
}
