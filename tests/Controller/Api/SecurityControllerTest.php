<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Api;

use Arodax\AdminBundle\Tests\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    /**
     * @covers \Arodax\AdminBundle\Controller\Api\SecurityController::roles
     */
    public function testRoles(): void
    {
        $client = static::createClient();

        $this->loginUser($client, 'admin@example.com');

        $uri = '/api/security/roles';

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));

        $container = self::getContainer();

        $serializer = $container->get('serializer');
        $roles = $container->get('Arodax\AdminBundle\Security\Authorization\RolesManager')->getRoles();

        $this->assertJsonStringEqualsJsonString($response->getContent(), $serializer->serialize($roles, 'jsonld'), sprintf('Invalid JSON response from "%s". Expected serialized list of available application roles.', $uri));
    }
}
