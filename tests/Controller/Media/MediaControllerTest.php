<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Media;

use Arodax\AdminBundle\Entity\Media\MediaObject;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;

class MediaControllerTest extends WebTestCase
{
    /**
     * Path to the public media dir.
     *
     * @var string|null
     */
    private ?string $mediaDir;

    public function tearDown(): void
    {
        parent::tearDown();

        if ($this->mediaDir) {
            $fs = new Filesystem();
            $fs->remove($this->mediaDir);
        }
    }

    /**
     * @covers \Arodax\AdminBundle\Controller\Media\MediaController::filter
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testFilter(): void
    {
        $client = static::createClient();
        $kernel = $client->getKernel();

        $this->mediaDir = $kernel->getProjectDir().'/public/media';

        if (!file_exists($this->mediaDir)) {
            mkdir($this->mediaDir, 0777);
        }

        $this->assertFileExists($this->mediaDir, sprintf('Expected to find public media dir in "%s" but this dir does not exist.', $this->mediaDir));
        $this->assertDirectoryIsWritable($this->mediaDir, sprintf('Expected to have writable dir "%s" but it\'s not.', $this->mediaDir));

        $image = imagecreatefromstring(base64_decode('iVBORw0KGgoAAAANSUhEUgAAA+gAAAPoBAMAAAC/jcnXAAAAG1BMVEUAAAD///8fHx+fn5/f39+/v78/Pz9fX19/f38xvMOiAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAI/klEQVR4nO3dz3PkxBUAYDNej32MgjfhaG+8wBFXkYQjTpaEYwxUyHFNyJIjhFSKo50E8m9HmtH8UHdPrejp8YjR953G8wx++6TXakk9mqMjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4HE8e3aRfH/6n8+q6vzTfxQObqF8pkd///VNVX302/T/93AdV9WHqff/Wldj5uOLksEtlM90+n4bPP930UyHbnKbLuXfqqW3CgYHlen0dhW9L5nq0H1ZJUv5q2rNH4sFB5Xp5HoteP51wVQH7rRKlnLaDIq//+GLZ1993/zCe4WCg8p0thedf/Dq2dvfNR3/ZrFMB+86Xcq7+t3fzF++UxfuaaHgoDKdNlv65ezl5J/16z8VS3XgnteVSJSyqccnix9Og4LkBweVabNHPL1Y/PBNyf1z2Jrh7z5RysvOtOd5d+zLDw4q00ln9tbMEssdigat3r9/eZQoZV3hl2s/1gX5ukRwUJk+qapfrAVPmj8wBmf1ROZlopRnQQHqgvy8QHBQmR49BPtjvUtclEh14JohrT7kxaW8DE9bb9YOePnBfDvIdBKeuD8Zx1Tuan6Qi0t5G26pu7VxMj+YbweZnoTbeNod7g/UtJ3JRKWcRmPy8apC+cFBZdrsABfd6PUY5u8P7a4dlfI4mshOVl2QHxxUpvUmDs8rLosMSsNW1+T8onkRlfIyntI8LEuUH8y2i0wn8Qzz7PBP2iY3iwsXUSkT26ku4NbBXDvJNLGF6/3gZ1umOnRfLi9VRKW8iUfk4+X5TX5wUJk+SYzl14d+pt7Mjdp/f1jKSeIayOmiMfKDg8q07vnz6C99c+g3Xerh7Q/ty7A4p6mby4uhLz+YayeZ1hs4vr3+xtZHooE7a+dGR3Epj1Oz2Nt2rMwPdqVWqkzuHy3TeiiPszo59On72WrQDUv5JLXDP7SdkR/s/vnlhlxzlRxdd5JpfcCPx5+zQ18/c7baEmEpL1MXKe7a411+sOOh+l30Xj1LTx39d5LpJHXNaHro52ynqyl1WMrkVnqjnfjkB9dNqipu9av0zZmdZJrevhsWXh6i8J/6kDpzWQyW+cGOuypq9cnNaw+p5TI9S55Ipsb8AxWWMjXHaaZFF9sFO6Zxq1+9/oJtuUyTs7z6PylyH/inICzlbWp/P2urlB/silq9R6MXzPQkeXb2MIb7bHNhKZOD3Gk7HuYHu6JW79HoBTNNHnPqk/cDvyS3EpYyOZ2Ztqcz+cFA0Op9Gr1gpqnZZZNT0U9nDFlQneTZTPPue1sFQ0Gr92n0cpmmz+fqdw/8OuxKXMrERmprmB+MdFq9V6OXy3TD5k3vCgcpKOWGSxTzauUHI51W79Xo5TJNn8TXg/6IN/r9xt/KD8bWWr1fo5fLdMPRO32kP0hBKZN3p5rJ8IdbBWNrrd6v0ctlmrzJ1szpbfSO8ht91eo9G91GLycuZWqpy7KUmcGEZav3bPRymW44I7fRA/NrHfnBlLbV+zZ6uUw3bvQDX0Wxsr+N3rZ630a30cvZ2/DetnrvRje8l7O3iVzb6r0b3USunD1u9Fmr9250G72cfV2caf9Y1f+DTy7OFLOny7BzzVNheq9BdRm2mD3dcJmb/ohGd8OlnP3cWl1Eb+LVcrvP1K3V/SyiaF1VqYWxG1hEUcxelku1JrMnt/ZtdculiomXGybWhK6WG+YGk+pGf6t/q5fL9CT5VKERL4x8jCXQrdnFuMQa+J1nagn0Hj7s0JpdjEusgd95pj7s8Pgfa2q1V917t7qPNRWzjw8wzrVX3Xu3ug8wFrOPjyrPLG+v9W11H1UuZi8PJWgsb6/1bXUPJSgm8VCPeJRb/FJ+MLJ2H71nq5fLNPl8mRHdb9nbg4bW7qP3bPVymY7zQUNrouIkBuST5SlOfjDQWTDTr9ULZpoaym/Hc0EuLmVij189dS8/GOgsmOnX6gUzTQxAI3h44EpUykd5TGiwMq5XqxfMdJyPCV2JSvkoDwQOVsb1avWCmY70gcBLUSnjLlh7ZnZ+sPs3wpVxfVq9YKaJcWAUj/5eiGe5j/CQ/2gJbJ9WL5npSB/yvxCXMvwejLot3ywQ7L4f7gw9Wr1kptHXeVyN6ZCeKOXuv7gnsda9R6uXzHSsX9zTSlzECL5Zq/tjfnAp+aGW17d60UyD7+QazVd0zSVKedUZ+66632KXH1yapC7IT3M2en4yzY533/lpRKN7qpTNwuTF87abUbJTj/zgyvPks+V+/PC+TTJ3s+96az2M6ht203dEms8gfD5/+e5NWI/84NLkz4k3p58k3txdps0u8XQ+3k/+W/QLoH8CUqWcrVJ9+r9Xb3/1flWFt5nzg4PKdLb8uvrLv7549W3za6NZ/TyTvPd5vPZ98qshcuvgoDI9ul4Lrkb6UUjf8H6+qsfHBYODyrSeTy63+X3ZVIduwyqHd2/aenxeNLiN4pnOjuWNp5s/jzEuk+8+e3H+0QcvCwd3YItk3vn+xc2LT3/YWWoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMA2/g8TjT+CttaD+gAAAABJRU5ErkJggg=='));
        imagejpeg($image, $this->mediaDir.'/test.jpg', 100);
        imagedestroy($image);

        $this->assertNotFalse($this->mediaDir.'/test.jpg', 'Could not generate a test image for MediaControllerTest::testFilter test.');


        $mediaObject = new MediaObject();
        $mediaObject->setOriginalFileName('test.jpg');
        $mediaObject->setFilePath('test.jpg');
        $mediaObject->setMime('image/jpeg');

        $entityManager = self::getContainer()->get('doctrine.orm.default_entity_manager');

        $entityManager->persist($mediaObject);
        $entityManager->flush();

        $client->request('GET', '/media/cache/admin/test.jpg');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $imageSize = getimagesizefromstring($response->getContent());

        $this->assertEquals('width="500" height="500"', $imageSize[3] ?? null, 'Expected image to be resized, but response image binary data could not be parsed.');
    }
}
