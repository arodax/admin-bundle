<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\AdminBundle\Tests\Controller\Routes;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @todo: Implement controller tests
 */
class RoutesControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $uri = '/routes.json';

        $client->request('GET', $uri);
        $response = $client->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), sprintf('Invalid HTTP status code, expected "%s" but got "%s".', 200, $response->getStatusCode()));
        $this->assertEquals('application/json', $response->headers->get('Content-Type'), sprintf('Failed to receive "application/json" content-type header, got "%s.', $response->headers->get('content-type')));
    }
}
