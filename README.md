# ARODAX Admin Bundle

![License](https://img.shields.io/badge/license-MIT-green)
![Symfony Version](https://img.shields.io/badge/Symfony-6.x-blue.svg)
![PHP Version](https://img.shields.io/badge/PHP-%3E%3D8.3-8892BF.svg)

## Overview

ARODAX Admin is a comprehensive content management system (CMS) and a platform
for developing additional web applications. This guide is intended for
developers and expert users. It is not aimed at ordinary users seeking
instructions on how to use the system from a user's perspective.

# Table of Contents

- [Overview](#overview)
- [Prerequisites for Development](#prerequisites-for-development)
- [Installing Docker](#installing-docker)
    - [For Windows](#for-windows)
    - [For MacOS](#for-macos)
    - [For Linux](#for-linux)
- [Installing PHP 8.3](#installing-php-83)
    - [For Windows](#for-windows-1)
    - [For MacOS](#for-macos-1)
    - [For Linux (Ubuntu)](#for-linux-ubuntu)
- [Installing NodeJS](#installing-nodejs)
    - [For Windows](#for-windows-2)
    - [For MacOS](#for-macos-2)
    - [For Linux (Ubuntu)](#for-linux-ubuntu-1)
- [Installing Yarn](#installing-yarn)
    - [For Windows](#for-windows-3)
    - [For MacOS](#for-macos-3)
    - [For Linux (Ubuntu)](#for-linux-ubuntu-2)
- [Installing Composer](#installing-composer)
    - [For Windows](#for-windows-4)
    - [For MacOS](#for-macos-4)
    - [For Linux (Ubuntu)](#for-linux-ubuntu-3)
- [Starting the Development Environment](#starting-the-development-environment)

## Prerequisites for Development

Before starting development on the ARODAX Admin Bundle, ensure you have the
following tools installed on your computer:

- Docker
- NodeJS 21+
- PHP 8.3+
- Yarn 4+
- Composer 2+

## Installing Docker

Docker is a platform for developing, shipping, and running applications in
isolated environments called containers. It simplifies the management of
application dependencies and prevents the "it works on my machine" problem.
Using Docker, you can easily set up, develop, and distribute your applications
across different environments.

### For Windows:

1. Visit the [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows) to download Docker Desktop for Windows.
2. Run the installer and follow the on-screen instructions.
3. Docker requires WSL 2 on Windows. The installer should guide you through the setup if it's not already installed.

### For MacOS:

1. Download Docker Desktop for Mac from [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-mac).
2. Open the downloaded `.dmg` file and drag Docker into your Applications folder.
3. Run Docker from your Applications folder, and you're all set.


### For Linux:

1. The installation process for Docker on Linux varies by distribution. Visit the [Docker documentation](https://docs.docker.com/engine/install/) for specific instructions for your distro.
2. Generally, you can install Docker using your package manager. For Ubuntu, for example, you can use the following commands:

   ```bash
   sudo apt update
   sudo apt install docker.io

For optimal experience also modify your hosts file to include the following:

```bash
127.0.0.1 mariadb
```

This will allow you access to the MariaDB database from your local machine with the hostname `mariadb`. The host file can be found at the following locations:

- Windows: C:\Windows\System32\drivers\etc\hosts
- MacOS: /etc/hosts
- Linux: /etc/hosts


## Installing PHP 8.3

PHP is a widely-used general-purpose scripting language that is especially suited for web development. For developing the ARODAX Admin Bundle, PHP version 8.3 or higher is required.

### For Windows:

1. **Download PHP:** Visit the [PHP for Windows](https://windows.php.net/download/) site. Download the Zip file for PHP 8.3 Non Thread Safe (NTS) x64.
2. **Extract PHP:** Unzip the downloaded file into a directory of your choice, for example, `C:\php`.
3. **Configure PHP:** Rename the `php.ini-development` file to `php.ini`. Open it with a text editor and adjust the necessary settings, like enabling extensions by removing the semicolon `;` from the start of the line.
4. **Add PHP to Path:** To make PHP globally accessible, add its path to the system's environment variables. Go to System Properties -> Environment Variables, and append `;C:\php` to the `Path` variable.
5. To install and enable the required PHP extensions on Windows, you’ll follow a series of steps that involve editing the php.ini configuration file. This file controls the settings of your PHP installation, including which extensions are enabled. Here’s how to do it:

    - After installing PHP, locate the php.ini file in your PHP installation directory (e.g., C:\php).
    - Open php.ini for Editing: Open the php.ini file with a text editor, such as Notepad or Visual Studio Code.
    - Enable Required Extensions: Within php.ini, search for lines corresponding to the extensions you wish to enable: ext-ctype, ext-gd, ext-iconv, ext-json, and ext-simplexml.
    - Remove the semicolon (;) at the beginning of the line to enable the extension. For example, to enable the ctype extension, change ;extension=ctype to extension=ctype.

       ```ini
       ;extension=ctype
       ;extension=gd
       ;extension=iconv
       ;extension=json
       ;extension=simplexml
       ```


### For MacOS:

1. **Install Homebrew:** If not already installed, open Terminal and install Homebrew by running:
   ```bash
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

2. **Install PHP:** After installing Homebrew, install PHP by executing:
    ```bash
   brew install php@8.3
   brew link php@8.3 --force

3. Tap into **Additional Extensions**: Some projects require additional PHP extensions. You can tap into Shivammathur's extensions for this:
    ```bash
   brew tap shivammathur/extensions
   brew install shivammathur/php/php@8.3-ext-ctype
   brew install shivammathur/php/php@8.3-ext-gd
   brew install shivammathur/php/php@8.3-ext-iconv
   brew install shivammathur/php/php@8.3-ext-json
   brew install shivammathur/php/php@8.3-ext-simplexml

### For Linux (Ubuntu):

1. Add PHP Repository: Open a terminal and add the ondrej/php repository, which includes the latest PHP versions:
   ```bash
   sudo apt update
   sudo apt -y install software-properties-common
   sudo add-apt-repository ppa:ondrej/php
   sudo apt update
   sudo apt install php8.3 php8.3-cli php8.3-common php8.3-opcache php8.3-mbstring php8.3-xml php8.3-gd php8.3-curl
   sudo apt install php8.3-ctype php8.3-gd php8.3-iconv php8.3-json php8.3-simplexml


## Installing NodeJS

Node.js is essential for modern web development, including working with front-end frameworks, or for running build tools like webpack or gulp. For developing with the ARODAX Admin Bundle, ensure you have Node.js version 21 or higher installed.


### For Windows:
**Download the Installer:** Visit the Node.js official website and download the Windows installer for Node.js 21 or higher.

### For MacOS:
You can download installer from the [Node.js official website](https://nodejs.org/en/download/) or use homebrew.


If **Homebrew is not installed**, you can install it by running the following command in the terminal:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Install node.js using Homebrew:
```bash
brew install node
```

### For Linux (Ubuntu):
NodeSource provides more current versions of Node.js than the official Ubuntu repositories. To install Node.js 21.x, you can use the following commands:

```bash
curl -fsSL https://deb.nodesource.com/setup_21.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## Installing Yarn
Yarn is a JavaScript package manager that facilitates code sharing and reuse by managing dependencies for your projects. Here’s how to install Yarn on various platforms:

### For Windows:
**Download the Installer:** Visit the Yarn official website and download the Windows installer for Yarn.

### For MacOS:
You can install Yarn using Homebrew. If Homebrew is not installed, you can install it by running the following command in the terminal:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Install Yarn using Homebrew:
```bash
brew install yarn
```

### For Linux (Ubuntu):
You can install Yarn using the following commands:

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
```

## Installing Composer
Composer is a dependency manager for PHP that simplifies the process of installing and managing PHP libraries. Here’s how to install Composer on various platforms:

### For Windows:
**Download the Installer:** Visit the Composer official website and download the Windows installer for Composer.

### For MacOS:
You can install Composer using Homebrew. If Homebrew is not installed, you can install it by running the following command in the terminal:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Install Composer using Homebrew:
```bash
brew install composer
```

### For Linux (Ubuntu):
You can install Composer using the following commands:

```bash
sudo apt update
sudo apt install php-cli unzip
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
```

## Starting the Development Environment

This bundle comes with a development environment with docker-compose.yml file. If you don't seee this file in your cloned
directory, you might probably clone the submodule only, without the main repository. To clone the main repository, run the following command:

```bash
git clone git@bitbucket.org:arodax/admin.arodax.com.git
```

After cloning the main repository, navigate to the root directory of the project and run the following command to start the development environment:

```bash
docker-compose up
```

This command will start docker contains. On the first run ever it will also pull the required
images from the Docker Hub, this might take a while. All next runs will be much faster and the
development environment will be ready in a few seconds.

When everything is finished you can access the application at the following URL:

```bash
https://localhost:8000
```

Make sure you're on https, as the application requires it. If you have any issues with the SSL certificate, you can
trust the certificate by following the instructions in the browser.

Now you also need to install required PHP and javascript packages. To do this, you need to run the following commands:

```bash
composer install --ignore-platform-reqs
yarn install
```

This will download and install all required packages for developing the application. When all packages are installed
run the command to build the application assets, CSS and javascript with the following command:

```bash
yarne encore dev --watch
```

After everything is done you should see no error when accessing the application in the browser on https://localhost:8000.
