<?php

/*
 * This file is part of the ARODAX ADMIN package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Deployer;

/** @psalm-suppress UndefinedFunction */

try {
    require 'recipe/common.php';

    // Number of releases to preserve in releases folder.
    set('keep_releases', 2);


    // Set deploy branch from current HEAD or use BITBUCKET_BRANCH if running through pipelines
    set('branch', function () {
        if (false === getenv('BITBUCKET_BRANCH')) {
            return runLocally('git rev-parse --abbrev-ref HEAD');
        }

        return getenv('BITBUCKET_BRANCH');
    });

    // Faster cloning by borrowing objects from existing clones.
    set('git_cache', function () {
        $gitVersion = run('{{bin/git}} version');
        $regs = [];
        if (preg_match('/((\d+\.?)+)/', $gitVersion, $regs)) {
            $version = $regs[1];
        } else {
            $version = '1.0.0';
        }
        return version_compare($version, '2.3', '>=');
    });

    set('git_tty', false);

    set('ssh_multiplexing', false);

    set('allow_anonymous_stats', false);

    set('release_name', function () {
        if (getenv('BITBUCKET_BUILD_NUMBER')) {
            return getenv('BITBUCKET_BUILD_NUMBER');
        }

        $list = array_map(function ($r) {
            return $r[1];
        }, get('releases_metainfo'));

        $list = array_filter($list, function ($release) {
            return preg_match('/^\d+$/', $release);
        });

        $nextReleaseNumber = 1;
        if (count($list) > 0) {
            $nextReleaseNumber = (int)max($list) + 1;
        }

        return (string)$nextReleaseNumber;
    });

    add('shared_files', [
        '.env.local'
    ]);

    add('shared_dirs', [
        'public/media',
        'src/Migrations',
        'var/log'
    ]);

    add('writable_dirs', [
        'var',
        'public/media'
    ]);


    task('deploy', [
        'deploy:prepare',
        'deploy:vendors',
        'deploy:sync-metadata-storage',
        'deploy:migrations',
        'deploy:build',
        'deploy:install',
        'deploy:cache',
        'deploy:publish',
        'deploy:reload-php'
    ]);


    task('deploy:update_code', function () {
        $repository = trim(get('repository'));
        $branch = get('branch');
        $git = get('bin/git');
        $gitCache = get('git_cache');
        $recursive = get('git_recursive', true) ? '--recursive' : '';
        $dissociate = get('git_clone_dissociate', true) ? '--dissociate' : '';
        $depth = $gitCache ? '' : '--depth 1';
        $options = [
            'tty' => get('git_tty', false),
        ];

        $at = '';
        if (!empty($branch)) {
            $at = '-b '.$branch;
        }

        // If option `tag` is set
        if (input()->hasOption('tag')) {
            $tag = input()->getOption('tag');
            if (!empty($tag)) {
                $at = '-b '.$tag;
            }
        }

        // If option `tag` is not set and option `revision` is set
        if (empty($tag) && input()->hasOption('revision')) {
            $revision = input()->getOption('revision');
            if (!empty($revision)) {
                $depth = '';
            }
        }

        // Enter deploy_path if present
        if (has('deploy_path')) {
            cd('{{deploy_path}}');
        }

        if ($gitCache && has('previous_release')) {
            try {
                run($git . ' clone ' . $at . ' ' . $recursive . ' --reference {{previous_release}} ' . $dissociate . ' ' . $repository . '  {{release_path}} 2>&1', $options);
            } catch (\Throwable $exception) {
                // If {{deploy_path}}/releases/{$releases[1]} has a failed git clone, is empty, shallow etc., git would throw error and give up. So we're forcing it to act without reference in this situation
                run($git . ' clone ' . $at . ' ' . $recursive . ' ' . $repository . ' {{release_path}} 2>&1', $options);
            }
        } else {
            // if we're using git cache this would be identical to above code in catch - full clone. If not, it would create shallow clone.
            run($git . ' clone ' . $at . ' ' . $depth . ' ' . $recursive . ' ' . $repository . ' {{release_path}} 2>&1', $options);
        }

        if (!empty($revision)) {
            run('cd {{release_path}} && ' . $git . ' checkout ' . $revision);
        }
    });

    task('deploy:vendors', function () {
        $composerPath = run('which composer');
        $yarnPath = run('which yarn');
        run("cd {{release_path}} && {$composerPath} install --optimize-autoloader --classmap-authoritative");
        run("cd {{release_path}} && {$yarnPath} install");
    });

    task('deploy:sync-metadata-storage', function () {
        $phpPath = run('which php8.3');
        run('cd {{release_path}} && ' . $phpPath . ' {{release_path}}/bin/console doctrine:migrations:sync-metadata-storage --no-interaction');
    });

    task('deploy:migrations', function () {
        $phpPath = run('which php8.3');
        run('cd {{release_path}} && ' . $phpPath . ' {{release_path}}/bin/console doctrine:migrations:migrate --no-interaction');
    });

    task('deploy:build', function () {
        $yarnPath = run('which yarn');
        run("cd {{release_path}} && {$yarnPath} encore production");
    });

    task('deploy:cache', function () {
        $phpPath = run('which php8.3');
        run('cd {{release_path}} && ' . $phpPath . ' {{release_path}}/bin/console cache:warmup');
    });

    task('deploy:install', function () {
        $phpPath = run('which php8.3');
        run('cd {{release_path}} && ' . $phpPath . ' {{release_path}}/bin/console arodax_admin:install:menu');
        run('cd {{release_path}} && ' . $phpPath . ' {{release_path}}/bin/console arodax_admin:install:settings');
    });

    task('deploy:reload-php', function () {
        run('sudo systemctl reload php8.3-fpm');
    });

    after('deploy:failed', 'deploy:unlock');

} catch (\Exception $e) {

}


